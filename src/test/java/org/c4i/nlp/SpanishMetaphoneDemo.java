package org.c4i.nlp;

import org.c4i.nlp.normalize.LuceneStemmer;
import org.c4i.nlp.normalize.SafeSnowballStemmer;
import org.c4i.nlp.normalize.SpanishStemmer;
import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.normalize.phonetic.Metaphone3;
import org.c4i.nlp.normalize.phonetic.MetaphoneES;

public class SpanishMetaphoneDemo {

    private static StringNormalizer phon;
    private static StringNormalizer stemmer;


    public static void main(String[] args) {
        phon = new MetaphoneES();


        String[] words = {
                "papas",
                "pappas",
                "patatas",
                "pataattas",
                "chicharrones",
                "xicharones",
                "uva",
                "uba",
                "huevo",
                "hombre",
                "ombre",
        };

        StringNormalizer metaphoneES = new MetaphoneES();
        StringNormalizer metaphone3 = new Metaphone3();

        System.out.printf("%s\t%s\t%s\t\n", "word", "Metaphone3", "MetaphoneES");
        for (String word : words) {
            System.out.printf("%s\t%s\t%s\t\n", word, metaphone3.normalize(word), metaphoneES.normalize(word));
        }

    }


}
