package org.c4i.nlp;

import org.c4i.nlp.normalize.SafeSnowballStemmer;

public class DutchStemmerDemo {
    public static void main(String[] args) {
        test("licht");
        test("lichtelijk");
        test("lichten");
        test("lichter");
        test("belichten");

    }

    private static void test(String s){
        SafeSnowballStemmer stemmer = new SafeSnowballStemmer("nl");
        System.out.println(s + " -> " + stemmer.normalize(s));
    }
}
