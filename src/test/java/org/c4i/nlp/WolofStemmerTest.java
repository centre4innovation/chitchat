package org.c4i.nlp;

import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.normalize.StringNormalizers;
import org.c4i.nlp.normalize.WolofStemmer;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class WolofStemmerTest {
    private static WolofStemmer stemmer;

    @BeforeClass
    public static void before() throws IOException {
        stemmer = new WolofStemmer();
    }

    @Test
    public void change(){
        assertEquals("jigéen-bi", stemmer.normalize("jigéen-yi"));
    }

    @Test
    public void leaveAsIs(){
        String w = "butik-bi";
        assertEquals(w, stemmer.normalize(w));
    }

    @Test
    public void baboon(){
        String w = "goŋ";
        assertNotEquals("gon", StringNormalizers.DEFAULT.normalize(w));
        assertEquals("gon", stemmer.normalize(w));
    }

    @Test
    public void eye(){
        String w = "bët";
        assertEquals("bet", StringNormalizers.DEFAULT.normalize(w));
    }

    @Test
    public void thread(){
        String w = "woñ";
        assertEquals("won", StringNormalizers.DEFAULT.normalize(w));
    }
}
