package org.c4i.nlp;

import org.apache.lucene.analysis.icu.segmentation.ICUTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.c4i.nlp.normalize.StringNormalizers;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Arvid Halma
 * @version 18-4-19
 */
public class KhmerDemo {
    public static void main(String[] args)  {
        // these two bunches of ripe bananas
        String txt = "ចេកទុំពីរស្និតនេះ";

        for (char c : txt.toCharArray()) {
            System.out.println("c = " + c);
            System.out.println("i = " + ((int)c));
        }

        System.out.println("---");
        // these two bunches of ripe bananas
        txt = StringNormalizers.UNICODE.normalize("ចេកទុំពីរស្និតនេះ");

        for (char c : txt.toCharArray()) {
            System.out.println("c = " + c);
            System.out.println("i = " + ((int)c));
        }

        System.out.println("tokens = " + tokenize(txt));


        // banana
        String word = "ចេក";

        System.out.println("txt = " + txt);
        System.out.println("word = " + word);

        final boolean wordLike = word.matches("(?U)\\w+");
        System.out.println("wordLike = " + wordLike);

        final char[] wchars = word.toCharArray();
        for (int i = 0; i < wchars.length; i++) {
            char c = wchars[i];
            System.out.println("c = " + c);
            System.out.println("Character.isAlphabetic(c) = " + Character.isAlphabetic(c));
            System.out.println("Character.isLetterOrDigit(c) = " + Character.isLetterOrDigit(c));
        }

        /*try {
            testIcu();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        System.out.println("kpat ---- ");
        Pattern kpat = Pattern.compile("\\p{L}\\p{M}*");
        Matcher m = kpat.matcher(txt);
        while (m.find()){
            System.out.println("m.group() = " + m.group());
        }


    }

    public static void testIcu() throws IOException {
        System.out.println("ICU--------------");
        String txt = "ចេកទុំពីរស្និតនេះ";
        Reader reader = new StringReader(txt);
        ICUTokenizer icut = new ICUTokenizer();
        icut.setReader(reader);
        icut.addAttribute(CharTermAttribute.class);
        icut.reset();
        while (icut.incrementToken()) {
            System.out.println(icut.toString());
            System.out.println(icut.getAttribute(CharTermAttribute.class));
        }
        icut.close();
    }



    public static List<String> tokenize(String text) {
        List<String> tokens = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(text,
                "\u17D4\u17D5\u0020\u00A0\u115f\u1160\u1680"
                        + "\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007"
                        + "\u2008\u2009\u200A\u200B\u200c\u200d\u200e\u200f"
                        + "\u2028\u2029\u202a\u202b\u202c\u202d\u202e\u202f"
                        + "\u205F\u2060\u2061\u2062\u2063\u206A\u206b\u206c\u206d"
                        + "\u206E\u206F\u3000\u3164\ufeff\uffa0\ufff9\ufffa\ufffb"
                        + ",.;()[]{}«»!?:\"'’‘„“”…\\/\t\n", true);
        while (st.hasMoreElements()) {
            tokens.add(st.nextToken());
        }
        return (tokens);
    }
}
