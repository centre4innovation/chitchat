package org.c4i.nlp;

import org.c4i.nlp.normalize.*;
import org.c4i.nlp.normalize.phonetic.Metaphone;
import org.c4i.nlp.normalize.phonetic.MetaphoneES;

import java.text.Normalizer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SpanishStemmerDemo {

    private static StringNormalizer phon;
    private static StringNormalizer stemmer;


    public static void main(String[] args) {
        phon = new MetaphoneES();


        String[] words = {
                "papa",
                "papas",
                "pappas",
                "patatas",
                "chicharrones",
                "xicharones",
                "torero",
                "toreros",
                "uva",
                "uvas",
                "hombres",
                "hombre",
                "mujeres",
                "mujer",
                "raros",
                "raro",
                "raras",
                "rara",
                "verlo",
                "dime",
                "compraras",
                "fideos",
                "serrana",
                "cabeza",
        };

        /*String[] words = {
                "papas",
                "uvas"
        };*/

        StringNormalizer luceneLt = new LuceneStemmer("es", true);
        StringNormalizer snowball = new SafeSnowballStemmer("es");
        StringNormalizer custom = new SpanishStemmer();

        
        /*System.out.printf("%12s%12s%12s%12s\n", "word", "luceneLt", "snowball", "custom");
        for (String word : words) {
            System.out.printf("%12s%12s%12s%12s\n", word, luceneLt.normalize(word), snowball.normalize(word), custom.normalize(word));
        }*/

        System.out.printf("%s\t%s\t%s\t%s\t\n", "word", "luceneLt", "snowball", "custom");
        for (String word : words) {
            System.out.printf("%s\t%s\t%s\t%s\t\n", word, luceneLt.normalize(word), snowball.normalize(word), custom.normalize(word));
        }
        

        System.out.println("Lucene Light Stemmer:");
        stemmer = new LuceneStemmer("es", true);
        for (String word : words) {
            normalize(word);
        }

        System.out.println("\nTartarus snowball:");
        stemmer = new SafeSnowballStemmer("es");
        for (String word : words) {
            normalize(word);
        }


        System.out.println("\nSpanish Stemmer:");
        stemmer = new SpanishStemmer(true);
        for (String word : words) {
            normalize(word);
        }



    }

    private static void normalize(String s){
        String stemmed = stemmer.normalize(s);
        System.out.println(s + "\t -> " + stemmed);
//        System.out.println(s + "\t -> " + stemmed + "\t -> " + phon.normalize(stemmed));
    }



}
