package org.c4i.chitchat;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.time.StopWatch;
import org.c4i.chitchat.api.model.LanguageProcessingConfig;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.chat.Conversation;
import org.c4i.nlp.chat.Message;
import org.c4i.nlp.match.*;
import org.c4i.nlp.match.Compiler;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * @author Arvid Halma
 * @version 14-4-2017 - 20:29
 */
public class ScriptTest {

    private final static int N = 10;
    static Nlp nlp;

    static final String bigSrc = "---\n" +
            "rule: \n" +
            "  reply: {within: 5}\n" +
            "---\n" +
            "\n" +
            "# Kakuma ChatBot, November 2018 \n" +
            "\n" +
            "# Welcome messages\n" +
            "@welcome <- hi | sasa | hello | hey | jambo | helo | heloo  \n" +
            "  | 'good morning' | 'good afternoon' | 'good evening' | 'good day' | 'How are you' | 'morning' \n" +
            "  | 'goodmorning' | 'Whats up'\n" +
            "  \n" +
            "@welcome -> Hi there! \n" +
            "  & Hi there!  I am WFP's automated chatbot. I'm not a real human but I can help you with questions about Bamba Chakula and food distribution (GFD). \n" +
            "  & You can also report a problem or file a complaint.\n" +
            "  & How can I help?\n" +
            "\n" +
            "# Callback function\n" +
            "# 1. SIMPLE (GFD)\n" +
            "\n" +
            "@problemcallback {addLabel: PhoneQuestion} -> Please type in your phone number, so we can call you back. \n" +
            "\n" +
            "@callback <- callbackfunction\n" +
            "@callback {addLabel: PhoneQuestion} -> Please type in your phone number, so we can call you back. \n" +
            "\n" +
            "@PhoneOK <- NUMBER & @PhoneQuestion\n" +
            "@PhoneSkip <- -NUMBER & @PhoneQuestion\n" +
            "@PhoneOK -> Thanks for your number. & We will call you as soon as possible. & Bye for now!\n" +
            "@PhoneSkip -> Without the phone number we cannot contact you. Please call the FREE helpline on 0800 722446. \n" +
            "\n" +
            "# 2. COMPLEX (BC)\n" +
            "@callbackBC <- callbackBC\n" +
            "@callbackBC {addLabel: PhoneQuestionBC} -> Please type in your phone number, so we can call you back. \n" +
            "@PhoneOKbc <- NUMBER & @PhoneQuestionBC\n" +
            "@PhoneSkipBC <- -NUMBER & @PhoneQuestionBC\n" +
            "@PhoneSkipBC -> Without the phone number we cannot contact you. Please call the FREE helpline on 0800 722446. \n" +
            "@PhoneOKbc {addLabel: hhIdQuestion}-> Thanks for your number. We will call you back. & In order to help you, we need also your household number. \n" +
            "                & Please give me now your household number.  \n" +
            "\n" +
            "@hhId <- NUMBER & @hhIdQuestion\n" +
            "@hhIdInvalid <- -NUMBER & @hhIdQuestion\n" +
            "\n" +
            "@hhId -> Thanks a lot. We will contact you as soon as possible. \n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "# General labels \n" +
            "@RESET <- restart | reset_chat | reset\n" +
            "# @RESET -> Ok... lets try again. & Tell me your question\n" +
            "\n" +
            "@yes <- y | yes\n" +
            "@good <- -(no | not) & (good | right | correct | ndio | sawa | cool | ok | okay | please)\n" +
            "@thank <- (thanks | (thank & you))\n" +
            "bye | ciao | cheers | see_you -> Bye!\n" +
            "thanks | thank | helpful | ok -> I hope I was able to help you. Feel free to ask other questions! \n" +
            "'that is all' | (@negate & question) | nothing -> I hope I was able to help you. Nice talking to you. Bye! \n" +
            "@thank -> I hope I was able to help you. Feel free to ask other questions!  \n" +
            "@good  -> I hope I was able to help you. Feel free to ask other questions!  \n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "@no <- (n | no | not | non | never | incorrect | la )\n" +
            "@negate <- @no | didn't | didnt | couldnt | can't | cant | cannot | wasn't | isn't |don't | dont | 'can not' \n" +
            "\n" +
            "@kalobeyei <- kalobeyei | kalo\n" +
            "@kakuma <- kakuma | kak\n" +
            "\n" +
            "@sensitive <- (emergency | misconduct | theft | stole | fraud | violence \n" +
            "  | corrupt| insult | rape | murder | dead | sick | treat | robbed | cheated | cheat) | ((emergency | misconduct | theft | stole | fraud | violence \n" +
            "  | corrupt| insult | rape | murder | dead | sick | treat | robbed ) & (bamba | distribution | shop | trader))\n" +
            "@sensitive -> This seems a complicated and important issue. \n" +
            "  & Please contact the helpdesk or the FREE helpline to help you out better \n" +
            "  & The number is 0800 722446\n" +
            "\n" +
            "@outOfScope <- resettlement\n" +
            "@outOfScope -> Sorry, I do not know about these topics. Please call the FREE helpline on 0800 722446.\n" +
            "  & This is a test bot that can help you with questions about Bamba Chakula and food distributions (GFD).\n" +
            "  & You can also report a problem or file a complaint.\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "# General food distribution \n" +
            "@gfd & when -> $DATEGFD \n" +
            "@gfd & (what | which) -> $FOODBASKET\n" +
            "\n" +
            "\n" +
            "@gfdIncrease <- (@gfd & ((add | increase | more | amount)  | (@negate & enough))) |  hunger | hungry  | starving | sorghum | foodComplaint\n" +
            "@gfdIncrease  -> What WFP distibutes depends on the kind of donations we get. We receive many food donations and try to provide the most to the people we assists.\n" +
            "                                         & The food basket must be nutritious and keep you heathy. This is cereal, pulses and oil - the rest you can buy with Bamba Chakula.\n" +
            "                                         & We try to do the best with what we have.\n" +
            "                                         \n" +
            "\n" +
            "@gfdProblem <-  (problem | issue | complain | complaint)\n" +
            "@gfdProblem  ->  You can choose from the topics below. You can make an official complaint, and we will call you back. \n" +
            "            & BUTTON(food quality, foodComplaint) BUTTON(food quantity, foodComplaint) BUTTON(official complaint, reportProblem) BUTTON(Bamba Chakula, bamba)\n" +
            "\n" +
            "@gfdOfficalComplaint  <- reportProblem \n" +
            "@gfdOfficalComplaint {addLabel: problemcallback} -> Please describe your official complaint in one message. \n" +
            "\n" +
            "# Bamba Chakula\n" +
            "@bcIncrease <- @bc & ((add & money) | increase | more | addition | (@negate & enough) | little | (@no & enough))\n" +
            "@bcIncrease {repeat, within: 1} -> The amount of money you receive through bamba chakula is determined by the donations WFP gets. &\n" +
            "  We are doing our best to provide as much assistance as possible. \n" +
            "  & If your your family size increased, and you think you should receive more money, you should register the new members with UNHCR.\n" +
            "\n" +
            "\n" +
            "\n" +
            "@bcWhen {within: all} <- @bc & when\n" +
            "@bcWhen  {addLabel: moreQuestions} -> $dateBamba   & Feel free to ask other questions... \n" +
            "\n" +
            "\n" +
            "@bcHow <- (bamba | bumba | mamba | chakula) \n" +
            "           & (((how | 'what to') & get) | 'what is' | (@negate & used)) &-much | bambainfo\n" +
            "@bcHow -> Bamba Chakula is a transfer of M-Pesa to WFP beneficiaries in Kakuma and Kalobeyi. \n" +
            "          & To receive it for the first time, you need to register with UNHCR.\n" +
            "          & To receive Bamba Chakula in Kakuma you need go to the food distribution and do biometrics/fingerprint. In Kalobeyei you need to do biometrics/fingerprint.\n" +
            "\n" +
            "@bcBalance {within: all} <- @bc & ((much | amount) & have ) | balance \n" +
            "@bcBalance -> You can get the balance  by typing \"*432*2#\" on your phone. \n" +
            "\n" +
            "\n" +
            "@bcPay <- @bc & (how & pay) | redeem | (access & money) \n" +
            "@bcPay -> Your trader knows how to pay with your Bamba Chakula. He also has a phone you can use if you need to. \n" +
            "          & If you still face problems to pay with Bamba Chakula, please call the FREE helpline on 0800 722446 or visit the helpdesk. \n" +
            "\n" +
            "\n" +
            "\n" +
            "@bcBroken {within: all} <- @bc & (broken | damaged | error) \n" +
            "@bcBroken -> We're sorry to hear that. & Please go to the helpdesk of the nearest food distribution point to get help.\n" +
            "@bcLost {within: all} <- (@bc & (lost | missing | reset | blocked | recover)) | pin | sim | puk\n" +
            "@bcLost ->  We're sorry to hear that. & We understand that you have a problem with Bamba Chakula (SIM, PIN or PUK). & For this, please go to the nearest WFP helpdesk or call the FREE helpline 0800 722446 , or leave your phone number and we will call you back. \n" +
            "            & BUTTON(call me back, callbackBC) \n" +
            "\n" +
            "@SIMproblem <- simcardproblem\n" +
            "@SIMproblem -> If your SIM card is broken or lost, please go to the helpdesk of the nearest food distribution point to get help or call the FREE helpline 0800 722446. If you do not yet have a Bamba Chakula SIM-card, please register with UNHCR.\n" +
            "                & If you do not yet have a bamba chakula SIM-card, please register with UNHCR. \n" +
            "@bcTransaction {within: all} <- transaction & statement\n" +
            "@bcTransaction -> Please contact the WFP Helpdesk to verify you are the head of the household.\n" +
            "\n" +
            "\n" +
            "@bcHowMuch {within: all} <- (@bc & ((much | amount) & (get | entitled | entitlement | receive))) | howmuchbamba | (@bc &amount)\n" +
            "@bcHowMuch & -(@kalobeyei | @kakuma) {repeat} -> First, are you in Kalobeyei or Kakuma? \n" +
            "\n" +
            "@hhSize <- (NUMBER | CARDINALNUMBER) & (@hhSizeQuestion | persons | people | size | siz | family) \n" +
            "@bcHowMuch & -@hhSize {addLabel: hhSizeQuestion } -> It also depends on the size of your family. & How MANY PERSONS are in your household? \n" +
            "\n" +
            "# Household size \n" +
            "@kakuma & @bcHowMuch & [\n" +
            "    @hhSize.number == 0 {continue} -> I guess your household has at least 1 person. With 1 person, you will get 500 KES.\n" +
            "    @hhSize.number == 1 {continue} -> You will get 500 KES\n" +
            "    @hhSize.number == 2 {continue} -> You will get 600 KES\n" +
            "    @hhSize.number == 3 {continue} -> You will get 900 KES\n" +
            "    @hhSize.number == 4 {continue} -> You will get 1200 KES\n" +
            "    @hhSize.number == 5 {continue} -> You will get 1500 KES\n" +
            "    @hhSize.number == 6 {continue} -> You will get 1800 KES\n" +
            "    @hhSize.number == 7 {continue} -> You will get 2100 KES\n" +
            "    @hhSize.number == 8 {continue} -> You will get 2400 KES\n" +
            "    @hhSize.number == 9 {continue} -> You will get 2700 KES\n" +
            "    @hhSize.number == 10 {continue} -> You will get 3000 KES\n" +
            "    @hhSize.number == 11 {continue} -> You will get 3300 KES\n" +
            "    @hhSize.number == 12 {continue} -> You will get 3600 KES\n" +
            "    @hhSize.number == 13 {continue} -> You will get 3900 KES\n" +
            "    @hhSize.number == 14 {continue} -> You will get 4200 KES\n" +
            "    @hhSize.number == 15 {continue} -> You will get 4500 KES\n" +
            "    @hhSize.number == 16 {continue} -> You will get 4800 KES\n" +
            "    @hhSize.number == 17 {continue} -> You will get 5100 KES\n" +
            "    @hhSize.number > 17 {continue} -> You have a big family to take care of! & You'll get 300 KES per person.\n" +
            "]\n" +
            "\n" +
            "@kalobeyei & @bcHowMuch & [\n" +
            "    @hhSize.number == 0 {continue} -> I guess your household has at least 1 person. With 1 person, you will get 1400 KES\n" +
            "    @hhSize.number == 1 {continue} -> You will get 1400 KES\n" +
            "    @hhSize.number == 2 {continue} -> You will get 2800 KES\n" +
            "    @hhSize.number == 3 {continue} -> You will get 4200 KES\n" +
            "    @hhSize.number == 4 {continue} -> You will get 5600 KES\n" +
            "    @hhSize.number == 5 {continue} -> You will get 7000 KES\n" +
            "    @hhSize.number == 6 {continue} -> You will get 8400 KES\n" +
            "    @hhSize.number > 6 {continue} -> You have a big family to take care of! & You'll get 1400 KES for each person.\n" +
            "]\n" +
            "\n" +
            "@bcHowMuch & @hhSize {removeLabel: [bc, bcHowMuch, hhSize]} -> Do you have any other questions about & BUTTON(GFD, gfd) BUTTON(Bamba Chakula, bc)\n" +
            "\n" +
            "@bcMissed {within: all} <- @bc & (missed | miss | delayed | (@negate & (get | time | receive | redeem | arrive | valid))) | misseddistributionbc | (@bc &@negate)\n" +
            "@bcMissed -> In Kakuma, you need go to the food distribution and do biometrics/fingerprint.\n" +
            "            & In Kalobeyei, you need to do biometrics/fingerprint. \n" +
            "            & If you missed the distribution and did not receive your Bamba Chakula, please call the FREE helpline on 0800 722446, or leave your number and we can call you back. \n" +
            "            & BUTTON(call me back, callbackBC) \n" +
            "\n" +
            "\n" +
            "# Main topics\n" +
            "@bc <- bamba | bumba | mamba | chakula | bc | money | sim | pin | puk | cash | m-pesa | pesa\n" +
            "@gfd <- GFD | GDF | distribute | distribution | food | basket\n" +
            "\n" +
            "@bc {within: 1} -> $dateBamba & Is there anything in particular you want to know?\n" +
            "        & You can ask a question or choose from the topics below. \n" +
            "        & BUTTON(Info about Bamba, bambainfo) BUTTON(get balance, balance) BUTTON(amount, howmuchbamba) BUTTON(SIM card, simcardproblem) BUTTON(missed distribution, misseddistributionbc) \n" +
            "\n" +
            "@gfd {within: 1} ->  $DATEGFD. \n" +
            "  & $FOODBASKET &  What else do you want to know? \n" +
            "\n" +
            "\n" +
            "\n" +
            "# Questions on how to contact\n" +
            "@connect <- contact | operator | call | connect\n" +
            "@connect  ->  If you wish help from an operator, we can also call you back. \n" +
            "                           & You can also call the FREE helpline on 0800 722446 or go to the helpdesk of the nearest food distribution point.\n" +
            "                           & BUTTON(please call me back, connecttooperator )\n" +
            "@callback2 <- connecttooperator | 'call me' | 'call back'\n" +
            "@callback2 {addLabel: PhoneQuestion} -> Please type in your phone number, so we can call you back. \n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "# FALLBACK STRATEGIES\n" +
            "  \n" +
            "() {addLabel: RESET}  ->  Hmmmm, what do you mean exactly?  \n" +
            "  & This is a chatbot that can help you with questions about Bamba Chakula or General Food Distribution (GFD). \n" +
            "  & What do you want to know about? \n" +
            "  & BUTTON(GFD, gfd)  BUTTON(Bamba Chakula, bc)  \n" +
            "   |\n" +
            "  Can you be more specific?\n" +
            "  & Please tell me your problem or ask a question...\n" +
            "  & What do you want to know about?\n" +
            "  & BUTTON(GFD, gfd)  BUTTON(Bamba Chakula, bc) \n" +
            "  | \n" +
            "  Sorry, I don't understand that.  \n" +
            "  & This is a test bot that can help you with questions about Bamba Chakula or General Food Distribution (GFD).\n" +
            "  & You can also report a problem or file a complaint.\n" +
            "  & BUTTON(GFD, gfd)  BUTTON(Bamba Chakula, bc)  \n" +
            "  |\n" +
            "  Sorry I don't understand. & I am only a simple chatbot, not a person. & Please call the FREE helpline on 0800 722446. & If you wish, we can also call you back. \n" +
            "                        &  BUTTON(please call me back, connecttooperator )\n";


    static String camareroScript = "@drinks <- juice | wine | beer | water\n" +
            "\n" +
            "@food <- rice | meat | tortilla | 'mole poblano'\n" +
            "\n" +
            "NOT (@drinks OR @food) -> Hi! What would you like to order?\n" +
            "\n" +
            "@food {continue} -> Aaah, @food... A nice choice\n" +
            "@drinks {continue} -> Ok, @drinks, it will be.\n" +
            "\n" +
            "NOT @drinks -> What would you like to drink?\n" +
            "NOT @drinks {repeat} -> Sorry we only offer \n" +
            "    & BUTTON(juice, juice) BUTTON(wine, wine) \n" +
            "    BUTTON(beer, beer) BUTTON(water, water)\n" +
            "\n" +
            "NOT @food -> What would you like to eat?\n" +
            "\n" +
            "@drinks AND @food -> \n" +
            "    I'll get a glass of @drinks and @food for dinner. \n" +
            "    & Thank you for your order!\n" +
            "\n" +
            "() {repeat} -> I'm only a simple bot. \n" +
            "    & What would you like to eat and drink?\n" +
            "\n";

    @BeforeClass
    public static void before() throws IOException {
        nlp = new Nlp(new File("data/nlp"), ImmutableMap.of("en", new LanguageProcessingConfig()));
        nlp.loadModels(false);
    }

    @Test
    public void matchSimple1(){
        List<Range> eval = new Eval(nlp).find("@fruit <- apple | pear", "I like apple juice").getRanges();

        for (Range range : eval) {
            System.out.println(range);
        }

        assertTrue(Eval.contains(eval, "fruit") && eval.size() == 1);
    }

    @Test
    public void matchSimple2(){
        List<Range> eval = new Eval(nlp).find("@fruit <- apple | pear", "I like cocktails").getRanges();

        for (Range range : eval) {
            System.out.println(range);
        }

        assertTrue(eval.isEmpty());
    }

    @Test
    public void matchSimple3(){
        List<Range> eval = new Eval(nlp).find("@fruit <- apple | pear\n" +
                "@drink <- milk | beer | cocktail", "Me like cocktail").getRanges();

        for (Range range : eval) {
            System.out.println(range);
        }

        assertTrue(Eval.contains(eval, "drink") && eval.size() == 1);
    }

    @Test
    public void matchWithComments1(){
        Script ruleSet = Compiler.compile("@a <- a #cc\n" +
                "@a -> aa\n" +
                "# c\n" +
                "-@a -> Aaa!\n" +
                "# c\n" +
                "-@a -> Aaa!\n" );
        System.out.println("@ruleSet <- ...\n" + ruleSet);
        List<Range> eval = new Eval(nlp).find(ruleSet, "The monkey eats a pear").getRanges();

        System.out.println(ruleSet);
        for (Range range : eval) {
            System.out.println(range);
        }

        assertTrue(Eval.contains(eval, "a"));
    }

    @Test
    public void matchWithComments2(){
        Script ruleSet = Compiler.compile("@vehicle <- car | bike | train  #won't match in this example\n" +
                "@fruit <- pear | (apple & -of_my_eye) | orange\n" +
                "\n" +
                "# Multiple answers, randomly picked\n" +
                "-@vehicle & -@fruit -> Hi! & What kind of car do you drive?\n" +
                "@vehicle -> I like fast cars | I use a bike | I don't like to travel\n" +
                "# Use the matched word for this rule\n" +
                "@fruit -> I like $fruit too! | I hate @fruit, though!\n" +
                "# Fallback case if no rule was matched\n" +
                "() -> I don't understand what you mean...");
        System.out.println("@ruleSet <- ...\n" + ruleSet);
        List<Range> eval = new Eval(nlp).find(ruleSet, "The monkey eats a pear").getRanges();

        System.out.println(ruleSet);
        for (Range range : eval) {
            System.out.println(range);
        }

        assertTrue(Eval.contains(eval, "fruit"));
    }



    @Test
    public void matchSimple4(){
        List<Range> eval = new Eval(nlp).find("@fruit <- apple | pear\n" +
                "@drink <- milk | beer | cocktail\n" +
                "@food <- bread | @fruit", "The monkey eats a pear").getRanges();

        for (Range range : eval) {
            System.out.println(range);
        }

        assertTrue(Eval.contains(eval, "food") && Eval.contains(eval, "fruit"));
    }

    @Test
    public void matchSimple4n(){
        Script ruleSet = Compiler.compile(
                "@fruit <- apple | pear\n" +
                        "@animal <- bear & -beer OR monkey\n" +
                        "@drink <- milk | beer | cocktail\n" +
                        "@food <- bread | @fruit");
        System.out.println("@ruleSet <- ...\n" + ruleSet);
        List<Range> ranges = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        String text = "The monkey eats a pear";
        final Eval eval = new Eval(nlp);
        for (int i = 0; i < N; i++) {
            ranges = eval.find(ruleSet, text).getRanges();
        }
        stopWatch.stop();

        for (Range range : ranges) {
            System.out.println(range);
        }
        System.out.println("@stopWatch <- " + stopWatch);
        System.out.println(eval.highlightWithTags(text, ranges));
        assertTrue(eval.contains(ranges, "food"));
    }

    @Test
    public void matchSimple4nMarkup(){
        Script ruleSet = Compiler.compile(
                "---\n" +
                        "languages : [ar,en]\n" +
                        "---\n" +
                        "# comment\n" +
                        "\n" +
                        "@fruit <- apple | pear\n" +
                        "@drink <- milk | beer | cocktail\n" +
                        "@food <- bread | @fruit\n" +
                        "\n" +
                        "@food2 <- bread | @fruit\n" +
                        "@food3 <- bread | @fruit\n" +
                        "@food4 <- bread | @fruit\n" +
                        "@food5 <- bread | @fruit\n" +
                        "@food6 <- bread | @fruit\n" +
                        "@food7 <- bread | @fruit\n");
        System.out.println("@ruleSet <- ...\n" + ruleSet);
        List<Range> ranges = null;
        StopWatch stopWatch = new StopWatch();

        stopWatch.start();
        final Eval eval = new Eval(nlp);
        for (int i = 0; i < N; i++) {
            ranges = eval.find(ruleSet, "The monkey eats a pear").getRanges();
        }
        stopWatch.stop();

        for (Range range : ranges) {
            System.out.println(range);
        }
        System.out.println("@stopWatch <- " + stopWatch);
        assertTrue(eval.contains(ranges, "food"));
    }

    @Test
    public void matchSimple4nMarkupParallel(){
        Script ruleSet = Compiler.compile(
                "---\n" +
                        "languages : [ar,en]\n" +
                        "---\n" +
                        "# comment\n" +
                        "\n" +
                        "@fruit <- apple | pear\n" +
                        "@drink <- milk | beer | cocktail\n" +
                        "@food <- bread | @fruit\n" +
                        "\n" +
                        "@food2 <- bread | @fruit\n" +
                        "@food3 <- bread | @fruit\n" +
                        "@food4 <- bread | @fruit\n" +
                        "@food5 <- bread | @fruit\n" +
                        "@food6 <- bread | @fruit\n" +
                        "@food7 <- bread | @fruit\n");
        System.out.println("@ruleSet <- ...\n" + ruleSet);
        StopWatch stopWatch = new StopWatch();

        stopWatch.start();
        ExecutorService executor = Executors.newFixedThreadPool(8);
        final Eval eval = new Eval(nlp);

        for (int i = 0; i < N; i++) {
            executor.execute(() -> {
                eval.find(ruleSet, "The monkey eats a pear").getRanges();
            });

        }
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException ignored) {}
        stopWatch.stop();
        System.out.println("Finished all threads");

        System.out.println("@stopWatch <- " + stopWatch);
    }

    public void matchMultinline(String src){
        Script ruleSet = Compiler.compile(src);
        System.out.println("ruleSet.dependencyTree() = " + ruleSet.dependencyTree());
        System.out.println("@ruleSet <- ...\n" + ruleSet);
        List<Range> eval = new Eval(nlp).find(ruleSet, "The monkey eats a pear").getRanges();

        System.out.println(ruleSet);
        for (Range range : eval) {
            System.out.println(range);
        }

        assertTrue(Eval.contains(eval, "food") && Eval.contains(eval, "fruit"));
    }

    @Test
    public void matchMultinlineBeforeOp(){
        matchMultinline(
                "@fruit <- apple \n" +
                        "  | pear\n" +
                        "@drink <- milk | beer | cocktail\n" +
                        "@food <- bread | @fruit");
    }

    @Test
    public void matchMultinlineAfterOp(){
        matchMultinline(
                "@fruit <- apple  | pear\n" +
                        "@drink <- milk | beer | \n" +
                        "    cocktail\n" +
                        "@food <- bread | @fruit");
    }

    @Test
    public void matchMultinlineWithEmptyLines(){
        matchMultinline(
                "@fruit <- apple  | pear\n" +
                        "@drink <- milk | beer | \n" +
                        "    cocktail\n" +
                        "   \n" +
                        "@food <- bread | @fruit");
    }

    @Test
    public void matchMultinlineStartEmpty(){
        matchMultinline(
                "@fruit <-\n" +
                        "   apple  | pear\n" +
                        "@drink <- milk | beer | \n" +
                        "    cocktail\n" +
                        "   \n" +
                        "@food <- bread | @fruit");
    }

    @Test
    public void matchMultinlineStartEmpty2(){
        matchMultinline(
                "\n\n@fruit <-\n" +
                        "   apple  | pear\n" +
                        "@drink <- milk | beer | \n" +
                        "    cocktail\n" +
                        "   \n" +
                        "@food <- bread | @fruit");
    }

    @Test(expected = MultiScriptException.class)
    public void recursionError1(){
        try {
            Script script = Compiler.compile("@fruit <- apple | pear | @fruit\n" +
                    "");
            System.out.println("script.dependencyTree() = " + script.dependencyTree());
        } catch (ScriptException e){
            System.out.println("@e <- " + e);
            throw e;
        }
    }

    @Test(expected = MultiScriptException.class)
    public void recursionError2(){
        try {
            Compiler.compile("@fruit <- apple | pear | @foo\n" +
                    "@foo <- @bar\n" +
                    "@bar <- @fruit");
        } catch (ScriptException e){
            System.out.println("e <- " + e);
            throw e;
        }
    }

    @Test(expected = MultiScriptException.class)
    public void recursionError3(){
        try {
            Compiler.compile("@fruit <- apple | pear | @foo\n" +
                    "@foo <- @bar\n" +
                    "@bar <- @foo");
        } catch (ScriptException e){
            System.out.println("e <- " + e);
            throw e;
        }
    }

    @Test
    public void noRecursionError1(){
        try {
            Script script = Compiler.compile(
                    "@foo <- apple | @bar | @baz\n" +
                            "@bar <- @baz\n" +
                            "@baz <- qux");
            System.out.println("script.dependencyGraph() = " + script.dependencyGraph());
            System.out.println("script.dependencyGraphInv() = " + script.dependencyGraphInv());
            System.out.println("script.dependencyTree() = " + script.dependencyTree());
        } catch (ScriptException e){
            System.out.println("e <- " + e);
            Assert.fail();
        }
    }

    @Test
    public void noRecursionError2(){
        try {
            Script script = Compiler.compile(
                    "@foo <- apple | @bar | @baz\n" +
                            "@bar <- @baz\n" +
                            "@baz <- qux");
            System.out.println("script.dependencyGraph() = " + script.dependencyGraph());
            System.out.println("script.dependencyGraphInv() = " + script.dependencyGraphInv());
            System.out.println("script.dependencyTree() = " + script.dependencyTree());
        } catch (ScriptException e){
            System.out.println("e <- " + e);
            Assert.fail();
        }
    }


    @Test
    public void testBigScriptCompile(){

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            Script script = null;
            for (int i = 0; i < 10; i++) {
                script = Compiler.compile(bigSrc);
            }
            stopWatch.stop();
            System.out.println("stopWatch = " + stopWatch);

            System.out.println("script.dependencyGraph() = " + script.dependencyGraph());
            System.out.println("script.dependencyGraphInv() = " + script.dependencyGraphInv());
            System.out.println("script.dependencyTree() = " + script.dependencyTree());
            // ~30 ms per compile


        } catch (ScriptException e){
            System.out.println("e <- " + e);
            Assert.fail();
        }
    }

    @Test
    public void testBigScriptReply(){
        testScriptReply(bigSrc, new String[]{"Hi!", "how much gfd?", "how much gfd?", "bc"});
    }

    @Test
    public void testBigScriptReplyWithEvalState(){
        testScriptReplyWithEvalState(bigSrc, new String[]{"Hi!", "how much gfd?", "how much gfd?", "bc"});
    }

    @Test
    public void testCamareroScriptReply(){
        testScriptReply(camareroScript, new String[]{"Hi!", "I want beer", "A pancake", "then tortilla..."});
    }

    @Test
    public void testCamareroScriptReplyWithEvalState(){
        testScriptReplyWithEvalState(camareroScript, new String[]{"Hi!", "I want beer", "A pancake", "then tortilla..."});
    }


    public void testScriptReply(String src, String[] userInput){

        try {
            Script script = Compiler.compile(src);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            System.out.println("reply with conv");
            for (int i = 0; i < N; i++) {
                final Eval eval = new Eval(nlp);
                final Conversation conv = new Conversation("test");

                for (int j = 0; j < userInput.length; j++) {
                    String in = userInput[j];
                    Message m = new Message();
                    m.setConversationId(conv.getId());
                    m.setText(in);
                    m.setIncoming(true);
                    conv.getMessages().add(m);

                    final Result reply = eval.reply(script, conv);
//                    System.out.println(j + ": " + in + " -> " + reply.getReplies().get(0).getText());
                }
            }

            stopWatch.stop();
            System.out.println("stopWatch = " + stopWatch);

            // ~30 ms per compile


        } catch (ScriptException e){
            System.out.println("e <- " + e);
            Assert.fail();
        }
    }


    public void testScriptReplyWithEvalState(String src, String[] userInput){

        try {
            Script script = Compiler.compile(src);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            System.out.println("reply with eval");
            for (int i = 0; i < N; i++) {
                final Eval eval = new Eval(nlp);
                final Conversation conv = new Conversation("test");
                Eval.EvalState state = new Eval.EvalState(new Script(script), conv);

                for (int j = 0; j < userInput.length; j++) {
                    String in = userInput[j];
                    Message m = new Message();
                    m.setConversationId(conv.getId());
                    m.setText(in);
                    m.setIncoming(true);
                    conv.getMessages().add(m);

                    final Result reply = eval.reply(script, state);
//                    System.out.println(j + ": " + in + " -> " + reply.getReplies().get(0).getText());
                }
            }

            stopWatch.stop();
            System.out.println("stopWatch = " + stopWatch);

            // ~30 ms per compile


        } catch (ScriptException e){
            System.out.println("e <- " + e);
            Assert.fail();
        }
    }





}
