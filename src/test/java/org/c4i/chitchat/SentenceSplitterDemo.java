package org.c4i.chitchat;


import org.c4i.nlp.tokenize.RegexSentenceSplitter;

/**
 * @author Arvid
 * @version 6-4-2015 - 12:06
 */
public class SentenceSplitterDemo {
    public static void main(String[] args) {
        String text;

        text = "Mr. Smith bought cheapsite.com for 1.5 million dollars, i.e. he paid a lot for it. Did he mind? Adam Jones Jr. thinks he didn't. In any case, this isn't true... Well, with a probability of .9 it isn't.";
        for (String sentence : new RegexSentenceSplitter().split(text)) {
            System.out.println("sentence = " + sentence);
        }

        System.out.println();
        text = "It words with exclamation marks as well!! Yeaaaah! really, you see?";
        for (String sentence : new RegexSentenceSplitter().split(text)) {
            System.out.println("sentence = " + sentence);
        }
    }
}
