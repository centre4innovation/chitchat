package org.c4i.chitchat;

import org.c4i.nlp.WordTest;
import org.c4i.nlp.ner.DataSheet;
import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.tokenize.MatchingWordTokenizer;

import java.io.IOException;

/**
 * @author Arvid Halma
 * @version 21-5-19
 */
public class PeruDatasheetDemo {

    public static void main(String[] args) throws IOException {
        String data =
                "VARIEDAD\tPRODUCTO\tMCDO\tPRECIO.KG\tFECHA\n" +
                        "papa blanca\tpapa\tmercado 1\t2.50\t2019-05-01\n" +
                        "papa amarillo\tpapa\tmercado 1\t3.10\t2019-05-01\n" +
                        "agi 1\taji\tmercado 1\t3.10\t2019-05-01\n" +
                        "agi 2\taji\tmercado 1\t3.10\t2019-05-01\n"
                ;

        DataSheet sheet1 = new DataSheet("PRECIOS", data, StringNormalizer.IDENTITY, new MatchingWordTokenizer(), WordTest.DEFAULT, true);
        System.out.println(sheet1);

        final DataSheet sheet2 = sheet1.withIndex("producto");
        System.out.println("sheet2 = " + sheet2);






    }
}
