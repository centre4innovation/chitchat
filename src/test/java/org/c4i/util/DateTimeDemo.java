package org.c4i.util;

import org.c4i.util.time.DateTimeUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.Date;
import java.util.Locale;


public class DateTimeDemo {
    public static void main(String[] args) {
        spanishString();
        parsePeruFecha();
        System.out.println("ISODateTimeFormat.basicDate().print(DateTime.now())  = " + ISODateTimeFormat.date().print(DateTime.now()));
    }

    public static void parsePeruFecha(){
        DateTime t = DateTimeUtil.parseLiberalDateTime("19/06/2019 09:05:08 a.m.");
        System.out.println(t);
    }

    public static void  spanishString(){
        DateTime t = DateTime.now();
//        DateTimeFormatter formatter = DateTimeFormat.forPattern("EEEE d 'de' MMMM 'de' yyyy");

        DateTimeFormatter formatter = DateTimeFormat.forStyle("F-").withLocale(Locale.forLanguageTag("es"));
        System.out.println("formatter.format(t) = " + t.toString(formatter));
    }
}
