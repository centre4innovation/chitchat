package org.c4i.util;

import com.google.re2j.Matcher;
import com.google.re2j.Pattern;

/**
 * @author Arvid Halma
 * @version 5-2-19
 */
public class Re2jDemo {

    public static void main(String[] args) {
        Matcher m = Pattern.compile("a").matcher("abaca");
        while (m.find()){
            System.out.println("m.group() = " + m.group() + " " + m.start());
        }
    }

}
