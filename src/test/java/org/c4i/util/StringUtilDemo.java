package org.c4i.util;

/**
 * @author Arvid Halma
 * @version 10-12-2017 - 20:03
 */
public class StringUtilDemo {
    public static void main(String[] args) {

        System.out.println("Distance");
        System.out.println("StringUtil.levenshteinDistance(\"abc\", \"def\") = " + StringUtil.levenshteinDistance("abc", "def"));
        System.out.println("StringUtil.levenshteinDistance(\"abc\", \"abc\") = " + StringUtil.levenshteinDistance("abc", "abc"));
        System.out.println("StringUtil.levenshteinDistance(\"abc\", \"bc\") = " + StringUtil.levenshteinDistance("abc", "bc"));
        System.out.println("StringUtil.levenshteinDistance(\"bc\", \"abc\") = " + StringUtil.levenshteinDistance("bc", "abc"));


        System.out.println("Similarity");
        System.out.println("StringUtil.levenshteinSimilarity(\"abc\", \"b\") = " + StringUtil.levenshteinSimilarity("abc", "b"));
        System.out.println("StringUtil.levenshteinSimilarity(\"b\", \"abc\") = " + StringUtil.levenshteinSimilarity("b", "abc"));
        System.out.println("StringUtil.levenshteinSimilarity(\"b\", \"b\") = " + StringUtil.levenshteinSimilarity("b", "b"));
        System.out.println("StringUtil.levenshteinSimilarity(\"abc\", \"def\") = " + StringUtil.levenshteinSimilarity("abc", "def"));
        System.out.println("StringUtil.levenshteinSimilarity(\"abc\", \"abc\") = " + StringUtil.levenshteinSimilarity("abc", "abc"));
        System.out.println("StringUtil.levenshteinSimilarity(\"abc\", \"bc\") = " + StringUtil.levenshteinSimilarity("abc", "bc"));
        System.out.println("StringUtil.levenshteinSimilarity(\"bc\", \"abc\") = " + StringUtil.levenshteinSimilarity("bc", "abc"));

        System.out.println("StringUtil.levenshteinSimilarity(\"aabbcc\", \"def\") = " + StringUtil.levenshteinSimilarity("aabbcc", "def"));
        System.out.println("StringUtil.levenshteinSimilarity(\"aabbcc\", \"aabbcc\") = " + StringUtil.levenshteinSimilarity("aabbcc", "aabbcc"));
        System.out.println("StringUtil.levenshteinSimilarity(\"aabbcc\", \"bbcc\") = " + StringUtil.levenshteinSimilarity("aabbcc", "bbcc"));
        System.out.println("StringUtil.levenshteinSimilarity(\"bbcc\", \"aabbcc\") = " + StringUtil.levenshteinSimilarity("bbcc", "aabbcc"));
        System.out.println("StringUtil.levenshteinSimilarity(\"bbcc\", \"aabbcccccccccccccc\") = " + StringUtil.levenshteinSimilarity("bbcc", "aabbcccccccccccccc"));

        System.out.println("StringUtil.levenshteinSimilarity(\"aabbcc\", \"aacc\") = " + StringUtil.levenshteinSimilarity("aabbcc", "aacc"));
        System.out.println("StringUtil.levenshteinSimilarity(\"aacc\", \"aabbcc\") = " + StringUtil.levenshteinSimilarity("aacc", "aabbcc"));
        System.out.println("StringUtil.levenshteinSimilarity(\"aacc\", \"aabbcccccccccccccc\") = " + StringUtil.levenshteinSimilarity("aacc", "aabbcccccccccccccc"));


        genLevenshtein("", "", 2);
        genLevenshtein("a", "a", 2);
        genLevenshtein("aa", "aa", 2);
        genLevenshtein("aaa", "aaa", 2);
        genLevenshtein("aaba", "aaba", 2);
        genLevenshtein("ababa", "ababa", 2);


        genLevenshtein("", "aa", 2);
        genLevenshtein("a", "aa", 2);
        genLevenshtein("aa", "aa", 2);
        genLevenshtein("aaa", "aa", 2);
        genLevenshtein("aba", "aa", 2);
        genLevenshtein("cba", "aa", 2);
        genLevenshtein("aaba", "aa", 2);
        genLevenshtein("ababa", "aa", 2);


        System.out.println("StringUtil.ngrams(2, \"arvid\") = " + StringUtil.ngrams(2, "arvid"));
        System.out.println("StringUtil.ngrams(2, \"a\") = " + StringUtil.ngrams(2, "a"));
        System.out.println("StringUtil.ngrams(2, \"\") = " + StringUtil.ngrams(2, ""));



    }

    public static void genLevenshtein(String a, String b, int minSubStrLen){
        System.out.println("--------");
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("minSubStrLenDist("+minSubStrLen+") = " + StringUtil.levenshteinDistance(a, b, minSubStrLen));

    }
}
