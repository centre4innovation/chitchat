package org.c4i.nlp.match;

import org.c4i.nlp.Nlp;
import org.c4i.util.StringUtil;
import com.google.common.collect.ImmutableList;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Common {@link Script} {@link Validator}s.
 * @author Arvid Halma
 */
public class Validators {

    public static final Validator COMMON_ERRORS =
            new CheckLabelNames()
            .compose(new CheckLabelReferences())
            .compose(new CheckReplyReferences())
            .compose(new CheckReplyTemplateReferences())
            .compose(new CheckReplyRuleContinueAddTextCombinations())
            .compose(new CheckLabelRuleProperties())
            .compose(new CheckLabelRulePreambleProperties())
            .compose(new CheckReplyRuleProperties())
            .compose(new CheckReplyRulePreambleProperties())
            .compose(new CheckDirectRecursion())
            .compose(new CheckCyclicRecursion());

    public static final Validator COMMON_WARNINGS =
            new CheckUnreachableReplyRules()
            .compose(new CheckAddLabelUsage())
            .compose(new CheckReplyRulePropertiesInBody());
//            .compose(new CheckFacebookLimitations());


    /**
     * Validate script rules (rule consistency) statically.
     * @param script The script.warnings list will be updated
     */
    public static void validate(Script script){
        script.warnings = Validators.COMMON_WARNINGS.check(script);
        List<ScriptException> errors = Validators.COMMON_ERRORS.check(script);
        if (!errors.isEmpty()){
            errors.addAll(script.warnings);
            throw new MultiScriptException(errors);
        }
    }

    /**
     * Validate script rules (rule consistency) statically and with respect to NLP resources.
     * @param script The script.warnings list will be updated
     * @param nlp available language models
     */
    public static void validate(Script script, Nlp nlp){
        validate(script);
        List<ScriptException> warnings = new CheckLabelPluginAvailability(nlp).check(script);
        script.warnings.addAll(warnings);
    }

    static class CheckLabelReferences implements Validator {
        @Override
        public List<ScriptException> check(final Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            s.labels.values().stream().flatMap(Collection::stream).forEach(rule -> {
                exceptions.addAll(checkReferences(s, rule.expression, rule.line));
            });
            return exceptions;
        }
    }

    static class CheckLabelPluginAvailability implements Validator {
        private Nlp nlp;

        public CheckLabelPluginAvailability(Nlp nlp) {
            this.nlp = nlp;
        }

        @Override
        public List<ScriptException> check(final Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            s.labels.values().stream().flatMap(Collection::stream).forEach(rule -> {
                Literal[][] expression = rule.expression;
                Arrays.stream(expression).flatMap(Arrays::stream).forEach(lit -> {
                    String ref = lit.tokens[0].getWord();
                    if (EntityPlugin.isPluginPattern(lit)
                            && nlp.getEntityPlugins(s.getConfig()).stream().noneMatch(ep -> ref.equals(ep.description())))
                    {
                        exceptions.add(new ScriptException(String.format("%s is an unsupported entity plugin (for this language). Double quote it to ignore this message.", ref), rule.line));
                    }
                });
            });
            return exceptions;
        }
    }

    static class CheckLabelNames implements Validator {
        Pattern NON_ALPHANUM = Pattern.compile("(?U)[\\W_]");
        @Override
        public List<ScriptException> check(final Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            s.labels.values().stream().flatMap(Collection::stream).forEach(rule -> {
                Matcher matcher = NON_ALPHANUM.matcher(rule.head);
                if(matcher.find()){
                    exceptions.add(new ScriptException(String.format("Invalid label name: @%s\nLabels can only contain letters and digits, no underscores or other characters: %s", rule.head, matcher.group()), rule.line));
                }
            });
            return exceptions;
        }
    }

    static class CheckReplyReferences implements Validator {
        @Override
        public List<ScriptException> check(final Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            for (ReplyRule rule : s.replies) {
                exceptions.addAll(checkReferences(s, rule.rule.expression, rule.line));
            }
            return exceptions;
        }
    }

    static class CheckReplyTemplateReferences implements Validator {
        @Override
        public List<ScriptException> check(Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            Set<String> labels = allDefinedLabels(s);

            for (ReplyRule reply : s.replies) {
                reply.replies.stream().flatMap(Collection::stream).forEach(msg -> {
                    Matcher matcher = Script.REPLY_TEMPLATE_REF.matcher(msg);
                    while (matcher.find()){
                        String label = matcher.group(1);
                        if("label".equals(label))
                            continue;

                        if(!labels.contains(label)) {
                            for (String definedLabel : labels) {
                                if (definedLabel.equalsIgnoreCase(label)) {
                                    exceptions.add(new ScriptException(String.format("References to labels are case sensitive. Use: @%s", definedLabel), reply.line));
                                }
                            }

                            exceptions.add(new ScriptException(String.format("There is a reference to a label that is not defined: @%s", label), reply.line));
                        }
                    }
                });
            }
            return exceptions;
        }
    }

    static class CheckDirectRecursion implements Validator {
        @Override
        public List<ScriptException> check(Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            s.labels.values().stream().flatMap(Collection::stream).forEach(rule -> {
                Literal[][] expression = rule.expression;
                Arrays.stream(expression).flatMap(Arrays::stream).forEach(lit -> {
                    String ref = lit.tokens[0].getWord();
                    if (lit.meta == '@' && rule.head.equals(ref)) {
                        exceptions.add(new ScriptException(String.format("Rule '@%s' contains a reference to itself. No recursion allowed.", ref), rule.line));
                    }
                });
            });
            return exceptions;
        }
    }


    static class CheckCyclicRecursion implements Validator {
        @Override
        public List<ScriptException> check(Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            // build graph
            Map<String, List<Script.Dependency>> graph = s.dependencyGraph();

            for (String v1 : graph.keySet()) {
                for (String v2 : graph.keySet()) {
                    if(!v1.equals(v2) && Script.Dependency.reachable(v1, v2, graph) && Script.Dependency.reachable(v2, v1, graph)){
                        // two-way dependencies
                        exceptions.add(new ScriptException(String.format("Rules '@%s' and '@%s' are defined in terms of each other.\nNo direct or cyclic recursion allowed when defining labels.", v1, v2), s.labels.get(v1).get(0).line));
                    }
                }
            }
            return exceptions;
        }
    }

    static class CheckUnreachableReplyRules implements Validator {
        @Override
        public List<ScriptException> check(Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            // no matches after reply(){repeat} = ... labels (it always matches)
            for (int i = 0; i < s.replies.size(); i++) {
                ReplyRule reply = s.replies.get(i);
                if (reply.isFallback()
                        && (reply.isProp("repeat") || reply.getAddLabels().contains("RESET"))
                        && i < s.replies.size() - 1)
                {
                    exceptions.add(new ScriptException("This reply will always keep matching.\nThe replies defined below will never be reached.", reply.line, ScriptException.Type.WARNING));
                }
            }
            return exceptions;
        }
    }

    static class CheckReplyRuleContinueAddTextCombinations implements Validator{
        @Override
        public List<ScriptException> check(Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            for (ReplyRule reply : s.replies) {
                if (reply.props.containsKey("continue") && reply.props.containsKey("addText")) {
                    exceptions.add(new ScriptException("The reply properties 'continue' and 'addText' can't be combined, It only doesn't works for a chain of continuations.", reply.line));
                }
            }
            return exceptions;
        }
    }

    @Deprecated // Cycle detections prevents infinite loops
    static class CheckReplyRuleContinueRepeatCombinations implements Validator{
        @Override
        public List<ScriptException> check(Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            for (ReplyRule reply : s.replies) {
                if (reply.props.containsKey("continue") && reply.props.containsKey("repeat")) {
                    exceptions.add(new ScriptException("The reply properties 'continue' and 'repeat' can't be combined, because of potential infinite loops.", reply.line));
                }
            }
            return exceptions;
        }
    }

    @Deprecated // Cycle detections prevents infinite loops
    static class CheckReplyRuleContinueRESETCombinations implements Validator{
        @Override
        public List<ScriptException> check(Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            for (ReplyRule reply : s.replies) {
                if (reply.props.containsKey("continue") && "RESET".equals(reply.props.get("addLabel"))) {
                    exceptions.add(new ScriptException("The reply properties 'continue' and 'addLabel: RESET' can't be combined, because of potential infinite loops.", reply.line));
                }
            }
            return exceptions;
        }
    }

    static class CheckReplyRulePropertiesInBody implements Validator {
        @Override
        public List<ScriptException> check(Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            for (ReplyRule reply : s.replies) {
                final String text = reply.replies.get(0).get(0);
                if(text.startsWith("{")){
                    exceptions.add(new ScriptException("It seems strange to start a reply with '{'.\nAre the properties defined after '->' in stead of before it?"));
                }
            }
            return exceptions;
        }
    }

    static class CheckLabelRuleProperties implements Validator {
        @Override
        public List<ScriptException> check(Script s) {
            return checkRuleProperties(s.labels.values().stream().collect(ArrayList::new, List::addAll, List::addAll));
        }
    }

    static class CheckLabelRulePreambleProperties implements Validator {
        @Override
        public List<ScriptException> check(Script s) {
            LabelRule rule = new LabelRule(null, null, 1);
            rule.props = new LinkedHashMap<>(s.getConfig().getRuleProperties().label);
            final List<ScriptException> scriptExceptions = checkRuleProperties(ImmutableList.of(rule));
            scriptExceptions.forEach(se -> se.message = "Error in (rule: label: ...): " +  se.message);
            return scriptExceptions;
        }
    }

    static class CheckReplyRuleProperties implements Validator{
        @Override
        public List<ScriptException> check(Script s) {
            return checkRuleProperties(s.replies);
        }
    }

    static class CheckReplyRulePreambleProperties implements Validator {
        @Override
        public List<ScriptException> check(Script s) {
            ReplyRule rule = new ReplyRule(null, null, 1);
            rule.props = new LinkedHashMap<>(s.getConfig().getRuleProperties().reply);
            final List<ScriptException> scriptExceptions = checkRuleProperties(ImmutableList.of(rule));
            scriptExceptions.forEach(se -> se.message = "Error in (rule: reply: ...): " +  se.message);
            return scriptExceptions;
        }
    }

    static class CheckFacebookLimitations implements Validator{
        @Override
        public List<ScriptException> check(Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);
            /*
            There is a limit for the number of elements and buttons you can add to the templates.
            There is also a limit for the length of messages and number of captions.
            Upon exceeding these limits, instead of having your text trimmed, you will receive an
            error and the API will (in some cases) refuse to send your message.

            Maximum length of characters is 80 for titles and subtitles (descriptions),
            and 320 for text messages. You can have up to 10 items among your generic templates
            with 3 buttons for each maximum. For quick messages, you can have up to 11 different buttons,
            but each of them must consist of 20 characters maximum.

            Source: https://chatbotsmagazine.com/listicle-of-things-missing-from-facebook-messenger-chatbot-platforms-documentation-d1d50922ef15
            */

            Pattern button = Pattern.compile("\\bBUTTON\\( *(.*?) *, *(.*?) *\\)");
            for (ReplyRule reply : s.replies) {
                reply.replies.stream().flatMap(List::stream).forEach(text -> {
                    Matcher buttonMatcher = button.matcher(text);
                    int buttonCount = 0;
                    while(buttonMatcher.find()){
                        buttonCount++;
                        String buttonText = buttonMatcher.group(1);
                        //String buttonValue = buttonMatcher.group(2);
                        if(buttonText.length() > 20){
                            exceptions.add(new ScriptException(String.format("The text for the quick reply button is too long (max 20 characters): '%s'", buttonText), reply.line));
                        }
                    }
                    if(buttonCount > 11){
                        exceptions.add(new ScriptException(String.format("There are %d quick reply buttons defined. The maximum is 11.", buttonCount), reply.line));
                    }

                    if(buttonCount > 0)
                        return;

                    if(text.length() > 320){
                        exceptions.add(new ScriptException(String.format("The reply text is too long: %d characters. The maximum is 11.", text.length()), reply.line));
                    }


                });
            }
            return exceptions;
        }
    }

    static class CheckAddLabelUsage implements Validator {
        @Override
        public List<ScriptException> check(Script s) {
            List<ScriptException> exceptions = new ArrayList<>(0);

            for (ReplyRule reply : s.replies) {
                if (reply.isProp("addLabel")) {
                    Object addLabel = reply.getProp("addLabel");
                    if (addLabel == null) {
                        continue;
                    }

                    if (addLabel instanceof String) {
                        final String strLabel = (String) addLabel;
                        if (!isLabelUsage(s, strLabel)) {
                            exceptions.add(new ScriptException(String.format("A label is added {addLabel: %s}, but never used.", strLabel), reply.line, ScriptException.Type.WARNING));
                        }
                    } else if (addLabel instanceof List) {
                        for (Object strLabel : (List) addLabel) {
                            if (!isLabelUsage(s, (String) strLabel)) {
                                exceptions.add(new ScriptException(String.format("A label is added {addLabel: %s}, but never used.", strLabel), reply.line, ScriptException.Type.WARNING));
                            }
                        }
                    }
                }
            }
            return exceptions;
        }
    }

    private static boolean isLabelUsage(Script s, String label){
        // reset is a special case
        if("RESET".equals(label))
            return true;

        // check label rule expressions
        for (List<LabelRule> labelRules : s.labels.values()) {
            for (LabelRule rule : labelRules) {
                Literal[][] expression = rule.expression;
                if (isLabelUsage(label, expression)) return true;
            }
        }

        // check reply conditions
        for (ReplyRule reply : s.replies) {
            final Literal[][] expression = reply.rule.expression;
            if (isLabelUsage(label, expression)) return true;
        }
        return false;
    }

    private static boolean isLabelUsage(String label, Literal[][] expression) {
        return Arrays.stream(expression).flatMap(Arrays::stream).anyMatch(lit -> lit.meta == '@' & label.equals(lit.tokens[0].getWord()));
    }


    private static List<ScriptException> checkRuleProperties(Collection<? extends Rule> rules){
        List<ScriptException> exceptions = new ArrayList<>(0);
        for (Rule rule : rules) {
            for (Map.Entry<String, Object> prop : rule.props.entrySet()) {
                String key = prop.getKey();

                if(!rule.isValidProp(key)){
                    if(rule.isValidProp(key.toLowerCase())){
                        exceptions.add(new ScriptException(String.format("Properties are case sensitive. Use {%s: ...}'.", key.toLowerCase()), rule.line));
                    }
                    exceptions.add(new ScriptException(String.format("This is not a valid property '%s'.", key), rule.line));
                } else {
                    Object val = prop.getValue();
                    if (!rule.isValidPropValue(key, val)) {
                        final String valStr = val.toString();
                        Set<String> vals = rule.validPropValues(key);
                        if (vals == null) {
                            exceptions.add(new ScriptException(String.format("The property '%s' doesn't take any arguments (remove the ': %s' part)", key, val), rule.line));
                        } else {
                            if (rule.isValidPropValue(key, (valStr).toLowerCase())) {
                                exceptions.add(new ScriptException(String.format("Property values are case sensitive. Use {%s, %s}.", key, valStr.toLowerCase()), rule.line));
                            }
                            exceptions.add(new ScriptException(String.format("The value of property '%s' is not valid (use one of the values: %s)", key, vals.toString()), rule.line));
                        }
                    }
                }
            }
        }
        return exceptions;
    }




    private static Stream<LabelRule> ruleStream(Script s){
        return s.getLabels().values().stream().flatMap(Collection::stream);
    }

    private static List<ScriptException> checkReferences(Script s, Literal[][] expression, int line) {
        final List<ScriptException> exceptions = new ArrayList<>(0);
        Arrays.stream(expression).flatMap(Arrays::stream).forEach(lit -> {
            List<String> refsToCheck;

            switch (lit.meta){
                case '@': refsToCheck = ImmutableList.of(lit.tokens[0].getWord()); break;
                case '=': {
                    refsToCheck = new ArrayList<>(2);
                    final int opIx = lit.getMarker();
                    String left = getLabelPart(lit.tokens[0].getWord());
                    if(left != null) refsToCheck.add(left);
                    String right = getLabelPart(lit.tokens[opIx+1].getWord());
                    if(right != null) refsToCheck.add(right);
                } break;
                default : refsToCheck = ImmutableList.of();
            }

            Set<String> labels = allDefinedLabels(s);
            for (String ref: refsToCheck){
                if (!labels.contains(ref)) {
                    if(ref.startsWith("@")){
                        exceptions.add(new ScriptException(String.format("When adding labels, don't use '@', just: %s", ref.substring(1)), line));
                    }

                    if(StringUtil.alphaIsAllUpperCase(ref)){
                        exceptions.add(new ScriptException(String.format("The label @%s is not defined.\nMaybe remove '@' if you want reference a data sheet (just: %s).", ref, ref), line));
                    }

                    for (String definedLabel : labels) {
                        if(definedLabel.equalsIgnoreCase(ref)){
                            exceptions.add(new ScriptException(String.format("References to labels are case sensitive. Use: @%s", definedLabel), line));
                        }
                    }

                    exceptions.add(new ScriptException(String.format("There is a reference to a label that is not defined: @%s", ref), line));

                }
            }
        });
        return exceptions;
    }

    private static Set<String> allDefinedLabels(Script s){
        // gather all labels that are possibly defined in the script
        // add label rule heads
        Set<String> allDefinedLabels = new HashSet<>(s.labels.keySet());
        // add labels from "addLabel" in reply props
        for (ReplyRule reply : s.replies) {
            allDefinedLabels.addAll(reply.getAddLabels());
        }
        return allDefinedLabels;
    }

    private static String getLabelPart(String compareOperand){
        if(compareOperand ==  null || !compareOperand.startsWith("@"))
            return null;
        int propSep = compareOperand.indexOf('.');
        if(propSep < 2){
            return null;
        }
        return compareOperand.substring(1, propSep);
    }
}
