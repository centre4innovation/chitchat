package org.c4i.nlp.match;

import org.c4i.nlp.tokenize.Token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Split all tokens even further in graphemes (say a letter followed by zero or more accents).
 * Some languages are written without spaces (e.g. Thai, Chinese, Japanese and Khmer).
 * We use a sequence of grapheme tokens as single word.
 * @author Arvid Halma
 */
public class GraphemeSplitPostProcessor implements Function<Script, Script> {
    private static Pattern grapheme = Pattern.compile("\\P{M}\\p{M}*");

    /**
     * N.B. The input script is modified.
     * @param script initial script
     * @return result where all meta='a' literals are split into graphemes.
     */
    @Override
    public Script apply(Script script) {
        // check label rule expressions
        for (List<LabelRule> labelRules : script.labels.values()) {
            for (LabelRule rule : labelRules) {
                Literal[][] expression = rule.expression;
                Arrays.stream(expression).flatMap(Arrays::stream).forEach(lit -> {
                    if(lit.meta == 'a'){
                        // word like
                        lit.tokens = split(lit.tokens);
                    }
                });
            }
        }
        return script;
    }

    public static List<String> split(String s){
        List<String> gs = new ArrayList<>();
        Matcher matcher = grapheme.matcher(s);
        while (matcher.find()){
            gs.add(matcher.group());
        }
        return gs;
    }



    public static Token[] split(Token[] ts){
        List<Token> gs = new ArrayList<>();
        for (Token t : ts) {
            for (String g : split(t.getNormalizedWord())) {
                gs.add(new Token(t).setWord(g)); // all other token params preserved.
            }
        }
        return gs.toArray(new Token[0]);
    }


}
