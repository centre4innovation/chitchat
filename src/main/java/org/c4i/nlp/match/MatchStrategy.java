package org.c4i.nlp.match;

import org.c4i.nlp.tokenize.Token;

import java.util.List;

/**
 * Abstract the different ways tokens can be compared for equality.
 * @author Arvid Halma
 * @version 5-2-19
 */
public interface MatchStrategy {

    MatchStrategy create(Token pattern);

    List<String> matches(Token b);

    char quote();
}
