package org.c4i.nlp.match;

import org.c4i.nlp.tokenize.Token;

import java.util.ArrayList;
import java.util.List;

import com.google.re2j.Matcher;
import com.google.re2j.Pattern;
import org.c4i.util.StringUtil;

/**
 * Match regular expression tokens.
 * RE2J is used to ensure linear time testing for security reasons (user defined expressions).
 * @author Arvid Halma
 * @version 5-2-19
 */
public class RegexMatchStrategy implements MatchStrategy {

    private Pattern pattern;

    public RegexMatchStrategy(Token pattern) {
        final String word = pattern.getWord();
        StringUtil.unquote(word);
        this.pattern = Pattern.compile(word);
    }

    @Override
    public MatchStrategy create(Token pattern) {
        return new RegexMatchStrategy(pattern);
    }

    @Override
    public List<String> matches(Token b) {
        Matcher m = pattern.matcher(b.getWord());
        if(m.matches()){
            final int n = m.groupCount();
            List<String> result = new ArrayList<>(n);
            for (int i = 1; i <= n; i++) {
                 result.add(m.group(i)); // only inner groups. Not the entire match.
            }
            return result;
        }
        return null;
    }

    @Override
    public char quote() {
        return '/';
    }
}
