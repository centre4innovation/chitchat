package org.c4i.nlp.match;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.common.collect.ImmutableMap;
import org.c4i.chitchat.api.model.LanguageProcessingConfig;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.generalize.Generalizer;
import org.c4i.nlp.match.query.LuceneConverter;
import org.c4i.nlp.match.query.PostgresConverter;
import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.tokenize.Token;
import org.c4i.util.EmojiAlias;
import org.c4i.util.StringUtil;
import org.parboiled.BaseParser;
import org.parboiled.MatcherContext;
import org.parboiled.Parboiled;
import org.parboiled.annotations.BuildParseTree;
import org.parboiled.common.StringUtils;
import org.parboiled.matchers.CharRangeMatcher;
import org.parboiled.parserunners.RecoveringParseRunner;
import org.parboiled.support.ParsingResult;
import org.parboiled.support.ToStringFormatter;
import org.parboiled.support.Var;
import org.parboiled.trees.GraphNode;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.parboiled.errors.ErrorUtils.printParseErrors;
import static org.parboiled.support.ParseTreeUtils.printNodeTree;
import static org.parboiled.trees.GraphUtils.printTree;

/**
 * Compiles a match pattern into a form, {@link Script}, that can be evaluated ({@link Eval}.
 *
 * @author Arvid Halma
 * @version 23-11-2015
 */
@SuppressWarnings("WeakerAccess")
@BuildParseTree
public class Compiler extends BaseParser<OperatorNode> {
    static final Pattern INDENT_PATTERN = Pattern.compile("^[ \t]*");

    // arrow symbol
    static final Pattern LABEL_PATTERN = Pattern.compile("(?U)^[ \t]*([{](.*?)[}])? *@(\\w+) *([{](.*?)[}])? *<- *(.*)", Pattern.DOTALL);
    static final Pattern REPLY_PATTERN = Pattern.compile("^[ \t]*([{](.*?)[}])? *(.*?) *([{](.*?)[}])? *-> *(.*)", Pattern.DOTALL);
    static final Pattern PREFIX_BLOCK_PATTERN = Pattern.compile("^[ \t]*(.*?) *\\[ *$");

    private final static CharRangeMatcher LETTER_MATCHER = new CharRangeMatcher('a', 'z') {
        @Override
        public boolean match(MatcherContext context) {
            final char c = context.getCurrentChar();
            if (c == '-' || c == '\'' || Character.isAlphabetic(c) || Character.isDigit(c)) {
                context.advanceIndex(1);
                context.createNode();
                return true;
            } else {
                return false;
            }
        }
    };


    static final ObjectMapper OBJECT_MAPPER;
    static final TypeReference<LinkedHashMap<String, Object>> PROPERTY_MAP_TYPE = new TypeReference<LinkedHashMap<String, Object>>() {};

    static {
        OBJECT_MAPPER = new ObjectMapper(new YAMLFactory());
        OBJECT_MAPPER.registerModule(new JodaModule());
    }

    /**
     * Compile source code to an evaluable/executable form.
     * @param src the source containing the labels
     * @return an internal representation of the labels
     */
    public static Script compile(String src) {
        return compile(src, new Nlp(null, ImmutableMap.of("en", new LanguageProcessingConfig())));
    }

    /**
     * Compile source code to an evaluable/executable form.
     * @param src the source containing the labels
     * @param nlp natural languange processing resource
     * @return an internal representation of the labels
     */
    public static Script compile(String src, Nlp nlp) {
        if(src == null){
            throw new MultiScriptException("The rule set source cannot be NULL.");
        }

        // Pre-process ...

        // 1. Split config and rule sections
        String[] lines = src.split("\\R");
        StringBuilder configBuilder = new StringBuilder();
        StringBuilder rulesBuilder = new StringBuilder();
        String config, rules;
        int rulesLineOffset = 1;
        boolean appendToRules = false;
        int yamlMarkerCount = 0;
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            if("---".equals(line)){
                yamlMarkerCount++;
                if(yamlMarkerCount >= 2) {
                    appendToRules = true;
                    rulesLineOffset = i + 1;
                    continue;
                }
            }
            if(appendToRules){
                rulesBuilder.append(line).append("\n");
            } else {
                configBuilder.append(line).append("\n");
            }
        }

        ScriptConfig scriptConfig = new ScriptConfig();
        if(!appendToRules) {
            // there was no config section...
            rules = configBuilder.toString();
        } else {
            // get yaml config
            config = configBuilder.toString();

            try{
                scriptConfig = OBJECT_MAPPER.readValue(config, ScriptConfig.class);
            } catch (IOException e){
                String message = e.getMessage();
                if(!message.startsWith("No content to map")){
                    // only when config section is non-empty
                    throw new MultiScriptException(e.getMessage());
                }
            }
            rules = rulesBuilder.toString();
        }

        // 2. remove comments
        rules = StringUtil.removeLineComments(rules, "#");

        // 3. create (multi-line) statements from single lines and compile
        String[] ruleLines = rules.split("\n");

        StringNormalizer normalizer = nlp.getNormalizer(scriptConfig);
        Boolean optimizeRuleLogic = scriptConfig.getOptimizeRuleLogic();
        List<LabelRule> ruleList = new ArrayList<>();
        List<ReplyRule> replyList = new ArrayList<>();


        int lineNr;
        int statementLineNr = 1;
        int tmpStatementLineNr = 1;
        try {
            String statement = "", tmpStatement = "", lastIndent = "";
            boolean allowContinueStatement = false;
            boolean closePrefixBlock = false;
            List<String> prefixes = new ArrayList<>();


            for (int i = 0; i <= ruleLines.length; i++) {
                lineNr = rulesLineOffset + i - 1;
                boolean processStatement = false; // buffer input mode

                if(i == ruleLines.length){
                    // process last pending statement
                    statement = tmpStatement;
                    statementLineNr = tmpStatementLineNr;
                    processStatement = true;
                } else {
                    String line = ruleLines[i];
                    final String trim = line.trim();

                    // Prefix block cases
                    final Matcher prefixBlockMatcher = PREFIX_BLOCK_PATTERN.matcher(line);
                    if(prefixBlockMatcher.find()){
                        prefixes.add(prefixes.size(), prefixBlockMatcher.group(1));
                        allowContinueStatement = false;
                        continue;
                    }

                    if (trim.equals("]")) {
                        if(prefixes.isEmpty()) {
                            statementLineNr = 1;
                            throw new ScriptException("No prefix block to be closed.", lineNr);
                        }
                        closePrefixBlock = true;
                        allowContinueStatement = false;
                    } else {
                        closePrefixBlock = false;
                    }


                    Matcher indentMatcher = INDENT_PATTERN.matcher(line);
                    String indent = "";
                    if(indentMatcher.find()) {
                        indent = indentMatcher.group();
                    }

                    if (allowContinueStatement && indent.length() > lastIndent.length()) {
                        // buffer input: line continuation
                        if(REPLY_PATTERN.matcher(line).find()){
                            statementLineNr = 1;
                            throw new ScriptException("Improper indentation: this line defines a reply (... -> ...) and is not a continuation of the last one.\n Make sure that in the case 'prefix blocks' are intended, square brackets are use: [ ... ]", lineNr);
                        }
                        tmpStatement += line + "\n";
                    } else {
                        // start of new rule
                        allowContinueStatement = true;
                        statementLineNr = tmpStatementLineNr;
                        tmpStatementLineNr = lineNr;
                        statement = tmpStatement;
                        if (!trim.isEmpty() & !closePrefixBlock) {
                            tmpStatement = line + "\n";
                        } else {
                            tmpStatement = "";
                            allowContinueStatement = false;
                        }
                        processStatement = i >= 1; // not first time, unless it is the only one
                        lastIndent = indent;
                    }
                }

                if(!processStatement){
                    continue; // keep collecting multi line inputs
                }

                if(statement.trim().isEmpty()){
                    if(closePrefixBlock){
                        prefixes.clear();
                    }
                    continue; // ignore empty lines
                }

                statement = String.join(" ", prefixes) + " " + statement;
                Matcher replyMatcher = REPLY_PATTERN.matcher(statement);
                if (replyMatcher.find()) {
                    // reply rule

                    // match groups:
                    //       ^[ \t]*([{](.*?)[}])? *(.*?) *([{](.*?)[}])? *-> *(.*)
                    // gr =           1  2            3      4   5              6

                    String head = replyMatcher.group(3).trim();
                    LinkedHashMap<String,Object> options = mergeYamlOptions(replyMatcher.group(1), replyMatcher.group(4));

                    String[] replies = StringUtil.unquote(replyMatcher.group(6).trim()).split("['\"]?\\s*\\|\\s*['\"]?");
                    List<List<String>> dnf = Arrays.stream(replies)
                            .map(ands -> Arrays.stream(ands.split("['\"]?\\s*&\\s*['\"]?")).map(String::trim).collect(Collectors.toList()))
                            .collect(Collectors.toList());

                    ReplyRule reply;
                    if(head.trim().equals("()")){
                        // default reply: no constraints
                        reply = new ReplyRule(new LabelRule("reply", new Literal[0][0], lineNr), dnf, lineNr);
                    } else {
                        Literal[][] matchRule = compileBody(head, optimizeRuleLogic, normalizer);
                        reply = new ReplyRule(new LabelRule("reply", matchRule, lineNr), dnf, lineNr);
                    }
                    if(options != null)
                        reply.setProps(options);

                    replyList.add(reply);

                }  else {
                    int nPrefixes = prefixes.size();
                    if(nPrefixes == 1){
                        statementLineNr = 1;

                        final String prefix = prefixes.get(0);
                        if(prefix.startsWith("{") && prefix.endsWith("}")){
                            // it can be a options prefix...
                            statement = prefix + statement;
                        } else {
                            throw new ScriptException("Prefix block can only contain options {...} for label rules.\nApplied prefixes: " + prefix, lineNr);
                        }

                    } else if(nPrefixes > 1){
                        throw new ScriptException("Don't nest prefix blocks for label rules.", lineNr);
                    }

                    // match rule
                    LabelRule rule = compileRule(statement, optimizeRuleLogic, normalizer);
                    rule.setLine(statementLineNr);
                    ruleList.add(rule);
                }

                if(closePrefixBlock){
                    prefixes.clear();
                }
            }
        } catch (ScriptException e) {
            e.setLine(statementLineNr + e.getLine()); // add line info
            throw new MultiScriptException(e);
        }

        // rewrite reply conditions containing literal searches (non-label refs)
        // int anonymousLabelId = 0;
        for (ReplyRule reply : replyList) {
            boolean doRewrite = false;
            expression: for (Literal[] disjunction : reply.rule.expression) {
                for (Literal disj : disjunction) {
                    if(disj.meta == '='){
                        final Token leftStartToken = disj.tokens[0];
                        final Token rightStartToken = disj.tokens[disj.marker + 1];
                        if((leftStartToken.isNormalizedMatch() && StringUtil.startsWithUpperCase(leftStartToken.getWord()))
                                || (rightStartToken.isNormalizedMatch() && StringUtil.startsWithUpperCase(rightStartToken.getWord())))
                        {
                            // NER class
                            doRewrite = true;
                        } else {
                            continue;
                        }
                    }
                    if(disj.meta != '@'){
                        // a text literal
                        doRewrite = true;
                    }

                    if(doRewrite)
                        break expression;

                }
            }
            if(doRewrite){
                // 1. create new rule
                String anoRuleHead = "label" + (reply.line);
                LabelRule anoRule = new LabelRule(anoRuleHead,
                        reply.rule.expression, reply.line);

                // find out about configured "within" value
//                Object within = scriptConfig.getRuleProperties().reply.getOrDefault("within", "all");
//                if("last".equals(within)){
//                    within = 1; // "last" is not a valid property of a label rule. 1 is quite equivalent.
//                }
//
//                anoRule.props.put("within", within);
                ruleList.add(anoRule);
                // 2. use new anoRule
                reply.rule.expression = new Literal[][]{{Literal.createReference(anoRuleHead)}};
            }
        }

        Script script = new Script(scriptConfig, ruleList, replyList);

        if(scriptConfig.getPostProcess().contains("grapheme")){
            script = new GraphemeSplitPostProcessor().apply(script);
        }

        // add warnings
        Validators.validate(script, nlp);

        return script;
    }

    private static LinkedHashMap<String, Object> mergeYamlOptions(String leftYaml, String rightYaml){
        LinkedHashMap<String,Object> optionsL = parseYamlOptions(leftYaml);
        LinkedHashMap<String,Object> optionsR = parseYamlOptions(rightYaml);

        // merge left and right options
        LinkedHashMap<String,Object> options;
        if(optionsL == null){
            options = optionsR;
        } else {
            if (optionsR == null) {
                options = optionsL;
            } else {
                // both option are defined...
                // possibly overwrite left properties with right properties
                optionsL.putAll(optionsR);
                options = optionsL;
            }
        }
        return options;
    }

    private static LinkedHashMap<String, Object> parseYamlOptions(String optionsText){
        if(optionsText != null){
            try {
                return OBJECT_MAPPER.readValue(optionsText, PROPERTY_MAP_TYPE);
            } catch (IOException e) {
                if(optionsText.matches(".*?:\\S.*")){
                    throw new ScriptException("Use a space after a colon when defining options: e.g. {key: [SPACE] value}");
                } else {
                    throw new ScriptException("Invalid property syntax.\nUse something like: {key1: value1, key2, key3: value3}");
                }
            }
        }
        return null;
    }

    /**
     * Compile a single label expression into an evaluable/executable form.
     * @param rule the label of the rule (applied tag)
     * @param simplify whether ti optimize the expression
     * @param normalizer how words are canonicalized
     * @return an internal representation of the label rule
     */
    public static LabelRule compileRule(String rule, boolean simplify, StringNormalizer normalizer){
        Matcher ruleMatcher = LABEL_PATTERN.matcher(rule);
        if (ruleMatcher.find()) {
            // rule: (?U)^[ 	]*([{](.*?)[}])? *@(\w+) *([{](.*?)[}])? *<- *(.*)
            // gr =                 1   2            3      4   5              6

            String head = ruleMatcher.group(3).trim();
            String body = ruleMatcher.group(6);
            LinkedHashMap<String,Object> options = mergeYamlOptions(ruleMatcher.group(1), ruleMatcher.group(4));

            LabelRule labelRule = new LabelRule(head, compileBody(body, simplify, normalizer));
            if(options != null)
                labelRule.setProps(options);

            return labelRule;

        } else {
            if(Pattern.compile("(?U)^[ \t\f]+([{](.*?)[}])? *@\\w+.*?<-.*", Pattern.DOTALL).matcher(rule).matches()){
                throw new ScriptException("Don't indent when defining a rule.\nIndentation is used to continue a rule on the next line(s).");
            }
            throw new ScriptException("The line does not assign a rule (@label <- ...)");
        }
    }

    /**
     * Compile a single expression into an evaluable/executable form.
     * @param expression the logical expression/body of the rule
     * @param simplify whether ti optimize the expression
     * @param normalizer how words are canonicalized
     * @return an internal representation of the label rule
     */
    public static Literal[][] compileBody(String expression, boolean simplify, StringNormalizer normalizer){
        // normalize emoticons
        expression = EmojiAlias.shortCodify(expression);

        Compiler parser = Parboiled.createParser(Compiler.class);
        ParsingResult<?> result;
        try {
            result = new RecoveringParseRunner(parser.ExpressionLine()).run(expression);
        } catch (Exception e) {
            // The parser may trow IllegalState exceptions (parboiled lib issue?)
            throw new ScriptException("The line contains an unknown error", 1);
        }
        if (result.hasErrors()) {
            String message = printParseErrors(result);
            Matcher lineMatcher = Pattern.compile("\\(line (\\d+)").matcher(message);
            int line = 1;
            if(lineMatcher.find()){
                line = Integer.parseInt(lineMatcher.group(1));
            }

            final int nErrors = result.parseErrors.size();
            if(nErrors > 2){
                message = "There are " + nErrors + " errors in this expression.\nShould this possibly be a reply '->' instead of a label '<-' rule?";
            } else {
                message = message.replaceAll(", expected.*", "..."); // remove internal debug info
            }
            throw new ScriptException(message, line);
        }

        Object ast = result.parseTreeRoot.getValue();
        OperatorNode cnfTree = CNFTransform.toCNFTree((OperatorNode) ast);
        ArrayList<ArrayList<Literal>> cnfList = CNFTransform.cnfTreeToCNFList(cnfTree);
        if(simplify){
            cnfList = CNFTransform.simplify(cnfList);
        }

        if(normalizer != null){
            cnfList.forEach(disj -> disj.forEach(lit -> normalizer.normalizeTokens(lit.getTokens())));
        }
        return CNFTransform.cnfListToCNFArray(cnfList);
    }

    org.parboiled.Rule ExpressionLine() {
        return Sequence(ZeroOrMore(AnyOf(" \n\t\f").label("Whitespace")), Expression(), ZeroOrMore(AnyOf(" \n\t\f").label("Whitespace")), EOI);
    }

    org.parboiled.Rule Expression() {
        Var<String> op = new Var<>();
        return Sequence(
                And(),
                ZeroOrMore(
                        // we use a FirstOf(String, String) instead of a AnyOf(String) so we can use the
                        // fromStringLiteral transformation (see below), which automatically consumes trailing whitespace
                        FirstOf("| ", "OR "), op.set(matchOrDefault("|")),
                        And(),
                        push(new OperatorNode("|", pop(1), pop()))
                )
        );
    }

    org.parboiled.Rule And() {
        Var<String> op = new Var<>();
        return Sequence(
                Atom(),
                ZeroOrMore(
                        FirstOf("& ", "AND "), op.set(matchOrDefault(("&"))),
                        Atom(),
                        push(new OperatorNode("&", pop(1), pop()))
                )
        );
    }

    org.parboiled.Rule Concat() {
        Var<String> op = new Var<>();
        return Sequence(
                FirstOf(SemicolonQuotedString(), SlashQuotedString(), DoubleQuotedString(), SingleQuotedString(), AnyOne(), AnyOneOrMore(), AnyZeroOrMore(), UnquotedString()),
                ZeroOrMore(
                        "_ ", op.set(matchOrDefault("_")),
                        FirstOf(SemicolonQuotedString(), SlashQuotedString(), DoubleQuotedString(), SingleQuotedString(), AnyOne(), AnyOneOrMore(), AnyZeroOrMore(), UnquotedString()),
                        push(new OperatorNode("_", pop(1), pop()))
                )
        );
    }

    org.parboiled.Rule Compare() {
        Var<String> op = new Var<>();
        return Sequence(
                CompareOperand(),
                FirstOf( "<=", ">=", "==", "!=", "<", ">"), op.set(matchOrDefault((""))), WhiteSpace(),
                CompareOperand(),
                push(new OperatorNode("=", pop(1), pop()).setValue(op.get()))
        );
    }

    org.parboiled.Rule CompareOperand() {
        return FirstOf(PropertyReference(), Number(), PropertyClass(), DoubleQuotedString(), SingleQuotedString(), UnquotedString());
    }

    org.parboiled.Rule Atom() {
        return FirstOf(Not(), Compare(), Reference(), Parens(), Concat());
    }


    org.parboiled.Rule Not() {
        Var<String> op = new Var<>();
        return Sequence(
                FirstOf("- ", "NOT "), op.set(matchOrDefault("-")),
                Atom(),
                push(new OperatorNode("-", pop(), null))
        );
    }

    org.parboiled.Rule Reference() {
        Var<String> op = new Var<>();
        return Sequence(
                FirstOf("@", "REFERENCE "), op.set(matchOrDefault("@")),
                UnquotedString(),
                push(new OperatorNode("@", pop(), null))
        );
    }

    org.parboiled.Rule PropertyReference() {
        Var<String> op = new Var<>();
        return Sequence(
                FirstOf("@", "REFERENCE "), op.set(matchOrDefault("@")),
                Sequence(LetterSequence(), ZeroOrMore(PERIOD, LetterSequence())),
                push(new OperatorNode("@", new OperatorNode("A", new OperatorNode("@"+matchOrDefault("")), null), null)),
                WhiteSpace()
        );
    }
    org.parboiled.Rule PropertyClass() {
        return Sequence(
                Sequence(LetterSequenceUppercase(), OneOrMore(PERIOD, LetterSequence())),
                push(new OperatorNode("A", new OperatorNode(matchOrDefault(null)), null)),
                WhiteSpace()
        );
    }

    org.parboiled.Rule Parens() {
        return Sequence("( ", Expression(), ") ");
    }

    org.parboiled.Rule AnyOne() {
        return Sequence("?", push(new OperatorNode("?")), WhiteSpace());
    }

    org.parboiled.Rule AnyOneOrMore() {
        return Sequence("+", push(new OperatorNode("+")), WhiteSpace());
    }

    org.parboiled.Rule AnyZeroOrMore() {
        return Sequence("*", push(new OperatorNode("*")), WhiteSpace());
    }

    org.parboiled.Rule Number() {
        return Sequence(
                // we use another Sequence in the "Number" Sequence so we can easily access the input text matched
                // by the three enclosed labels with "match()" or "matchOrDefault()"
                Sequence(
                        Optional("-"),
                        OneOrMore(Digit()),
                        Optional(".", OneOrMore(Digit()))
                ),

                // the matchOrDefault() call returns the matched input text of the immediately preceding rule
                // or a default string (in this case if it is run during error recovery (resynchronization))
                push(new OperatorNode("1", new OperatorNode(matchOrDefault("0")), null)),
                WhiteSpace()
        );
    }

    org.parboiled.Rule Digit() {
        return CharRange('0', '9');
    }

    org.parboiled.Rule WhiteSpace() {
        return
                ZeroOrMore(
                        FirstOf(
                                AnyOf(" \t\f").label("Whitespace"),
                                Sequence(Newline(), OneOrMore(AnyOf(" \t\f"))).label("Whitespace")
                        )
                );
    }

    org.parboiled.Rule Newline() {
        return FirstOf('\n', Sequence('\r', Optional('\n')));
    }

    org.parboiled.Rule QuotedStr(String quote, String operator) {
        return  Sequence( quote,
                Sequence(
                        ZeroOrMore(
                                FirstOf(
                                        String("\\"+quote),
                                        Sequence(TestNot(AnyOf(quote+"\r\n\\")), ANY)
                                )
                        ).suppressSubnodes(), push(new OperatorNode(matchOrDefault("")))
                ),
                quote, WhiteSpace(), push(new OperatorNode(operator, pop(), null)
                ));
    }

    org.parboiled.Rule SingleQuotedString() {
        return QuotedStr("'", "A");
    }

    org.parboiled.Rule DoubleQuotedString() {
        return QuotedStr("\"", "E");
    }

    org.parboiled.Rule SemicolonQuotedString() {
        return QuotedStr(":", ":");
    }

    org.parboiled.Rule SlashQuotedString() {
        return QuotedStr("/", "/");
    }

    org.parboiled.Rule UnquotedString() {
        return Sequence(
                LetterSequence(),
                push(new OperatorNode("A", new OperatorNode(matchOrDefault("")), null)), WhiteSpace());
    }

    /**
     * A string containing unicode letters or single quote or dash.
     * @return word-like rule
     */
    org.parboiled.Rule LetterSequence() {
        return OneOrMore(LETTER_MATCHER);
    }

    org.parboiled.Rule LetterSequenceUppercase() {
        return OneOrMore(FirstOf(
                CharRange("Basic Latin Uppercase"),
                Ch('-' ), // e.g. lower-case
                CharRange("Digits")
        ));
    }

    org.parboiled.Rule CharRange(String name){
        char[] chars = CharRangeUtil.RANGES.get(name);
        return CharRange(chars[0], chars[1]);
    }

    @SuppressWarnings("unused")
    org.parboiled.Rule SimplePredicate() {
        return Sequence(
                UnquotedString(),
                Optional(LPAR, Optional(
                        UnquotedString(),
                        ZeroOrMore(COMMA, UnquotedString())
                ), RPAR)
        );
    }

    org.parboiled.Rule Terminal(String string) {
        return Sequence(string, WhiteSpace()).label('\'' + string + '\'');
    }

    final org.parboiled.Rule LPAR = Terminal("(");
    final org.parboiled.Rule RPAR = Terminal(")");
    final org.parboiled.Rule COMMA = Terminal(",");
    final org.parboiled.Rule PERIOD = Terminal(".");

    // we redefine the rule creation for string literals to automatically match trailing whitespace if the string
    // literal ends with a space character, this way we don't have to insert extra whitespace() labels after each
    // character or string literal

    @Override
    protected org.parboiled.Rule fromStringLiteral(String string) {
        return string.endsWith(" ") ?
                Sequence(String(string.substring(0, string.length() - 1)), WhiteSpace()) :
                String(string);
    }

    /**
     * Generalize all word instances in a script, by adding variants (e.g. hypernyms/synonyms)
     * @param script original script
     * @param generalizer the way of deriving relevant words from a single word
     * @return a new script that matches more often
     */
    public static Script generalize(Script script, Generalizer generalizer){
        Map<String, List<LabelRule>> labels = script.labels;
        List<LabelRule> extRules = new ArrayList<>();
        for (String label : labels.keySet()) {
            for (LabelRule rule : labels.get(label)) {
                Literal[][] expression = rule.expression;
                Literal[][] extExpression = new Literal[expression.length][];
                for (int i = 0, expressionLength = expression.length; i < expressionLength; i++) {
                    Literal[] disjunction = expression[i];
                    List<Literal> extDisjunction = new ArrayList<>();
                    for (Literal literal : disjunction) {
                        extDisjunction.add(literal);
                        if (literal.meta != 'a') {
                            continue;
                        }
                        Collection<Token[]> extension = generalizer.extend(literal.tokens);
                        for (Token[] tokens : extension) {
                            extDisjunction.add(new Literal(tokens, literal.negated, literal.meta));
                        }
                    }
                    extExpression[i] = extDisjunction.toArray(new Literal[0]);

                }
                extRules.add(new LabelRule(rule.head, extExpression, rule.line));
            }
        }
        return new Script(script.config, extRules, script.replies);

    }

    /**
     * CLI Interactive mode for expressions
     * @param args ignored
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        Compiler parser = Parboiled.createParser(Compiler.class);

        while (true) {
            System.out.print("Enter a match expression (single RETURN to exit)!\n");
            String input = new Scanner(System.in).nextLine();
            if (StringUtils.isEmpty(input)) break;

            ParsingResult<?> result = new RecoveringParseRunner(parser.ExpressionLine()).run(input);

            if (result.hasErrors()) {
                System.out.println("\nParse Errors:\n" + printParseErrors(result));
            } else {
                Object value = result.parseTreeRoot.getValue();
                if (value != null) {
                    String str = value.toString();
                    int ix = str.indexOf('|');
                    if (ix >= 0) str = str.substring(ix + 2); // extract value part of AST node toString()
                    System.out.println(input + " = " + str + '\n');
                }
                if (value instanceof GraphNode) {
                    System.out.println("\nAbstract Syntax Tree:\n" +
                            printTree((GraphNode) value, new ToStringFormatter(null)) + '\n');

                    OperatorNode cnfTree = CNFTransform.toCNFTree((OperatorNode) value);
                    System.out.println("CNF Abstract Syntax Tree:\n" +
                            printTree(cnfTree, new ToStringFormatter(null)) + '\n');

                    ArrayList<ArrayList<Literal>> cnfList = CNFTransform.cnfTreeToCNFList(cnfTree);
                    System.out.println("CNF[][]        = " + cnfList);
                    cnfList = CNFTransform.simplify(cnfList);
                    System.out.println("CNF[][] simple = " + cnfList);

                    Literal[][] cnfArray = CNFTransform.cnfListToCNFArray(cnfList);
                    String luceneQuery = new LuceneConverter().convert(cnfArray);
                    System.out.println("Lucene = " + luceneQuery);
                    String postgreSQL = new PostgresConverter().convert(cnfArray);
                    System.out.println("PostgreSQL = " + postgreSQL);

                } else {
                    System.out.println("\nParse Tree:\n" + printNodeTree(result) + "\n");
                }
            }
        }
    }

}