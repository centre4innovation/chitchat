package org.c4i.nlp.match;

import java.util.ArrayList;
import java.util.List;

/**
 * Script checker.
 * @author Arvid Halma
 */
public interface Validator {

    List<ScriptException> check(Script s);

    default Validator compose(final Validator v){
        final Validator base = this;
        return s -> {
            List<ScriptException> exceptions = base.check(s);
            exceptions.addAll(v.check(s));
            return exceptions;
        };
    }

    static List<ScriptException> applyAll(Script s,  Validator ... vs){
        List<ScriptException> exceptions = new ArrayList<>();
        for (Validator v : vs) {
            exceptions.addAll(v.check(s));
        }
        return exceptions;
    }


}
