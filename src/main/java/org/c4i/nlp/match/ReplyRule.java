package org.c4i.nlp.match;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.c4i.nlp.Nlp;
import org.c4i.util.time.DateTimeUtil;
import org.c4i.util.RegexUtil;
import org.c4i.util.ReverseIterator;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Possible replies for when a rule matches.
 * @author Arvid Halma
 * @version 6-7-17
 */
public class ReplyRule extends Rule{
    LabelRule rule;
    List<List<String>> replies;

    protected static final Map<String, Set<String>> VALID_PROPS = new HashMap<>();
    static {
        VALID_PROPS.put("continue", null);
        VALID_PROPS.put("repeat", null);
        VALID_PROPS.put("speak", null);
        VALID_PROPS.put("within", ImmutableSet.of("last", "all", "NUMBER"));
        VALID_PROPS.put("addText", ImmutableSet.of("STRING"));
        VALID_PROPS.put("addLabel", ImmutableSet.of("STRING", "LIST"));
        VALID_PROPS.put("removeLabel", ImmutableSet.of("STRING", "LIST"));
        VALID_PROPS.put("reflect", null);
    }

    public static final Pattern USER_VAR = Pattern.compile("\\$(\\w+)\\b");
    public static final Pattern BUTTON_PROP = Pattern.compile("\\bBUTTON *\\((.+?),.*?\\)");
    public static final Pattern CHECKBOX_PROP = Pattern.compile("\\bCHECKBOX *\\((.+?),.*?\\)");
    public static final Pattern IMAGE_PROP = Pattern.compile("\\bIMAGE *\\(.*?\\)");
    public static final Pattern SPEAK_PROP = Pattern.compile("\\bSPEAK *\\(.*?\\)");

    public ReplyRule(LabelRule rule, List<List<String>> replies) {
        this.rule = rule;
        this.replies = replies;
    }

    public ReplyRule(LabelRule rule, List<List<String>> replies, int line) {
        this.rule = rule;
        this.replies = replies;
        this.line = line;
    }

    @Override
    public Map<String, Set<String>> validProps() {
        return VALID_PROPS;
    }

    public LabelRule getRule() {
        return rule;
    }

    public ReplyRule setRule(LabelRule rule) {
        this.rule = rule;
        return this;
    }

    public List<List<String>> getReplies() {
        return replies;
    }

    public ReplyRule setReplies(List<List<String>> replies) {
        this.replies = replies;
        return this;
    }

    public List<String> randomReply(){
        return replies.get((int)Math.floor(replies.size() * Math.random()));
    }

    public List<String> randomReply(Script script, String text, List<Range> ranges, Nlp nlp) {
        List<String> result = new ArrayList<>();

        final String lang = script.config.getLanguages().get(0);
        final Locale locale = Locale.forLanguageTag(lang);

        List<String> templates = randomReply();
        for (String template : templates) {
            Matcher labelVarMatcher = LABEL_VAR.matcher(template);
            StringBuffer sb = new StringBuffer();
            nextVar:
            while (labelVarMatcher.find()) {
                boolean normalize = !labelVarMatcher.group(1).equals("\"");
                String[] refParts = labelVarMatcher.group().substring(1).split("\\.");

                String label = refParts[0];
                String sub = refParts.length > 1 ? refParts[1] : null;

                if("count".equals(sub)) {
                    String subsub = refParts.length > 2 ? refParts[2] : null;
                    // optionally, filter by prop existence (duck typing ranges)
                    int count = (int) rangeStreamFor(ranges, label)
                            .filter(r -> subsub == null || r.props.containsKey(subsub)).count();
                    labelVarMatcher.appendReplacement(sb, Matcher.quoteReplacement(""+count));
                    continue;
                } else if("date".equals(sub)) {
                    try {
                        String subsub = refParts[2];
                        String t = Range.findWithProp(ranges, subsub).props.get(subsub);
                        labelVarMatcher.appendReplacement(sb, Matcher.quoteReplacement(DateTimeUtil.fullDateString(locale, t)));
                    } catch (Exception e) {
                        labelVarMatcher.appendReplacement(sb, Matcher.quoteReplacement(labelVarMatcher.group()));
                    }
                    continue;
                } else if("price".equals(sub)) {
                    try {
                        String subsub = refParts[2];
                        String p = Range.findWithProp(ranges, subsub).props.get(subsub);
                        labelVarMatcher.appendReplacement(sb, String.format(locale,"%.2f", Double.parseDouble(p)));
                    } catch (Exception e) {
                        labelVarMatcher.appendReplacement(sb, Matcher.quoteReplacement(labelVarMatcher.group()));
                    }
                    continue;
                } else if("table".equals(sub) || "tablenh".equals(sub)) { // nh = no header
                    List<Range> rows = rangeStreamFor(ranges, label).collect(Collectors.toList());
                    if(rows.isEmpty()){
                        labelVarMatcher.appendReplacement(sb, Matcher.quoteReplacement(
                                "<table><tr><td>...</td></tr></table>"
                        ));
                    } else {
                        // determine columns to show
                        List<String> cols;
                        if(refParts.length > 2){
                            cols = Arrays.asList(refParts).subList(2, refParts.length);
                        } else {
                            cols = new ArrayList<>(rows.get(0).props.keySet());
                        }

                        // render
                        StringBuilder table = new StringBuilder("<table>");
                        // header
                        if("table".equals(sub)) {
                            // not the "nh"/no header variant
                            table.append("<tr>");
                            for (String k : cols) {
                                table.append("<th style=\"text-align: left\">").append(k).append("</th>");
                            }
                            table.append("</tr>");
                        }
                        // body
                        for (Range row : rows) {
                            table.append("<tr>");
                            for (String k : cols) {
                                final String v = row.props.getOrDefault(k, "");
                                if(RegexUtil.NUMBER.matcher(v).matches()){
                                    if(k.matches("(?i).*(precio|price|eur|usd).*")){
                                        // price-like, use 2 decimals
                                        table.append("<td style=\"text-align: right\">").append(String.format(locale,"%.2f", Double.parseDouble(v))).append("</td>");
                                    } else {
                                        table.append("<td style=\"text-align: right\">").append(v).append("</td>");
                                    }
                                } else {
                                    table.append("<td>").append(v).append("</td>");
                                }
                            }
                            table.append("</tr>");
                        }
                        table.append("</table>");
                        labelVarMatcher.appendReplacement(sb, Matcher.quoteReplacement(table.toString()));
                    }
                    continue;
                }

                for (Range range : new ReverseIterator<>(rangeStreamFor(ranges, label).collect(Collectors.toList()))) { // reverse: latest value is considered more relevant
                        // replace variable
                        String replacement;
                        if (sub == null || "text".equals(sub)) {
                            replacement = text.substring(range.charStart, range.charEnd);
                        } else {
                            if(!range.props.containsKey(sub)){
                                // this range doesn't contain "sub" prop. Maybe the next range does?
                                continue;
                            }
                            replacement = range.props.get(sub);
                            if(sub.matches("(?i).*(precio|price|eur|usd).*") && RegexUtil.NUMBER.matcher(replacement).matches()){
                                replacement = String.format("%.2f", Double.parseDouble(replacement));
                            }
                        }

                        // possibly reflect the reply as well
                        if(isProp("reflect", script.getConfig().getRuleProperties().getReply())){
                            replacement = nlp.getSubstitution(script.getConfig()).apply(replacement);
                        }

                        labelVarMatcher.appendReplacement(sb, replacement == null ? "?" : Matcher.quoteReplacement(normalize ? replacement.toLowerCase() : replacement));
                        continue nextVar;
                }
            }
            labelVarMatcher.appendTail(sb);

            String reply = nlp.applyReplyVariables(sb.toString());

            // additional reply plugins
            Map<String, List<ReplyPlugin>> replyPlugins = nlp.getReplyPlugins();
            for (String language : script.config.getLanguages()) {
                if(replyPlugins.containsKey(language)){
                    for (ReplyPlugin replyPlugin : replyPlugins.get(lang)) {
                        reply = replyPlugin.updateReply(reply, text, script, ranges, nlp);
                    }
                }
            }


            // Speak
            if(isProp("speak", script.getConfig().getRuleProperties().getReply())
                    && !SPEAK_PROP.matcher(reply).find()) // override with local speak term
            {
                // Generate SPEAK() tags
                // Filter out other tags first...
                String speak;
                speak = BUTTON_PROP.matcher(reply).replaceAll(", $1 ");
                speak = CHECKBOX_PROP.matcher(speak).replaceAll(", $1 ");
                speak = IMAGE_PROP.matcher(speak).replaceAll(" ");
                reply += " SPEAK("+speak+")";
            }

            result.add(reply);
        }


        return result;
    }

    private Stream<Range> rangeStreamFor(List<Range> ranges, String label) {
        return ranges.stream().filter(range -> {
            return range.label.equals(label) // requested label matches
                    || (label.startsWith("label") // anonymous label
                    && !isFallback() // non-empty expression
                    && range.label.equals(this.rule.expression[0][0].tokens[0].getWord())); // this specific anonymous label
        });
    }

    public boolean isFallback(){
        return rule.expression.length == 0; // no constraints
    }

    public List<String> getAddLabels() {
        return getAddLabelsFrom(props);
    }

    public List<String> getAddLabels(Map<String, Object> backup) {
        List<String> result = new ArrayList<>(0);
        // check own properties
        result.addAll(getAddLabelsFrom(props));
        // add the ones from the backup props
        result.addAll(getAddLabelsFrom(backup));
        return result;
    }

    public List<String> getAddLabelsFrom(Map<String, Object> map) {
        Object addLabel = map.get("addLabel");
        if (addLabel != null) {
            if (addLabel instanceof String) {
                return ImmutableList.of((String) addLabel);
            } else if (addLabel instanceof List) {
                return (List) addLabel;
            }
        }
        return ImmutableList.of();
    }


    @Override
    public String toString() {
        return CNFTransform.toString(rule.expression)  + " "
                + propertyString() +"-> "
                + replies.stream()
                    .map(ands -> String.join(" & ", ands))
                    .collect(Collectors.joining(" | "));
    }
}
