package org.c4i.nlp.match;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A collection of labels, that defines labels that are triggered under the given conditions.
 * Also replies can be defined to respond to the detected labels in a text.
 *
 * @author Arvid Halma
 * @version 13-4-2017 - 20:52
 */
public class Script {
    ScriptConfig config;
    final Map<String, List<LabelRule>> labels;
    final List<ReplyRule> replies;
    List<ScriptException> warnings;

    public static final Pattern REPLY_TEMPLATE_REF = Pattern.compile("(?U)@(\\w+?)\\b.");


    public Script() {
        config = new ScriptConfig();
        labels = new LinkedHashMap<>();
        replies = new ArrayList<>();
        warnings = new ArrayList<>(0);
    }

    public Script(Script script) {
        this(script.config, script.labels.values().stream().collect(ArrayList::new, List::addAll, List::addAll), script.replies);
    }

    public Script(ScriptConfig config, Collection<LabelRule> labels, List<ReplyRule> replies) {
        this();
        this.config = config == null ? new ScriptConfig() : config;

        for (LabelRule rule : labels) {
            if(!this.labels.containsKey(rule.head)){
                this.labels.put(rule.head, new ArrayList<>(1));
            }
            this.labels.get(rule.head).add(rule);
        }
        this.replies.addAll(replies);
    }

    public ScriptConfig getConfig() {
        return config;
    }

    public Map<String, List<LabelRule>> getLabels() {
        return labels;
    }

    public List<ReplyRule> getReplies() {
        return replies;
    }

    public List<ScriptException> getWarnings() {
        return warnings;
    }


    public Map<String, List<Dependency>> dependencyGraph(){
        Map<String, List<Dependency>> graph = new HashMap<>();
        labels.values().stream().flatMap(Collection::stream).forEach(rule -> {
            Arrays.stream(rule.expression).flatMap(Arrays::stream).forEach(lit -> {
                if(lit.meta == '@') {
                    String ref = lit.tokens[0].getWord();
                    List<Dependency> edgeList = graph.getOrDefault(ref, new ArrayList<>());
                    edgeList.add(new Dependency(ref, rule.head));
                    graph.put(ref, edgeList);

                }
            });
        });
        return graph;
    }

    public Map<String, List<Dependency>> dependencyGraphInv(){
        Map<String, List<Dependency>> graph = new HashMap<>();
        labels.values().stream().flatMap(Collection::stream).forEach(rule -> {
            String head = rule.head;
            graph.putIfAbsent(head, new ArrayList<>());
            Arrays.stream(rule.expression).flatMap(Arrays::stream).forEach(lit -> {
                if(lit.meta == '@') {
                    String ref = lit.tokens[0].getWord();
                    List<Dependency> edgeList = graph.get(head);
                    edgeList.add(new Dependency(head, ref));
                    graph.put(head, edgeList);
                }
            });
        });
        return graph;
    }


    public Set<DependencyNode> dependencyTree(){
        List<Dependency> edgeList = dependencyGraph().values().stream().flatMap(Collection::stream).collect(Collectors.toList());
        Set<String> heads = labels.keySet();

        Set<DependencyNode> result  = new LinkedHashSet<>();
        for (String head : heads) {
            result.addAll(dependencyTree(edgeList, head));
        }
        return result;
    }

    public Set<DependencyNode> dependencyTree(List<Dependency> edgeList, String label){
        Set<DependencyNode> nodes = new LinkedHashSet<>();
        Set<Dependency> deps = edgeList.stream().filter(dep -> Objects.equals(dep.to, label)).collect(Collectors.toSet());
        if(deps.isEmpty()){
            nodes.add(new DependencyNode(label));
        } else {
            DependencyNode node = new DependencyNode(label);
            for (Dependency dep : deps) {
                node.children.addAll(dependencyTree(edgeList, dep.from));
            }
            nodes.add(node);
        }

        return nodes;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Script)) return false;
        Script script = (Script) o;
        return Objects.equals(config, script.config) &&
                Objects.equals(labels, script.labels) &&
                Objects.equals(replies, script.replies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(config, labels, replies);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        try {
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            String yaml = mapper.writeValueAsString(config);
            result.append(yaml);
            result.append("\n---\n");
        } catch (JsonProcessingException ignored) {
            // unusable config
        }

        labels.values().stream().flatMap(Collection::stream).forEach(rule -> {
            result.append(rule).append('\n');
        });

        result.append('\n');
        replies.forEach(reply -> result.append(reply).append('\n'));

        return result.toString();
    }

    /**
     * A link between a rule that has a reference to another rule, used in its expression.
     */
    public static class Dependency {
        public String from;
        public String to;

        public Dependency(String from, String to) {
            this.from = from;
            this.to = to;
        }

        public static boolean reachable(final String src, final String dst, final Map<String, List<Dependency>> graph){
            return reachable(src, dst, graph, new HashSet<>());
        }

        public static boolean reachable(final String src, final String dst, final Map<String, List<Dependency>> graph, final Set<String> visited){
            if(!graph.containsKey(src))
                return false;
            List<Dependency> edgeList = graph.get(src);
            if(edgeList == null)
                return false;

            // depth first search
            for (Dependency edge : edgeList) {
                boolean reach;
                if (dst.equals(edge.to))
                    reach = true;
                else {
                    if(visited.contains(edge.to)){
                        return false;
                    }
                    visited.add(edge.to);
                    reach = reachable(edge.to, dst, graph, visited);
                }

                if (reach) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public String toString() {
            return to + " <- " + from;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Dependency that = (Dependency) o;
            return Objects.equals(from, that.from) &&
                    Objects.equals(to, that.to);
        }

        @Override
        public int hashCode() {
            return Objects.hash(from, to);
        }
    }

    /**
     * A single label with links to its related labels.
     */
    public static class DependencyNode {
        public String label;
        public Set<DependencyNode> children;

        public DependencyNode(String label) {
            this.label = label;
            this.children = new LinkedHashSet<>(2);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DependencyNode that = (DependencyNode) o;
            return Objects.equals(label, that.label);
        }

        @Override
        public int hashCode() {
            return Objects.hash(label);
        }

        @Override
        public String toString() {
            return label + ":"+ children;
        }
    }
}
