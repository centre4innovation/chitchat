package org.c4i.nlp.match;

import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.List;

/**
 * List of {@link ScriptException}s.
 * @author Arvid Halma
 */
public class MultiScriptException extends IllegalArgumentException{
    List<ScriptException> exceptions;

    public MultiScriptException(String message) {
        this.exceptions = ImmutableList.of(new ScriptException(message));
    }

    public MultiScriptException(ScriptException ... exceptions) {
        this.exceptions = Arrays.asList(exceptions);
    }

    public MultiScriptException(List<ScriptException> exceptions) {
        this.exceptions = exceptions;
    }

    public List<ScriptException> getExceptions() {
        return exceptions;
    }

    @Override
    public String toString() {
        return "MultiScriptException{" +
                "exceptions=" + exceptions +
                '}';
    }
}
