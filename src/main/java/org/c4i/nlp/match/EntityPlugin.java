package org.c4i.nlp.match;

import org.c4i.nlp.tokenize.Token;
import org.c4i.util.StringUtil;

import java.util.Collection;
import java.util.List;

/**
 * Special case token matchers.
 * For example for named entity recognition or regex implementations.
 * @author Arvid Halma
 * @version 4-8-2017 - 10:09
 */
public interface EntityPlugin {

    /**
     * Checks if the given {@link Literal} triggers this plugin.
     * @param lit the current literal/{@link Token}[]
     * @return true, if {@link #find(Token[], Literal, String, int, Collection)} should be called, else false.
     */
    boolean accept(Literal lit);

    /**
     * Try to find classes/labels in the text, given a query/literal
     * @param text the array of tokens in the text
     * @param lit the query
     * @param label the class/label to assign to a matched range
     * @param location offset at which the match should occur
     * @param context access to previously matched parts. The set is ordered: ranges matched later,
     *                appear earlier when iterating over it. I.e.: findFirst() is last mention match.
     * @return a list of matches
     */
    List<Range> find(Token[] text, Literal lit, String label, int location, Collection<Range> context);

    default String description(){
        return getClass().getSimpleName().toUpperCase();
    }

    static boolean isPluginPattern(Literal lit){
        if(lit.tokens.length != 1){
            return false;
        }
        return lit.meta == 'a' // word-like
               && lit.tokens[0].isNormalizedMatch() // not double quoted
               && StringUtil.isAllUpperCase(lit.tokens[0].getWord());
    }
}
