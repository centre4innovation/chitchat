/**
 * Convert (a subset of) ChitChat rule expression to queries for external database systems.
 */
package org.c4i.nlp.match.query;