package org.c4i.nlp.match;

import org.c4i.nlp.tokenize.Token;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Objects;

/**
 * Match tokens exactly.
 * @author Arvid Halma
 * @version 5-2-19
 */
public class ExactMatchStrategy implements MatchStrategy {
    private Token a;

    public ExactMatchStrategy(Token pattern) {
        this.a = pattern;
    }

    @Override
    public MatchStrategy create(Token pattern) {
        return new ExactMatchStrategy(pattern);
    }

    @Override
    public List<String> matches(Token b) {
        return Objects.equals(a.getWord(), b.getWord()) ? ImmutableList.of() : null;
    }

    @Override
    public char quote() {
        return '"';
    }
}
