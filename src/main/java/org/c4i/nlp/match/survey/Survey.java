package org.c4i.nlp.match.survey;

import java.util.LinkedHashMap;

/**
 * A list of {@link Question}s
 * @version Arvid Halma
 */
public class Survey {
    /**
        Map question id to question object.
     */
    public LinkedHashMap<String, Question> questions;

    @Override
    public String toString() {
        return questions.toString();
    }
}
