package org.c4i.nlp.match;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Syntax error with line location
 */
public class ScriptException extends IllegalArgumentException{
    String message;
    int line;
    Type type;

    enum Type {INFO, WARNING, ERROR}

    public ScriptException(String message, int line, Type type) {
        this.message = message;
        this.line = line;
        this.type = type;
    }

    public ScriptException(String message, int line) {
        this(message, line, Type.ERROR);
    }

    public ScriptException(String message) {
        this(message, 1, Type.ERROR);
    }

    public ScriptException(IOException yamlError){
        int line = 1;
        Matcher matcher = Pattern.compile("\\bline (\\d+)", Pattern.MULTILINE).matcher(yamlError.getMessage());
        if(matcher.find()){
            line = Integer.parseInt(matcher.group(1));
        }
        this.message = "There is an error in this YAML configuration line.\n" +yamlError.getMessage();
        this.line = line;
        this.type = Type.ERROR;
    }

    public ScriptException(Exception e){
        this(e.getMessage(), 1);
    }

    @Override
    public String getMessage() {
        return message;
    }

    public ScriptException setMessage(String message) {
        this.message = message;
        return this;
    }

    public int getLine() {
        return line;
    }

    public ScriptException setLine(int line) {
        this.line = line;
        return this;
    }

    public Type getType() {
        return type;
    }

    public ScriptException setType(Type type) {
        this.type = type;
        return this;
    }

    @Override
    public String toString() {
        return "ScriptException{" +
                "message='" + message + '\'' +
                ", line=" + line +
                ", type=" + type +
                '}';
    }
}
