package org.c4i.nlp.ner;

import org.apache.commons.io.IOUtils;
import org.c4i.nlp.StopWords;
import org.c4i.nlp.WordTest;
import org.c4i.nlp.match.*;
import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.normalize.StringNormalizers;
import org.c4i.nlp.tokenize.MatchingWordTokenizer;
import org.c4i.nlp.tokenize.Token;
import org.c4i.nlp.tokenize.TokenUtil;
import org.c4i.nlp.tokenize.Tokenizer;
import org.c4i.util.Combinatorics;
import org.c4i.util.Csv;
import com.google.common.collect.ImmutableList;
import org.c4i.util.StringUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Check for token sequence occurrences, and retrieve additional data when matched.
 * @author Arvid Halma
 * @version 01-08-2017
 */
public class DataSheet implements NamedEntityRecognition, EntityPlugin {
    private Map<String, List<EntityRecord>> index; // first column term to matching records
    private String indexColumn; // column's name that is indexed (first one by default)
    private List<String> columns; // all column names
    private List<EntityRecord> records; // all records (rows)
    private String entityType; // data set/table name
    private String entityTypeUpper; // name of the matching class in scripts

    private StringNormalizer normalizer;
    private WordTest wordTest;
    private boolean partialKeys;
    private int maxDamerauLevenshteinDistance = 2; // max characters that could differ when doing normalization

    private List<String> partialKeyGreyList = ImmutableList.of(); // parts of keys, that can't be stand-alone matches. They are too instinctive (e.g. common adjectives)
    private Function<Range, String> rangeToUniqueKey = range -> range.value;

    public DataSheet(String entityType) {
        this.index = new HashMap<>();
        this.records = new ArrayList<>();
        this.entityType = entityType.toLowerCase();
        this.entityTypeUpper = entityType.toUpperCase();
        this.normalizer = StringNormalizer.IDENTITY;
    }

    @Deprecated
    public DataSheet(String entityType, Map<String, String> props, StringNormalizer normalizer, Tokenizer tokenizer) throws IOException {
        this(entityType);
        this.normalizer = normalizer;
        List<String> keys = new ArrayList<>(props.keySet());
        this.indexColumn = props.values().iterator().next();
        keys.sort((o1, o2) -> Integer.compare(o2.length(), o1.length())); // longer, more specific ones first
        for (String k : keys) {
            EntityRecord record = new EntityRecord();
            record.setOrg(k);
            List<Token> tokens = tokenizer.tokenize(record.getOrg());
            normalizer.normalizeTokens(tokens);
            record.setTokens(tokens);
            addEntry(record.setOrg(k));
        }
    }

    public DataSheet(String entityType, File file, StringNormalizer normalizer, Tokenizer tokenizer) throws IOException {
        this(entityType, file, normalizer, tokenizer, StopWords.EMPTY_STOPWORDS);
    }

    public DataSheet(String entityType, File file, StringNormalizer normalizer, Tokenizer tokenizer, WordTest wordTest) throws IOException {
        this(entityType, new FileInputStream(file), normalizer, tokenizer, wordTest, false);
    }

    public DataSheet(String entityType, String data, StringNormalizer normalizer, Tokenizer tokenizer, WordTest wordTest) throws IOException {
        this(entityType, IOUtils.toInputStream(data, "UTF-8"), normalizer, tokenizer, wordTest, false);
    }

    public DataSheet(String entityType, String data, StringNormalizer normalizer, Tokenizer tokenizer, WordTest wordTest, boolean partialKeys) throws IOException {
        this(entityType, IOUtils.toInputStream(data, "UTF-8"), normalizer, tokenizer, wordTest, partialKeys);
    }

    public DataSheet(String entityType, InputStream inputStream, StringNormalizer normalizer, Tokenizer tokenizer, WordTest wordTest, boolean partialKeys) throws IOException {
        this(entityType);
        this.normalizer = normalizer;
        this.wordTest = wordTest;
        this.partialKeys = partialKeys;
        List<EntityRecord> entities = new ArrayList<>();
        new Csv()
                .setInputStream(inputStream)
                .formatTsv()
                .setUseHeader(true)
                .setColumnNameNormalizer(StringNormalizers.LOWER_CASE)
                .process(row -> {
                            String orgKey = row.getString(0);
                            if(orgKey == null || orgKey.isEmpty() || !wordTest.test(orgKey))
                                return;

                            EntityRecord record = new EntityRecord();
                            record.setOrg(orgKey);

                            columns = new ArrayList<>(row.getColumns());
                            indexColumn = columns.get(0);
                            for (String propKey : columns) {
                                String propVal = row.getString(propKey);
                                if(propVal != null) {
                                    if ("exclude".equals(propKey)) {
                                        List<Token[]> excludeTokens = new ArrayList<>();
                                        String[] parts = propVal.split("\\s*,\\s*");
                                        for (String value : parts) {
                                            Token[] excludeTok = tokenizer.tokenize(value).toArray(new Token[0]);
                                            normalizer.normalizeTokens(excludeTok);
                                            excludeTokens.add(excludeTok);
                                        }
                                        record.setExclude(excludeTokens);
                                    } else {
                                        record.props.put(propKey, propVal.intern());
                                    }
                                }
                            }

                            List<Token> tokens = tokenizer.tokenize(record.getOrg());
                            normalizer.normalizeTokens(tokens);
                            record.setTokens(tokens);
                            entities.add(record);

                            if(tokens.size() > 1 && partialKeys){
                                // skip stopwords
                                tokens = tokens.stream().filter(t -> wordTest.test(StringNormalizers.DEFAULT.normalize(t.getWord()))).collect(Collectors.toList());

                                for (List<Token> subList : Combinatorics.subLists(tokens)) {
                                    // add extra entry
                                    EntityRecord partialKeyRecord = new EntityRecord(record);
                                    partialKeyRecord.setTokens(subList);
                                    partialKeyRecord.setOrg(TokenUtil.toSentence(subList));
                                    entities.add(partialKeyRecord);

                                    // add extra entries, with a "middle" word removed.
                                    // e.g. azucar blanca importada ->  azucar importada
                                    if(subList.size() > 2){
                                        for (int i = 1; i < subList.size()-1; i++) {
                                            EntityRecord partialKeyRecord2 = new EntityRecord(record);
                                            ArrayList<Token> subList2 = new ArrayList<>(subList);
                                            subList2.remove(i);
                                            partialKeyRecord.setTokens(subList2);
                                            partialKeyRecord.setOrg(TokenUtil.toSentence(subList2));
                                            entities.add(partialKeyRecord2);
                                        }
                                    }
                                }
                            }
                        }
                );

        inputStream.close();

        entities.sort((o1, o2) -> Integer.compare(o2.tokens.size(), o1.tokens.size())); // longer, more specific ones first
        for (EntityRecord record : entities) {
            addEntry(record);
        }
    }

    private void addEntry(EntityRecord entry) {
        if (!entry.tokens.isEmpty()){
            String nword = entry.tokens.get(0).getNormalizedWord();
            if(!nword.isEmpty()){
                // add to index
                if(!index.containsKey(nword)){
                    index.put(nword, new ArrayList<>());
                }
                index.get(nword).add(entry);
                // add for linear access
                records.add(entry);
            }
        }
    }

    public String getEntityType() {
        return entityType;
    }

    public DataSheet setEntityType(String entityType) {
        this.entityType = entityType.toLowerCase();
        this.entityTypeUpper = entityType.toUpperCase();
        return this;
    }

    public List<String> getPartialKeyGreyList() {
        return partialKeyGreyList;
    }

    public DataSheet setPartialKeyGreyList(List<String> partialKeyGreyList) {
        this.partialKeyGreyList = partialKeyGreyList;
        return this;
    }

    public Function<Range, String> getRangeToUniqueKey() {
        return rangeToUniqueKey;
    }

    public DataSheet setRangeToUniqueKey(Function<Range, String> rangeToUniqueKey) {
        this.rangeToUniqueKey = rangeToUniqueKey;
        return this;
    }

    @Override
    public boolean accept(Literal lit) {
        return lit.equals(entityTypeUpper);
    }

    @Override
    public List<Range> find(Token[] text, Literal ignoredLit, String ignoredLabel, int location, Collection<Range> context) {
        List<Range> result = new ArrayList<>();
        Token token = text[location];
        String nword = token.getNormalizedWord();



        // keep track of best match so far (most number of tokens)
        int patLenMatched = 0;

        // exacter matches even match with StringNormalizers.DEFAULT (some more aggressive normalizers may include more matches)
        // e.g. "Aji" and "Ajo" both become "AJ". But "aji" matches better with the first one.
        List<Range> exacterMatches = new ArrayList<>();

        // matches that may be too aggressively normalized, or that are partialKeyGreyListed.
        // e.g. "uva" and "huevo" both become "UV", but "uba" should only match the first
        List<Range> blacklistMatches = new ArrayList<>();

        if(index.containsKey(nword)){
            // first part occurs... do proper check
            List<EntityRecord> possiblePatterns = index.get(nword);

            nextPat:
            for (EntityRecord record : possiblePatterns) {
                int patLen = record.tokens.size();
                if(location + patLen > text.length ){
                    // pattern too long
                    continue; // nextPat;
                }

                for (int k = 1; k < patLen; k++) {
                    // first part already matched, check remaining ones
                    Token patToken = record.tokens.get(k);
                    if (!patToken.getNormalizedWord().equals(text[location + k].getNormalizedWord())) {
                        continue nextPat;
                    }
                }

                if(record.exclude == null || !findExclude(text, record.exclude)) {
                    // this pattern matched

                    if (patLen < patLenMatched) {
                        // there were already more specific tokens matched.
                        // discard matches from here
                        break;
                    }

                    if(patLen == 1 && !wordTest.test(StringNormalizers.DEFAULT.normalize(token.getWord()))){
                        // query word is a stopword
                        continue ;
                    }

                    patLenMatched = patLen;

                    Range range = new Range(entityType, location, location + patLen, text[location].getCharStart(), text[location + patLen - 1].getCharEnd());
                    range.props = new LinkedHashMap<>(record.props);
                    range.props.put("type", entityType);
                    range.setValue(record.props.get(indexColumn));
                    result.add(range);

                    // see if it is a non-distinctive grey-list word
                    if (patLen == 1 && partialKeyGreyList.contains(StringNormalizers.DEFAULT.normalize(text[location].getWord()))) {
                        blacklistMatches.add(range);
                    } else {
                        // now see if it would still be a match, with a potentially more conservative normalizer
                        boolean isExacterMatch = true;
                        for (int i = location; i < location + patLen; i++) {
                            if (!StringNormalizers.DEFAULT.matches(text[i], record.tokens.get(i - location))) {
                                isExacterMatch = false;
                                break;
                            }
                        }
                        if (isExacterMatch) {
                            exacterMatches.add(range);
                        } else {
                            // now see if it is too different
                            boolean tooInexactMatch = false;
                            for (int i = location; i < location + patLen; i++) {
                                String defaultWord = StringNormalizers.DEFAULT.normalize(text[i].getWord());
                                String recordWord = StringNormalizers.DEFAULT.normalize(record.tokens.get(i - location).getWord());
                                if (StringUtil.damerauLevenshteinDistance(defaultWord, recordWord) > maxDamerauLevenshteinDistance) {
                                    tooInexactMatch = true;
                                    break;
                                }
                            }

                            if (tooInexactMatch) {
                                blacklistMatches.add(range);
                            }
                        }
                    }
                }
            }
        }

        if(patLenMatched > 0) {
            if(exacterMatches.size() > 0 && exacterMatches.size() < result.size()){
                // there is a non-empty subset of matches that are more relevant: use those!
                result = exacterMatches;
            }

            // remove of too weird matched
            result.removeAll(blacklistMatches);

            // Sort, deduplicate results
            TreeSet<Range> set = new TreeSet<>(Comparator.comparing(rangeToUniqueKey));
            set.addAll(result);
            return new ArrayList<>(set);
        }
        return result;
    }

    @Override
    public List<Range> find(List<Token> tokens) {
        List<Range> result = new ArrayList<>();
        Token[] text = tokens.toArray(new Token[0]);

        nextWord:
        for (int i = 0, tokensSize = tokens.size(); i < tokensSize; i++) {
            Token token = tokens.get(i);
            String nword = token.getNormalizedWord();
            if(index.containsKey(nword)){
                // first part occurs... do proper check
                List<EntityRecord> possiblePatterns = index.get(nword);
                nextPat:
                for (EntityRecord record : possiblePatterns) {
                    int patLen = record.tokens.size();
                    for (int k = 1; k < patLen; k++) {
                        // first part already matched, check remaining ones
                        Token patToken = record.tokens.get(k);
                        if (!patToken.getNormalizedWord().equals(tokens.get(i + k).getNormalizedWord())) {
                            continue nextPat;
                        }
                    }

                    // a match!
                    if(record.exclude == null || !findExclude(text, record.exclude)) {
                        Range range = new Range(entityType, i, i + patLen, tokens.get(i).getCharStart(), tokens.get(i + patLen - 1).getCharEnd());
                        range.props = new LinkedHashMap<>(record.props);
                        range.props.put("type", entityType);
                        result.add(range);
                        continue nextWord;
                    }
                }
            }
        }
        return result;
    }

    private boolean findExclude(Token[] text, List<Token[]> exclude) {
        final Eval eval = new Eval(null);
        for (Token[] tokens : exclude) {
            if (!eval.findFirst(text, tokens).isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public DataSheet withIndex(String column){
        DataSheet sheet = new DataSheet(entityType);
        sheet.indexColumn = column;
        sheet.columns = columns;
        Tokenizer tokenizer = new MatchingWordTokenizer();
        for (EntityRecord record : records) {
            EntityRecord r = new EntityRecord();
            final String key = record.getProps().get(column);
            if(key != null && !key.isEmpty()) {
                r.setOrg(key);
                r.tokens = tokenizer.tokenize(key);
                normalizer.normalizeTokens(r.tokens);
                r.setProps(record.props);
                sheet.addEntry(r);
            }
        }
        return sheet;
    }

    @Override
    public String description() {
        return entityTypeUpper;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.join("\t", columns));
        sb.append('\n');
        for (EntityRecord record : records) {
            sb.append(record.toString()).append('\n');
        }
        return sb.toString();
    }

    /**
     * Row/entry object.
     */
    private static class EntityRecord {
        String org;
        List<Token> tokens;
        List<Token[]> exclude;
        Map<String, String> props;

        public EntityRecord() {
            props = new LinkedHashMap<>();
        }

        public EntityRecord(EntityRecord that) {
            this.org = that.org;
            this.tokens = that.tokens;
            this.exclude = that.exclude;
            this.props = that.props;
        }

        public String getOrg() {
            return org;
        }

        public EntityRecord setOrg(String org) {
            this.org = org;
            return this;
        }

        public List<Token> getTokens() {
            return tokens;
        }

        public EntityRecord setTokens(List<Token> tokens) {
            this.tokens = tokens;
            return this;
        }

        public List<Token[]> getExclude() {
            return exclude;
        }

        public EntityRecord setExclude(List<Token[]> exclude) {
            this.exclude = exclude;
            return this;
        }

        public Map<String, String> getProps() {
            return props;
        }

        public EntityRecord setProps(Map<String, String> props) {
            this.props = props;
            return this;
        }

        @Override
        public String toString() {
            return String.join("\t", props.values());
        }
    }
}
