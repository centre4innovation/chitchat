package org.c4i.nlp.ner;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.util.Duration;
import io.dropwizard.validation.MinDuration;
import org.c4i.chitchat.api.Config;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.WordTest;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Automatically overwrite a given datasheet with the latest data retrieved via an URL.
 */
public class LiveDataSheetConfig {
    @JsonIgnore
    private final Logger logger = LoggerFactory.getLogger(LiveDataSheetConfig.class);

    @JsonProperty
    protected String lang;
    @JsonProperty
    protected String name;
    @JsonProperty
    protected String url;

    @JsonProperty(defaultValue = "10m")
    @NotNull
    @MinDuration(
            value = 1L,
            unit = TimeUnit.SECONDS,
            inclusive = true
    )
    private Duration updateInterval = Duration.minutes(10);

    public LiveDataSheetConfig() {
    }

    public LiveDataSheetConfig(String name, String url, Duration updateInterval) {
        this.name = name;
        this.url = url;
        this.updateInterval = updateInterval;
    }


    public String getLang() {
        return lang;
    }

    public LiveDataSheetConfig setLang(String lang) {
        this.lang = lang;
        return this;
    }

    public String getName() {
        return name;
    }

    public String getDataSheetName(){
        return lang + "/" + name;
    }

    public LiveDataSheetConfig setName(String name) {
        this.name = name;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public LiveDataSheetConfig setUrl(String url) {
        this.url = url;
        return this;
    }

    public Duration getUpdateInterval() {
        return updateInterval;
    }

    public LiveDataSheetConfig setUpdateInterval(Duration updateInterval) {
        this.updateInterval = updateInterval;
        return this;
    }

    public void onUpdate(){

    }

    public void init(Config config){
        // 1. setup interval loop
        //   2. retrieve CSV
        //   3. validate (parse)
        //     4. save in DB
        //     5. update datasheet reference
        //     6. call onUpdate()

        String csv = null;
        try {
            csv = Jsoup.connect(url).followRedirects(true).get().text();
            logger.warn("Live data sheet '{}' successfully retrieved data from {}", name, url);
        } catch (IOException e) {
            logger.error("Live data sheet '{}' could not be retrieved data from {}, reason: {}", name, url, e.getMessage());
            return;
        }
        Nlp nlp = config.getNlp();

        DataSheet sheet;
        try {
            sheet = new DataSheet(name, csv, nlp.getNormalizer(lang), nlp.getWordTokenizer(lang), WordTest.DEFAULT);
            logger.warn("Live data sheet '{}' successfully parsed", name);
        } catch (IOException e) {
            logger.error("Live data sheet '{}' could not be parsed: {}", name, e.getMessage());
            return;
        }

        String docName = getDataSheetName();
        try {
            config.databaseResource.saveDataSheet(docName, csv);
            logger.warn("Live data sheet '{}' successfully saved to DB as '{}'", name, docName);
        } catch (Exception e) {
            e.printStackTrace();
            logger.warn("Live data sheet '{}' could not be save to DB as '{}', reason: {}", name, docName, e.getMessage());
        }

        try {
            onUpdate();
        } catch (Exception e) {
            logger.warn("Live data sheet '{}'failed in onUpdate(): {}", name, e.getMessage());
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LiveDataSheetConfig)) return false;
        LiveDataSheetConfig that = (LiveDataSheetConfig) o;
        return Objects.equals(lang, that.lang) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lang, name);
    }

    @Override
    public String toString() {
        return "LiveDataSheetConfig{" +
                "lang='" + lang + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", updateInterval=" + updateInterval +
                '}';
    }
}
