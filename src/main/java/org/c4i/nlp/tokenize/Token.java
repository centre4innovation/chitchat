package org.c4i.nlp.tokenize;


import org.c4i.nlp.match.ExactMatchStrategy;
import org.c4i.nlp.match.MatchStrategy;
import org.c4i.nlp.match.NormalizedMatchStrategy;
import org.c4i.nlp.normalize.StringNormalizer;

import java.util.List;

/**
 * Word-like part of a text.
 * @author Arvid Halma
 * @version 27-9-2015 - 13:29
 */
public class Token implements Comparable<Token>, CharSequence{

    public static final Token ANY_ONE = new Token("?");
    public static final Token ANY_ZERO_OR_MORE = new Token("*");
    public static final Token ANY_ONE_OR_MORE = new Token("+");

    private String word;
    private String normalizedWord;
    private int location;
    private String tag;
    private String origin;
    private double weight;

    private int charStart, charEnd;
    private int sentence;
    private int section;

    private MatchStrategy matchStrategy;


    public Token(Token token) {
        this.word = token.word;
        this.normalizedWord = token.normalizedWord;
        this.location = token.location;
        this.tag = token.tag;
        this.origin = token.origin;
        this.weight = token.weight;
        this.charStart = token.charStart;
        this.charEnd = token.charEnd;
        this.section = token.section;
        // create similar matchStrategy, but with a reference to this token
        this.matchStrategy = token.matchStrategy.create(this);
    }

    public Token(String word) {
        this(word, word, 0, null, null, 1.0, true);
    }

    public Token(String word, int location) {
        this(word, word, location, null, null, 1.0, true);
    }

    public Token(String word, String tag, String origin, double weight) {
        this(word, word, 0, tag, origin, weight, true);
    }

    public Token(String word, String normalizedWord, int location, String tag, String origin, double weight) {
        this(word, normalizedWord, location, tag, origin, weight, true);
    }

    public Token(String word, String normalizedWord, int location, String tag, String origin, double weight, boolean matchOnNormalized) {
        this.word = word;
        this.normalizedWord = normalizedWord;
        this.location = location;
        this.tag = tag;
        this.origin = origin;
        this.weight = weight;
        this.matchStrategy = matchOnNormalized ? new NormalizedMatchStrategy(this) : new ExactMatchStrategy(this);
    }

    public String getWord() {
        return word;
    }

    public Token setWord(String word) {
        this.word = word;
        this.normalizedWord = null;
        return this;
    }

    public String getNormalizedWord() {
        return normalizedWord == null ? word : normalizedWord;
    }

    public Token setNormalizedWord(String normalizedWord) {
        this.normalizedWord = normalizedWord;
        return this;
    }

    public int getLocation() {
        return location;
    }

    public Token setLocation(int location) {
        this.location = location;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public Token setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public String getOrigin() {
        return origin;
    }

    public Token setOrigin(String origin) {
        this.origin = origin;
        return this;
    }

    public double getWeight() {
        return weight;
    }

    public Token setWeight(double weight) {
        this.weight = weight;
        return this;
    }

    public int getSection() {
        return section;
    }

    public Token setSection(int section) {
        this.section = section;
        return this;
    }

    public int getSentence() {
        return sentence;
    }

    public Token setSentence(int sentence) {
        this.sentence = sentence;
        return this;
    }

    public MatchStrategy getMatchStrategy() {
        return matchStrategy;
    }

    public Token setMatchStrategy(MatchStrategy matchStrategy) {
        this.matchStrategy = matchStrategy;
        return this;
    }

    public Token setNormalizedMatch(){
        this.matchStrategy = new NormalizedMatchStrategy(this);
        return this;
    }

    public boolean isNormalizedMatch() {
        return matchStrategy instanceof NormalizedMatchStrategy;
    }


    public Token setExactMatch(){
        this.matchStrategy = new ExactMatchStrategy(this);
        return this;
    }

    public boolean isExactMatch() {
        return matchStrategy instanceof ExactMatchStrategy;
    }

    public List<String> match(Token t){
        return matchStrategy.matches(t);
    }

    public Token normalize(StringNormalizer normalizer){
        this.normalizedWord = normalizer.normalize(this.word);
        return this;
    }

    public boolean isNormalized(){
        return normalizedWord != null;
    }

    @Override
    public int compareTo(Token t) {
        if (word == null || t.word == null)
            return 0;
        if(word.isEmpty() || t.word.isEmpty())
            return 0;
        // put operator-like chars (+,*, more general matches) later
        if(word.charAt(0) < 48 && t.word.charAt(0) >= 48)
            return 1;
        if(t.word.charAt(0) < 48 && word.charAt(0) >= 48)
            return -1;
        else return word.compareTo(t.word);
    }

    @Override
    public int length() {
        return toString().length();
    }

    @Override
    public char charAt(int index) {
        return toString().charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return toString().subSequence(start, end);
    }

    public int getCharStart() {
        return charStart;
    }

    public Token setCharStart(int charStart) {
        this.charStart = charStart;
        return this;
    }

    public int getCharEnd() {
        return charEnd;
    }

    public Token setCharEnd(int charEnd) {
        this.charEnd = charEnd;
        return this;
    }

    public void addTokenOffset(int offset){
        this.location += offset;
    }

    public void addCharOffset(int offset){
        this.charStart += offset;
        this.charEnd += offset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Token)) return false;

        Token token = (Token) o;

        if(this == ANY_ONE || token == ANY_ONE){
            return true;
        }

        return matchStrategy.matches(token) != null;
    }

    @Override
    public int hashCode() {
        String w = isNormalizedMatch() ? (normalizedWord == null ? word : normalizedWord) : word;
        return w != null ? w.hashCode() : 0;
    }

    @Override
    public String toString() {
        return isNormalizedMatch() ? getNormalizedWord() : getWord();
    }

    public String toExtendedString() {
        if(weight == 1.0){
            String s;
            if(tag == null){
                s = getNormalizedWord();
            } else {
                s = String.format("%s/%s", getNormalizedWord(), tag);
            }

            if(origin != null){
                s += ":" + origin;
            }

            return s;
        }
        return String.format("%s/%s(%.2f):%s", normalizedWord, tag, weight, origin == null ? "" : origin);
    }
}
