package org.c4i.nlp.chat;

import org.c4i.nlp.Nlp;
import org.c4i.nlp.match.Eval;
import org.c4i.nlp.match.Result;
import org.c4i.nlp.match.Script;

/**
 * A bot that uses a {@link Script} to define its replies.
 * @author Arvid Halma
 * @version 9-8-17
 */
public class ScriptBot implements ChatBot{
    Script script;
    Nlp nlp;

    public ScriptBot(Script script, Nlp nlp) {
        this.script = script;
        this.nlp = nlp;
    }

    @Override
    public Result reply(Conversation conversation) {
        return new Eval(nlp).reply(script, conversation);
    }

    public Result reply(Eval.EvalState state) {
        return new Eval(nlp).reply(script, state);
    }

    public Script getScript() {
        return script;
    }

    public Nlp getNlp() {
        return nlp;
    }
}
