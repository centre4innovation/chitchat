package org.c4i.nlp.chat;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.collect.ImmutableList;
import org.c4i.nlp.match.Eval;
import org.c4i.nlp.match.Script;
import org.c4i.util.Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Group message into conversation, by senderId and time interval
 * @author Arvid Halma
 * @version 4-7-17
 */
public class EvalStateManager {

    private Cache<String, Eval.EvalState> cache;
    private final Logger logger = LoggerFactory.getLogger(EvalStateManager.class);

    public EvalStateManager(){
        this(100_000, 10, TimeUnit.MINUTES, ImmutableList.of());
    }

    public EvalStateManager(int maxEntries, long maxEntryTime, TimeUnit maxEntryTimeUnit, List<ConversationListener> removalListeners) {
        this.cache = CacheBuilder.newBuilder()
                .maximumSize(maxEntries)
                .expireAfterAccess(maxEntryTime, maxEntryTimeUnit)
                .removalListener((RemovalListener<String, Eval.EvalState>) removalNotification ->
                        removalListeners.forEach(cl -> cl.timout(removalNotification.getValue().conversation)))
                .build();

        // force cleanup

        /*
            So what are the main differences between the Timer and the ExecutorService solution:

            Timer can be sensitive to changes in the system clock; ScheduledThreadPoolExecutor is not
            Timer has only one execution thread; ScheduledThreadPoolExecutor can be configured with any number of threads
            Runtime Exceptions thrown inside the TimerTask kill the thread, so following scheduled tasks won’t run further; with ScheduledThreadExecutor – the current task will be canceled, but the rest will continue to run
            http://www.baeldung.com/java-timer-and-timertask
         */
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(
        new TimerTask() {
            @Override
            public void run() {
                cache.cleanUp();
            }
        }, maxEntryTime, (maxEntryTime+1)/2, maxEntryTimeUnit);
    }

    /**
     * Creates a unique key for messages from senderId and recipientId (or the reverse) for a given channel.
     * So, independent of the message direction, create a unique hash/key.
     * @param senderId one user
     * @param recipientId the other user
     * @param channel the name of the channel
     * @return a unique hash
     */
    private String key(String senderId, String recipientId, String channel) {
        String[] conversationId = new String[]{senderId, recipientId};
        Arrays.sort(conversationId);
        return Hash.sha1Hex(conversationId[0] + conversationId[1] + channel);
    }

    private String key(Message msg){
        return key(msg.getSenderId(), msg.getRecipientId(), msg.getChannel());
    }

    public Eval.EvalState update(Script script, Message msg){
        String recipientId = msg.getRecipientId();
        String senderId = msg.getSenderId();
        String channel = msg.getChannel();
        String key = key(senderId, recipientId, channel);

        try {
            Eval.EvalState state = cache.get(key,
                    () -> new Eval.EvalState(new Script(script), new Conversation()
                            .setId(Hash.sha1Hex(key + msg.getTimestamp().getMillis())) // append timestamp and rehash to define the conversation id
                            .setUserId(senderId)
                            .setBotId(recipientId)
                            .setChannel(msg.getChannel()))
            );
            Conversation conversation = state.conversation;
            msg.setConversationId(conversation.getId());
            msg.setOrder(conversation.getMessages().size());
            conversation.getMessages().add(msg);
            return state;
        } catch (ExecutionException e) {
            logger.error("Error while managing conversations.", e);
            throw new RuntimeException(e);
        }
    }

    public Eval.EvalState getEvalState(String senderId, String recipientId, String channel){
        String id = key(senderId, recipientId, channel);
        return cache.getIfPresent(id);
    }

    public void reset(String senderId, String recipientId, String channel) {
        String key = key(senderId, recipientId, channel);
        cache.invalidate(key);
    }

    public void reset() {
        cache.invalidateAll();
    }

}
