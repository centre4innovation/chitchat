package org.c4i.nlp.chat;

import org.c4i.nlp.match.Result;

/**
 * A ChatBot interface that, given a previous conversation (list of messages), defines a reply (list of messages).
 * @author Arvid Halma
 * @version 4-7-17
 */
public interface ChatBot {

    Result reply(Conversation conversation);

}
