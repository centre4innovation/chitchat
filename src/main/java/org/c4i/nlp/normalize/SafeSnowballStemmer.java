package org.c4i.nlp.normalize;


/**
 * Snowball stemmer, multi threading safe.
 * @author Arvid Halma
 * @version 9-4-2015 - 20:51
 */
public class SafeSnowballStemmer implements StringNormalizer {
    private String lang;

    public SafeSnowballStemmer(String lang) {
       this.lang = lang;
        // trigger IllegalArgument exception, when lang is unsupported
       new SnowballStemmer(lang);
    }

    @Override
    public synchronized String normalize(String string) {
        return new SnowballStemmer(lang).normalize(string);
    }

    @Override
    public String toString() {
        return "SafeSnowballStemmer{" +
                "lang='" + lang + '\'' +
                '}';
    }


}
