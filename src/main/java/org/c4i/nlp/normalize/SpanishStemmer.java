package org.c4i.nlp.normalize;

/**
 * Spanish stemmer tring to remove inflectional suffixes for nouns and adjectives (plurals, and gender (-a, -o, -e).
 *
 * With <code>normalizeGender = true</code>:
 * <pre>
     hombres	 -> hombre
     hombre	 -> hombre
     mujeres	 -> mujer
     mujer	 -> mujer
     raros	 -> raro
     raro	 -> raro
     raras	 -> rara
     rara	 -> rara
 * </pre>
 * With <code>normalizeGender = true</code>:
 * <pre>
     hombres	 -> hombr
     hombre	 -> hombr
     mujeres	 -> mujer
     mujer	 -> mujer
     raros	 -> rar
     raro	 -> rar
     raras	 -> rar
     rara	 -> rar
 * </pre>
 * Accents are assumed to be already removed {@link StringNormalizers#NO_ACCENTS} and lowercased {@link StringNormalizers#LOWER_CASE}.
 * @author Arvid Halma
 * @version 30-8-19
 */
public class SpanishStemmer implements StringNormalizer {
    boolean normalizeGender;

    public SpanishStemmer() {
        this.normalizeGender = false;
    }
    public SpanishStemmer(boolean normalizeGender) {
        this.normalizeGender = normalizeGender;
    }

    @Override
    public String normalize(String word) {
        int len = word.length() ;

        if (len > 3) {

            if (word.endsWith("eses")) {
                //  corteses -> cortÈs
                word = word.substring(0, len - 2);
                return word;
            }

            if (word.endsWith("eres")) {
                //  mujeres -> mujer
                word = word.substring(0, len - 2);
                return word;
            }

            if (word.endsWith("ces")) {
                //  dos veces -> una vez
                word = word.substring(0, len - 3);
                word = word + 'z';
                return word;
            }

            if (word.endsWith("os") || word.endsWith("as") || word.endsWith("es")) {
                //  ending with -os, -as  or -es
                word = word.substring(0, len - 1);
                len--;
                if(!normalizeGender) {
                    return word;
                }
            }
        }

        if (normalizeGender && len > 2) {
            if (word.endsWith("o") || word.endsWith("a") || word.endsWith("e")) {
                //  ending with  -o,  -a, or -e: uva -> uv
                word = word.substring(0, len - 1);
                return word;
            }

        }
        return word;
    }
}
