package org.c4i.nlp.normalize.phonetic;

import org.c4i.nlp.normalize.StringNormalizer;

/**
 * The Spanish Metaphone Algorithm (Algoritmo del Metáfono para el Español)
 * 
 * Ported from: https://github.com/amsqr/Spanish-Metaphone/blob/master/phonetic_algorithms_es.py
 *
 * This script implements the Metaphone algorithm (c) 1990 by Lawrence Philips.
 * It was inspired by the English double metaphone algorithm implementation by
 * Andrew Collins - January 12, 2007 who claims no rights to this work
 * (http://www.atomodo.com/code/double-metaphone)
 *  
 * The metaphone port adapted to the Spanish Language is authored
 * by Alejandro Mosquera <amosquera@dlsi.ua.es> November, 2011
 * and is covered under this copyright:
 * Copyright 2011, Alejandro Mosquera <amosquera@dlsi.ua.es>.  All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Java version:
 * @author Arvid Halma
 */
public class MetaphoneES implements StringNormalizer {

    public MetaphoneES() {
    }

    @Override
    public String normalize(String string) {
        return metaphone(string);
    }

    private static boolean isVowel(String s, int i){
        return "AEIOU".indexOf(s.charAt(i)) >= 0;
    }

    private static String simplify(String s){
        s = s.replace("á", "A");
        s = s.replace("ç", "S");
        s = s.replace("é", "E");
        s = s.replace("í", "I");
        s = s.replace("ó", "O");
        s = s.replace("ú", "U");
        s = s.replace("ñ", "NY");
        s = s.replace("ch", "X");
        s = s.replace("gü", "W");
        s = s.replace("ü", "U");
        s = s.replace("b", "V");
        s = s.replace("z", "S");
        s = s.replace("ll", "Y");
        return s;
    }

    private static String metaphone(String s){
        // initialize metaphone key string
        StringBuilder metaKey = new StringBuilder();
   		// set maximum metaphone key size
        int keyLength = 6;
   		// set current position to the beginning
        int currentPos = 0;
        String orgString = s + "    ";
	   	// Let's replace some spanish characters  easily confused
        orgString = simplify(orgString.toLowerCase());
	   	// convert string to uppercase
        orgString = orgString.toUpperCase();

        
        //  main loop
        while (metaKey.length() < keyLength) {

            // break out of the loop if greater or equal than the length
            if (currentPos >= orgString.length()) {
                break;
            }
            // get character from the string
            char current_char = orgString.charAt(currentPos);

            // if it is a vowel, and it is at the begining of the string,
            // set it as part of the meta key
            if (isVowel(orgString, currentPos) && (currentPos == 0)) {
                metaKey.append(current_char);
                currentPos++;
            } else {
                // Let"s check for consonants  that have a single sound
                // or already have been replaced  because they share the same
                // sound like "B" for "V" and "S" for "Z"

                if ("DFJKMNPTVLY".indexOf(orgString.charAt(currentPos)) >= 0) {
                    metaKey.append(current_char);

                    // increment by two if a repeated letter is found
                    if (orgString.charAt(currentPos + 1) == current_char) {
                        currentPos += 2;
                    } else { //  increment only by one
                        currentPos++;
                    }
                } else {  // check consonants with similar confusing sounds
                    if (current_char == 'C') {
                        // special case "acción", "reacción",etc.
                        if (orgString.charAt(currentPos + 1) == 'C') {
                            metaKey.append("X");
                            currentPos += 2;

                        } else {
                            final char nextChar = orgString.charAt(currentPos + 1);
                            if (nextChar == 'I' || nextChar == 'E') {
                                //  special case "cesar", "cien", "cid", "conciencia"
                                metaKey.append("Z");
                                currentPos += 2;
                            } else {
                                metaKey.append("K");
                                currentPos++;
                            }
                        }

                    } else if (current_char == 'G') {
                        //  special case "gente", "ecologia",etc
                        final char nextChar = orgString.charAt(currentPos + 1);
                        if (nextChar == 'I' || nextChar == 'E') {
                            metaKey.append("J");
                            currentPos += 2;
                        } else {
                            metaKey.append("G");
                            currentPos++;
                        }
                    } else if (current_char == 'H') {
                        // since the letter "h" is silent in spanish,
                        // let"s set the meta key to the vowel after the letter "h"
                        if (isVowel(orgString, currentPos + 1)) {
                            metaKey.append(orgString.charAt(currentPos + 1));
                            currentPos += 2;
                        } else {
                            metaKey.append("H");
                            currentPos++;
                        }
                    } else if (current_char == 'Q') {
                        if (orgString.charAt(currentPos) == 'U') {
                            currentPos += 2;
                        } else {
                            currentPos++;
                        }
                        metaKey.append("K");
                    } else if (current_char == 'W') {
                        metaKey.append('U');
                        currentPos++;
                    } else if (current_char == 'R') {
                        //  perro, arrebato, cara
                        currentPos++;
                        metaKey.append("R");
                    } else if (current_char == 'S') {
                        //  spain
                        if (!isVowel(orgString, currentPos + 1) && currentPos == 0) {
                            metaKey.append("ES");
                        } else {
                            metaKey.append("S");
                        }
                        currentPos++;

                    } else if (current_char == 'Z') {
                        //  zapato
                        currentPos++;
                        metaKey.append("Z");
                    } else if (current_char == 'X') {
                        // some mexican spanish words like"Xochimilco","xochitl"
                        if (!isVowel(orgString, currentPos + 1) && s.length() > 1 && currentPos == 0) {
                            metaKey.append("EX");
                        } else {
                            metaKey.append("X");
                        }
                        currentPos++;

                    } else {
                        currentPos++;
                    }
                }
            }
        }
		 // trim any blank characters
        metaKey = new StringBuilder(metaKey.toString().trim());
		   
		 // return the final meta key string
        return metaKey.toString();
    }

    public static void main(String[] args) {
        String[] words = {"X","xplosion","escalera","scalera","mi","tu","su","te","ochooomiiiillllllll","complicado","ácaro","ácido","clown","down","col","clon","waterpolo","aquino","rebosar","rebozar","grajea","gragea","encima","enzima","alhamar","abollar","aboyar","huevo","webo","macho","xocolate","chocolate","axioma","abedul","a","gengibre","yema","wHISKY","google","xilófono","web","guerra","pingüino","si","ke","que","tu","gato","gitano","queso","paquete","cuco","perro","pero","arrebato","hola", "zapato", "españa", "garrulo", "expansión", "membrillo", "jamón","risa","caricia", "llaves", "paella","cerilla"};
        for (String word : words) {
            System.out.println(word + " -> " + metaphone(word));
        }
    }

}
