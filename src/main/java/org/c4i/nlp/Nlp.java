package org.c4i.nlp;

import com.google.common.collect.ImmutableMap;
import org.c4i.chitchat.api.model.LanguageProcessingConfig;
import org.c4i.chitchat.api.plugin.FlipReplyPlugin;
import org.c4i.nlp.detect.ConstantLanguageDetector;
import org.c4i.nlp.detect.LanguageDetector;
import org.c4i.nlp.detect.OpenLanguageDetector;
import org.c4i.nlp.generalize.FirstOfGeneralizer;
import org.c4i.nlp.generalize.Generalizer;
import org.c4i.nlp.generalize.RelatedWordsGeneralizer;
import org.c4i.nlp.match.*;
import org.c4i.nlp.substitution.MultiSubstitution1Way;
import org.c4i.nlp.substitution.Substitution2Way;
import org.c4i.nlp.ner.*;
import org.c4i.nlp.normalize.*;
import org.c4i.nlp.normalize.kstem.KStem;
import org.c4i.nlp.normalize.phonetic.*;
import org.c4i.nlp.tokenize.*;
import org.parboiled.common.FileUtils;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.Collections.synchronizedList;
import static java.util.Collections.synchronizedMap;

/**
 * Natural Language Processing resource manager.
 * @author Arvid Halma
 * @version 12-11-2017 - 15:02
 */
public class Nlp {
    private final Logger logger = LoggerFactory.getLogger(ScriptConfig.class);

    private File baseDir;

    private final Set<String> languages = new HashSet<>();
    private final Map<String, LanguageProcessingConfig> languageProcessing;
    private LanguageDetector languageDetector;

    private MultiSubstitution1Way replyVariables;

    // Language to model
    private final Map<String, StopWords> stopWords = synchronizedMap(new HashMap<>());
    private final Map<String, StringNormalizer> stringNormalizers = synchronizedMap(new HashMap<>());
    private final Map<String, Tokenizer> tokenizers = synchronizedMap(new HashMap<>());
    private final Map<String, SentenceSplitter> sentenceSplitters = synchronizedMap(new HashMap<>());
    private final Map<String, List<EntityPlugin>> entityPlugins = synchronizedMap(new HashMap<>());
    private final Map<String, Generalizer> generalizers = synchronizedMap(new HashMap<>());
    private final Map<String, Substitution2Way> substitutions = synchronizedMap(new HashMap<>());
    private final Map<String, List<ReplyPlugin>> replyPlugins = synchronizedMap(new HashMap<>());

    public Nlp(){
        this(null, ImmutableMap.of("en", new LanguageProcessingConfig()));
    }

    public Nlp(File baseDir, Map<String, LanguageProcessingConfig> languageProcessing) {
        this.baseDir = baseDir;
        this.languageProcessing = languageProcessing;
        this.replyVariables = new MultiSubstitution1Way();
    }


    private void updateLanguageSupport(){
        languages.clear();
        File[] langDirs = baseDir.listFiles((current, name) -> new File(current, name).isDirectory());
        if(langDirs == null)
            return;
        for (File langDir : langDirs) {
            String lang = langDir.getName();
            if(languageProcessing == null || languageProcessing.containsKey(lang))
                languages.add(lang);
        }
    }

    public boolean hasFeature(String lang, String feature){
        if(languageProcessing == null)
            return true; // nothing defined at all, so support whatever is available, otherwise it's useless
        LanguageProcessingConfig lpc = languageProcessing.get(lang);
        if(lpc == null)
            return false; // the language is not supported, therefore the feature isn't

        return lpc.getFeatures().contains(feature);
    }

    public Set<String> getLanguages() {
        return languages;
    }

    public void loadModels(boolean async){
        ExecutorService executor = async ? Executors.newFixedThreadPool(16) : Executors.newSingleThreadScheduledExecutor();

        if(baseDir == null){
            return;
        }

        try {
            languageDetector = new OpenLanguageDetector(new File(baseDir, "langdetect-183.bin"), "en", 200);
        } catch (IOException e) {
            logger.error("Can't load language detector model: " + e.getMessage());
            languageDetector = new ConstantLanguageDetector("en");
        }

        updateLanguageSupport();

        // Load all language features
        for (String lang : languages) {
            File langDir = new File(baseDir, lang);

            // STRING NORMALIZERS
            // from more precise/conservative, to more aggressive  versions
            final List<StringNormalizer> normalizers = createNormalizers(lang);
            stringNormalizers.put(lang, normalizers.get(normalizers.size()-1));

            // STOPWORDS
            if(hasFeature(lang, "stopwords")) {
                executor.execute(() -> {
                    try {
                        logger.warn("Loading [{}] stopwords ", lang);
                        stopWords.put(lang, new StopWords(
                                new File(langDir, "stopwords_" + lang + ".txt"),
                                normalizers
                        ));
                        logger.warn("Finished loading [{}] stopwords ", lang);
                    } catch (IOException e) {
                        logger.error("Can't load stopwords for lang = " + lang);
                        stopWords.put(lang, StopWords.EMPTY_STOPWORDS);
                    }
                });

            } else {
                stopWords.put(lang, StopWords.EMPTY_STOPWORDS);
            }

            // TOKENIZERS
            if(hasFeature(lang, "tokenizer")) {
                try {
                    logger.warn("Loading [{}] tokenizer", lang);
                    tokenizers.put(lang, new OpenTokenizer(new File(langDir, lang + "-token.bin")));
                } catch (Exception e) {
                    tokenizers.put(lang, getWordTokenizer(lang));
                }
            } else {
                tokenizers.put(lang, getWordTokenizer(lang));
            }

            entityPlugins.put(lang, synchronizedList(new ArrayList<>()));

            // OPEN NLP NER
            if(hasFeature(lang, "ner") || hasFeature(lang, "ner-person")) {
                executor.execute(() -> {
                    File modelFile = new File(langDir, lang + "-ner-person.bin");
                    try {
                        logger.info("Loading [{}] PERSON NER", lang);
                        entityPlugins.get(lang).add(new OpenNER("person", modelFile));
                        logger.warn("Finished loading [{}] PERSON NER", lang);
                    } catch (IOException e) {
                        logger.warn("Can't load OpenNER: {}", modelFile);
                    }
                });
            }

            if(hasFeature(lang, "ner") || hasFeature(lang, "ner-date")) {
                executor.execute(() -> {
                    File modelFile = new File(langDir, lang + "-ner-date.bin");
                    try {
                        logger.info("Loading [{}] DATE NER", lang);
                        entityPlugins.get(lang).add(new OpenNER("date", modelFile));
                        logger.warn("Finished loading [{}] DATE NER", lang);
                    } catch (IOException e) {
                        logger.warn("Can't load OpenNER: {}", modelFile);
                    }
                });
            }

            if(hasFeature(lang, "ner") || hasFeature(lang, "ner-time")) {
                executor.execute(() -> {
                    File modelFile = new File(langDir, lang + "-ner-time.bin");
                    try {
                        logger.info("Loading [{}] TIME NER", lang);
                        entityPlugins.get(lang).add(new OpenNER("time", modelFile));
                        logger.warn("Finished loading [{}] TIME NER", lang);
                    } catch (IOException e) {
                        logger.warn("Can't load OpenNER: {}", modelFile);
                    }
                });
            }

            if(hasFeature(lang, "ner") || hasFeature(lang, "ner-location")) {
                executor.execute(() -> {
                    File modelFile = new File(langDir, lang + "-ner-location.bin");
                    try {
                        logger.info("Loading [{}] LOCATION NER", lang);
                        entityPlugins.get(lang).add(new OpenNER("location", modelFile));
                        logger.warn("Finished loading [{}] LOCATION NER", lang);
                    } catch (IOException e) {
                        logger.warn("Can't load OpenNER: {}", modelFile);
                    }
                });
            }

            // DEFAULT PLUGINS
            entityPlugins.get(lang).add(new TrueFalse());
            entityPlugins.get(lang).add(new RegexFinder(RegexUtil.NUMBER, "NUMBER"));
            entityPlugins.get(lang).add(new RegexFinder(RegexUtil.EMAIL, "EMAIL"));
            entityPlugins.get(lang).add(new RegexFinder(RegexUtil.URL, "URL"));
            entityPlugins.get(lang).add(new EmoFinder());
            entityPlugins.get(lang).add(new CnnHeadlineFinder());

            // REPLY PLUGINS
            replyPlugins.put(lang, synchronizedList(new ArrayList<>()));
            replyPlugins.get(lang).add(new FlipReplyPlugin());


            // GENERALIZERS
            if(hasFeature(lang, "synonyms")) {
                executor.execute(() -> {
                    logger.info("Loading [{}] synonyms", lang);
                    File modelFile = new File(langDir, lang + "-synonyms.csv");
                    try {
                        generalizers.put(lang, new RelatedWordsGeneralizer(new RelatedWords(modelFile, getNormalizer(lang))));
                    } catch (IOException e) {
                        logger.warn("Can't load synonyms: {}", modelFile);
                    }
                    logger.warn("Finished loading [{}] synonyms", lang);
                });
            }

            try {
                entityPlugins.get(lang).add(new DateTimeFinder(new File(langDir, "DATETIME.csv" ), getNormalizer(lang), getWordTokenizer(lang)));
            } catch (IOException ignored) {}

            try {
                entityPlugins.get(lang).add(new FlexNumberFinder(new File(new File(langDir, "datasheet"), "CARDINALNUMBER.csv"), getNormalizer(lang), getWordTokenizer(lang)));
            } catch (IOException ignored) {}

            // SUBSTITUTIONS
            if("en".equals(lang)){
                Map<String, String> substMap = new HashMap<>();
                substMap.put("am", "are");
                substMap.put("was", "were");
                substMap.put("i", "you");
                substMap.put("i'd", "you would");
                substMap.put("i've", "you have");
                substMap.put("i'll", "you will");
                substMap.put("my", "your");
                substMap.put("are", "am");
                substMap.put("you've", "I have");
                substMap.put("you'll", "I will");
                substMap.put("your", "my");
                substMap.put("yours", "mine");
                substMap.put("you", "me");
                substMap.put("me", "you");
                substitutions.put(lang, new Substitution2Way(substMap, true));
            } else {
                substitutions.put(lang, new Substitution2Way());
            }
        }



        // Wait until all data is loaded
        if(!async){
            executor.shutdown();
            try {
                executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            } catch (InterruptedException ignored) {}
        }

    }

    public void addReplyPlugin(String lang, ReplyPlugin replyPlugin){
        if(!replyPlugins.containsKey(lang))
            replyPlugins.put(lang, new ArrayList<>());

        replyPlugins.get(lang).add(replyPlugin);
    }

    /**x
     * List of datasheet names: e.g. "en/CITY" (with lang folder, without extension)
     * @param lang language
     * @return list of names in /data/nlp/{lang}/datasheet
     */
    public List<String> getFileDataSheetNames(String lang){
        List<String> names = new ArrayList<>();
        File langDir = new File(baseDir, lang);

        if(hasFeature(lang, "datasheet")) {
            File[] datasheets = new File(langDir, "datasheet").listFiles((dir, name) -> name.endsWith(".csv"));
            if (datasheets != null) {
                for (File nerFile : datasheets) {
                    String name = nerFile.getName();
                    if (name.contains("."))
                        name = name.substring(0, name.lastIndexOf('.'));

                    names.add(lang + "/" + name);
                }
            }
        }
        return names;
    }

    public String loadFileDataSheet(String name){
        File file = new File(baseDir, name.replaceFirst("/", "/datasheet/") + ".csv");
        String data = FileUtils.readAllText(file, Charset.forName("UTF-8"));
        loadDataSheet(name, data);
        return data;
    }

    public void loadDataSheet(String name, String data){
        String[] parts = name.split("/");
        if(parts.length != 2) {
            logger.error("The data sheet with name '{}' is skipped, since it does not follow the  'lang/NAME' format.", name);
            return;
        }
        String lang = parts[0];
        String sheet = parts[1];

        boolean partialKeys = sheet.contains("SEARCH") || sheet.contains("BUSCA");
        try {
            loadDataSheet(lang, sheet, new DataSheet(sheet, data, stringNormalizers.get(lang), new SplittingWordTokenizer(),
                    (partialKeys ? getStopWords(lang) : StopWords.EMPTY_STOPWORDS), partialKeys));
        } catch (IOException e) {
            logger.error("Can't load datasheet: {}", name);
        }
    }


    public void loadDataSheet(String lang, String sheetName, DataSheet dataSheet){
        entityPlugins.putIfAbsent(lang, synchronizedList(new ArrayList<>()));

        logger.warn("Loading datasheet '{}/{}'", lang, sheetName);
        List<EntityPlugin> langPlugins = entityPlugins.get(lang);
        // remove old version
        langPlugins.removeIf(plugin -> sheetName.equals(plugin.description()));
        // add new version
        langPlugins.add(dataSheet);
    }

    public DataSheet getDataSheet(String lang, String sheetName){
        List<EntityPlugin> langPlugins = entityPlugins.getOrDefault(lang, ImmutableList.of());
        final Optional<EntityPlugin> plugin = langPlugins.stream().filter(ep -> sheetName.equals(ep.description())).findFirst();
        return (DataSheet) plugin.orElse(null);
    }


    public String applyReplyVariables(String txt) {
        return replyVariables.apply(txt);
    }

    public Map<String, String> getReplyVariables(String name) {
        return replyVariables.get(name);
    }

    public Nlp setReplyVariables(String name, String tsv) {
        this.replyVariables.putRewrites(name, tsv);
        return this;
    }

    public Nlp setReplyVariables(String name, Map<String, String> replyVariables) {
        this.replyVariables.putRewrites(name, replyVariables);
        return this;
    }

    public Map<String, StopWords> getStopWords() {
        return stopWords;
    }

    public Map<String, StringNormalizer> getNormalizers() {
        return stringNormalizers;
    }

    public Map<String, Tokenizer> getTokenizers() {
        return tokenizers;
    }

    public Map<String, SentenceSplitter> getSentenceDetectors() {
        return sentenceSplitters;
    }

    public Map<String, List<EntityPlugin>> getEntityPlugins() {
        return entityPlugins;
    }

    public List<EntityPlugin> getEntityPlugins(ScriptConfig config){
        return getEntityPlugins(config.getLanguages());
    }

    public List<EntityPlugin> getEntityPlugins(List<String> langs){
        List<EntityPlugin> result = new ArrayList<>();
        for (String lang : langs) {
            result.addAll(entityPlugins.getOrDefault(lang, ImmutableList.of()));
        }
        return result;
    }

    public Generalizer getGeneralizer(String lang){
        return generalizers.getOrDefault(lang, Generalizer.DEFAULT);
    }

    public Generalizer getGeneralizer(ScriptConfig config){
        return getGeneralizer(config.getLanguages());
    }

    public Generalizer getGeneralizer(List<String> langs){
        FirstOfGeneralizer generalizer = new FirstOfGeneralizer();
        for (String lang : langs) {
            generalizer.add(getGeneralizer(lang));
        }
        return generalizer;
    }


    public StopWords getStopWords(String lang){
        return stopWords.getOrDefault(lang, StopWords.EMPTY_STOPWORDS);
    }

    public Map<String, StopWords> getStopWords(ScriptConfig config){
        return getStopWords(config.getLanguages());
    }

    public Map<String, StopWords> getStopWords(List<String> langs){
        Map<String, StopWords> result = new HashMap<>();
        for (String lang : langs) {
            result.put(lang, getStopWords(lang));
        }
        return result;
    }

    public Substitution2Way getSubstitution(String lang){
        return substitutions.getOrDefault(lang, new Substitution2Way());
    }

    public Substitution2Way getSubstitution(ScriptConfig config){
        for (String lang : config.getLanguages()) {
            if(substitutions.containsKey(lang)){
                return substitutions.get(lang);
            }
        }
        return new Substitution2Way();
    }

    public StringNormalizer getNormalizer(ScriptConfig config){
        return getNormalizer(config.getLanguages());
    }

    public StringNormalizer getNormalizer(String lang){
        return stringNormalizers.getOrDefault(lang, StringNormalizers.DEFAULT);
    }

    private StringNormalizer getNormalizer(List<String> langs){
        // Compose normalizers by language
        StringNormalizer result = StringNormalizer.IDENTITY;
        for (String lang : langs) {
            result = result.andThen(getNormalizer(lang));
        }
        return result;
    }

    /**
     * Creates a list of normalizers. It uses a list of names of the normalization steps, defined for the
     * given language {@link LanguageProcessingConfig#getNormalizers()}. Let's say it is ["default", "stem", " metaphone"].
     * The returned list of {@link StringNormalizer}s is constructed by combining all corresponding normalizers,
     * upto index i. So here: <code>
     *     [Default(), Stem(Default()), Metaphone(Stem(Default()))]
     * </code>
     *
     * @param lang the given language code
     * @return a list of less and less conservative normalizers.
     */
    private List<StringNormalizer> createNormalizers(String lang){
        List<StringNormalizer> result = new ArrayList<>();
        // Compose normalizers by type
        LanguageProcessingConfig lpc = languageProcessing.get(lang);
        if(lpc == null) {
            result.add(StringNormalizers.DEFAULT);
            return result;
        }
        StringNormalizer current = StringNormalizer.IDENTITY;
        for (String name : lpc.getNormalizers()) {
            if("default".equals(name)){
                current = current.andThen(StringNormalizers.DEFAULT);
            } else if("lowercase".equals(name)){
                current = current.andThen(StringNormalizers.LOWER_CASE);
            } else if("removeaccents".equals(name)){
                current = current.andThen(StringNormalizers.NO_ACCENTS);
            } else if("alphanumeric".equals(name)){
                current = current.andThen(StringNormalizers.ALPHA_NUM_ONLY);
            } else if("stem".equals(name)){
                StringNormalizer stemmerNomalizer = StringNormalizer.IDENTITY;

                try {
                    stemmerNomalizer = new LuceneStemmer(lang, false).compose(stemmerNomalizer);
                } catch (IllegalArgumentException e){
                    try {
                        stemmerNomalizer = new SafeSnowballStemmer(lang).compose(stemmerNomalizer);
                    } catch (IllegalArgumentException ignored){
                        logger.error("No 'stem' normalizer supported for language: {}", lang);
                    }
                }

                current = current.andThen(stemmerNomalizer);
            } else if("stemlight".equals(name)){
                StringNormalizer stemmerNomalizer = StringNormalizer.IDENTITY;

                try {
                    stemmerNomalizer = new LuceneStemmer(lang, true).compose(stemmerNomalizer);
                } catch (IllegalArgumentException e){
                    try {
                        stemmerNomalizer = new SnowballStemmer(lang).compose(stemmerNomalizer);
                    } catch (IllegalArgumentException ignored){}
                }

                current = current.andThen(stemmerNomalizer);
            } else if ("kstem".equals(name)) {
                current = current.andThen(new KStem());
            } else if ("porter".equals(name)) {
                current = current.andThen(new PorterStemmer());
            } else if ("snowball".equals(name)) {
                current = current.andThen(new SafeSnowballStemmer(lang));
            } else if ("esstem".equals(name)) {
                current = current.andThen(new SpanishStemmer());
            } else if ("metaphone".equals(name)) {
                switch (lang) {
                    case "es": current = current.andThen(new MetaphoneES()); break;
                    case "nl": current = current.andThen(new MetaphoneNL()); break;
                    default: current = current.andThen(new Metaphone()); break;
                }
            } else if ("metaphone2".equals(name)) {
                current = current.andThen(new Metaphone2());
            } else if ("metaphone3".equals(name)) {
                current = current.andThen(new Metaphone3());
            } else if ("metaphone3long".equals(name)) {
                current = current.andThen(new Metaphone3Long(4));
            } else if ("soundex".equals(name)) {
                current = current.andThen(new Soundex2FR());
            } else if ("sortinner".equals(name)) {
                current = current.andThen(StringNormalizers.SORTED_WITHIN);
            }
            result.add(current);
        }
        return result;
    }

    public SentenceSplitter getSentenceSplitter(ScriptConfig config){
        return sentenceSplitters.getOrDefault(config.getLanguages().get(0), new RegexSentenceSplitter());
    }

    public SentenceSplitter getSentenceSplitter(String lang){
        return sentenceSplitters.getOrDefault(lang, new RegexSentenceSplitter());
    }

    public Tokenizer getWordTokenizer(ScriptConfig config){
        return getWordTokenizer(config.getLanguages().get(0));
    }

    public Tokenizer getWordTokenizer(String lang){
        LanguageProcessingConfig lpc = languageProcessing.get(lang);
        if(lpc == null){
            return new MatchingWordTokenizer();
        }
        switch (lpc.getWordTokenizer()){
            case "grapheme": return new GraphemeTokenizer();
            case "splitting": return new SplittingWordTokenizer();
            case "default": return tokenizers.getOrDefault(lang, new MatchingWordTokenizer());
            default: return new MatchingWordTokenizer(); // "matching"
        }
    }

    public Map<String, List<ReplyPlugin>> getReplyPlugins() {
        return replyPlugins;
    }

    public Map<String, Map<String, Object>> info(){
        Map<String, Map<String, Object>> result = new HashMap<>();
        for (String lang : languages) {
            String displayLanguage = Locale.forLanguageTag(lang).getDisplayLanguage();
            final List<String> features = languageProcessing.get(lang).getFeatures();
            LinkedHashMap<String, Object> modelInfo = new LinkedHashMap<>();
            modelInfo.put("language code", lang);
            modelInfo.put("enabled features", features.isEmpty() ? "ALL" : features);
            modelInfo.put("word normalization", languageProcessing.get(lang).getNormalizers());
            modelInfo.put("word tokenizer", tokenizers.get(lang).description());
            modelInfo.put("sentence splitter", getSentenceSplitter(lang).description());
            modelInfo.put("stop words", stopWords.get(lang).description());
            modelInfo.put("entity recognition", entityPlugins.get(lang).stream().map(EntityPlugin::description).collect(Collectors.toList()));
            result.put(displayLanguage, modelInfo);
        }
        return result;
    }
}
