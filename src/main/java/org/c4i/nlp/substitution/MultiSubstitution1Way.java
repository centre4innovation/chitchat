package org.c4i.nlp.substitution;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A collection of string substitutions.
 * All replacements will be applied, with the assumption that the replacement values are not also keys.
 * see: https://stackoverflow.com/questions/1326682/java-replacing-multiple-different-substring-in-a-string-at-once-or-in-the-most
 * @author Arvid Halma
 */
public class MultiSubstitution1Way implements Substitution {
    private Pattern pattern = Pattern.compile("(?U)\\$([\\w\\-]+)\\.([\\w\\-]+)");


    private Map<String, Map<String, String>> nameToRewrites;

    public MultiSubstitution1Way() {
        nameToRewrites = new LinkedHashMap<>();
    }

    public void putRewrites(String name, String tsv){
        final Map<String, String> rewrites = new LinkedHashMap<>();
        Arrays.stream(tsv.split("\n"))
                .map(line -> line.split("\t"))
                .filter(row -> row.length > 1)
                .forEach(row -> rewrites.put(row[0].trim(), row[1].trim()));
        putRewrites(name, rewrites);
    }

    public void putRewrites(String name, Map<String, String> rewrites){
        nameToRewrites.put(name, rewrites);
    }

    public Map<String, String> get(String name){
        return nameToRewrites.get(name);
    }

    public Set<String> names(){
        return nameToRewrites.keySet();
    }

    @Override
    public String apply(String text){
        if(nameToRewrites.isEmpty()) {
            return text;
        }
        Matcher matcher = pattern.matcher(text);

        StringBuffer sb = new StringBuffer();
        while(matcher.find()) {
            String datasetName = matcher.group(1);
            String variable = matcher.group(2);

            Map<String, String> rewrites = nameToRewrites.get(datasetName);
            if(rewrites == null) {
                matcher.appendReplacement(sb, Matcher.quoteReplacement(matcher.group(0)));
                continue;
            }

            String value = rewrites.get(variable);
            if(value != null){
                matcher.appendReplacement(sb, Matcher.quoteReplacement(value));
            } else {
                matcher.appendReplacement(sb, Matcher.quoteReplacement(matcher.group(0)));
            }

        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    @Override
    public Map<String, String> asMap() {
        return nameToRewrites.values().stream().flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue));
    }
}
