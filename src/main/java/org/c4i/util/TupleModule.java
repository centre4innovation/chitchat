package org.c4i.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.c4i.graph.GraphModule;
import org.c4i.graph.PropVertex;
import org.c4i.graph.PropWeightedEdge;
import org.jgrapht.Graph;

import java.io.IOException;

/**
 * {@link Tuple.Tuple2} to JSON
 * @author Arvid Halma
 */

public class TupleModule extends SimpleModule {

    public TupleModule() {
        super("TupleModule", Version.unknownVersion());
        addSerializer(Tuple.Tuple2.class, new TupleSerializer());
    }

    public static class TupleSerializer extends StdSerializer<Tuple.Tuple2> {

        public TupleSerializer() {
            this(null);
        }

        public TupleSerializer(Class<Tuple.Tuple2> t) {
            super(t);
        }

        @Override
        public void serialize(
                Tuple.Tuple2 t, JsonGenerator jgen, SerializerProvider provider)
                throws IOException {
            jgen.writeStartArray();
            jgen.writeObject(t.a);
            jgen.writeObject(t.b);
            jgen.writeEndArray();
        }
    }

}

