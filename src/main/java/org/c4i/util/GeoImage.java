package org.c4i.util;

import com.google.common.collect.ImmutableList;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Create a map with markers.
 * @author Arvid Halma
 * @version 06-03-20
 */
public class GeoImage {
    private Image background;
    private Point2D.Double ne, se, sw, nw;

    public GeoImage(File backgroundImg, Point2D.Double ne, Point2D.Double se, Point2D.Double sw, Point2D.Double nw) throws IOException {
        this(ImageIO.read(backgroundImg), ne, se, sw, nw);
    }

    public GeoImage(Image background, Point2D.Double ne, Point2D.Double se, Point2D.Double sw, Point2D.Double nw) {
        this.background = background;
        this.ne = ne;
        this.se = se;
        this.sw = sw;
        this.nw = nw;
    }


    public Point2D.Double toBaryCoordinates(Point2D.Double lngLat){
        //          w v1
        // b = nw +-----+ ne = c
        //        |   / |
        //   v v0 |  /  | k f0
        //        | /   |
        // a = sw +-----+ se = d
        //          l f1

        // Compute barycentric coordinates (u, v, w) for
        // point p with respect to triangle (a, b, c)
        final Point2D.Double
                v0 = minus(sw, nw),
                v1 = minus(ne, nw),
                v2 = minus(lngLat, nw);
        double den = v0.x * v1.y - v1.x * v0.y;
        double v = (v2.x * v1.y - v1.x * v2.y) / den;
        double w = (v0.x * v2.y - v2.x * v0.y) / den;
        double u = 1.0 - v - w;

        // This directly solves the 2x2 linear system
        // v v0 + w v1 = v2

        if(v + w > 1 ){
            // try other triangle
            final Point2D.Double
                    f0 = minus(ne, se),
                    f1 = minus(sw, se),
                    f2 = minus(lngLat, se);

            double den2 = f0.x * f1.y - f1.x * f0.y;
            double k = (f2.x * f1.y - f1.x * f2.y) / den2;
            double l = (f0.x * f2.y - f2.x * f0.y) / den2;
            double m = 1.0 - k - l;
            return new Point2D.Double(1-l, 1-k);

        } else {
            return new Point2D.Double(w, v);
        }
    }

    public void renderPng(File target, List<Point2D.Double> lngLatPoints) throws IOException {
        BufferedImage image = render(lngLatPoints);
        ImageIO.write(image, "PNG", target);
    }

    public BufferedImage render(List<Point2D.Double> lngLatPoints){
        int w = background.getWidth(null);
        int h = background.getHeight(null);
        BufferedImage im = new BufferedImage(w, h, BufferedImage.TYPE_3BYTE_BGR);
        final Graphics2D g = (Graphics2D) im.getGraphics();
        g.drawImage(background, 0, 0, null);

        double r = 15;
        for (Point2D.Double pt : lngLatPoints) {
            // draw marker
            final Point2D.Double baryPt = toBaryCoordinates(pt);
            renderDotMarker(g, (baryPt.y)* w, (1.0-baryPt.x)  * h, r);
        }
        g.dispose();

        return im;
    }

    private void renderDotMarker(Graphics2D g, double x, double y, double r) {
        double r2 = 0.5 * r;
        final Ellipse2D circle = new Ellipse2D.Double(x - r2, y - r2, r, r);
        final Paint lightRed = Color.RED.brighter();
        final Paint darkRed = Color.RED.darker();
        g.setPaint(lightRed);
        g.fill(circle);
        g.setPaint(darkRed);
        g.draw(circle);
    }

    private static Point2D.Double minus(Point2D.Double a, Point2D.Double b){
        return new Point2D.Double(a.x - b.x, a.y - b.y);
    }

    public static void main(String[] args) throws IOException {
        BufferedImage img = ImageIO.read(new File("src/main/resources/assets/assets/usr/wfp/honduras.png"));

        final Point2D.Double ne = new Point2D.Double(8, 4);
        final Point2D.Double se = new Point2D.Double(10, 3);
        final Point2D.Double sw = new Point2D.Double(5, 2);
        final Point2D.Double nw = new Point2D.Double(6, 5);
        final GeoImage geoImage = new GeoImage(img, ne, se, sw, nw);

        System.out.println("geoImage.toBaryCoordinates(nw) = " + geoImage.toBaryCoordinates(nw));
        System.out.println("geoImage.toBaryCoordinates(ne) = " + geoImage.toBaryCoordinates(ne));
        System.out.println("geoImage.toBaryCoordinates(sw) = " + geoImage.toBaryCoordinates(sw));
        System.out.println("geoImage.toBaryCoordinates(se) = " + geoImage.toBaryCoordinates(se));

        final GeoImage hnImage = new GeoImage(img,
                new Point2D.Double(-89.31884765625, 15.987734284909871),
                new Point2D.Double(-83.82568359375001, 15.987734284909871),
                new Point2D.Double(-83.82568359375001, 13.33083009512624),
                new Point2D.Double(-89.31884765625, 13.33083009512624)
        );

        hnImage.renderPng(new File("hnMapTest.png"), ImmutableList.of(new Point2D.Double(-87.21036085406735, 14.090866604776622)));
    }
}
