package org.c4i.util;


import java.util.Objects;

/**
 * A heterogeneous container of fixed length.
 *
 * @author Arvid Halma
 * @version 15-feb-2010
 * Time: 17:57:02
 */
public class Tuple{

    private Tuple(){}

    public static <A,B> Tuple2<A,B> tuple(A a, B b){
        return new Tuple2<A,B>(a,b);
    }

    public static <A,B,C> Tuple3<A,B,C> tuple(A a, B b, C c){
        return new Tuple3<A,B,C>(a,b,c);
    }

    public static <A,B,C,D> Tuple4<A,B,C,D> tuple(A a, B b, C c, D d){
        return new Tuple4<A,B,C,D>(a,b,c,d);
    }

    /**
     * A heterogeneous container for 2 elements.
     * @param <A> type of first element.
     * @param <B> type of second element.
     */
    public static final class Tuple2<A,B> {
        public A a;
        public B b;

        public Tuple2() {}

        public Tuple2(A a, B b) {
            this.a = a;
            this.b = b;
        }

        @Override
        public String toString() {
            return "<"+a+", "+b+">";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Tuple2)) return false;
            Tuple2<?, ?> tuple2 = (Tuple2<?, ?>) o;
            return Objects.equals(a, tuple2.a) &&
                    Objects.equals(b, tuple2.b);
        }

        @Override
        public int hashCode() {
            int result = a != null ? a.hashCode() : 0;
            result = 31 * result + (b != null ? b.hashCode() : 0);
            return result;
        }
    }

    /**
     * A heterogeneous container for 3 elements.
     * @param <A> type of first element.
     * @param <B> type of second element.
     * @param <C> type of third element.
     */
    public static final class Tuple3<A,B,C> {
        public A a;
        public B b;
        public C c;

        public Tuple3() {}

        public Tuple3(A a, B b, C c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        @Override
        public String toString() {
            return "<"+a+", "+b+", "+c+">";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Tuple3)) return false;
            Tuple3<?, ?, ?> tuple3 = (Tuple3<?, ?, ?>) o;
            return Objects.equals(a, tuple3.a) &&
                    Objects.equals(b, tuple3.b) &&
                    Objects.equals(c, tuple3.c);
        }

        @Override
        public int hashCode() {
            int result = a != null ? a.hashCode() : 0;
            result = 31 * result + (b != null ? b.hashCode() : 0);
            result = 31 * result + (c != null ? c.hashCode() : 0);
            return result;
        }
    }

    /**
     * A heterogeneous container for 4 elements.
     * @param <A> type of first element.
     * @param <B> type of second element.
     * @param <C> type of third element.
     * @param <D> type of fourth element.
     */
    public static final class Tuple4<A,B,C,D> {
        public A a;
        public B b;
        public C c;
        public D d;

        public Tuple4() {
        }

        public Tuple4(A a, B b, C c, D d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }

        @Override
        public String toString() {
            return "<"+a+", "+b+", "+c+", "+d+">";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Tuple4)) return false;
            Tuple4<?, ?, ?, ?> tuple4 = (Tuple4<?, ?, ?, ?>) o;
            return Objects.equals(a, tuple4.a) &&
                    Objects.equals(b, tuple4.b) &&
                    Objects.equals(c, tuple4.c) &&
                    Objects.equals(d, tuple4.d);
        }

        @Override
        public int hashCode() {
            int result = a != null ? a.hashCode() : 0;
            result = 31 * result + (b != null ? b.hashCode() : 0);
            result = 31 * result + (c != null ? c.hashCode() : 0);
            result = 31 * result + (d != null ? d.hashCode() : 0);
            return result;
        }
    }
}