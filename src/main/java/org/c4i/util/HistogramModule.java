package org.c4i.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * {@link Histogram} to JSON
 * @author Arvid Halma
 */
public class HistogramModule extends SimpleModule {

    public HistogramModule() {
        super("HistogramModule", Version.unknownVersion());
        addSerializer(Histogram.class, new HistogramSerializer());
    }

    public static class HistogramSerializer extends StdSerializer<Histogram> {

        public HistogramSerializer() {
            this(null);
        }

        public HistogramSerializer(Class<Histogram> t) {
            super(t);
        }

        @Override
        public void serialize(
                Histogram histogram, JsonGenerator jgen, SerializerProvider provider)
                throws IOException {
            jgen.writeObject(histogram.asMap());
        }
    }
}