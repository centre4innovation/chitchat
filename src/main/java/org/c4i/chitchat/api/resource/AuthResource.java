package org.c4i.chitchat.api.resource;

import io.dropwizard.auth.Auth;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.c4i.chitchat.api.sec.KeycloakUser;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.IDToken;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import java.util.*;


@Path("/auth")
@Api("/auth")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class AuthResource {

    @Context
    private HttpServletRequest request;

    @GET
    @Path("/userInfo")
    @ApiOperation(
            value = "get user name",
            response = HashMap.class)
    public HashMap<String, String> getUserInfo(@Context SecurityContext context) throws ServletException {
        if (context.getUserPrincipal() != null) {
            @SuppressWarnings("unchecked")
            KeycloakPrincipal<KeycloakSecurityContext> kp = (KeycloakPrincipal<KeycloakSecurityContext>) context.getUserPrincipal();

            IDToken idt = kp.getKeycloakSecurityContext().getIdToken();
            HashMap<String,String> userMap = new HashMap<>();
            userMap.put("Name", idt.getName());
            userMap.put("PreferredUsername", idt.getPreferredUsername());
            userMap.put("Email", idt.getEmail()); /*
            userMap.put("Permissions", kp.getKeycloakSecurityContext().getAuthorizationContext().getPermissions().toString());
            userMap.put("Realm", kp.getKeycloakSecurityContext().getRealm());
*/
            return userMap;
        } else {
            return null;
        }

    }

    @GET
    @Path("/roles")
    @ApiOperation(
            value = "get user roles",
            response = Set.class)
    public Set<String> getUserRoles(@Context SecurityContext context) {
        KeycloakPrincipal<KeycloakSecurityContext> kp = (KeycloakPrincipal<KeycloakSecurityContext>) context.getUserPrincipal();
        if (kp != null) {
            Set<String> roles = kp.getKeycloakSecurityContext().getToken().getRealmAccess().getRoles();
//            roles.remove("user");
            return roles;
        } else {
            return Collections.emptySet();
        }
    }

    // See https://www.keycloak.org/docs/latest/securing_apps/index.html#_multi_tenancy For multi-realm support

    @POST
    @Path("/logout")
    @RolesAllowed("ADMIN")
    @ApiOperation(
            value = "logout user",
            response = Boolean.class )
    public Boolean logout(@Context SecurityContext context) throws ServletException {
        if (context.getUserPrincipal() != null) {
            request.logout();
            return true;
        } else {
            return false;
        }
    }

}
