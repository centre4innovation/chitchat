package org.c4i.chitchat.api.resource;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;
import org.c4i.chitchat.api.Config;
import org.c4i.nlp.chat.ConversationListener;
import org.c4i.nlp.chat.EvalStateManager;
import org.c4i.chitchat.api.model.TextDoc;
import org.c4i.nlp.chat.Message;
import org.c4i.nlp.chat.ScriptBot;
import org.c4i.nlp.match.Compiler;
import org.c4i.nlp.match.Eval;
import org.c4i.nlp.match.Result;
import org.c4i.nlp.match.Script;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * The ChatResourceBase manages how incoming messages are mapped with the right script to a reply.
 * It takes care of mapping an incoming message to the entire conversation, using a (on demand) pre-compiled script,
 * to generate a reply. Event hooks ({@link ConversationListener}s) are also called appropriately.
 * @author Arvid Halma
 */
public class ChatResourceBase {
    private final Logger logger = LoggerFactory.getLogger(ChatResourceBase.class);

    protected Config config;

    protected String channelBaseName; // e.g. "chitchat" or "fb"
    protected Cache<String, ScriptBot> chatBots; // script name to ScriptBot
    protected Map<String, EvalStateManager> evalStateManagerMap; // channel name to EvalStateManager
    protected List<ConversationListener> listeners;

    /**
     * Create a
     * @param channelBaseName the basic chatbot platform identifier, such as "chistchat" or "fb"
     * @param config glabal application config
     * @param listeners conversation event hooks
     */
    public ChatResourceBase(String channelBaseName, Config config, List<ConversationListener> listeners) {
        this.channelBaseName = channelBaseName;
        this.config = config;

        this.chatBots = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterAccess(60, TimeUnit.SECONDS)
                .build();

        this.evalStateManagerMap = new ConcurrentHashMap<>();

        this.listeners = listeners;
    }

    /**
     * Convert script name to channel name
     * @param scriptName
     * @return channelBaseName + "-" + scriptName
     */
    public String channelName(String scriptName){
        return channelBaseName + "-" + scriptName;
    }

    /**
     * Clears all script cache. After incoming messages, reply() will have to compile the script from source again.
     */
    public void resetScripts(){
        chatBots.invalidateAll();
    }

    /**
     * Clears the script cache. After incoming messages, reply() will have to compile the script from source again.
     * @param scriptName the name of the script without the channel prefix.
     */
    public void resetScript(String scriptName){
        chatBots.invalidate(scriptName);
    }

    /**
     * Clears all conversation states.
     */
    public void resetConversations() {
        evalStateManagerMap.values().forEach(EvalStateManager::reset);
    }

    /**
     * Number of current conversations.
     * @return number of chat sessions.
     */
    public long sessionCount(){
        return evalStateManagerMap.size();
    }

    /**
     * Number of chatbot scripts used.
     * @return the scripts used to generate replies.
     */
    public long botCount(){
        return chatBots.size();
    }

    /**
     * Create a result (with replies) from an incoming message.
     * @param scriptName the script name of the source that is used.
     * @param message the incoming message from a remote user
     * @return
     * @throws IOException
     */
    public Result reply(String scriptName, Message message) {
        // get the script
        ScriptBot scriptBot;
        try {
            scriptBot = chatBots.get(scriptName, () -> {

                TextDoc src = config.dao.textDocDao.getLastUpdatedByName("ccs", scriptName);
                if (src != null) {
                    try {
                        Script script = Compiler.compile(src.getBody(), config.getNlp());
                        return new ScriptBot(script, config.getNlp());
                    } catch (Exception e) {
                        throw new WebApplicationException("The script \"" + scriptName + "\" contains an error: " + e.getMessage());
                    }
                } else {
                    throw new WebApplicationException("There is no script with name: " + scriptName);
                }

            });
        } catch (ExecutionException e) {
            throw new WebApplicationException("There is a problem loading script: " + scriptName, e);
        }

        // prepare message
        String channel = channelName(scriptName);
        message.setIncoming(true);
        message.setChannel(channel);
        message.setTimestamp(DateTime.now()); // override client timestamp

        // get the conversation state
        EvalStateManager chairman = evalStateManagerMap.computeIfAbsent(channel, s ->
                new EvalStateManager(10000,
                        config.appConfig.getMaxConversationTimeInterval().toSeconds(), TimeUnit.SECONDS,
                        ImmutableList.of(config.databaseResource)));

        Eval.EvalState state = chairman.update(scriptBot.getScript(), message);

        listeners.forEach(cl -> cl.onReceive(state.conversation, message));

        Result result = scriptBot.reply(state);
        result.setWarnings(scriptBot.getScript().getWarnings());

        // update conversation
        for (Message m : result.getReplies()) {
            chairman.update(scriptBot.getScript(), m);
        }
        listeners.forEach(cl -> cl.onSend(state.conversation, result));

        // Remove unnecessary data
        result.setProfile(null);
        result.setHighlight(null);
        result.setWarnings(null);

        return result;
    }

    /**
     * Clears conversation state for a user-bot-script combination.
     * @param senderId userId or botId
     * @param recipientId userId or botId
     * @param channel script name
     * @return
     */
    public void clearConversation(String senderId, String recipientId, String channel) {
        EvalStateManager stateManager = evalStateManagerMap.get(channel);
        stateManager.reset(senderId, recipientId, channel);
    }


}
