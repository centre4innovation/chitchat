package org.c4i.chitchat.api.resource;

import io.swagger.annotations.Api;
import org.c4i.chitchat.api.Config;
import org.c4i.chitchat.api.view.PageParams;
import org.c4i.chitchat.api.view.PageView;
import org.c4i.chitchat.api.view.SimpleView;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;

/**
 * User interface server side rendering
 * @version 24-10-16
 * @author Arvid Halma
 */

@Path("/ui")
@Api("/ui")
@Consumes({MediaType.APPLICATION_JSON})
@Produces(MediaType.TEXT_HTML)
public class FrontEndResource {
    private Config config;

    public FrontEndResource(Config config) {
        this.config = config;
    }

    @Context
    private HttpServletRequest request;

    @GET
    public PageView base(){
        throw redirect("/api/v1/ui/dashboard");
    }

    @GET
    @Path("chatclient")
    public SimpleView chatclient(@QueryParam("script") @DefaultValue("") String script){
        return script.toLowerCase().contains("cmam") ?
                new SimpleView("chatclient-sn.ftl") :
                new SimpleView("chatclient.ftl");
    }

    @GET
    @Path("dashboard")
    public PageView dashboard(){
        return new PageView(new PageParams("dashboard.ftl", config));
    }


    @GET
    @Path("tutorial")
    public PageView tutorial(){
        return new PageView (new PageParams("tutorial.ftl", config));
    }

    @GET
    @Path("create/survey")
    public PageView scriptBuilder(){
        return new PageView (new PageParams("create-survey.ftl", config));
    }

    @GET
    @Path("create/match")
    public PageView scriptMatch() { return new PageView (new PageParams("create-match.ftl", config));
    }

    @GET
    @Path("create/script")
    public PageView scriptChat(){
        return new PageView (new PageParams("create-script.ftl", config));
    }

    @GET
    @Path("create/qa")
    public PageView scriptLevi(){
        return new PageView (new PageParams("create-qa.ftl", config));
    }

    @GET
    @Path("data/variables")
    @RolesAllowed("user")
    public PageView variables() {
        return new PageView (new PageParams("data-variables.ftl", config));
    }

    @GET
    @Path("data/sheets")
    @RolesAllowed("user")
    public PageView datasheets() {
        return new PageView (new PageParams("data-sheets.ftl", config));
    }

    @GET
    @Path("data/export")
    @RolesAllowed("user")
    public PageView export() {
        return new PageView (new PageParams("data-export.ftl", config));
    }

    @GET
    @Path("analyse/overview")
    @RolesAllowed("user")
    public PageView overview() {
        return new PageView (new PageParams("analyse-overview.ftl", config));
    }

   /* @GET
    @Path("analyse/conversations")
    public PageView conversations(){
        return new PageView("analyse-conversations.ftl");
    }

    @GET
    @Path("analyse/statistics")
    @RolesAllowed("ADMIN")
    public PageView statistics(){
        return new PageView("analyse-analyse.ftl");
    }*/

    @GET
    @Path("analyse/trend")
    @RolesAllowed("user")
    public PageView trend(){
        return new PageView (new PageParams("analyse-trend.ftl", config));
    }

    @GET
    @Path("analyse/graph")
    @RolesAllowed("user")
    public PageView graph(){
        return new PageView (new PageParams("analyse-graph.ftl", config));
    }

    @GET
    @Path("live")
    @RolesAllowed("user")
    public PageView live() { return new PageView (new PageParams("live.ftl", config));}

    @GET
    @Path("live/fb")
    @RolesAllowed("user")
    public PageView liveFacebook() {
        return new PageView (new PageParams("live-fb.ftl", config));
    }

    @GET
    @Path("live/chitchat")
    @RolesAllowed("user")
    public PageView liveChitchat() {
        return new PageView (new PageParams("live-chitchat.ftl", config));
    }

    @GET
    @Path("live/telegram")
    @RolesAllowed("user")
    public PageView liveTelegram() {
        return new PageView (new PageParams("live-telegram.ftl", config));
    }

    @GET
    @Path("system/docs")
    @RolesAllowed("ADMIN")
    public PageView docs(){
        return new PageView (new PageParams("system-docs.ftl", config));
    }

    @GET
    @Path("system/apidoc")
    @RolesAllowed("ADMIN")
    public PageView apidoc(){
        return new PageView (new PageParams("system-apidoc.ftl", config));
    }

    @GET
    @Path("system/performance")
    @RolesAllowed("ADMIN")
    public PageView performance() {
        return new PageView (new PageParams("system-performance.ftl", config));
    }

    @GET
    @Path("system/info")
    @RolesAllowed("ADMIN")
    public PageView system() {
        return new PageView (new PageParams("system.ftl", config));
    }

    @GET
    @Path("system/speech")
    @RolesAllowed("ADMIN")
    public PageView systemSpeech() {
        return new PageView (new PageParams("system-speech.ftl", config));
    }

    @GET
    @Path("about")
    public PageView about() {
        return new PageView (new PageParams("about.ftl", config));
    }

    @GET
    @Path("privacy")
    public PageView privacy() {
        return new PageView(new PageParams("privacy.ftl", config));
    }

    @GET
    @Path("logout")
    public PageView logout(@Context SecurityContext context) throws ServletException {
        if (context.getUserPrincipal() != null) {
            request.logout();
        }
        return new PageView(new PageParams("about.ftl", config));
    }

    private static WebApplicationException redirect(String path) {
        URI uri = UriBuilder.fromUri(path).build();
        Response response = Response.seeOther(uri).build();
        return new WebApplicationException(response);
    }



}
