package org.c4i.chitchat.api.resource;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.c4i.chitchat.api.Config;
import org.c4i.nlp.chat.ConversationListener;
import org.c4i.nlp.chat.Message;
import org.c4i.nlp.chat.ScriptBot;
import org.c4i.nlp.match.Result;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

/**
 * Multi tenant Chitchat bot connector.
 * Hosts different {@link ScriptBot}s.
 * @author Arvid Halma
 */
@Path("/channel/chitchat/mt")
@Api("/channel/chitchat/mt")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ChitChatMultiTenantResource extends ChatResourceBase {

    private final Logger logger = LoggerFactory.getLogger(ChitChatMultiTenantResource.class);

    public ChitChatMultiTenantResource(Config config, List<ConversationListener> listeners) {
        super("chitchat", config, listeners);
    }

    @POST
    @Path("/script/reload")
    @RolesAllowed("ADMIN")
    @ApiOperation(
            value = "Load the latest 'ChitChat live script' from the database and use it as the current chatbot",
            response = Boolean.class)
    public boolean loadLiveScript(){
        super.resetScripts();
        return true;
    }

    @DELETE
    @Path("/conversation/reset")
    @RolesAllowed("ADMIN")
    @ApiOperation(
            value = "Reset all conversation states for this channel",
            notes = "All previous messages in conversations will be ignored.")
    public void resetConversations() {
        super.resetConversations();
    }

    @DELETE
    @Path("/{scriptName}/conversation/reset/{userId}/{botId}")
    @ApiOperation(
            value = "Reset a specific conversation state for this channel",
            notes = "All previous messages in conversations will be ignored.")
    public Response resetConversation(@PathParam("scriptName") String scriptName,
                                      @PathParam("userId") String userId,
                                      @PathParam("botId") String botId) {
        super.clearConversation(userId, botId, channelBaseName + "-" + scriptName);
        return Response.ok().build();
    }


    @GET
    @RolesAllowed("ADMIN")
    @Path("/conversation/recover")
    @ApiOperation(
            value = "Recover all conversation states for this channel stored in the DB",
            response = Integer.class)
    @Deprecated
    public int recoverConversations() {
        //todo: EvalStateManagers can't reload from just conversations but need other state info

        /*final DateTime now = DateTime.now();
        final DateTime start = now.minus(config.appConfig.getMaxConversationTimeInterval().toMilliseconds());

        int n = 0;
        for (String channel : config.dao.conversationDao.channels()) {
            if(!channel.startsWith("chitchat-")){
                continue; // only chitchat multi tenant type channels, please
            }
            final List<Conversation> conversations = config.dao.conversationDao.conversations(channel, start, now);
            ConversationManager chairman = evalStateManagerMap.computeIfAbsent(channel, s ->
                    new ConversationManager(10000,
                            config.appConfig.getMaxConversationTimeInterval().toSeconds(), TimeUnit.SECONDS,
                            ImmutableList.of(config.databaseResource)));
            chairman.load(conversations);
            logger.warn("Recovered {} conversations from DB for channel '{}'", conversations.size(), channel);
            n += conversations.size();
        }
        return n;*/
        return 0;
    }

    @POST
    @Timed
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/{scriptName}/reply")
    @ApiOperation(
            value = "Generate a reply, given the last message from the user.",
            notes = "Internally, previous messages are kept, so it can be used by a bot for context.",
            response = Result.class)
    public Result reply(
            @PathParam("scriptName") String scriptName,
            @Context Request request,
            @FormDataParam("msg") String msg) throws IOException
    {
        Message message = this.config.getObjectMapper().readValue(msg, Message.class);
        return reply(scriptName, message);
    }

}