package org.c4i.chitchat.api.resource.demo;


import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.ImmutableList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.c4i.chitchat.api.Config;
import org.c4i.chitchat.api.model.*;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.chat.Conversation;
import org.c4i.nlp.chat.Message;
import org.c4i.nlp.match.*;
import org.c4i.nlp.match.Compiler;
import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.normalize.StringNormalizers;
import org.c4i.nlp.tokenize.MatchingWordTokenizer;
import org.c4i.nlp.tokenize.Token;
import org.c4i.nlp.tokenize.TokenUtil;
import org.c4i.nlp.tokenize.Tokenizer;
import org.c4i.util.Histogram;
import org.c4i.util.StringUtil;
import org.c4i.util.time.TimeValue;
import org.c4i.util.time.Timeline;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.hibernate.validator.constraints.Length;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.RolesAllowed;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Database storage. Restricted version that doesn't store conversations.
 * @author Arvid Halma
 * @version 23-11-2015
 */

@Path("/db")
@Api("/db")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class DatabaseResource extends org.c4i.chitchat.api.resource.DatabaseResource {

    private final Logger logger = LoggerFactory.getLogger(DatabaseResource.class);

    public DatabaseResource(Config configuration) {
        super(configuration);
    }

    @Override
    public void start() {}

    @Override
    public void stop() throws Exception {
        logger.warn("Shutting down DB thread pool...");
        dbLogPool.shutdown();
        boolean success = dbLogPool.awaitTermination(5, TimeUnit.SECONDS);
        if(success){
            logger.warn("Shutting down DB thread pool: SUCCESS");
        } else {
            logger.error("Shutting down DB thread pool: FAIL");
        }
    }

    @Override
    @Timed
    public void onReceive(Conversation conversation, Message message) {
        // don't store conversations/messages
    }

    @Override
    @Timed
    public void onSend(Conversation conversation, Result result) {
        // don't store conversations/messages
    }

    @Override
    @Timed
    public void timout(Conversation conversation) {
        logger.info("Ending conversation after timeout: {}", conversation.getId());
    }

    @Override
    @Timed
    public void reset(Conversation conversation) {
        logger.info("Ending conversation after reset: {}", conversation.getId());
    }

    @PUT
    @Path("/sheet/load/{name}")
    @ApiOperation(
            value = "Load the latest data sheet",
            notes = "Either loads from the database or, when it does not exist, from file '/data/{lang}/datasheet'")
    public TextDoc loadDataSheet(@PathParam("name") String name) {
        TextDoc sheetDoc = config.dao.textDocDao.getLastUpdatedByName("datasheet", name);
        if (sheetDoc == null) {
            final String body = config.getNlp().loadFileDataSheet(name);
            sheetDoc = saveTextDoc("datasheet", name, body); // save in db for next time
        } else {
            config.getNlp().loadDataSheet(name, sheetDoc.getBody());
            logger.info("Finished loading data sheet: {}", name);
            sheetDoc.setBody(null);
        }
        return sheetDoc;
    }

    @POST
    @Path("/sheet")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Save datasheet",
            notes = "Updates or inserts the datasheet in the database")
    public void saveDataSheet(@FormDataParam("name") String name, @FormDataParam("tsv") String tsv) {
        throw new WebApplicationException("Saving not allowed on this demo server.", Response.Status.FORBIDDEN);
    }

    @PUT
    @Path("/vars/load/{name}")
    @ApiOperation(
            value = "Load the latest reply variables from the database")
    public void loadReplyVariables(@PathParam("name") String name) {
        TextDoc doc = config.dao.textDocDao.getLastUpdatedByName("vars", name);
        config.getNlp().setReplyVariables(name, doc.getBody());
    }

    @POST
    @Path("/vars")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Save the latest reply variables",
            notes = "They will be readily available in scripts.")
    public void setReplyVariables(@FormDataParam("name") String name, @FormDataParam("tsv") String tsv) {
        // throw new WebApplicationException("Saving not allowed on this demo server.", Response.Status.FORBIDDEN);
    }


    /////////////// TextDoc /////////////// 

    @GET
    @Path("/textdoc/{type}/meta")
    @ApiOperation(
            value = "Get text document meta information by id",
            notes = "Everything, but the body",
            response = TextDoc.class,
            responseContainer = "List"
    )
    public List<TextDoc> getTextDocMetas(@PathParam("type") String type) {
        if("ccs".equals(type))
            type = "ccsdemo"; // override user input: demo mode
        return config.dao.textDocDao.getAllMetaLastUpdated(type);
    }


    @DELETE
    @Path("/textdoc/{type}/{name}")
    @ApiOperation(
            value = "Delete all text document versions by type and name")
    public void deleteTextDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 128) String name) {
        throw new WebApplicationException("Deleting not allowed on this demo server.", Response.Status.FORBIDDEN);
    }


    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/textdoc/{type}/{name}")
    @ApiOperation(
            value = "Save a text document given a type and name",
            notes = "It won't overwrite possible earlier versions, but create another version if the same name is used."
    )
    public TextDoc saveTextDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 128) String name, @FormDataParam("body") String body) {
        throw new WebApplicationException("Saving not allowed on this demo server.", Response.Status.FORBIDDEN);
    }

    /////////////// JsonDoc /////////////// 



    @DELETE
    @Path("/jsondoc/{type}/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Delete all JSON document versions by type and name")
    public void deleteJsonDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 128) String name) {
        throw new WebApplicationException("Deleting not allowed on this demo server.", Response.Status.FORBIDDEN);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/jsondoc/{type}/{name}")
    @ApiOperation(
            value = "Save a JSON document given a type and name",
            notes = "It won't overwrite possible earlier versions, but create another version if the same name is used."
    )
    public void saveJsonDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 128) String name, @FormDataParam("body") String body) {
        throw new WebApplicationException("Saving not allowed on this demo server.", Response.Status.FORBIDDEN);
    }


    @GET
    @Path("/script/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(
            value = "Download script files bundled in a zip file.",
            response = Response.class
    )
    public Response exportScripts() throws WebApplicationException {
        throw new WebApplicationException("Exporting not allowed on this demo server.", Response.Status.FORBIDDEN);
    }


    /////////////// Conversations /////////////// 
    @POST
    @Path("/conversation/import")
    @ApiOperation(
            value = "Upload conversation data as CSVs bundled in a zip file.",
            response = Response.class
    )
    @Timed
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Map<String, Integer> importConversation(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws WebApplicationException
    {
        throw new WebApplicationException("Uploading not allowed on this demo server.", Response.Status.FORBIDDEN);
    }

    @GET
    @Path("/conversation/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(
            value = "Download conversation data as CSVs bundled in a zip file.",
            response = Response.class
    )
    public Response exportConversation(@QueryParam("channel") String channel) throws WebApplicationException {
        throw new WebApplicationException("Exporting not allowed on this demo server.", Response.Status.FORBIDDEN);
    }

    @GET
    @Path("/conversation/export/anonymous")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(
            value = "Download anonymized conversation data as CSVs bundled in a zip file.",
            response = Response.class
    )
    public Response exportAnonymousConversation(@QueryParam("channel") String channel) throws WebApplicationException {
        throw new WebApplicationException("Exporting not allowed on this demo server.", Response.Status.FORBIDDEN);
    }

    @GET
    @Path("/conversation/between")
    @ApiOperation(
            value = "Get conversations within a period",
            notes = "A conversation is a map with properties as: id, channel, from, to, labels...",
            response = Map.class,
            responseContainer = "List"
    )
    public List<Map<String, Object>> getConversationsBetween(
            @QueryParam("channel") String channel,
            @QueryParam("from") String from,
            @QueryParam("to") String to
    ) {
        channel = "demo"; // override user input: demo mode
        DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
        DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);
        return config.dao.conversationDao.conversationOverview(channel, fromDate, toDate);
    }

    @GET
    @Path("/conversation/stats")
    @ApiOperation(
            value = "Get messsage counts per available channel",
            response = Scored.class,
            responseContainer = "List"
    )
    public List<Scored<String>> getConversationStats() {
        return config.dao.conversationDao.conversationMessageCountDemo();
    }

    @DELETE
    @Path("/conversation")
    @ApiOperation(
            value = "Remove all conversations for a channel, along with the associated data from the database"
    )
    public void deleteConversationsForChannel(@QueryParam("channel") String channel) {
        throw new WebApplicationException("Deleting not allowed on this demo server.", Response.Status.FORBIDDEN);
    }

    @DELETE
    @Path("/conversation/{id}")
    @ApiOperation(
            value = "Remove a conversation, along with the associated data from the database"
    )
    public void deleteConversationById(@PathParam("id") String id) {
        throw new WebApplicationException("Deleting not allowed on this demo server.", Response.Status.FORBIDDEN);
    }

    @POST
    @Timed
    @Path("/match")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Analyse all selected conversations by running a matching script",
            response = AnalysisResult.class
    )
    public AnalysisResult match(
            @FormDataParam("from") String from,
            @FormDataParam("to") String to,
            @FormDataParam("labels") String labels,
            @FormDataParam("script") String source) {
        String channel = "demo"; // override user input: demo mode
        DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
        DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);

        List<AnalysisMessage> messages = Collections.synchronizedList(new ArrayList<>());
        Histogram<String> histogram = new Histogram<>();
        Map<String, List<TimeValue>> timeLineMap = new HashMap<>();

        final List<String> labelList = Arrays.asList(labels.split(","));
        Script script = Compiler.compile(source, config.getNlp());
        final List<Message> selectedMessages = config.dao.conversationDao.channelMessages(channel, fromDate, toDate);

        // parallelStream() is ~2x faster with 8 cores...
        selectedMessages.parallelStream().forEach(message -> {
            Result matchResult = new Eval(config.getNlp()).find(script, message.getText());

            if (labels.isEmpty() || matchResult.containsAllLabel(labelList)) {
                Histogram<String> histogramResult = Eval.histogram(matchResult.getRanges());
                histogram.join(histogramResult);

                messages.add(new AnalysisMessage(message.getTimestamp(), message.getSenderId(), matchResult.getHighlight(), matchResult.getRanges()));
            }
        });

        Timeline<AnalysisMessage> tl = new Timeline<>();
        tl.addAll(messages);
        tl = tl.select(tl.getStartDateTime(), tl.getEndDateTime().plus(Period.days(1)));
        for (String label : histogram.getEntries()) {
            Timeline<AnalysisMessage> labelLine = tl.filter(sm -> sm.getMatches().stream().anyMatch(r -> r.label.equals(label)));
            Timeline<Double> counts = labelLine.dayStats();
            timeLineMap.put(label, counts.asTimeValueList(d -> d));
        }

        return new AnalysisResult(histogram.asMap(), messages, timeLineMap);
    }

    @POST
    @Timed
    @Path("/conversation/match")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Analyse all selected conversations by running a matching script",
            response = AnalysisResult.class
    )
    public AnalysisConvResult conversationMatch(
            @FormDataParam("from") String from,
            @FormDataParam("to") String to,
            @FormDataParam("labels") String labels,
            @FormDataParam("script") String source) {
        String channel = "demo"; // override user input: demo mode
        DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
        DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);

        Histogram<String> labelHistogram = new Histogram<>();
        Map<String, Histogram<String>> wordHistograms = Collections.synchronizedMap(new HashMap<>());

        final List<String> labelList = Arrays.asList(labels.split(","));
        final Nlp nlp = config.getNlp();

        Script script = Compiler.compile(source, nlp);

        List<Conversation> conversations = config.dao.conversationDao.conversations(channel, fromDate, toDate);
        List<Result> results = Collections.synchronizedList(new ArrayList<>());

        final StringNormalizer normalizer = nlp.getNormalizer(script.getConfig());
        final Tokenizer wordTokenizer = nlp.getWordTokenizer(script.getConfig());

        // parallelStream() is ~2x faster with 8 cores...
        conversations.parallelStream().forEach(conversation -> {
            Result matchResult = new Eval(nlp).reply(script, conversation);
            // add entire conversation to result (not done by default)
            matchResult.setConversation(conversation);
            // strip replies (not needed for analysis)
            matchResult.setReplies(null);

            if (labels.isEmpty() || matchResult.containsAllLabel(labelList)) {
                final List<Range> ranges = matchResult.getRanges();
                for (Range range : ranges) {
                    wordHistograms.computeIfAbsent(range.label, key -> new Histogram<>());
                    final List<Token> words = wordTokenizer.tokenize(range.value);
                    normalizer.normalizeTokens(words);
                    wordHistograms.get(range.label).add(TokenUtil.toSentenceMaybeNormalized(words));
                }


                Histogram<String> histogramResult = Eval.histogram(ranges);
                labelHistogram.join(histogramResult);

                results.add(matchResult);
            }
        });

        Map<String, Map<String, Double>> wordHistoMaps = new HashMap<>();
        for (String label : labelHistogram) {
            wordHistoMaps.put(label, wordHistograms.get(label).asSortedMap(10));
        }


        Map<String, List<TimeValue>> timeLineMap = new HashMap<>();
        Timeline<Result> tl = new Timeline<>();
        tl.addAll(results);
        tl = tl.select(tl.getStartDateTime(), tl.getEndDateTime().plus(Period.days(1)));
        for (String label : labelHistogram.getEntries()) {
            Timeline<Result> labelLine = tl.filter(sm -> sm.containsLabel(label));
            Timeline<Double> counts = labelLine.dayStats();
            timeLineMap.put(label, counts.asTimeValueList(d -> d));
        }

        return new AnalysisConvResult(labelHistogram.asMap(), wordHistoMaps, results, timeLineMap);
    }

    @POST
    @Timed
    @Path("/conversation/match/db")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Analyse all selected conversations by running a matching script",
            response = AnalysisResult.class
    )
    public AnalysisConvResult conversationMatchDb(
            @FormDataParam("from") String from,
            @FormDataParam("to") String to,
            @FormDataParam("labels") String labels) {
        String channel = "demo"; // override user input: demo mode
        DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
        DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);

        Histogram<String> labelHistogram = new Histogram<>();
        Map<String, Histogram<String>> wordHistograms = Collections.synchronizedMap(new HashMap<>());

        final List<String> labelList = Arrays.asList(labels.split(","));
        List<Conversation> conversations = config.dao.conversationDao.conversations(channel, fromDate, toDate);
        List<Range> ranges = config.dao.conversationDao.conversationRanges(channel, fromDate, toDate);
        Map<String, List<Range>> convToRanges = new HashMap<>();
        for (Range range : ranges) {
            final String cid = range.conversationId;
            convToRanges.computeIfAbsent(cid, key -> new ArrayList<>());
            convToRanges.get(cid).add(range);
        }

        List<Result> results = Collections.synchronizedList(new ArrayList<>());

        final StringNormalizer normalizer = StringNormalizers.DEFAULT;
        final Tokenizer wordTokenizer = new MatchingWordTokenizer();

        // parallelStream() is ~2x faster with 8 cores...
        conversations.parallelStream().forEach(conversation -> {
            final List<Range> convRanges = convToRanges.getOrDefault(conversation.getId(), ImmutableList.of());
            final Map<String, List<Range>> convRangeMap = new HashMap<>();
            for (Range convRange : convRanges) {
                convRangeMap.computeIfAbsent(convRange.label, key -> new ArrayList<>(2));
                convRangeMap.get(convRange.label).add(convRange);
            }

            StringBuilder inText = new StringBuilder();
            for (Message message : conversation.getMessages()) {
                if (message.getIncoming()) {
                    inText.append(message.getText()).append(" \n\n");
                }
            }

            Result matchResult = new Result();
            matchResult.setMatches(convRangeMap);
            matchResult.setConversation(conversation);
            matchResult.setHighlight(Eval.highlightWithTags(inText.toString(), convRanges));


            if (labels.isEmpty() || matchResult.containsAllLabel(labelList)) {
                for (Range range : convRanges) {
                    wordHistograms.computeIfAbsent(range.label, key -> new Histogram<>());
                    final List<Token> words = wordTokenizer.tokenize(range.value);
                    normalizer.normalizeTokens(words);
                    wordHistograms.get(range.label)
                            .add(
                                    TokenUtil.toSentenceMaybeNormalized(words)
                            );
                }


                Histogram<String> histogramResult = Eval.histogram(convRanges);
                labelHistogram.join(histogramResult);

                results.add(matchResult);
            }
        });

        Map<String, Map<String, Double>> wordHistoMaps = new HashMap<>();
        for (String label : labelHistogram) {
            wordHistoMaps.put(label, wordHistograms.get(label).asSortedMap(10));
        }


        Map<String, List<TimeValue>> timeLineMap = new HashMap<>();
        Timeline<Result> tl = new Timeline<>();
        tl.addAll(results);
        tl = tl.select(tl.getStartDateTime(), tl.getEndDateTime().plus(Period.days(1)));
        for (String label : labelHistogram.getEntries()) {
            Timeline<Result> labelLine = tl.filter(sm -> sm.containsLabel(label));
            Timeline<Double> counts = labelLine.dayStats();
            timeLineMap.put(label, counts.asTimeValueList(d -> d));
        }

        return new AnalysisConvResult(labelHistogram.asMap(), wordHistoMaps, results, timeLineMap);
    }

    @GET
    @Timed
    @Path("/conversation/extract")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Extract CSV from data",
            response = AnalysisResult.class
    )
    public String conversationExtract(
            @QueryParam("script") String script,
            @QueryParam("from") String from,
            @QueryParam("to") String to,
            @QueryParam("labels") String labels) {
        String channel = "demo"; // override user input: demo mode
        DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
        DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);
        final List<String> labelList = Arrays.asList(labels.split(","));

        final List<Range> ranges = new ArrayList<>();
        final HashMap<String, Conversation> conversationMap = new HashMap<>();

        if (script == null || script.isEmpty()) {
            ranges.addAll(config.dao.conversationDao.conversationRanges(channel, fromDate, toDate));
            List<Conversation> conversations = config.dao.conversationDao.conversations(channel, fromDate, toDate);
            for (Conversation conversation : conversations) {
                conversationMap.put(conversation.getId(), conversation);
            }
        } else {
            final Nlp nlp = config.getNlp();
            Script scr = Compiler.compile(script, nlp);

            List<Conversation> conversations = config.dao.conversationDao.conversations(channel, fromDate, toDate);

            conversations.forEach(conversation -> {
                Result matchResult = new Eval(nlp).reply(scr, conversation);
                if (matchResult != null) {
                    conversationMap.put(conversation.getId(), conversation);
                    ranges.addAll(matchResult.getRanges());
                }
            });
        }


        LinkedHashMap<String, LinkedHashMap<String, String>> data = new LinkedHashMap<>();

        LinkedHashMap<String, String> emptyRow = new LinkedHashMap<>();
        for (String label : labelList) {
            emptyRow.put(label, "");
        }

        for (Range range : ranges) {
            final String conv = range.conversationId;
            if (labels.contains(range.label)) {
                data.putIfAbsent(conv, new LinkedHashMap<>(emptyRow));

                data.get(conv).computeIfPresent(range.label, (k, v) -> v.isEmpty() ? range.getValue() : v + ";" + range.getValue());
            }
        }

        StringBuilder sb = new StringBuilder();
        // header
        sb.append("conversation\t");
        sb.append(String.join("\t", labelList));
        sb.append("\ttext");
        sb.append('\n');
        for (Map.Entry<String, LinkedHashMap<String, String>> row : data.entrySet()) {
            final String convId = row.getKey();
            sb.append(StringUtil.quote(convId));
            sb.append('\t');
            sb.append(row.getValue().values().stream().map(StringUtil::quote).collect(Collectors.joining("\t")));
            sb.append('\t');
            sb.append(StringUtil.quote(conversationMap.get(convId).toConversationString()));
            sb.append('\n');
        }
        return sb.toString();
    }
}