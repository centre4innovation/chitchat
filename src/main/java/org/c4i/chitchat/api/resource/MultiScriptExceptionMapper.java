package org.c4i.chitchat.api.resource;

import com.google.common.collect.ImmutableMap;
import org.c4i.nlp.match.MultiScriptException;
import org.c4i.nlp.match.ScriptException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Turn {@link ScriptException} into {@link javax.ws.rs.WebApplicationException}
 * @author Arvid Halma
 * @version 8-8-2017 - 21:08
 */
public class MultiScriptExceptionMapper implements ExceptionMapper<MultiScriptException> {
    @Override
    public Response toResponse(MultiScriptException e) {
        return Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(e.getExceptions().stream().map(se -> ImmutableMap.of(
                        "type", se.getType(),
                        "line", se.getLine(),
                        "message", se.getMessage()
                )).toArray()).build();
    }
}
