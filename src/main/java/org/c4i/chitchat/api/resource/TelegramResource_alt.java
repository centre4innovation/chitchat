/**package org.c4i.chitchat.api.resource;

import com.github.messenger4j.send.senderaction.SenderAction;
import io.dropwizard.jersey.params.LongParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.c4i.chitchat.api.Config;
import org.c4i.chitchat.api.chat.ConversationListener;
import org.c4i.chitchat.api.chat.ConversationManager;
import org.c4i.chitchat.api.model.TextDoc;
import org.c4i.nlp.chat.Conversation;
import org.c4i.nlp.chat.Message;
import org.c4i.nlp.chat.ScriptBot;
import org.c4i.nlp.match.Compiler;
import org.c4i.nlp.match.Result;
import org.c4i.nlp.match.Script;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.api.methods.ActionType;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendChatAction;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Facebook chatbot endpoint.
 * @author Boaz Manger
 *
@Path("/channel/telegram")
@Api("/channel/telegram")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class TelegramResource extends TelegramWebhookBot {

    private Config config;

    private final Logger logger = LoggerFactory.getLogger(FacebookResource.class);

    private ScriptBot chatBot;
    private ConversationManager conversationManager;
    private List<ConversationListener> listeners;

    private Message getMessage(Update update) {
        Message message = new Message();
        message.setId(update.getUpdateId().toString());
        message.setText(update.getMessage().getText());
        message.setSenderId(update.getMessage().getFrom().getId().toString());
        message.setTimestamp(new DateTime(update.getMessage().getDate()));
        message.setConversationId(update.getMessage().getChatId().toString());
        message.setIncoming(true);
        message.setChannel("telegram");
        return message;
    }

    @Override
    public BotApiMethod onWebhookUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {

            final Message message = getMessage(update);
            Conversation conversation = conversationManager.update(message);

            logger.info("Received text message from '{}' at '{}' with content: {} (mid: {})",
                    message.getSenderId(), message.getTimestamp(), message.getText(), message.getId());

            listeners.forEach(cl -> cl.onReceive(conversation, message));

            SendChatAction sendChatAction = new SendChatAction();
            sendChatAction.setChatId(message.getConversationId());
            sendChatAction.setAction(ActionType.TYPING);
            try {
                Boolean wasSuccessfull = execute(sendChatAction);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

            Result result = chatBot.reply(conversation);
            if (result.getMatches().containsKey("RESET")) {
                // a reset label was triggered
                conversationManager.reset(message.getSenderId(), "telegram");
                listeners.forEach(cl -> cl.reset(conversation));
            }

            Pattern iPattern = Pattern.compile("IMAGE\\((.*?)\\)");
            Pattern bPattern = Pattern.compile("BUTTON\\((.*?) *, *(.*?)\\)");

            List<Message> replies = result.getReplies();

            if (replies.size() > 1) {
                for (ListIterator<Message> i = replies.listIterator(); i.hasNext(); ) {
                    Message m = i.next();

                    if (m.getText().contains("BUTTON") & i.hasPrevious()) {
                        replies.get(i.previousIndex() - 1).setText(replies.get(i.previousIndex() - 1).getText() + " " + m.getText());
                        i.remove();
                    }
                }
            }

            for (Message reply : replies) {
                conversationManager.update(reply);

                String text = reply.getText();
                Matcher matcher = iPattern.matcher(text);
                String type;
                String url = null;
                ReplyKeyboardMarkup keyboardMarkup = null;

                if (matcher.find()) {
                    url = matcher.group(1);
                    type = "image";
                } else {
                    type = "text";
                }

                if (text.contains("BUTTON")) {
                    keyboardMarkup = new ReplyKeyboardMarkup();
                    List<KeyboardRow> keyboard = new ArrayList<>();

                    Matcher matcherb = bPattern.matcher(text);
                    while (matcherb.find()) {
                        KeyboardRow row = new KeyboardRow();
                        row.add(matcherb.group(1));
                        keyboard.add(row);
                    }
                    keyboardMarkup.setKeyboard(keyboard);
                    reply.setText(bPattern.matcher(reply.getText()).replaceAll(""));
                }

                if (type.equals("text")) {
                    SendMessage messageReply = new SendMessage();
                    messageReply.setChatId(message.getConversationId());
                    messageReply.setText(reply.getText());
                    messageReply.setReplyMarkup(keyboardMarkup);
                    try {
                        return messageReply;
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }

                } else if (type.equals("image")) {
                    SendPhoto sendPhotoRequest = new SendPhoto();
                    sendPhotoRequest.setChatId(message.getConversationId());
                    sendPhotoRequest.setPhoto(url);
                    sendPhotoRequest.setReplyMarkup(keyboardMarkup);
                    try {
                        // Execute the method
                        return sendPhotoRequest;
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }


                listeners.forEach(cl -> cl.onSend(conversation, result));

            }
        }
        return null;
    }

    @Override
    public String getBotUsername() {
        return this.config.telegramSettings.getBotUsername();
    }

    @Override
    public String getBotToken() {
        return this.config.telegramSettings.getBotToken();
    }

    @Override
    public String getBotPath() {
        //arbitrary path to deliver updates on, username is an example.
        return this.config.telegramSettings.getBotUsername();
    }

    @POST
    @Path("/script/reload")
    @ApiOperation(
            value = "Load the latest 'Telegram live script' from the database and use it as the current chatbot",
            response = Boolean.class)
    public void loadLiveScript(){
        TextDoc src = config.dao.textDocDao.getLastUpdatedByName("ccs", "Telegram live script");
        Script script = Compiler.compile(src.getBody(), config.getNlp());
        this.chatBot = new ScriptBot(script, config.getNlp());
    }

    @GET
    @RolesAllowed("ADMIN")
    @Path("/conversation/recover")
    @ApiOperation(
            value = "Recover all conversation" +
                    " states for this channel stored in the DB",
            response = Integer.class)
    public int recoverConversations() {
        final DateTime now = DateTime.now();
        final DateTime start = now.minus(config.getMaxConversationTimeIntervalSeconds() * 1000);
        final List<Conversation> conversations = config.dao.conversationDao.conversations("telegram", start, now);
        conversationManager.load(conversations);
        logger.warn("Recovered {} conversations from DB for channel '{}'", conversations.size(), "telegram");
        return conversations.size();
    }

    @DELETE
    @Path("/conversation/reset")
    @ApiOperation(
            value = "Reset all conversation states for this channel",
            notes = "All previous messages in conversations will be ignored.",
            response = Boolean.class)
    public void resetConversations() {
        conversationManager.reset();
    }

    @POST
    @Path("/webhook/{botToken}")
    @ApiOperation(
            value = "Telegram callback endpoint to process messages",
            response = String.class)
    public void callback(@PathParam("botToken")LongParam botToken, Update update) throws IOException {

        logger.info("Received Telegram Platform callback");

        onWebhookUpdateReceived(update);
    }

    }
*/