package org.c4i.chitchat.api.resource;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.c4i.chitchat.api.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Serving image data
 * @author Arvid Halma
 * @version 14-11-19
 */
@Path("/img")
@Api("/img")
@Consumes({MediaType.APPLICATION_JSON})
@Produces( MediaType.APPLICATION_OCTET_STREAM )
public class ImageResource {
    private Config config;
    private final Logger logger = LoggerFactory.getLogger(ImageResource.class);
    public ImageResource(Config configuration) {
        this.config = configuration;
    }

    @GET
    @Timed
    @Path("/{name}")
    @Produces( MediaType.APPLICATION_OCTET_STREAM )
    @ApiOperation(
            value = "Retrieve a image file.")
    public Response getImage(@PathParam("name") String name) {
        return serveImage(config.appConfig.getDataDir() + "/img/" + name);
    }

    @GET
    @Timed
    @Path("/peruplot/{name}")
    @Produces( MediaType.APPLICATION_OCTET_STREAM )
    @ApiOperation(
            value = "Retrieve a image plot file.")
    public Response getPeruPlot(@PathParam("name") String name) {
        return serveImage(config.appConfig.getDataDir() + "/img/peruplot/" + name);
    }

    @GET
    @Timed
    @Path("/hnplot/{name}")
    @Produces( MediaType.APPLICATION_OCTET_STREAM )
    @ApiOperation(
            value = "Retrieve a image plot file.")
    public Response getHNPlot(@PathParam("name") String name) {
        return serveImage(config.appConfig.getDataDir() + "/img/hnplot/" + name);
    }

    private Response serveImage(String file) {
        String ext = file.substring(file.length()-3).toLowerCase();
        try {
            final File f = new File(file);
            return Response.ok(new FileInputStream(f), "image/"+ext)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + f.getName() + "\"")
                    .build();
        } catch (FileNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
