package org.c4i.chitchat.api.resource;


import com.codahale.metrics.annotation.Timed;
import io.dropwizard.auth.Auth;
import com.google.common.collect.ImmutableList;
import io.dropwizard.lifecycle.Managed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.c4i.chitchat.api.Config;
import org.c4i.nlp.chat.ConversationListener;
import org.c4i.chitchat.api.db.JsonDocMapper;
import org.c4i.chitchat.api.db.RangeMapper;
import org.c4i.chitchat.api.model.*;
import org.c4i.chitchat.api.sec.KeycloakUser;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.chat.Conversation;
import org.c4i.nlp.chat.Message;
import org.c4i.nlp.match.Compiler;
import org.c4i.nlp.match.*;
import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.normalize.StringNormalizers;
import org.c4i.nlp.tokenize.MatchingWordTokenizer;
import org.c4i.nlp.tokenize.Token;
import org.c4i.nlp.tokenize.TokenUtil;
import org.c4i.nlp.tokenize.Tokenizer;
import org.c4i.util.*;
import org.c4i.util.time.DateTimeUtil;
import org.c4i.util.time.TimeValue;
import org.c4i.util.time.Timeline;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.hibernate.validator.constraints.Length;
import org.jdbi.v3.sqlobject.transaction.Transaction;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.RolesAllowed;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Database storage
 * @author Arvid Halma
 * @version 23-11-2015
 */

@Path("/db")
@Api("/db")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class DatabaseResource implements ConversationListener, Managed {

    protected Config config;
    protected final Logger logger = LoggerFactory.getLogger(DatabaseResource.class);
    // Used for DB writes that are like logging actions
    protected final ExecutorService dbLogPool;

    public DatabaseResource(Config configuration) {
        this.config = configuration;
        // use half of the db connections to write message data
        this.dbLogPool = Executors.newFixedThreadPool(config.getDataSourceFactory().getMaxSize() / 2);
    }

    @Override
    public void start() throws Exception {}

    @Override
    public void stop() throws Exception {
        logger.warn("Shutting down DB thread pool...");
        dbLogPool.shutdown();
        boolean success = dbLogPool.awaitTermination(5, TimeUnit.SECONDS);
        if(success){
            logger.warn("Shutting down DB thread pool: SUCCESS");
        } else {
            logger.error("Shutting down DB thread pool: FAIL");
        }
    }

    @Override
    @Timed
    public void onReceive(Conversation conversation, Message message) {
        dbLogPool.execute(() -> {
            config.dao.conversationDao.insertConversationIfNew(conversation);
            config.dao.conversationDao.insertMessageIfNew(message);
        });
    }

    @Override
    @Timed
    public void onSend(Conversation conversation, Result result) {
        dbLogPool.execute(() -> {
            // config.dao.conversationDao.insertConversationIfNew(conversation); // chatbots don't send before receiving, so conversation exists
            config.dao.conversationDao.insertMessagesIfNew(result.getReplies());
            config.dao.conversationDao.insertRangesIfNew(result.getRanges());
            DateTime timestamp = conversation.lastMessage().getTimestamp();
            config.dao.textDocDao.upsert(
                    new TextDoc()
                            .setId(conversation.getId())
                            .setType("highlight")
                            .setBody(result.getHighlight())
                            .setCreated(timestamp)
                            .setUpdated(timestamp)
            );
        });
    }

    @Override
    @Timed
    public void timout(Conversation conversation) {
        logger.info("Ending conversation after timeout: {}", conversation.getId());
    }

    @Override
    @Timed
    public void reset(Conversation conversation) {
        logger.info("Ending conversation after reset: {}", conversation.getId());
    }

    public void loadAllDataSheets(boolean async) {
        ExecutorService executor = async ? Executors.newFixedThreadPool(100) : Executors.newSingleThreadScheduledExecutor();

        List<String> dbDataSheetNames = ImmutableList.of();
        if(!"demo".equals(config.appConfig.getMode())) {
            // first, prefer datasheets stored in the DB over sheets from files (when not in "demo" mode)
            dbDataSheetNames = config.dao.textDocDao.getAllNames("datasheet");
        }

        for (String name : dbDataSheetNames) {
            if (name.length() > 4) {
                if (config.getNlp().hasFeature(name.substring(0, 2), "datasheet")) {
                    executor.execute(() -> loadDataSheet(name));
                }
            }
        }

        // add missing datasheets from file
        for (String lang : config.getNlp().getLanguages()) {
            final List<String> fileDataSheetNames = config.getNlp().getFileDataSheetNames(lang);
            for (String name : fileDataSheetNames) {
                executor.execute(() -> loadDataSheet(name));
            }
        }

        if(!async){
            executor.shutdown();
            try {
                executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            } catch (InterruptedException ignored) {}
        }
    }

    private void loadDataSheet(String name) {
        TextDoc sheetDoc = config.dao.textDocDao.getLastUpdatedByName("datasheet", name);
        if (sheetDoc == null) {
            final String body = config.getNlp().loadFileDataSheet(name);
            saveTextDoc("datasheet", name, body, null, Collections.singleton("_public"));
        } else {
            config.getNlp().loadDataSheet(name, sheetDoc.getBody());
            logger.info("Finished loading data sheet: {}", name);
        }
    }

    @PUT
    @Path("/sheet/load/{name}")
    @ApiOperation(
            value = "Load the latest data sheet",
            notes = "Either loads from the database or, when it does not exist, from file '/data/{lang}/datasheet'")
    public TextDoc loadDataSheet(@PathParam("name") String name, @Auth KeycloakUser user) {
        Set<String> roles = user.getRoles();
        TextDoc sheetDoc = config.dao.textDocDao.getLastUpdatedByName("datasheet", name);
        if (sheetDoc == null) {
            final String body = config.getNlp().loadFileDataSheet(name);
            sheetDoc = saveTextDoc("datasheet", name, body, null, roles);// save in db for next time
        } else {
            config.getNlp().loadDataSheet(name, sheetDoc.getBody());
            logger.info("Finished loading data sheet: {}", name);
            sheetDoc.setBody(null);
        }
        return sheetDoc;
    }

    public void saveDataSheet(String name, String tsv) {
        saveTextDoc("datasheet", name, tsv, null, Collections.singleton("_public"));
        loadDataSheet(name);
    }

    @PUT
    @Path("/vars/load")
    @ApiOperation(
            value = "Load all latest reply variables from the database")
    public void loadReplyVariables() {
        List<TextDoc> docs = config.dao.textDocDao.getAllLastUpdated("vars");
        for (TextDoc doc : docs) {
            final String name = doc.getName();
            config.getNlp().setReplyVariables(name, doc.getBody());
            logger.info("Finished loading reply variables: {}", name);
        }
    }

    @PUT
    @Path("/vars/load/{name}")
    @ApiOperation(
            value = "Load the latest reply variables from the database")
    public void loadReplyVariables(@PathParam("name") String name) {
        TextDoc doc = config.dao.textDocDao.getLastUpdatedByName("vars", name);
        config.getNlp().setReplyVariables(name, doc.getBody());
    }

    @POST
    @Path("/vars")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Save the latest reply variables",
            notes = "They will be readily available in scripts.")
    public void setReplyVariables(@FormDataParam("name") String name, @FormDataParam("tsv") String tsv) {
        config.dao.textDocDao.upsert(new TextDoc().setType("vars").setName(name).setBody(tsv));
        config.getNlp().setReplyVariables(name, tsv);
    }

    @GET
    @Path("/vars/{name}")
    @ApiOperation(
            value = "Get all reply variables",
            response = Map.class)
    public Map<String, String> getReplyVariables(@PathParam("name") String name) {
        return config.getNlp().getReplyVariables(name);
    }

    /////////////// TextDoc ///////////////
    /////////////// Unprotected ///////////

    @GET
    @Path("/textdoc/type")
    @ApiOperation(
            value = "List all unique document types.",
            response = String.class,
            responseContainer = "List"
    )
    public List<String> getTypes() {
        return config.dao.textDocDao.getTypes();
    }

    @GET
    @Path("/textdoc/type/doc")
    @ApiOperation(
            value = "List all types that follow the pattern 'doc/%'",
            notes = "List of distinct document types",
            response = String.class,
            responseContainer = "List"
    )
    public List<String> getDocLikeType() {
        return config.dao.textDocDao.getDocLikeTypes();
    }

    /////////////// Protected /////////////

    @GET
    @Path("/textdoc/id/{id}")
    @ApiOperation(
            value = "Get text document by id",
            response = TextDoc.class)
    public TextDoc getTextDoc(@PathParam("id") String id, @Auth KeycloakUser user) {
        Collection<String> roles = user.getRoles();
        if (config.dao.aceDao.hasAccessToDoc(id, roles)) {
            return config.dao.textDocDao.getById(id);
        } else {
            throw new WebApplicationException("You do not have access to the textDoc with id '" + id + "'.", Response.Status.FORBIDDEN);
        }
    }

    @GET
    @Path("/textdoc/id/{id}/body")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get text document body/content by id",
            response = String.class)
    public String getTextDocBody(@PathParam("id") String id, @Auth KeycloakUser user) {
        Collection<String> roles = user.getRoles();
        if (config.dao.aceDao.hasAccessToDoc(id, roles)) {
            return config.dao.textDocDao.getById(id).getBody();
        } else {
            throw new WebApplicationException("You do not have access to the textDoc with id '" + id + "'.", Response.Status.FORBIDDEN);
        }
    }

    @GET
    @Path("/textdoc/{type}/meta")
    @ApiOperation(
            value = "Get text document meta information by id",
            notes = "Everything, but the body",
            response = TextDoc.class,
            responseContainer = "List"
    )
    public List<TextDoc> getTextDocMetas(@PathParam("type") String type, @Auth KeycloakUser user) {
        return config.dao.aceDao.getAllMetaLastUpdatedByRoles(type, new ArrayList<>(user.getRoles()));
    }

    @GET
    @Path("/textdoc/{type}/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get last updated text document content/body by type and name",
            response = TextDoc.class)
    public String getLastTextDoc(@PathParam("type") String type, @PathParam("name") String name, @Auth KeycloakUser user) {
        Collection<String> roles = user.getRoles();
        final TextDoc doc = config.dao.textDocDao.getLastUpdatedByName(type, name);
        if (config.dao.aceDao.hasAccessToDoc(doc.getId(), roles)) {
            return doc.getBody();
        } else {
            throw new WebApplicationException("You do not have permissions to access '" + name + "' or this document does not exist.", Response.Status.FORBIDDEN);
        }
    }

    @GET
    @Path("/textdoc/{type}")
    @ApiOperation(
            value = "Get all text documents by type",
            response = TextDoc.class,
            responseContainer = "List")
    public List<TextDoc> getTextDocs(@PathParam("type") String type, @Auth KeycloakUser user) {
        return config.dao.aceDao.getAllLastUpdatedByRoles(type, new ArrayList<>(user.getRoles()));
    }

    @GET
    @Path("/textdoc/{type}/{name}/meta")
    @ApiOperation(
            value = "Get text document meta information for a given type and name",
            notes = "I.e. retrieve all versions. Everything, but the body",
            response = TextDoc.class,
            responseContainer = "List"
    )
    public List<TextDoc> getTextDocVersionMetas(@PathParam("type") String type, @PathParam("name") String name, @Auth KeycloakUser user) {
        return config.dao.aceDao.getAllMetasByRoles(type, name, new ArrayList<>(user.getRoles()));
    }

    @DELETE
    @Path("/textdoc/{type}/{name}")
    @ApiOperation(
            value = "Delete all text document versions by type and name")
    public void deleteTextDoc(@PathParam("type") String type,
                              @PathParam("name") @NotNull @Length(min = 1, max = 128) String name,
                              @Auth KeycloakUser user) {
        List<String> roleList = config.dao.aceDao.getRolesByIds(config.dao.textDocDao.getAllMetas(type, name)
                .stream().map(TextDoc::getId).collect(Collectors.toList()));
        roleList.remove("_public");
        try {
            if (!user.getRoles().containsAll(roleList)) {
                throw new WebApplicationException("You do not have the appropriate permissions to delete '" + name + "'.", Response.Status.FORBIDDEN);
            } else {
                final int n = config.dao.textDocDao.deleteByName(type, name);
                if (n == 0) {
                    throw new WebApplicationException("The " + type + " document '" + name + " ' was not found");
                }
            }
        } catch (WebApplicationException e) {
            this.logger.warn("User '" + user.getName() + "' tried to delete textdoc '" + name + "' but did not have appropriate permissions.", e);
        }
    }

    @GET
    @Path("/textdoc/{type}/name")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get text document names for a given type",
            response = String.class,
            responseContainer = "List"
    )
    public List<String> getTextDocNames(@PathParam("type") String type, @Auth KeycloakUser user) {
        Set<String> roles = user.getRoles();
        return config.dao.textDocDao.getScopedNames(type, new ArrayList<>(roles));
    }

    /////////////// Save TextDoc ///////////////
    /////////////// Protected    ///////////////

    @Transaction
    public TextDoc saveTextDoc(String type, String name, String body, String tag, Set<String> roles) {
        TextDoc textDoc = config.dao.textDocDao.upsert(new TextDoc().setType(type).setName(name).setBody(body).setTag(tag));
        List<Ace> aces = roles.stream().map(r -> new Ace(textDoc.getId(), r)).collect(Collectors.toList());
        config.dao.aceDao.upsert(aces);
        if(textDoc.getType().contains("ccs")){
            // Make sure caches are flushed when a script is saved again, so the different channels use it directly
            config.chitChatMultiTenantResource.loadLiveScript();
        }
        return textDoc;
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/textdoc/{type}/{name}")
    @ApiOperation(
            value = "Save a text document given a type and name",
            notes = "It won't overwrite possible earlier versions, but create another version if the same name is used."
    )
    public TextDoc saveTextDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 128) String name,
                               @FormDataParam("body") String body, @Auth KeycloakUser user) throws WebApplicationException {
        Set<String> roles = user.getRoles();
        TextDoc textDoc = config.dao.textDocDao.getLastMetaUpdatedByName(type, name);
        if (textDoc != null) {
            List<String> rolesById = config.dao.aceDao.getRolesById(textDoc.getId());
            if (!roles.containsAll(rolesById)) {
                // File exists and user has no permissions to access file
                throw new WebApplicationException(Response.Status.FORBIDDEN);
            }
        }
        return saveTextDoc(type, name, body, null, roles);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/textdoc/{type}/{name}/tag/{tag}")
    @ApiOperation(
            value = "Save a text document given a type, name and tag",
            notes = "It won't overwrite possible earlier versions, but create another version if the same name is used."
    )
    public TextDoc saveTextDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 128) String name,
                            @FormDataParam("body") String body, @PathParam("tag") String tag, @Auth KeycloakUser user)
            throws WebApplicationException {
        Set<String> roles = user.getRoles();
        TextDoc textDoc = config.dao.textDocDao.getLastMetaUpdatedByName(type, name, tag);
        if (textDoc != null) {
            List<String> rolesById = config.dao.aceDao.getRolesById(textDoc.getId());
            if (!roles.containsAll(rolesById)) {
                // File exists and user has no permissions to access file
                throw new WebApplicationException(Response.Status.FORBIDDEN);
            }
        }
        return saveTextDoc(type, name, body, tag, roles);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/textdoc/{type}/{name}/{roles}")
    @ApiOperation(
            value = "Save a text document given a type and name",
            notes = "It won't overwrite possible earlier versions, but create another version if the same name is used."
    )
    public TextDoc saveTextDocRole(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 128) String name,
                                   @FormDataParam("body") String body, @Auth KeycloakUser user, @PathParam("roles") String inputRoles) throws
            WebApplicationException {
        Set<String> roles = user.getRoles();
        Set<String> givenRoles = new HashSet<>(Arrays.asList(inputRoles.split(",")));
        if (!roles.containsAll(givenRoles)) {
            throw new WebApplicationException("User does not have the roles it attempts to save under.", Response.Status.FORBIDDEN);
        }
        TextDoc textDoc = config.dao.textDocDao.getLastMetaUpdatedByName(type, name);
        if (textDoc != null) {
            List<String> rolesById = config.dao.aceDao.getRolesById(textDoc.getId());
            if (!roles.containsAll(rolesById)) {
                // File exists and user has no permissions to access file
                throw new WebApplicationException(Response.Status.FORBIDDEN);
            }
        }
        return saveTextDoc(type, name, body, null, givenRoles);
    }

    /////////////// JsonDoc /////////////// 

    @GET
    @Path("/jsondoc/id/{id}")
    @ApiOperation(
            value = "Get JSON document by id",
            response = JsonDoc.class)
    public JsonDoc getJsonDoc(@PathParam("id") String id) {
        return config.dao.jsonDocDao.getById(id);
    }

    @GET
    @Path("/jsondoc/id/{id}/body")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get JSON document body/content by id",
            response = String.class)
    public Map<String, Object> getJsonDocBody(@PathParam("id") String id) {
        return config.dao.jsonDocDao.getById(id).getBody();
    }

    @GET
    @Path("/jsondoc/{type}/meta")
    @ApiOperation(
            value = "Get JSON document meta information by id",
            notes = "Everything, but the body",
            response = JsonDoc.class,
            responseContainer = "List"
    )
    public List<JsonDoc> getJsonDocMetas(@PathParam("type") String type) {
        return config.dao.jsonDocDao.getAllMetaLastUpdated(type);
    }

    @GET
    @Path("/jsondoc/{type}/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get last updated JSON document content/body by type and name",
            response = JsonDoc.class)
    public Map<String, Object> getLastJsonDoc(@PathParam("type") String type, @PathParam("name") String name) {
        final JsonDoc doc = config.dao.jsonDocDao.getLastUpdatedByName(type, name);
        if (doc == null) {
            throw new WebApplicationException(404);
        }
        return doc.getBody();
    }

    @GET
    @Path("/jsondoc/{type}")
    @ApiOperation(
            value = "Get all JSON documents by type",
            response = JsonDoc.class,
            responseContainer = "List")
    public List<JsonDoc> getJsonDocs(@PathParam("type") String type) {
        return config.dao.jsonDocDao.getAllLastUpdated(type);
    }

    @GET
    @Path("/jsondoc/{type}/{name}/meta")
    @ApiOperation(
            value = "Get JSON document meta information for a given type and name",
            notes = "I.e. retrieve all versions. Everything, but the body",
            response = JsonDoc.class,
            responseContainer = "List"
    )
    public List<JsonDoc> getJsonDocVersionMetas(@PathParam("type") String type, @PathParam("name") String name) {
        return config.dao.jsonDocDao.getAllMetas(type, name);
    }

    @DELETE
    @Path("/jsondoc/{type}/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Delete all JSON document versions by type and name")
    public void deleteJsonDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 128) String name) {
        config.dao.jsonDocDao.deleteByName(type, name);
    }

    @GET
    @Path("/jsondoc/{type}/name")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get JSON document names for a given type",
            response = String.class,
            responseContainer = "List"
    )
    public List<String> getJsonDocNames(@PathParam("type") String type) {
        return config.dao.jsonDocDao.getAllNames(type);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/jsondoc/{type}/{name}")
    @ApiOperation(
            value = "Save a JSON document given a type and name",
            notes = "It won't overwrite possible earlier versions, but create another version if the same name is used."
    )
    public void saveJsonDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 128) String name, @FormDataParam("body") String body) {
        final LinkedHashMap<String, Object> bodyMap = JsonDocMapper.readJson(body);
        config.dao.jsonDocDao.upsert(new JsonDoc().setName(name).setType(type).setBody(bodyMap));
    }


    @GET
    @Path("/script/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(
            value = "Download script files bundled in a zip file.",
            response = Response.class
    )
    public Response exportScripts(@Auth KeycloakUser user) throws IOException {
        java.nio.file.Path tempDirPath = Files.createTempDirectory("chitchat");
        File tempDirFile = tempDirPath.toFile();

        List<TextDoc> scripts = config.dao.aceDao.getAllLastUpdatedByRoles("ccs", new ArrayList<>(user.getRoles()));
        List<String> files = new ArrayList<>(scripts.size());
        for (TextDoc script : scripts) {
            String name = script.getName();
            name = name.replaceAll("(?U)[^\\w._ ]+", "_") + ".ccs";
            File file = new File(tempDirFile, name);
            Boolean success = file.setLastModified(script.getUpdated().getMillis());
            if(!success) {
                throw new IOException("The last Modified date of file '" + file.getName() + "' could not be modified.");
            }
            FileUtils.write(file, script.getBody(), "UTF-8");
            files.add(file.getAbsolutePath());
        }

        String zipName = "chitchat-script-export-" + System.currentTimeMillis() + ".zip";
        String destinationFile = new File(tempDirFile, zipName).getAbsolutePath();
        ZipUtil.zip(destinationFile, files);

        InputStream is = new FileInputStream(destinationFile);
        return Response.ok(is)
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + zipName + "\"")
                .build();
    }


    /////////////// Conversations ///////////////

    @POST
    @Path("/conversation/import")
    @ApiOperation(
            value = "Upload conversation data as CSVs bundled in a zip file.",
            response = Response.class
    )
    @Timed
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Map<String, Integer> importConversation(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException
    {
        return importConversation(uploadedInputStream, fileDetail.getFileName());
    }

    private Map<String, Integer> importConversation(InputStream uploadedInputStream, String fileName) throws IOException
    {
        logger.info("uploading file for import: {}", fileName);
        String fileId = DigestUtils.sha1Hex(fileName);
        File tmpDir = Files.createTempDirectory("upload").toFile();
        logger.info("uploading file for import, tmp dir: {}", tmpDir);
        File uploadedZipFile = new File(tmpDir, fileId);
        FileUtils.copyInputStreamToFile(uploadedInputStream, uploadedZipFile);
        ZipUtil.unzip(uploadedZipFile.getAbsolutePath(), tmpDir.getAbsolutePath());

        CSVFormat format = CSVFormat.TDF
                .withIgnoreSurroundingSpaces()
                .withFirstRecordAsHeader()
                .withNullString("");

        int conversationCount = 0, conversationError = 0, messageCount = 0, messageError = 0, labelCount = 0, labelError = 0;
        File conversationFile = new File(tmpDir, "conversation.txt");
        if(conversationFile.exists()){
            // id	lang	botid	userid	channel
            CSVParser parser = CSVParser.parse(conversationFile, StandardCharsets.UTF_8, format);
            List<Conversation> conversations = new ArrayList<>();
            for (CSVRecord record : parser) {
                try {
                    conversations.add(new Conversation()
                            .setId(record.get("id"))
                            .setLang(record.get("lang"))
                            .setBotId(record.get("botid"))
                            .setUserId(record.get("userid"))
                            .setChannel(record.get("channel")));
                } catch (Exception e) {
                    logger.warn("Error when uploading conversation: row {}, line: {}", record.getRecordNumber(), record.toString());
                    conversationError++;
                }
            }
            config.dao.conversationDao.upsertConversations(conversations);
            conversationCount = conversations.size();
            conversations.clear();
        }

        File messageFile = new File(tmpDir, "message.txt");
        if(messageFile.exists()){
            // id	incoming	conversationid	senderid	recipientid	text	timestamp
            CSVParser parser = CSVParser.parse(messageFile, StandardCharsets.UTF_8, format);
            List<Message> messages = new ArrayList<>();
            int order = 0;
            for (CSVRecord record : parser) {
                try {
                    messages.add(new Message()
                            .setId(record.get("id"))
                            .setIncoming(Boolean.valueOf(record.get("incoming")))
                            .setOrder(order++)
                            .setConversationId(record.get("conversationid"))
                            .setSenderId(record.get("senderid"))
                            .setRecipientId(record.get("recipientid"))
                            .setText(record.get("text"))
                            .setTimestamp(DateTimeUtil.parseLiberalDateTime(record.get("timestamp"))));
//                            .setTimestamp(ISODateTimeFormat.dateTimeParser().parseDateTime(record.get("timestamp"))));
                } catch (Exception e) {
                    logger.warn("Error when uploading message: row {}, line: {}", record.getRecordNumber(), record.toString());
                    messageError++;
                }
            }
            config.dao.conversationDao.upsertMessages(messages);
            messageCount = messages.size();
            messages.clear();
        }

        File labelFile = new File(tmpDir, "label.txt");
        if(labelFile.exists()){
            // id	label	value	conversationid	tokenstart	tokenend	charstart	charend	props	section
            CSVParser parser = CSVParser.parse(labelFile, StandardCharsets.UTF_8, format);
            List<Range> ranges= new ArrayList<>();
            for (CSVRecord record : parser) {
                try {
                    ranges.add(new Range()
                            .setLabel(record.get("label"))
                            .setValue(record.get("value"))
                            .setConversationId(record.get("conversationid"))
                            .setTokenStart(Integer.parseInt(record.get("tokenstart")))
                            .setTokenEnd(Integer.parseInt(record.get("tokenend")))
                            .setCharStart(Integer.parseInt(record.get("charstart")))
                            .setCharEnd(Integer.parseInt(record.get("charend")))
                            .setProps(RangeMapper.parseProps(record.get("props")))
                            .setSection(Integer.parseInt(record.get("section"))));
                } catch (Exception e) {
                    logger.warn("Error when uploading ranges: row {}, line: {}", record.getRecordNumber(), record.toString());
                    labelError++;

                }
            }
            config.dao.conversationDao.upsertRanges(ranges);
            labelCount = ranges.size();
            ranges.clear();
        }

        LinkedHashMap<String, Integer> result = new LinkedHashMap<>();
        result.put("conversation", conversationCount);
        result.put("conversationError", conversationError);
        result.put("message", messageCount);
        result.put("messageError", messageError);
        result.put("label", labelCount);
        result.put("labelError", labelError);
        logger.info("import finished: {}", result);
        return result;
    }

    private List<String> channelsByRoles(Set<String> roles) {
        List<String> channels = config.dao.conversationDao.channelsFormatted();
        if (roles.contains("ADMIN")) {
            return config.dao.conversationDao.channels();
        } else {
            //todo: turn into a single DB query
            return channels.stream()
                    .filter(ch -> config.dao.aceDao.hasAccessToDoc(config.dao.textDocDao.getLastMetaUpdatedByName("ccs", ch).getId(), roles))
                    .collect(Collectors.toList());
        }
    }

    @GET
    @Path("/conversation/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(
            value = "Download conversation data as CSVs bundled in a zip file.",
            response = Response.class
    )
    public Response exportConversation(@QueryParam("channel") String channel, @Auth KeycloakUser user) throws IOException, WebApplicationException {
        java.nio.file.Path tempDirPath = Files.createTempDirectory("chitchat");
        File tempDirFile = tempDirPath.toFile();
        String cnvFile = new File(tempDirFile, "conversation.txt").getAbsolutePath();
        String msgFile = new File(tempDirFile, "message.txt").getAbsolutePath();
        String lblFile = new File(tempDirFile, "label.txt").getAbsolutePath();

        List<String> channelAccess = channelsByRoles(user.getRoles());
        if (!channelAccess.contains(channel)) {
            throw new WebApplicationException("You do not have permission to export from the '" + channel + "' channel.", Response.Status.FORBIDDEN);
        }

        config.dao.conversationDao.exportConversation(cnvFile, channel);
        config.dao.conversationDao.exportMessage(msgFile, channel);
        config.dao.conversationDao.exportRange(lblFile, channel);

        String zipName = "chitchat-export-" + System.currentTimeMillis() + ".zip";
        String destinationFile = new File(tempDirFile, zipName).getAbsolutePath();
        ZipUtil.zip(destinationFile,
                ImmutableList.of(cnvFile, msgFile, lblFile));

        InputStream is = new FileInputStream(destinationFile);
        return Response.ok(is)
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + zipName + "\"")
                .build();
    }

    @GET
    @Path("/conversation/export/anonymous")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(
            value = "Download anonymized conversation data as CSVs bundled in a zip file.",
            response = Response.class
    )
    public Response exportAnonymousConversation(@QueryParam("channel") String channel, @Auth KeycloakUser user) throws IOException, WebApplicationException {
        java.nio.file.Path tempDirPath = Files.createTempDirectory("chitchat");
        File tempDirFile = tempDirPath.toFile();
        String cnvFile = new File(tempDirFile, "conversation.txt").getAbsolutePath();
        String msgFile = new File(tempDirFile, "message.txt").getAbsolutePath();
        String lblFile = new File(tempDirFile, "label.txt").getAbsolutePath();

        List<String> channelAccess = channelsByRoles(user.getRoles());
        if (!channelAccess.contains(channel)) {
            throw new WebApplicationException("You do not have permission to export from the '" + channel + "' channel.", Response.Status.FORBIDDEN);
        }

        config.dao.conversationDao.exportAnonymousConverstation(cnvFile, channel);
        config.dao.conversationDao.exportAnonymousMessage(msgFile, channel);
        config.dao.conversationDao.exportAnonymousRange(lblFile, channel);

        String zipName = "chitchat-anonymous-export-" + System.currentTimeMillis() + ".zip";
        String destinationFile = new File(tempDirFile, zipName).getAbsolutePath();
        ZipUtil.zip(destinationFile,
                ImmutableList.of(cnvFile, msgFile, lblFile));

        InputStream is = new FileInputStream(destinationFile);
        return Response.ok(is)
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + zipName + "\"")
                .build();
    }

    @GET
    @Path("/conversation/result/{id}")
    @ApiOperation(
            value = "Get conversation messages and highlight",
            response = Result.class
    )
    public Result getConversationResult(@PathParam("id") String id) {
        Conversation conversation = config.dao.conversationDao.getConversation(id);
        List<Range> ranges = config.dao.conversationDao.conversationRanges(id);
        if(conversation == null){
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        StringBuilder inText = new StringBuilder();
        for (Message message : conversation.getMessages()) {
            if (message.getIncoming()) {
                inText.append(message.getText()).append(" \n\n");
            }
        }

        Result result = new Result();
        result.setConversation(conversation);
        result.setHighlight(Eval.highlightWithTags(inText.toString(), ranges));
        return result;
    }

    @GET
    @Path("/conversation/between")
    @ApiOperation(
            value = "Get conversations within a period",
            notes = "A conversation is a map with properties as: id, channel, from, to, labels...",
            response = Map.class,
            responseContainer = "List"
    )
    public List<Map<String, Object>> getConversationsBetween(
            @QueryParam("channel") String channel,
            @QueryParam("from") String from,
            @QueryParam("to") String to,
            @Auth KeycloakUser user) {
        List<String> channelAccess = channelsByRoles(user.getRoles());
        if (!channelAccess.contains(channel)) {
            throw new WebApplicationException("You do not have permission to export from the '" + channel + "' channel.", Response.Status.FORBIDDEN);
        }

        DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
        DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);
        return config.dao.conversationDao.conversationOverview(channel, fromDate, toDate);
    }

    @GET
    @Path("/conversation/stats")
    @ApiOperation(
            value = "Get messsage counts per available channel",
            response = Scored.class,
            responseContainer = "List"
    )
    public List<Scored<String>> getConversationStats(@Auth KeycloakUser user) {
        List<String> channelAccess = channelsByRoles(user.getRoles());
        return config.dao.conversationDao.conversationMessageCount()
                .stream()
                .filter(stats -> channelAccess.contains(stats.getValue()))
                .collect(Collectors.toList());
    }

    @GET
    @Path("/conversation/freqs")
    @ApiOperation(
            value = "Conversation counts per day, per available channel",
            response = Map.class
    )
    public Map<String, List<TimeValue>> getConversationFreqs(@Auth KeycloakUser user) {
        Map<String, List<TimeValue>> result = new LinkedHashMap<>();
        for (String channel : channelsByRoles(user.getRoles())) {
            result.put(channel, config.dao.conversationDao.dailyConversationCounts(channel));
        }
        return result;
    }

    @GET
    @Path("/conversation/{id}/message")
    @ApiOperation(
            value = "Get message for the specified conversation",
            response = Message.class,
            responseContainer = "List"
    )
    public List<Message> getConversationMessages(@PathParam("id") String id, @Auth KeycloakUser user) throws WebApplicationException {
        List<String> channelAccess = channelsByRoles(user.getRoles());
        if (channelAccess.contains(config.dao.conversationDao.getConversationWithoutMessages(id).getChannel())) {
            return config.dao.conversationDao.conversationMessages(id);
        } else {
            throw new WebApplicationException("You do not have access to messages in this channel.", Response.Status.FORBIDDEN);
        }
    }

    @GET
    @Path("/conversation/{id}/range")
    @ApiOperation(
            value = "Get matched ranges for the specified conversation",
            response = Range.class,
            responseContainer = "List"
    )
    public List<Range> getConversationRanges(@PathParam("id") String id, @Auth KeycloakUser user) throws WebApplicationException {
        List<String> channelAccess = channelsByRoles(user.getRoles());
        if (channelAccess.contains(config.dao.conversationDao.getConversationWithoutMessages(id).getChannel())) {
            return config.dao.conversationDao.conversationRanges(id);
        } else {
            throw new WebApplicationException("You do not have access to messages in this channel.", Response.Status.FORBIDDEN);
        }
    }

    @GET
    @Path("/labels/{channel}")
    @ApiOperation(
            value = "Get all unique labels that matched for a given channel",
            response = Range.class,
            responseContainer = "List"
    )
    public List<String> getLabels(@PathParam("channel") String channel, @Auth KeycloakUser user) throws WebApplicationException {
        List<String> channelAccess = channelsByRoles(user.getRoles());
        if (channelAccess.contains(channel)) {
            return config.dao.conversationDao.labels(channel);
        } else {
            throw new WebApplicationException("You do not have access to messages in this channel.", Response.Status.FORBIDDEN);
        }
    }

    @GET
    @Path("/channel")
    @ApiOperation(
            value = "Get all unique channels",
            response = String.class,
            responseContainer = "List"
    )
    public List<String> getChannels(@Auth KeycloakUser user) {
        return channelsByRoles(user.getRoles());
    }

    @DELETE
    @Path("/conversation")
    @ApiOperation(
            value = "Remove all conversations for a channel, along with the associated data from the database"
    )
    public void deleteConversationsForChannel(@QueryParam("channel") String channel, @Auth KeycloakUser user) throws WebApplicationException {
        List<String> channels = channelsByRoles(user.getRoles());
        if (channels.contains(channel)) {
            config.dao.conversationDao.deleteConversationsForChannel(channel);
        } else {
            throw new WebApplicationException("You do not have access to this channel", Response.Status.FORBIDDEN);
        }
    }

    @DELETE
    @Path("/conversation/{id}")
    @ApiOperation(
            value = "Remove a conversation, along with the associated data from the database"
    )
    public void deleteConversationById(@PathParam("id") String id, @Auth KeycloakUser user) throws WebApplicationException {
        List<String> channels = channelsByRoles(user.getRoles());
        if (channels.contains(config.dao.conversationDao.getConversationWithoutMessages(id).getChannel())) {
            config.dao.conversationDao.deleteConversationById(id);
        } else {
            throw new WebApplicationException("You do not have access to this channel.", Response.Status.FORBIDDEN);
        }
    }

    @POST
    @Timed
    @Path("/sample/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @RolesAllowed("ADMIN")
    @ApiOperation(
            value = "Upload conversations for the sample channel",
            response = Response.class
    )
    @Deprecated
    public Response uploadFile(
            @FormDataParam("file") final InputStream fileInputStream,
            @FormDataParam("file") final FormDataContentDisposition contentDispositionHeader) throws IOException {

        String fileName = contentDispositionHeader.getFileName();

        CSVParser parser = CSVParser.parse(fileInputStream, StandardCharsets.UTF_8, CSVFormat.EXCEL.withIgnoreSurroundingSpaces());
        List<Message> messages = new ArrayList<>();
        List<Conversation> conversations = new ArrayList<>();

        int i = 0;
        for (CSVRecord record : parser) {
            try {
                Message msg = new Message();
                msg.setIncoming(true);
                //msg.setRecipientId("Unknown");
                msg.setSenderId(StringUtil.truncate(record.get(1), 64));
                msg.setChannel("sample");
                msg.setId(StringUtil.truncate("msg-" + record.get(0) + record.get(1), 64));
                msg.setText(record.get(2));
                msg.setTimestamp(DateTimeUtil.parseLiberalDateTime(record.get(0)));
                msg.setOrder(i++);
                String conversationId = StringUtil.truncate(msg.getTimestamp().toString().substring(0, 10) + "_" + record.get(1), 64);

                msg.setConversationId(conversationId);


                messages.add(msg);

                conversations.add(new Conversation(conversationId).setUserId(msg.getSenderId()).setBotId("Unknown").setChannel("sample"));
            } catch (IndexOutOfBoundsException e) {
                System.out.println("record = " + record);
                System.out.println("e = " + e);
            }
        }

        // Save
        config.dao.conversationDao.upsertConversations(conversations);
        config.dao.conversationDao.upsertMessages(messages);

        return Response.status(200).entity(fileName).build();
    }

    @POST
    @Timed
    @Path("/match")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Analyse all selected conversations by running a matching script",
            response = AnalysisResult.class
    )
    public AnalysisResult match(
            @FormDataParam("channel") String channel,
            @FormDataParam("from") String from,
            @FormDataParam("to") String to,
            @FormDataParam("labels") String labels,
            @FormDataParam("script") String source,
            @Auth KeycloakUser user) throws WebApplicationException {
        List<String> channels = channelsByRoles(user.getRoles());
        if (!channels.contains(channel)) {
            throw new WebApplicationException("You do not have permission to analyse the selected channel", Response.Status.FORBIDDEN);
        } else {
            DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
            DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);

            List<AnalysisMessage> messages = Collections.synchronizedList(new ArrayList<>());
            Histogram<String> histogram = new Histogram<>();
            Map<String, List<TimeValue>> timeLineMap = new HashMap<>();

            final List<String> labelList = Arrays.asList(labels.split(","));
            Script script = Compiler.compile(source, config.getNlp());
            final List<Message> selectedMessages = config.dao.conversationDao.channelMessages(channel, fromDate, toDate);

            // parallelStream() is ~2x faster with 8 cores...
            selectedMessages.parallelStream().forEach(message -> {
                Result matchResult = new Eval(config.getNlp()).find(script, message.getText());

                if (labels.isEmpty() || matchResult.containsAllLabel(labelList)) {
                    Histogram<String> histogramResult = Eval.histogram(matchResult.getRanges());
                    histogram.join(histogramResult);

                    messages.add(new AnalysisMessage(message.getTimestamp(), message.getSenderId(), matchResult.getHighlight(), matchResult.getRanges()));
                }
            });

        Timeline<AnalysisMessage> tl = new Timeline<>();
        if(!messages.isEmpty()) {
            tl.addAll(messages);
            tl = tl.select(tl.getStartDateTime(), tl.getEndDateTime().plus(Period.days(1)));
            for (String label : histogram.getEntries()) {
                Timeline<AnalysisMessage> labelLine = tl.filter(sm -> sm.getMatches().stream().anyMatch(r -> r.label.equals(label)));
                Timeline<Double> counts = labelLine.dayStats();
                timeLineMap.put(label, counts.asTimeValueList(d -> d));
            }
        }

            return new AnalysisResult(histogram.asMap(), messages, timeLineMap);
        }
    }

    @POST
    @Timed
    @Path("/conversation/match")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Analyse all selected conversations by running a matching script",
            response = AnalysisResult.class
    )
    public AnalysisConvResult conversationMatch(
            @FormDataParam("channel") String channel,
            @FormDataParam("from") String from,
            @FormDataParam("to") String to,
            @FormDataParam("labels") String labels,
            @FormDataParam("script") String source,
            @Auth KeycloakUser user) throws WebApplicationException {
        List<String> channels = channelsByRoles(user.getRoles());
        if (!channels.contains(channel)) {
            throw new WebApplicationException("You do not have permission to analyse the selected channel", Response.Status.FORBIDDEN);
        } else {
            DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
            DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);

            Histogram<String> labelHistogram = new Histogram<>();
            Map<String, Histogram<String>> wordHistograms = Collections.synchronizedMap(new HashMap<>());

            final List<String> labelList = Arrays.asList(labels.split(","));
            final Nlp nlp = config.getNlp();

            Script script = Compiler.compile(source, nlp);

            List<Conversation> conversations = config.dao.conversationDao.conversations(channel, fromDate, toDate);
            List<Result> results = Collections.synchronizedList(new ArrayList<>());

            final StringNormalizer normalizer = nlp.getNormalizer(script.getConfig());
            final Tokenizer wordTokenizer = nlp.getWordTokenizer(script.getConfig());

            // parallelStream() is ~2x faster with 8 cores...
            conversations.parallelStream().forEach(conversation -> {
                Result matchResult = new Eval(nlp).reply(script, conversation);
                // add entire conversation to result (not done by default)
                matchResult.setConversation(conversation);
                // strip replies (not needed for analysis)
                matchResult.setReplies(null);

                if (labels.isEmpty() || matchResult.containsAllLabel(labelList)) {
                    final List<Range> ranges = matchResult.getRanges();
                    for (Range range : ranges) {
                        wordHistograms.computeIfAbsent(range.label, key -> new Histogram<>());
                        final List<Token> words = wordTokenizer.tokenize(range.value);
                        normalizer.normalizeTokens(words);
                        wordHistograms.get(range.label).add(TokenUtil.toSentenceMaybeNormalized(words));
                    }


                    Histogram<String> histogramResult = Eval.histogram(ranges);
                    labelHistogram.join(histogramResult);

                    results.add(matchResult);
                }
            });

            Map<String, Map<String, Double>> wordHistoMaps = new HashMap<>();
            for (String label : labelHistogram) {
                wordHistoMaps.put(label, wordHistograms.get(label).asSortedMap(10));
            }


        Map<String, List<TimeValue>> timeLineMap = new HashMap<>();
        if(!results.isEmpty()) {
            Timeline<Result> tl = new Timeline<>();
            tl.addAll(results);
            tl = tl.select(tl.getStartDateTime(), tl.getEndDateTime().plus(Period.days(1)));
            for (String label : labelHistogram.getEntries()) {
                Timeline<Result> labelLine = tl.filter(sm -> sm.containsLabel(label));
                Timeline<Double> counts = labelLine.dayStats();
                timeLineMap.put(label, counts.asTimeValueList(d -> d));
            }
        }

            return new AnalysisConvResult(labelHistogram.asMap(), wordHistoMaps, results, timeLineMap);
        }
    }

    @POST
    @Timed
    @Path("/conversation/match/db")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Analyse all selected conversations by running a matching script",
            response = AnalysisResult.class
    )
    public AnalysisConvResult conversationMatchDb(
            @FormDataParam("channel") String channel,
            @FormDataParam("from") String from,
            @FormDataParam("to") String to,
            @FormDataParam("labels") String labels,
            @Auth KeycloakUser user) throws WebApplicationException {
        List<String> channels = channelsByRoles(user.getRoles());
        if (!channels.contains(channel)) {
            throw new WebApplicationException("You do not have permission to analyse the selected channel", Response.Status.FORBIDDEN);
        } else {
            DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
            DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);

            Histogram<String> labelHistogram = new Histogram<>();
            Map<String, Histogram<String>> wordHistograms = Collections.synchronizedMap(new HashMap<>());

            final List<String> labelList = Arrays.asList(labels.split(","));
            List<Conversation> conversations = config.dao.conversationDao.conversations(channel, fromDate, toDate);
            List<Range> ranges = config.dao.conversationDao.conversationRanges(channel, fromDate, toDate);
            Map<String, List<Range>> convToRanges = new HashMap<>();
            for (Range range : ranges) {
                final String cid = range.conversationId;
                convToRanges.computeIfAbsent(cid, key -> new ArrayList<>());
                convToRanges.get(cid).add(range);
            }

            List<Result> results = Collections.synchronizedList(new ArrayList<>());

            final StringNormalizer normalizer = StringNormalizers.DEFAULT;
            final Tokenizer wordTokenizer = new MatchingWordTokenizer();

            // parallelStream() is ~2x faster with 8 cores...
            conversations.parallelStream().forEach(conversation -> {
                final List<Range> convRanges = convToRanges.getOrDefault(conversation.getId(), ImmutableList.of());
                final Map<String, List<Range>> convRangeMap = new HashMap<>();
                for (Range convRange : convRanges) {
                    convRangeMap.computeIfAbsent(convRange.label, key -> new ArrayList<>(2));
                    convRangeMap.get(convRange.label).add(convRange);
                }

            /*StringBuilder inText = new StringBuilder();
            for (Message message : conversation.getMessages()) {
                if (message.getIncoming()) {
                    inText.append(message.getText()).append(" \n\n"); // todo: think about addText?
                }
            }*/

                Result matchResult = new Result();
                matchResult.setMatches(convRangeMap);
                // Strip conversation content to reduce size
                conversation.setMessages(conversation.getMessages().subList(0, 1));
                conversation.getMessages().get(0).setText(null);

                matchResult.setConversation(conversation);
//            matchResult.setHighlight(Eval.highlightWithTags(inText.toString(), convRanges));


                if (labels.isEmpty() || matchResult.containsAllLabel(labelList)) {
                    for (Range range : convRanges) {
                        wordHistograms.computeIfAbsent(range.label, key -> new Histogram<>());
                        final List<Token> words = wordTokenizer.tokenize(range.value);
                        normalizer.normalizeTokens(words);
                        wordHistograms.get(range.label)
                                .add(
                                        TokenUtil.toSentenceMaybeNormalized(words)
                                );
                    }


                    Histogram<String> histogramResult = Eval.histogram(convRanges);
                    labelHistogram.join(histogramResult);

                    results.add(matchResult);
                }
            });

            Map<String, Map<String, Double>> wordHistoMaps = new HashMap<>();
            for (String label : labelHistogram) {
                wordHistoMaps.put(label, wordHistograms.get(label).asSortedMap(10));
            }


            Map<String, List<TimeValue>> timeLineMap = new HashMap<>();
            if (!results.isEmpty()) {
                Timeline<Result> tl = new Timeline<>();
                tl.addAll(results);
                tl = tl.select(tl.getStartDateTime(), tl.getEndDateTime().plus(Period.days(1)));
                for (String label : labelHistogram.getEntries()) {
                    Timeline<Result> labelLine = tl.filter(sm -> sm.containsLabel(label));
                    Timeline<Double> counts = labelLine.dayStats();
                    timeLineMap.put(label, counts.asTimeValueList(d -> d));
                }
            }
            return new AnalysisConvResult(labelHistogram.asMap(), wordHistoMaps, results, timeLineMap);
        }
    }

    @GET
    @Timed
    @Path("/conversation/extract")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Extract CSV from data",
            response = AnalysisResult.class
    )
    public String conversationExtract(
            @QueryParam("script") String script,
            @QueryParam("channel") String channel,
            @QueryParam("from") String from,
            @QueryParam("to") String to,
            @QueryParam("labels") String labels,
            @Auth KeycloakUser user) throws WebApplicationException {
        List<String> channels = channelsByRoles(user.getRoles());
        if (!channels.contains(channel)) {
            throw new WebApplicationException("You do not have permission to analyse the selected channel", Response.Status.FORBIDDEN);
        } else {
            DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
            DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);
            final List<String> labelList = Arrays.asList(labels.split(","));

            final List<Range> ranges = new ArrayList<>();
            final HashMap<String, Conversation> conversationMap = new HashMap<>();

            if (script == null || script.isEmpty()) {
                ranges.addAll(config.dao.conversationDao.conversationRanges(channel, fromDate, toDate));
                List<Conversation> conversations = config.dao.conversationDao.conversations(channel, fromDate, toDate);
                for (Conversation conversation : conversations) {
                    conversationMap.put(conversation.getId(), conversation);
                }
            } else {
                final Nlp nlp = config.getNlp();
                Script scr = Compiler.compile(script, nlp);

                List<Conversation> conversations = config.dao.conversationDao.conversations(channel, fromDate, toDate);

                conversations.forEach(conversation -> {
                    Result matchResult = new Eval(nlp).reply(scr, conversation);
                    if (matchResult != null) {
                        conversationMap.put(conversation.getId(), conversation);
                        ranges.addAll(matchResult.getRanges());
                    }
                });
            }


            LinkedHashMap<String, LinkedHashMap<String, String>> data = new LinkedHashMap<>();

            LinkedHashMap<String, String> emptyRow = new LinkedHashMap<>();
            for (String label : labelList) {
                emptyRow.put(label, "");
            }

            for (Range range : ranges) {
                final String conv = range.conversationId;
                if (labels.contains(range.label)) {
                    data.putIfAbsent(conv, new LinkedHashMap<>(emptyRow));

                    data.get(conv).computeIfPresent(range.label, (k, v) -> v.isEmpty() ? range.getValue() : v + ";" + range.getValue());
                }
            }

            StringBuilder sb = new StringBuilder();
            // header
            sb.append("conversation\t");
            sb.append(String.join("\t", labelList));
            sb.append("\ttext");
            sb.append('\n');
            for (Map.Entry<String, LinkedHashMap<String, String>> row : data.entrySet()) {
                final String convId = row.getKey();
                sb.append(StringUtil.quote(convId));
                sb.append('\t');
                sb.append(row.getValue().values().stream().map(StringUtil::quote).collect(Collectors.joining("\t")));
                sb.append('\t');
                sb.append(StringUtil.quote(conversationMap.get(convId).toConversationString()));
                sb.append('\n');
            }
            return sb.toString();
        }
    }

}