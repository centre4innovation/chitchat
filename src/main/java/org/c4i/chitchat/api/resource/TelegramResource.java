package org.c4i.chitchat.api.resource;

import com.google.common.collect.ImmutableMap;
import com.pengrad.telegrambot.BotUtils;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.ChatAction;
import com.pengrad.telegrambot.model.request.Keyboard;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.request.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.c4i.chitchat.api.Config;
import org.c4i.nlp.chat.ConversationListener;
import org.c4i.nlp.chat.ConversationManager;
import org.c4i.chitchat.api.model.TelegramSettings;
import org.c4i.chitchat.api.model.TextDoc;
import org.c4i.nlp.chat.Conversation;
import org.c4i.nlp.chat.Message;
import org.c4i.nlp.chat.ScriptBot;
import org.c4i.nlp.match.Compiler;
import org.c4i.nlp.match.Result;
import org.c4i.nlp.match.Script;
import org.c4i.util.StringUtil;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.net.MalformedURLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Facebook chatbot endpoint.
 * @author Boaz Manger
 */
@Path("/channel/telegram")
@Api("/channel/telegram")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class TelegramResource {

    private Config config;
    private Map<String, TelegramBot> botMap;

    private final Logger logger = LoggerFactory.getLogger(TelegramResource.class);

    private ScriptBot chatBot;
    private ConversationManager conversationManager;
    private List<ConversationListener> listeners;

    public TelegramResource(Config config, ConversationManager conversationManager, List<ConversationListener> listeners) throws MalformedURLException {
        this.config = config;
        this.conversationManager = conversationManager;
        this.listeners = listeners;
        this.botMap = new HashMap<>();

        for (TelegramSettings.TelegramCredentials c : this.config.telegramSettings.getTelegramCredentials()) {
            try {
            TelegramBot tempBot = new TelegramBot(c.getBotToken());
            String webhookUrl = tempBot.execute(new GetWebhookInfo()).webhookInfo().url();
                String webhookTarget = this.config.telegramSettings.getWebhookBaseURI() + c.getBotName();
            if (!webhookTarget.equals(webhookUrl)) {
                tempBot.execute(new SetWebhook().url(webhookTarget));
            }
                this.botMap.put(c.getBotName(), tempBot);
            } catch (Exception e) {
                logger.error("Problem in loading (one of) the Telegram bots.", e);
            }

        }
    }

    @POST
    @Path("/script/reload")
    @ApiOperation(
            value = "Load the latest 'Telegram live script' from the database and use it as the current chatbot",
            response = Boolean.class)
    public void loadLiveScript() {
        loadLiveScript(true);
    }

    public void loadLiveScript(boolean use) {
        TextDoc src = config.dao.textDocDao.getLastUpdatedByName("ccs", "Telegram live script");
        Script script = Compiler.compile(src.getBody(), config.getNlp());
        if(use) {
            this.chatBot = new ScriptBot(script, config.getNlp());
        }
    }

    @GET
    @RolesAllowed("ADMIN")
    @Path("/conversation/recover")
    @ApiOperation(
            value = "Recover all conversation" +
                    " states for this channel stored in the DB",
            response = Integer.class)
    public int recoverConversations() {
        final DateTime now = DateTime.now();
        final DateTime start = now.minus(config.appConfig.getMaxConversationTimeInterval().toMilliseconds());
        final List<Conversation> conversations = config.dao.conversationDao.conversations("telegram", start, now);
        conversationManager.load(conversations);
        logger.warn("Recovered {} conversations from DB for channel '{}'", conversations.size(), "telegram");
        return conversations.size();
    }

    @DELETE
    @Path("/conversation/reset")
    @ApiOperation(
            value = "Reset all conversation states for this channel",
            notes = "All previous messages in conversations will be ignored.",
            response = Boolean.class)
    public void resetConversations() {
        conversationManager.reset();
    }

    @GET
    @Path("/config/info")
    @ApiOperation(
            response = String.class,
            value = "Get token information")
    @Consumes({MediaType.APPLICATION_JSON})
    public List<Map<String,String>> configInfo() {
        return this.config.telegramSettings.credentials.stream().map(c ->
                ImmutableMap.of(
                        "botName", c.getBotName(),
                        "botUsername", c.getBotUsername(),
                        "botToken", StringUtil.truncate(c.getBotToken(), 4) +"..."
                ))
                .collect(Collectors.toList());
    }

    @POST
    @Path("/webhook/{botName}")
    @ApiOperation(
            value = "Telegram callback endpoint to process messages",
            response = String.class)
    public void onWebhookUpdate(@PathParam("botName") String botName, final String updateString) {
        logger.info("Received Telegram Platform callback");

        Update update = BotUtils.parseUpdate(updateString);

        logger.info("Telegram botName: '{}' webhook update: {}", botName, update);

        TelegramBot bot = botMap.get(botName);

        if ((update.message() != null) && (update.message().text() != null) && (update.message().voice() == null)) {
            final Message message = getMessage(update, botName);
            Conversation conversation = conversationManager.update(message);

            logger.info("Received text message from '{}' at '{}' with content: {} (mid: {})",
                    message.getSenderId(), message.getTimestamp(), message.getText(), message.getId());

            listeners.forEach(cl -> cl.onReceive(conversation, message));

            Result result = chatBot.reply(conversation);
            if (result.getMatches().containsKey("RESET")) {
                // a reset label was triggered
                conversationManager.reset(message.getSenderId(), "telegram", "telegram");
                listeners.forEach(cl -> cl.reset(conversation));
            }

            Pattern iPattern = Pattern.compile("IMAGE\\((.*?)\\)");
            Pattern bPattern = Pattern.compile("BUTTON\\((.*?) *(, *(.*?))?\\)"); // optional 2nd arg (which is ignored)

            List<Message> replies = result.getReplies();

            if (replies.size() > 1) {
                bot.execute(new SendChatAction(update.message().chat().id(), ChatAction.typing));

                for (ListIterator<Message> i = replies.listIterator(); i.hasNext(); ) {
                    Message m = i.next();

                    if (m.getText().contains("BUTTON") & i.hasPrevious()) {
                        replies.get(i.previousIndex() - 1).setText(replies.get(i.previousIndex() - 1).getText() + " " + m.getText());
                        i.remove();
                    }
                }
            }

            for (Message reply : replies) {
                conversationManager.update(reply);

                String text = reply.getText();
                Matcher matcher = iPattern.matcher(text);
                String type;
                String url = null;
                Keyboard keyboardMarkup = null;
                AbstractSendRequest send = null;

                if (matcher.find()) {
                    url = matcher.group(1);
                    if (url.endsWith(".gif")) {
                        type = "animation";
                    } else {
                        type = "image";
                    }
                } else {
                    type = "text";
                }

                if (text.contains("BUTTON")) {
                    ArrayList<String> keyboard = new ArrayList<>();

                    Matcher matcherb = bPattern.matcher(text);
                    while (matcherb.find()) {
                        keyboard.add(matcherb.group(1));
                    }
                    keyboardMarkup = new ReplyKeyboardMarkup(keyboard.toArray(new String[0])).oneTimeKeyboard(true).resizeKeyboard(true);

                    reply.setText(bPattern.matcher(reply.getText()).replaceAll(""));
                }

                switch (type) {
                    case "text":
                        send = new SendMessage(update.message().chat().id(), reply.getText());
                        break;
                    case "animation":
                        send = new SendAnimation(update.message().chat().id(), url);
                        break;
                    case "image":
                        send = new SendPhoto(update.message().chat().id(), url);
                        break;
                }
                if (keyboardMarkup != null) {
                    send.replyMarkup(keyboardMarkup);
                }
                bot.execute(send);

                listeners.forEach(cl -> cl.onSend(conversation, result));
            }
        } else if (update.message().voice() != null) {
            // no text message was send, possibly voice
            final List<String> languages = chatBot.getScript().getConfig().getLanguages();
            if(languages.contains("es")){
                bot.execute(new SendMessage(update.message().chat().id(), "Los mensajes de voz no son compatibles :( Por favor escríbeme algo ..."));
            } else {
                bot.execute(new SendMessage(update.message().chat().id(), "Voice messages are not supported :( Please write me something..."));
            }
        }
    }

    private Message getMessage(Update update, String botToken) {
        Message message = new Message();

        com.pengrad.telegrambot.model.Message telegramMessage = update.message();

        message.setId(telegramMessage.messageId().toString());
        message.setText(telegramMessage.text());
        message.setSenderId(telegramMessage.from().id().toString());
        message.setRecipientId(botToken);
        message.setTimestamp(DateTime.now());
        message.setConversationId(telegramMessage.chat().id().toString());
        message.setIncoming(true);
        message.setChannel("telegram");
        return message;
    }
}
