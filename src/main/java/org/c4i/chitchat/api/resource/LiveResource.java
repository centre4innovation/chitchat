package org.c4i.chitchat.api.resource;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.c4i.chitchat.api.Config;
import org.c4i.chitchat.api.model.FacebookSettings;
import org.c4i.chitchat.api.model.LiveChannel;
import org.c4i.chitchat.api.model.TelegramSettings;
import org.c4i.chitchat.api.model.TextDoc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Provides info on different live channels.
 * @author Boaz Manger
 */
@Path("/channel/live")
@Api("/channel/live")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class LiveResource {
    private HashMap<String, LiveChannel> channelHashMap;

    private Config config;

    private final Logger logger = LoggerFactory.getLogger(LiveResource.class);

    public LiveResource(Config config) {
        this.channelHashMap = new HashMap<>();
        List<TextDoc> textDoc = config.dao.textDocDao.getAllLastUpdated("liveccs");
        ArrayList<String> nameList = new ArrayList<String>();
        for (TextDoc i : textDoc) {
            String tempName = i.getName();
            nameList.add(tempName);
        }

        if (config.facebookSettings != null) {
            for (FacebookSettings.FacebookCredentials c : config.facebookSettings.credentials) {
                LiveChannel tempChann = new LiveChannel(c.getPageId(), "Facebook", nameList.contains(c.getPageId()));
                channelHashMap.put(c.getPageId(), tempChann);
            }
        }
        if (config.telegramSettings != null) {
            for (TelegramSettings.TelegramCredentials c : config.telegramSettings.getTelegramCredentials()) {
                LiveChannel tempChann = new LiveChannel(c.getBotName(), "Telegram", nameList.contains(c.getBotName()));
                channelHashMap.put(c.getBotName(), tempChann);
            }
        }
        channelHashMap.put("WebClient", new LiveChannel("ChitChat", "WebClient", nameList.contains("ChitChat")));
    }

    @GET
    @Path("/channelInfo")
    @ApiOperation(
            value = "get channel info",
            response = HashMap.class)
    public Collection<LiveChannel> channelinfo(){
        return this.channelHashMap.values();
    }

    @GET
    @Path("/channelInfo/{channelName}")
    @ApiOperation(
            value = "get channel info",
            response = LiveChannel.class)
    public LiveChannel liveChannel(@PathParam("channelName") String channelName) {
        return this.channelHashMap.get(channelName);
    }

    @PUT
    @Path("/channelInfo/update")
    @ApiOperation(
            value = "Retrieve script status for all channels")
    public void updateScripts() {
        List<TextDoc> textDoc = config.dao.textDocDao.getAllLastUpdated("liveccs");
        for (TextDoc i : textDoc) {
            String tempName = i.getName();
            if (channelHashMap.containsKey(tempName)) {
                channelHashMap.replace(tempName, new LiveChannel(tempName, channelHashMap.get(tempName).getChannelType(), Boolean.TRUE));
            }
        }
    }
}
