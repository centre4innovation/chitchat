package org.c4i.chitchat.api;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.ahus1.keycloak.dropwizard.KeycloakConfiguration;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.c4i.chitchat.api.db.Dao;
import org.c4i.chitchat.api.model.*;
import org.c4i.chitchat.api.plugin.honduras.HNWeatherConfig;
import org.c4i.chitchat.api.plugin.honduras.HNWeatherEntityPlugin;
import org.c4i.chitchat.api.plugin.peru.*;
import org.c4i.nlp.ner.LiveDataSheetConfig;
import org.c4i.chitchat.api.resource.*;
import org.c4i.chitchat.api.sec.AuthConfiguration;
import org.c4i.nlp.Nlp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * The Configuration class which specifies environment-specific parameters.
 * These parameters are specified in a YAML configuration file which is deserialized
 * to an instance of your application's configuration class and validated.
 *
 * Docs:
 * https://dropwizard.github.io/dropwizard/manual/configuration.html
 *
 * @author Arvid Halma
 * @version 3-4-2016
 */
@SuppressWarnings("WeakerAccess")
public class Config extends Configuration {

    private final Logger logger = LoggerFactory.getLogger(Config.class);

    @Valid
    @NotNull
    @JsonProperty("app")
    public AppConfig appConfig;

    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    @JsonProperty("languageProcessing")
    private Map<String, LanguageProcessingConfig> languageSupport;


    @JsonProperty("swagger")
    private SwaggerBundleConfiguration swaggerBundleConfiguration;

    @Valid
    @NotNull
    @JsonProperty("auth")
    public AuthConfiguration auth;

    @Valid
    @JsonProperty("facebookSettings")
    public FacebookSettings facebookSettings;

    @Valid
    @JsonProperty("telegramSettings")
    public TelegramSettings telegramSettings;

    @Valid
    @JsonProperty("twilioSettings")
    public TwilioSettings twilioSettings;

    @Valid
    @JsonProperty("liveDataSheets")
    public List<LiveDataSheetConfig> liveDataSheetConfigs;

    @Valid
    @JsonProperty("peru")
    public PeruPriceConfig peruPriceConfig;
    @Valid
    @JsonProperty("honduras")
    public HNWeatherConfig hnWheatherConfig;

    @JsonIgnore
    public PeruPriceHistoryUpdater peruPriceUpdater;

    @JsonIgnore
    public Dao dao;

    @JsonIgnore
    private ObjectMapper objectMapper;

    @JsonIgnore
    private Nlp nlp;

    @JsonIgnore
    public FacebookResource facebookResource;
    @JsonIgnore
    public TelegramResource telegramResource;
    @JsonIgnore
    public DevBotResource devBotResource;
    @JsonIgnore
    public DevQAResource devQAResource;
    @JsonIgnore
    public ChitChatResource chitchatResource;
    @JsonIgnore
    public ChitChatMultiTenantResource chitChatMultiTenantResource;
    @JsonIgnore
    public TwilioResource twilioResource;
    @JsonIgnore
    public DatabaseResource databaseResource;
    @JsonIgnore
    public LiveResource liveResource;

    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory) {
        this.database = factory;
    }

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    public SwaggerBundleConfiguration getSwaggerBundleConfiguration() {
        return swaggerBundleConfiguration;
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public void loadNlp(boolean async){
        this.nlp = new Nlp(new File(appConfig.getDataDir(), "nlp"), languageSupport);
        this.nlp.loadModels(async);
    }

    public Nlp getNlp() {
        return nlp;
    }

    public void loadLiveDatasheets(){
        /*if(liveDataSheetConfigs == null){
            return;
        }
        for (LiveDataSheetConfig ldsc : liveDataSheetConfigs) {
            final String dataSheetName = ldsc.getDataSheetName();
            SundialJobScheduler.addSimpleTrigger(dataSheetName, dataSheetName, -1, TimeUnit.SECONDS.toMillis(3));
        }*/
    }

    public void startPeru(){
        if(peruPriceConfig == null){
            return;
        }

        // data acquisition task
        peruPriceUpdater = new PeruPriceHistoryUpdater(this);
        peruPriceUpdater.start();
        // plot rendering
        nlp.addReplyPlugin("es", peruPriceUpdater);
        // table rendering
        nlp.addReplyPlugin("es", new PeruPriceTableReplyPlugin());
        nlp.addReplyPlugin("es", new PeruPriceTableTxtReplyPlugin());
        // entity plugin (datasheet)
        nlp.getEntityPlugins().get("es").add(new PeruPriceEntityPlugin(nlp));
    }

    public void startHonduras(){
        if(hnWheatherConfig == null){
            return;
        }

        // entity plugin (datasheet)
        final HNWeatherEntityPlugin hnWeatherPlugin = new HNWeatherEntityPlugin(this);
        nlp.getEntityPlugins().get("es").add(hnWeatherPlugin);
        nlp.addReplyPlugin("es", hnWeatherPlugin);
    }

    private KeycloakConfiguration keycloakConfiguration = new KeycloakConfiguration();

    public KeycloakConfiguration getKeycloakConfiguration() {
        return keycloakConfiguration;
    }

    public void setKeycloakConfiguration(KeycloakConfiguration keycloakConfiguration) {
        this.keycloakConfiguration = keycloakConfiguration;
    }


}