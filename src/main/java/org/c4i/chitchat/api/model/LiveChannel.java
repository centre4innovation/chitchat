package org.c4i.chitchat.api.model;

/**
 * Class that describes attributes of a Live Channel
 * @author Boaz Manger
 */
public final class LiveChannel {

    private final String channelName;
    private final String channelType;
    private final Boolean activeScript;

    public LiveChannel(String channelName, String channelType, Boolean activeScript) {
        this.channelName = channelName;
        this.channelType = channelType;
        this.activeScript = activeScript;
    }

    public LiveChannel(String channelName, String channelType) {
        this.channelName = channelName;
        this.channelType = channelType;
        this.activeScript = Boolean.FALSE;
    }

    public String getChannelName() {return this.channelName;}
    public String getChannelType() {return this.channelType;}
    public Boolean getActiveScript() {return this.activeScript;}
}