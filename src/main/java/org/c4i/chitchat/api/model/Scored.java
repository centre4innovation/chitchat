package org.c4i.chitchat.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Scored values, aka Values with a weight tag.
 * @author Arvid Halma
 * @version 4-6-2015 - 20:59
 */
public class Scored<V> implements Comparable<Scored<V>>{
    @JsonProperty
    private V value;

    @JsonProperty
    private double score;

    public Scored() {
    }

    public Scored(V value, double score) {
        this.value = value;
        this.score = score;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public int compareTo(Scored<V> that) {
        return that == null ? 0 : Double.compare(this.score, that.score);
    }

    @Override
    public String toString() {
        return value + " (" + score + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Scored)) return false;

        Scored<?> scored = (Scored<?>) o;

        if (Double.compare(scored.score, score) != 0)
            return false;
        return Objects.equals(value, scored.value);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = value != null ? value.hashCode() : 0;
        temp = Double.doubleToLongBits(score);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
