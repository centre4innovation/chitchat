package org.c4i.chitchat.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.validation.MinDuration;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.File;
import io.dropwizard.util.Duration;
import java.util.concurrent.TimeUnit;

/**
 * Application runtime specific configuration.
 */
public class AppConfig {

    @JsonProperty(defaultValue = "data")
    private File dataDir = new File("data");

    @JsonProperty(defaultValue = "true")
    private boolean loadDataAsync = true;


    @JsonProperty(defaultValue = "10m")
    @NotNull
    @MinDuration(
        value = 1L,
        unit = TimeUnit.SECONDS,
        inclusive = true
    )
    private Duration maxConversationTimeInterval = Duration.minutes(10);

    @JsonProperty(defaultValue = "10000")
    @Min(1)
    private int maxConversations = 10_000;

    @JsonProperty(defaultValue = "true")
    private boolean prettyPrintJsonResponse = true;


    @JsonProperty(defaultValue = "prod")
    private String mode = "prod";


    public Duration getMaxConversationTimeInterval() {
        return maxConversationTimeInterval;
    }

    public int getMaxConversations() {
        return maxConversations;
    }

    public boolean isLoadDataAsync() {
        return loadDataAsync;
    }

    public File getDataDir() {
        return dataDir;
    }

    public boolean isPrettyPrintJsonResponse() {
        return prettyPrintJsonResponse;
    }

    public String getMode() {
        return mode;
    }

    @Override
    public String toString() {
        return "AppConfig{" +
                "dataDir=" + dataDir +
                ", loadDataAsync=" + loadDataAsync +
                ", maxConversationTimeInterval=" + maxConversationTimeInterval +
                ", maxConversations=" + maxConversations +
                ", prettyPrintJsonResponse=" + prettyPrintJsonResponse +
                '}';
    }
}
