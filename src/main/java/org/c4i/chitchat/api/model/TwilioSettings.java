package org.c4i.chitchat.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Twilio credentials.
 */
public class TwilioSettings {
    @NotEmpty
    @JsonProperty
    private String webhookBaseURI;  // https://api.twilio.com/2010-04-01

    @JsonProperty
    public List<TwilioCredentials> credentials;

    public String getWebhookBaseURI() {
        return webhookBaseURI;
    }

    public TwilioSettings setWebhookBaseURI(String webhookBaseURI) {
        this.webhookBaseURI = webhookBaseURI;
        return this;
    }

    public List<TwilioCredentials> getCredentials() {
        return credentials;
    }

    public TwilioSettings setCredentials(List<TwilioCredentials> credentials) {
        this.credentials = credentials;
        return this;
    }

    /**
     * HTTP requests to the REST API are protected with HTTP Basic authentication.
     * You will use your Twilio account SID as the username and your auth token as the password for HTTP Basic authentication.
     * https://www.twilio.com/docs/iam/api
     */
    public static class TwilioCredentials {
        @NotEmpty
        @JsonProperty
        private String accountSID;

        @NotEmpty
        @JsonProperty
        private String authToken;

        public TwilioCredentials(){}

        public TwilioCredentials(String accountSID, String authToken) {
            this.accountSID = accountSID;
            this.authToken = authToken;
        }

        public String getAccountSID() {
            return accountSID;
        }

        public TwilioCredentials setAccountSID(String accountSID) {
            this.accountSID = accountSID;
            return this;
        }

        public String getAuthToken() {
            return authToken;
        }

        public TwilioCredentials setAuthToken(String authToken) {
            this.authToken = authToken;
            return this;
        }
    }
}