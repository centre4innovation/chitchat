package org.c4i.chitchat.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Ace {
    @JsonProperty
    private String fid;

    @JsonProperty
    private String role;

    public Ace() {
    }

    public Ace(String fid, String role) {
        this.fid = fid;
        this.role = role;
    }

    public String getFid() {
        return fid;
    }

    public Ace setFid(String fid) {
        this.fid = fid;
        return this;
    }

    public String getRole() {
        return role;
    }

    public Ace setRole(String role) {
        this.role = role;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ace ace = (Ace) o;
        return Objects.equals(fid, ace.fid) &&
                Objects.equals(role, ace.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fid, role);
    }
}
