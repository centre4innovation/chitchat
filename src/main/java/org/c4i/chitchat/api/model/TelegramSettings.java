package org.c4i.chitchat.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Telegram credentials.
 */
public class TelegramSettings {
    @NotEmpty
    @JsonProperty
    private String webhookBaseURI;

    @JsonProperty
    public List<TelegramCredentials> credentials;

    public List<TelegramCredentials> getTelegramCredentials() {
        return credentials;
    }

    public TelegramSettings setTelegramCredentials(List<TelegramCredentials> credentials, String webhookBaseURI) {
        this.credentials = credentials;
        this.webhookBaseURI = webhookBaseURI;
        return this;
    }

    public String getWebhookBaseURI() {
        return webhookBaseURI;
    }

    public static class TelegramCredentials {
        @NotEmpty
        @JsonProperty
        private String botToken;

        @NotEmpty
        @JsonProperty
        private String botName;

        @NotEmpty
        @JsonProperty
        private String botUsername;

        public TelegramCredentials(){}

        public TelegramCredentials(String botName, String botToken, String botUsername) {
            this.botName = botName;
            this.botToken = botToken;
            this.botUsername = botUsername;
        }

        public String getBotToken() {
            return botToken;
        }

        public String getBotName() {
            return botName;
        }

        public String getBotUsername() {
            return botUsername;
        }

        }
}