package org.c4i.chitchat.api.sec;

import de.ahus1.keycloak.dropwizard.AbstractKeycloakAuthenticator;
import de.ahus1.keycloak.dropwizard.KeycloakConfiguration;
import org.keycloak.KeycloakSecurityContext;

import javax.servlet.http.HttpServletRequest;

public class KeycloakAuthenticator extends AbstractKeycloakAuthenticator<KeycloakUser> {

    public KeycloakAuthenticator(KeycloakConfiguration configuration) {
        super(configuration);
    }

    @Override
    protected KeycloakUser prepareAuthentication(KeycloakSecurityContext securityContext, HttpServletRequest request,
                                                 KeycloakConfiguration configuration) {
        return new KeycloakUser(securityContext, request, configuration);
    }
}
