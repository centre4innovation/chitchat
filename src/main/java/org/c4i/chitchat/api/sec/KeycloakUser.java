package org.c4i.chitchat.api.sec;

import de.ahus1.keycloak.dropwizard.AbstractUser;
import de.ahus1.keycloak.dropwizard.KeycloakConfiguration;
import org.keycloak.KeycloakSecurityContext;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import java.util.HashSet;
import java.util.Set;

public class KeycloakUser extends AbstractUser {

    public KeycloakUser(KeycloakSecurityContext securityContext, HttpServletRequest request,
                        KeycloakConfiguration keycloakConfiguration) {
        super(request, securityContext, keycloakConfiguration);
    }

    public void checkUserInRole(String role) {
        if (!getRoles().contains(role)) {
            throw new ForbiddenException();
        }
    }

    @Override
    public String getName() {
        return securityContext.getToken().getName();
    }
}
