package org.c4i.chitchat.api.sec;

import io.dropwizard.auth.Authorizer;

import javax.ws.rs.ForbiddenException;

public class KeycloakAuthorizer implements Authorizer<KeycloakUser> {

    @Override
    public boolean authorize(KeycloakUser keycloakUser, String role) {
        try {
            keycloakUser.checkUserInRole(role);
            return true;
        } catch (ForbiddenException e) {
            return false;
        }
    }
}
