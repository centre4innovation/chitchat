package org.c4i.chitchat.api.db;


/**
 * Access to Data Acces Objects.
 * Inception exception thrown!
 * @author Arvid Halma
 * @version 13-4-18
 */
public class Dao {
    public BaseDao baseDao;
    public ConversationDao conversationDao;
    public TextDocDao textDocDao;
    public JsonDocDao jsonDocDao;
    public AceDao aceDao;

    public Dao(BaseDao baseDao, ConversationDao conversationDao, TextDocDao textDocDao, JsonDocDao jsonDocDao, AceDao aceDao) {
        this.baseDao = baseDao;
        this.conversationDao = conversationDao;
        this.textDocDao = textDocDao;
        this.jsonDocDao = jsonDocDao;
        this.aceDao = aceDao;
    }
}
