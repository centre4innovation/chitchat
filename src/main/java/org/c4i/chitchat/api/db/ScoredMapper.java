package org.c4i.chitchat.api.db;



import org.c4i.chitchat.api.model.Scored;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Turn database results into {@link Scored} values.
 * @author Arvid Halma
 * @version 1-8-2016
 */
public class ScoredMapper implements RowMapper<Scored<String>> {
    @Override
    public Scored<String> map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Scored<>(
                rs.getObject("value").toString(),
                rs.getDouble("score")
        );
    }
}