package org.c4i.chitchat.api.db;

import org.jdbi.v3.sqlobject.SqlObject;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

/**
 * Check database core functionality
 * @author Arvid Halma
 * @version 13-4-18
 */
public interface BaseDao extends SqlObject {
    @SqlQuery(// language=SQL
            "SELECT version();"
    )
    String getPostgresVersion();


    @SqlQuery(// language=SQL
            "SELECT count(extname) FROM pg_extension WHERE extname = 'uuid-ossp';"
    )
    Boolean hasUuidOssp();

    @SqlQuery(// language=SQL
            "SELECT count(extname) FROM pg_extension WHERE extname = 'pgcrypto';"
    )
    Boolean hasPgcrypto();

}
