package org.c4i.chitchat.api.db;

import org.c4i.chitchat.api.model.Ace;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AceMapper implements RowMapper<Ace> {
    @Override
    public Ace map(ResultSet rs, StatementContext ctx) {
        return new Ace()
                .setFid("id")
                .setRole("role");
    }
}