package org.c4i.chitchat.api.db;

import org.c4i.chitchat.api.model.Ace;
import org.c4i.chitchat.api.model.TextDoc;
import org.jdbi.v3.sqlobject.SqlObject;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.BindBeanList;
import org.jdbi.v3.sqlobject.customizer.BindList;
import org.jdbi.v3.sqlobject.statement.SqlBatch;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import javax.xml.soap.Text;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Access Control Entries.
 */
public interface AceDao extends SqlObject {
    @SqlUpdate(// language=SQL
            "INSERT INTO ace (fid, role) " +
                    "VALUES (:fid, :role) " +
                    "ON CONFLICT (fid, role) \n" +
                    "DO UPDATE SET role = :role\n" +
                    "WHERE ace.fid = :fid;")
    void upsert(@BindBean Ace ace);

    @SqlBatch(// language=SQL
            "INSERT INTO ace (fid, role) " +
                    "VALUES (:fid, :role) " +
                    "ON CONFLICT (fid, role) \n" +
                    "DO UPDATE SET role = :role\n" +
                    "WHERE ace.fid = :fid;")
    void upsert(@BindBean List<Ace> aces);

    @SqlQuery(// language=SQL
            "SELECT DISTINCT(role) FROM ace WHERE fid = :fid;" )
    List<String> getRolesById(@Bind("fid") String fid);

    @SqlQuery(// language=SQL
            "SELECT DISTINCT(role) FROM ace WHERE fid IN (<fids>)" )
    List<String> getRolesByIds(@BindList("fids") List<String> fids);

    @SqlQuery(// language=SQL
            "SELECT fid, array_agg(role) as roles\n" +
                    "FROM ace\n" +
                    "GROUP BY fid\n" +
                    "HAVING array_agg(role) <@ ARRAY[<roles>, '_public']" )
    Set<String> getFidsByRoles(@BindList("roles") Collection<String> roles);

    @SqlQuery(// language=SQL
            "SELECT t1.id, t1.name, t1.tag, t1.type, t1.created, t2.maxupdated AS updated, null AS body\n" +
                    "FROM textdoc t1 \n" +
                    "RIGHT JOIN (SELECT fid, array_agg(role) as roles\n" +
                    "              FROM ace\n" +
                    "             GROUP BY fid\n" +
                    "            HAVING array_agg(role) <@ ARRAY[<roles>, '_public']) AS fids ON t1.id = fids.fid\n" +
                    "INNER JOIN\n" +
                    "(\n" +
                    "  SELECT max(updated) maxupdated, name\n" +
                    "  FROM textdoc\n" +
                    "  GROUP BY name\n" +
                    ") t2\n" +
                    "  ON t1.name = t2.name\n" +
                    "  AND t1.updated = t2.maxupdated\n" +
                    " WHERE t1.type = :type" )
    List<TextDoc> getAllMetaLastUpdatedByRoles(@Bind("type") String type, @BindList("roles") List<String> roles);

    @SqlQuery(// language=SQL
            "SELECT t1.id, t1.name, t1.tag, t1.type, t1.created, t2.maxupdated AS updated, t1.body\n" +
                    "FROM textdoc t1 \n" +
                    "RIGHT JOIN (SELECT fid, array_agg(role) as roles\n" +
                    "              FROM ace\n" +
                    "             GROUP BY fid\n" +
                    "            HAVING array_agg(role) <@ ARRAY[<roles>, '_public']) AS fids ON t1.id = fids.fid\n" +
                    "INNER JOIN\n" +
                    "(\n" +
                    "  SELECT max(updated) maxupdated, name\n" +
                    "  FROM textdoc\n" +
                    "  GROUP BY name\n" +
                    ") t2\n" +
                    "  ON t1.name = t2.name\n" +
                    "  AND t1.updated = t2.maxupdated\n " +
                    " WHERE type = :type" )
    List<TextDoc> getAllLastUpdatedByRoles(@Bind("type") String type, @BindList("roles") List<String> roles);

    @SqlQuery(// language=SQL
            "SELECT SELECT t1.id, t1.name, t1.tag, t1.type, t1.created, t1.updated, null AS body\n" +
                    "FROM textdoc t1 \n" +
                    "RIGHT JOIN (SELECT fid, array_agg(role) as roles\n" +
                    "              FROM ace\n" +
                    "             GROUP BY fid\n" +
                    "            HAVING array_agg(role) <@ ARRAY[<roles>, '_public']) AS fids ON t1.id = fids.fid\n" +
                    "WHERE name = :name AND type = :type " +
                    "ORDER BY updated DESC;" )
    List<TextDoc> getAllMetasByRoles(@Bind("type") String type, @Bind("name") String name, @BindList("roles") List<String> roles);

    default Boolean hasAccessToDoc(String id, Collection<String> roles){
        return getFidsByRoles(roles).contains(id);
    }


}
