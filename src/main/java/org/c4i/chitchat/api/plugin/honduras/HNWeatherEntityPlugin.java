package org.c4i.chitchat.api.plugin.honduras;

import com.google.common.collect.ImmutableList;
import org.c4i.chitchat.api.Config;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.match.*;
import org.c4i.nlp.tokenize.Token;
import org.c4i.util.Csv;
import org.c4i.util.GeoImage;
import org.c4i.util.time.*;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Find latest prices of commodities in Peru given MINAGRI data.
 * @author Arvid Halma
 */
public class HNWeatherEntityPlugin implements EntityPlugin, ReplyPlugin {
    private final Logger logger = LoggerFactory.getLogger(HNWeatherEntityPlugin.class);
    private final File plotDir;

    Config config;

    HNData hnData;

    public static final Pattern HNPLOT_PROP = Pattern.compile("\\bHNPLOT *\\((.*?)\\)");
    public static final Pattern HNMAP_PROP = Pattern.compile("\\bHNMAP *\\((.*?)\\)");

    private GeoImage geoImage;

    public HNWeatherEntityPlugin(Config config) {
        this.config = config;
        this.plotDir = new File(config.appConfig.getDataDir(), "img/hnplot");
        plotDir.mkdirs();

        hnData = new HNData(config);

        try {
            // geoImage = new GeoImage(new File(config.appConfig.getDataDir(), "img/hnplot/honduras.png"),
            geoImage = new GeoImage(ImageIO.read(getClass().getResource("resources/assets/assets/usr/wfp/honduras.png")),
                    new Point2D.Double(-89.31884765625, 15.987734284909871),
                    new Point2D.Double(-83.82568359375001, 15.987734284909871),
                    new Point2D.Double(-83.82568359375001, 13.33083009512624),
                    new Point2D.Double(-89.31884765625, 13.33083009512624)
            );
        } catch (Exception e) {
            logger.error("Could not load Honduras map image.", e);
        }
    }

    @Override
    public boolean accept(Literal lit) {
        return lit.equals("HNWEATHER");
    }

    @Override
    public List<Range> find(Token[] text, Literal lit, String label, int location, Collection<Range> context) {

        // find last location filter
        List<Range> stations = context.stream().filter(r -> "where".equals(r.label)).collect(Collectors.toList());
        String locationName = stations.isEmpty() ? "Honduras" : stations.get(0).value;
        Optional<Range> whenFilter = context.stream().filter(r -> "when".equals(r.label)).findFirst();
        Optional<Range> whatFilter = context.stream().filter(r -> "what".equals(r.label)).findFirst();

        final ImmutableList<Optional<Range>> filters = ImmutableList.of(whenFilter, whatFilter);
        if(stations.isEmpty() && filters.stream().noneMatch(Optional::isPresent)){
            // no information defined at all
            return ImmutableList.of();
        }

        final LinkedHashMap<String, String> props = new LinkedHashMap<>();
        props.put("HNWEATHER", "true");
        props.put("time", whenFilter.isPresent() ? whenFilter.get().props.get("timestamp") : DateTime.now().toString());
        props.put("location", locationName);
        props.put("stations", String.join(";", stations.stream().map(range -> range.props.get("tabla")).collect(Collectors.toSet())));
        props.put("stationcount", String.valueOf(stations.size()));
        final String what = whatFilter.isPresent() ? whatFilter.get().props.get("datatype") : "TemperaturaAmbiente";
        props.put("datatype", what);
        props.put("dataname", HNData.DATATYPE_TO_NAME.get(what));
        props.put("dataunit", HNData.DATATYPE_TO_UNIT.get(what));
        // all measurements
        final DateTime t = DateTimeUtil.parseLiberalDateTime(props.get("time"));
//        props.put("TemperaturaAmbiente", ""+timeline.interpolatedValue(t, rec -> rec.temperaturaAmbiente, StatReduce.MEAN));
//        props.put("HumedadAmbiente", ""+timeline.interpolatedValue(t, rec -> rec.humedadAmbiente, StatReduce.MEAN));
//        props.put("HumedadSuelo", ""+timeline.interpolatedValue(t, rec -> rec.humedadSuelo, StatReduce.MEAN));
//        props.put("Lluvia", ""+timeline.interpolatedValue(t, rec -> rec.lluvia, StatReduce.MEAN));
//        props.put("Luxometro", ""+timeline.interpolatedValue(t, rec -> rec.luxometro, StatReduce.MEAN));
//        props.put("VelocidadViento", ""+timeline.interpolatedValue(t, rec -> rec.velocidadViento, StatReduce.MEAN));
//        props.put("DireccionViento", ""+timeline.interpolatedValue(t, rec -> rec.direccionViento, StatReduce.MEAN));

        Range result = new Range(label, 0, text.length, 0, text[text.length-1].getCharEnd(), props);
        return ImmutableList.of(result);
    }

    @Override
    public String description() {
        return "HNWEATHER";
    }


    @Override
    public String updateReply(String reply, String userText, Script script, List<Range> ranges, Nlp nlp) {
        Matcher matcher = HNPLOT_PROP.matcher(reply);
        long id = System.currentTimeMillis();

        if(matcher.find()) {
            Optional<Range> hnweatherRangeOptional = ranges.stream().filter(r -> r.props.containsKey("HNWEATHER")).findFirst();
            if (!hnweatherRangeOptional.isPresent()) {
                return reply;
            }

            Range hnweatherRange = hnweatherRangeOptional.get();
            final String what = hnweatherRange.props.get("datatype");
            List<String> stations = Arrays.asList(hnweatherRange.props.get("stations").split(";"));
            if (stations.get(0).isEmpty()) {
                stations = new ArrayList<>(hnData.getStations().keySet());
            }

            Timeline<HNWeatherRecord> timeline = hnData.timeline(stations);

            try {
                String baseUrl = matcher.group(1).trim();
                final Timeline<Double> maxSeries = timeline.resample(Period.days(1), record -> record.get(what), StatReduce.MAX, Double.NaN);
                final Timeline<Double> meanSeries = timeline.resample(Period.days(1), record -> record.get(what), StatReduce.MEAN, Double.NaN);
                final Timeline<Double> minSeries = timeline.resample(Period.days(1), record -> record.get(what), StatReduce.MIN, Double.NaN);
                File plotFile = new File(plotDir, "plot-" + id + ".png");
                File mapFile = new File(plotDir, "map-" + id + ".png");
                if (!plotFile.exists()) {
                    HNPlotX.plot(HNData.DATATYPE_TO_NAME.get(what), HNData.DATATYPE_TO_UNIT.get(what), minSeries, meanSeries, maxSeries, plotFile);
                    List<Point2D.Double> lngLatPoints = stations.stream()
                            .map(s -> {
                                HNStation station = hnData.getStation(s);
                                return new Point2D.Double(station.longitud, station.latitud);
                            }).collect(Collectors.toList());
                    geoImage.renderPng(mapFile, lngLatPoints);
                }
                reply = HNPLOT_PROP.matcher(reply).replaceAll("IMAGE("+baseUrl+"/api/v1/img/hnplot/"+plotFile.getName()+")");
            } catch (IOException exception) {
                reply = HNPLOT_PROP.matcher(reply).replaceAll("No plot available...");
                logger.warn("Could not generate HNPLOT", exception);
            }
        }

        matcher = HNMAP_PROP.matcher(reply);
        if(matcher.find()) {
            Optional<Range> hnweatherRangeOptional = ranges.stream().filter(r -> r.props.containsKey("HNWEATHER")).findFirst();
            if (!hnweatherRangeOptional.isPresent()) {
                return reply;
            }

            Range hnweatherRange = hnweatherRangeOptional.get();
            List<String> stations = Arrays.asList(hnweatherRange.props.get("stations").split(";"));
            if (stations.get(0).isEmpty()) {
                stations = new ArrayList<>(hnData.getStations().keySet());
            }

            try {
                String baseUrl = matcher.group(1).trim();
                File mapFile = new File(plotDir, "map-" + id + ".png");
                if (!mapFile.exists()) {
                    List<Point2D.Double> lngLatPoints = stations.stream()
                            .map(s -> {
                                HNStation station = hnData.getStation(s);
                                return new Point2D.Double(station.longitud, station.latitud);
                            }).collect(Collectors.toList());
                    geoImage.renderPng(mapFile, lngLatPoints);
                }
                reply = HNMAP_PROP.matcher(reply).replaceAll("IMAGE(" + baseUrl + "/api/v1/img/hnplot/" + mapFile.getName() + ")");
            } catch (IOException exception) {
                reply = HNMAP_PROP.matcher(reply).replaceAll("No map available...");
                logger.warn("Could not generate HNMAP", exception);

            }
        }
        return reply;
    }

}
