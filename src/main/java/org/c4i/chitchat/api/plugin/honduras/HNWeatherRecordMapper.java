package org.c4i.chitchat.api.plugin.honduras;

import org.c4i.chitchat.api.model.TextDoc;
import org.c4i.util.time.DateTimeUtil;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeParser;
import org.joda.time.format.ISODateTimeFormat;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Turn database results into {@link TextDoc} values.
 * @author Arvid Halma
 */
public class HNWeatherRecordMapper implements RowMapper<HNWeatherRecord> {
    DateTimeFormatter dateTimeParser = ISODateTimeFormat.dateTimeParser();

    @Override
    public HNWeatherRecord map(ResultSet rs, StatementContext ctx) throws SQLException {
        HNWeatherRecord record = new HNWeatherRecord();
        record.id = rs.getLong("id");
        record.fecha = dateTimeParser.parseDateTime(rs.getString("fecha").replace(' ', 'T'));
        record.bateria = rs.getDouble("bateria");
        record.humedadSuelo = rs.getDouble("humedad_suelo");
        record.humedadAmbiente = rs.getDouble("humedad_ambiente");
        record.temperaturaAmbiente = rs.getDouble("temperatura_ambiente");
        record.velocidadViento = rs.getDouble("velocidad_viento");
        record.direccionViento = rs.getDouble("direccion_viento");
        record.lluvia = rs.getDouble("lluvia");
        record.luxometro = rs.getDouble("luxometro");
        return record;
    }
}