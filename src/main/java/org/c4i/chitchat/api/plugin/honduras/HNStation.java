package org.c4i.chitchat.api.plugin.honduras;

import java.util.Objects;

/**
 * Weather station location
 * @author Arvid Halma
 */
public class HNStation {
    public long id;
    public String nombre;
    public double latitud;
    public double longitud;
    public String tabla;
    public String departamentos;
    public String municipios;
    public int habilitada;

    public HNStation() {
    }



    @Override
    public String toString() {
        return "HNStation{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                ", tabla='" + tabla + '\'' +
                ", departamentos_id=" + departamentos +
                ", municipios_id=" + municipios +
                ", habilitada=" + habilitada +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HNStation hnStation = (HNStation) o;
        return id == hnStation.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
