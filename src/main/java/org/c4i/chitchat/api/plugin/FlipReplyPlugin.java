package org.c4i.chitchat.api.plugin;

import org.c4i.nlp.Nlp;
import org.c4i.nlp.match.Range;
import org.c4i.nlp.match.ReplyPlugin;
import org.c4i.nlp.match.Script;
import org.c4i.util.StringUtil;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Converts "Arvid & Halma!" to "(╯°□°)╯︵ ¡ɐɯlɐH ⅋ pᴉʌɹⱯ"
 */
public class FlipReplyPlugin implements ReplyPlugin {
    // https://en.wikipedia.org/wiki/Transformation_of_text#Upside-down_text
    static String up = ";,.'\"!?_&1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static String dn = "؛'˙,„¡¿‾⅋⇂ᘔƐ߈59ㄥ860ⱯᗺƆᗡƎℲ⅁HIᒋꓘ⅂WNOԀტᴚSꞱՈΛMX⅄Zɐqɔpǝɟɓɥᴉɾʞlɯuodbɹsʇnʌʍxʎz";

    public static final Pattern FLIP_PROP = Pattern.compile("\\bFLIP *\\((.*?)\\)");

    public static String flip(String s){
        StringBuilder result = new StringBuilder("(╯°□°)╯︵ ");
        StringUtil.reverse(s).chars().forEach(c -> {
            int i = up.indexOf(c);
            result.append(i >= 0 ? dn.charAt(i) : (char)c);
        });
        return result.toString();
    }

    @Override
    public String updateReply(String reply, String userText, Script script, List<Range> ranges, Nlp nlp) {
        Matcher matcher = FLIP_PROP.matcher(reply);
        StringBuffer result = new StringBuffer();
        while (matcher.find()){
            matcher.appendReplacement(result, Matcher.quoteReplacement(flip(matcher.group(1))));
        }
        matcher.appendTail(result);
        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println("FLIP(\"Arvid & Halma!\") = " + flip("Arvid & Halma!"));
    }
}
