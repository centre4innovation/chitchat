package org.c4i.chitchat.api.plugin.peru;

import com.google.common.collect.ImmutableList;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.c4i.chitchat.api.Config;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.match.Range;
import org.c4i.nlp.match.ReplyPlugin;
import org.c4i.nlp.match.Script;
import org.c4i.nlp.ner.DataSheet;
import org.c4i.nlp.normalize.StringNormalizers;
import org.c4i.util.*;
import org.c4i.util.time.DateTimeUtil;
import org.c4i.util.time.TimeValue;
import org.c4i.util.time.Timeline;
import org.joda.time.DateTime;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Scheduled job to retrieve latest price data and make them available as data sheets.
 * "VARIEDAD","PRODUCTO","MCDO","PRECIOKG","FECHA"
 * @author Arvid Halma
 * @version 21-5-19
 */
public class PeruPriceHistoryUpdater implements ReplyPlugin {
    private final Logger logger = LoggerFactory.getLogger(PeruPriceHistoryUpdater.class);

    private Config config;
    private int count = 0;
    private DateTime lastUpdate = DateTime.now();
    private String lastHash;

    private ScheduledExecutorService scheduledExecutorService;
    private ScheduledFuture<?> scheduledFuture;

    private Map<String, Timeline<TimeValue>> timeLineMap;

    private File plotDir;

    // parts of keys, that can't be stand-alone matches. They are too instinctive (e.g. common adjectives)
    private List<String> partialKeyGreyList = ImmutableList.of();


    public static final Pattern PERUPLOT_PROP = Pattern.compile("\\bPERUPLOT *\\((.*?)\\)");

    public PeruPriceHistoryUpdater(Config config) {
        this.config = config;
        this.plotDir = new File(config.appConfig.getDataDir(), "img/peruplot");
        plotDir.mkdirs();
        this.timeLineMap = new LinkedHashMap<>();
        if(config.peruPriceConfig.stopwords != null) {
            partialKeyGreyList = config.peruPriceConfig.stopwords;
            config.getNlp().getStopWords("es").addAll(partialKeyGreyList);
        }
    }

    public void start(){
        logger.warn("Starting PeruPriceUpdater service...");
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        UpdateTask task = new UpdateTask();

        // init Delay = 5 sec, repeat the task every config.updateInterval
        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(task, 5, config.peruPriceConfig.updateInterval.toSeconds(), TimeUnit.SECONDS);
    }

    public void stop(){
        logger.warn("Stopping PeruPriceUpdater service...");
        scheduledFuture.cancel(true);
        scheduledExecutorService.shutdown();
    }

    public int getCount() {
        return count;
    }

    public DateTime getLastUpdate() {
        return lastUpdate;
    }

    private class UpdateTask implements Runnable {
        final String lang = "es";
        final String sheetName = "BUSCAPRECIOSPERU";

        @Override
        public void run() {
            try {
                if(DateTime.now().getMillis() - lastUpdate.getMillis() < 3000){
                    // called within 3 seconds. Prevent DoS attack with buffered/scheduled tasks.
                    return;
                }

                String url = config.peruPriceConfig.url + "PERU_PRECIOS__30_DIAS.csv";

                // todo http://sistemas.minagri.gob.pe/CSV_AGROCHATEA/DATA/PERU_PRECIOS_30_DIAS_TOTAL.csv
                // String url = config.peruPriceConfig.url + "PERU_PRECIOS_30_DIAS_TOTAL.csv";

                // Clear plot cache
                try {
                    FileUtils.cleanDirectory(plotDir);
                } catch (IOException e){
                    logger.error("Could not delete stale plots in {}, reason: {}", plotDir, e.getMessage());
                }

                // Now...
                // 1. setup interval loop
                //   2. retrieve CSV, skip 3. if it is the same as last time
                //   3. validate (parse)
                //     4. save in DB
                //     5. update datasheet reference
                //     6. call onUpdate()

                String csv;
                try {
                    final Connection.Response response = Jsoup.connect(url).ignoreContentType(true).followRedirects(true).execute();

                    // csv = new String(response.bodyAsBytes(), StandardCharsets.UTF_8);
                    csv = new String(response.bodyAsBytes(), Charset.forName("windows-1252"));
                    csv = csv.replaceAll(";|\",\"", "\t").replace("\"", "");

                    logger.warn("Peru price data successfully retrieved data from {}", url);
                } catch (IOException e) {
                    logger.error("Peru price data could not be retrieved data from {}, reason: {}", url, e.getMessage());
                    return;
                }


                final String hash = Hash.sha256Hex(csv);
                if (hash.equals(lastHash)) {
                    // already up-to-date
                    logger.info("Peru price data already up-to-date: {}", url);
                    return;
                }

                try {
                    timeLineMap = timeLineMap(csv);
                } catch (IOException e) {
                    logger.error("Peru price data could not be parsed: {}", e.getMessage());
                    return;
                }

                // keep latest price
                csv = deduplicate(csv);


                Nlp nlp = config.getNlp();

                DataSheet sheet;
                try {
                    sheet = new DataSheet(sheetName, csv, nlp.getNormalizer(lang), nlp.getWordTokenizer(lang), nlp.getStopWords(lang), true);
                    logger.warn("Peru price data successfully parsed");
                } catch (Exception e) {
                    logger.error("Peru price data could not be parsed: {}", e.getMessage());
                    return;
                }

                sheet.setPartialKeyGreyList(partialKeyGreyList);
                sheet.setRangeToUniqueKey(range -> range.props.get("variedad")+range.props.get("mcdo"));

                String docName = lang + "/" + sheetName;
                try {
                    config.databaseResource.saveTextDoc("datasheet", docName, csv, null, Collections.singleton("_public"));
                    logger.warn("Peru price data successfully saved to DB as '{}'", docName);
                } catch (Exception e) {
                    logger.error("Peru price data could not be save to DB as '{}', reason: {}", docName, e.getMessage());
                    return;
                }

                try {
                    config.getNlp().loadDataSheet(lang, sheetName, sheet);
                    logger.warn("Peru price data available as '{}'", docName);
                } catch (Exception e) {
                    logger.error("Peru price data '{}' is not available, reason: {}", docName, e.getMessage());
                    return;
                }



                count++;
                lastUpdate = DateTime.now();
                lastHash = hash;
            } catch (Throwable e){
                // Catch all exceptions and errors
                // Any thrown exception or error reaching the executor causes the executor to halt.
                // No more invocations on the Runnable, no more work done.
                // This work stoppage happens silently, you'll not be informed.
                logger.error("Peru price update failed, unknown reason: {}", e.getMessage());
            }
        }

        private Map<String, Timeline<TimeValue>> timeLineMap(String csv) throws IOException {
            Map<String, Timeline<TimeValue>> timeLineMap = new LinkedHashMap<>();
            try {
                new Csv()
                        .setInputStream(IOUtils.toInputStream(csv, "UTF-8"))
                        .formatTsv()
                        .setUseHeader(true)
                        .setColumnNameNormalizer(StringNormalizers.LOWER_CASE)
                        .process(row -> {
                            String variedad = row.getString("variedad");
                            String mcdo = row.getString("mcdo");
                            String key = variedad + " | " + mcdo;
                            if(!timeLineMap.containsKey(key)){
                                timeLineMap.put(key, new Timeline<>());
                            }
                            timeLineMap.get(key).add(new TimeValue(DateTimeUtil.parseLiberalDateTime(row.getString("fecha")), row.getDouble("preciokg")));
                        });
            } catch (Exception ignored) {}
            return timeLineMap;
        }

        /**
         * Only keep the first instance of rows, where the variaded per mercado is the same.
         * @param csv data with price data upto 30 days back.
         * @return csv with the first (i.e. last-updated) price for each variedad
         */
        private String deduplicate(String csv){
            StringBuilder clean = new StringBuilder();
            Set<String> done = new HashSet<>();
            for (String line : csv.split("\n")) {
                String[] cells = line.split("\t");
                String key = cells[0] + cells[2]; // variedad + mcdo
                // todo: all regions
                // String key = cells[0] + cells[3]; // variedad + mcdo
                if(!done.contains(key)){
                    clean.append(line).append('\n');
                    done.add(key);
                }
            }
            return clean.toString();
        }
    }

    private Map<String, Timeline<TimeValue>> timelines(List<Range> ranges){
        Map<String, Timeline<TimeValue>> result = new LinkedHashMap<>();
        for (Range range : ranges) {
            String key = range.props.get("variedad") + " | " + range.props.get("mcdo") ;
            Timeline<TimeValue> timeValues = timeLineMap.get(key);
            if(timeValues != null){
                result.put(key, timeValues);
            }
        }
        return result;
    }

    @Override
    public String updateReply(String reply, String userText, Script script, List<Range> ranges, Nlp nlp) {
        try {
            Matcher matcher = PERUPLOT_PROP.matcher(reply);
            if(matcher.find()){
                String baseUrl = matcher.group(1).trim();

                Map<String, Timeline<TimeValue>> timelines = timelines(ranges);
                // id = variedad combination + timeslot, per say 10 min update interval to prevent cached images to show up.
                String id = Hash.sha1Hex(String.join(",", timelines.keySet())
                        + (System.currentTimeMillis()/config.peruPriceConfig.updateInterval.toMilliseconds()));
//                File imgFile = File.createTempFile("plot", ".png", imgDir);
                File imgFile = new File(plotDir, "plot-"+id+".png");
                if(!imgFile.exists()) {
                    PeruPricePlotX.plot(timelines, imgFile);
                }
                reply = PERUPLOT_PROP.matcher(reply).replaceAll("IMAGE("+baseUrl+"/api/v1/img/peruplot/"+imgFile.getName()+")");
            }
        } catch (IOException ignored) {}
        return reply;
    }

}
