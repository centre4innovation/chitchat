package org.c4i.chitchat.api.plugin.honduras;

import org.c4i.chitchat.api.model.TextDoc;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Turn database results into {@link TextDoc} values.
 * @author Arvid Halma
 */
public class HNStationMapper implements RowMapper<HNStation> {
    @Override
    public HNStation map(ResultSet rs, StatementContext ctx) throws SQLException {
        HNStation hnStation = new HNStation();
        hnStation.id = rs.getLong("id");
        hnStation.nombre = rs.getString("nombre");
        hnStation.latitud = rs.getDouble("latitud");
        hnStation.longitud = rs.getDouble("longitud");
        hnStation.departamentos = rs.getString("departamentos");
        hnStation.municipios = rs.getString("municipios");
        hnStation.tabla = rs.getString("tabla");
        hnStation.habilitada = rs.getInt("habilitada");

        return hnStation;
    }
}