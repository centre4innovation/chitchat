package org.c4i.chitchat.api.plugin.peru;

import org.c4i.chitchat.api.Config;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.ner.DataSheet;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Scheduled job to retrieve latest price data and make them available as data sheets.
 * @author Arvid Halma
 * @version 21-5-19
 */
public class PeruPriceUpdater {
    private final Logger logger = LoggerFactory.getLogger(PeruPriceUpdater.class);

    private Config config;
    private int count = 0;
    private DateTime lastUpdate;
    private String lastFileName; // e.g. PERU_PRECIOS_20190614.csv

    private ScheduledExecutorService scheduledExecutorService;
    private ScheduledFuture<?> scheduledFuture;

    public PeruPriceUpdater(Config config) {
        this.config = config;
    }

    public void start(){
        logger.warn("Starting PeruPriceUpdater service...");
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        UpdateTask task = new UpdateTask();

        // init Delay = 5 sec, repeat the task every config.updateInterval
        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(task, 5, config.peruPriceConfig.updateInterval.toSeconds(), TimeUnit.SECONDS);
    }

    public void stop(){
        logger.warn("Stopping PeruPriceUpdater service...");
        scheduledFuture.cancel(true);
        scheduledExecutorService.shutdown();
    }

    public int getCount() {
        return count;
    }

    public DateTime getLastUpdate() {
        return lastUpdate;
    }

    private class UpdateTask implements Runnable {
        final String lang = "es";
        final String sheetName = "BUSCAPRECIOSPERU";

        @Override
        public void run() {
            final String baseUrl = config.peruPriceConfig.url;
            String fileName = "PERU_PRECIOS_" +  DateTime.now().toString(DateTimeFormat.forPattern("yyyyMMdd")) + ".csv";
            String url = baseUrl + fileName;

            // initialize URL with last available date
            for (int i = 0; i < config.peruPriceConfig.maxDaysBack; i++) {
                fileName = "PERU_PRECIOS_" +  DateTime.now().minus(Period.days(i)).toString(DateTimeFormat.forPattern("yyyyMMdd")) + ".csv";
                url = baseUrl + fileName;
                if(urlExists(url)){
                    break;
                }
            }
            logger.info("Checking Peru prices for: {} ", url);

            if(lastFileName != null && fileName.compareTo(lastFileName) <= 0){
                // already up-to-date
                logger.info("Peru price data already up-to-date: {}", url);
                return;
            }

            // Now...
            // 1. setup interval loop
            //   2. retrieve CSV
            //   3. validate (parse)
            //     4. save in DB
            //     5. update datasheet reference
            //     6. call onUpdate()

            String csv;
            try {
                final Connection.Response response = Jsoup.connect(url).ignoreContentType(true).followRedirects(true).execute();

                // csv = new String(response.bodyAsBytes(), StandardCharsets.UTF_8);
                csv = new String(response.bodyAsBytes(), Charset.forName("windows-1252"));
                csv = csv.replaceAll(",", "\t").replace("\"", "");

                logger.warn("Peru price data successfully retrieved data from {}", url);
            } catch (IOException e) {
                logger.error("Peru price data could not be retrieved data from {}, reason: {}", url, e.getMessage());
                return;
            }
            Nlp nlp = config.getNlp();

            DataSheet sheet;
            try {
                sheet = new DataSheet(sheetName, csv, nlp.getNormalizer(lang), nlp.getWordTokenizer(lang), nlp.getStopWords("es"), true);
                logger.warn("Peru price data successfully parsed");
            } catch (Exception e) {
                logger.error("Peru price data could not be parsed: {}", e.getMessage());
                return;
            }

            String docName = "es/"+sheetName;
            try {
                config.databaseResource.saveTextDoc("datasheet", docName, csv, null, Collections.singleton("_public"));
                logger.warn("Peru price data successfully saved to DB as '{}'", docName);
            } catch (Exception e) {
                logger.error("Peru price data could not be save to DB as '{}', reason: {}", docName, e.getMessage());
                return;
            }

            try {
                config.getNlp().loadDataSheet(lang, sheetName, sheet);
                logger.warn("Peru price data available as '{}'", docName);

                // Derive another data sheet, with product types as index.
//                final DataSheet prodSheet = sheet.withIndex("producto").setEntityType("PERUPROD");
//                config.getNlp().loadDataSheet(lang, "PERUPROD", prodSheet);
//                logger.warn("Peru price data available as '{}'", "es/PERUPROD");

            } catch (Exception e) {
                logger.error("Peru price data '{}' is not available, reason: {}", docName, e.getMessage());
                return;
            }

            count++;
            lastUpdate = DateTime.now();
            lastFileName = fileName;
        }

        /**
         * Check if a given resource is available.
         * The HEAD is also an HTTP request method that is identical to GET except that it does not return the response body.
         * It acquires the response code along with the response headers that we'll receive if the same resource is requested with a GET method.
         * By using the HEAD method and thereby not downloading the response body, we reduce the response time and bandwidth, and we improve performance.
         * Although most modern servers support the HEAD method, some home-grown or legacy servers might reject the HEAD method with an invalid method type error. So, we should use the HEAD method with caution.
         * The default is to follow a redirect.
         * See: https://www.baeldung.com/java-check-url-exists
         * @param url The url to check
         * @return true if the status code equals 200, else false
         */
        public boolean urlExists(String url){
            try {
                URL u = new URL(url);
                HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                huc.setRequestMethod("HEAD");
                return huc.getResponseCode() == 200;
            } catch (IOException e) {
                return false;
            }
        }
    }

}
