package org.c4i.chitchat.api.plugin.honduras;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.util.Duration;
import io.dropwizard.validation.MinDuration;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Settings on where and how to retrieve the weather data for WFP Honduras.
 * @author Arvid Halma
 * @version 16-12-19
 */
public class HNWeatherConfig {

    @JsonProperty
    protected String url;

    @JsonProperty(defaultValue = "10m")
    @NotNull
    @MinDuration(
            value = 1L,
            unit = TimeUnit.SECONDS,
            inclusive = true
    )
    protected Duration updateInterval = Duration.minutes(10);

    @JsonProperty("database")
    protected HNDatabaseConfig database;

    @JsonProperty(defaultValue = "7")
    protected int maxDaysBack;

    public HNWeatherConfig() {

    }


}
