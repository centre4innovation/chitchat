package org.c4i.chitchat.api.plugin.honduras;

import org.c4i.util.StringUtil;
import org.c4i.util.time.TimeValue;
import org.c4i.util.time.Timeline;
import org.joda.time.DateTime;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;
import org.knowm.xchart.style.XYStyler;
import org.knowm.xchart.style.markers.SeriesMarkers;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

/**
 * Plot measurements over time
 * @author Arvid Halma
 * @version 11-11-19
 */
public class HNPlotX {

    public HNPlotX() {

    }

    public static void plot(String title, String yAxisTitle, Timeline<Double> minLine, Timeline<Double> meanLine, Timeline<Double> maxLine, File file) throws IOException {

        // Create Chart
        int height = Math.max(500, 32*1+150); // ensure legend fits
        XYChart chart = new XYChart(800, height);
        final XYStyler styler = chart.getStyler();

        chart.setTitle(title);
        styler.setLegendVisible(false);


        chart.setXAxisTitle(""); // Dates are clear from the axis labels
        // chart.setXAxisTitle("Fecha");
        chart.setYAxisTitle(yAxisTitle);

        // Customize Chart
        styler.setLegendPosition(Styler.LegendPosition.InsideSW);
        styler.setLegendBackgroundColor(new Color(1f,1f,1f,.7f )); // transparent white
        styler.setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        styler.setYAxisLabelAlignment(Styler.TextAlignment.Right);
        styler.setPlotMargin(0);
        styler.setXAxisLabelRotation(-45);
        styler.setChartBackgroundColor(Color.WHITE);
        DateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag("es"));
        styler.setDatePattern("dd-MMM-yyyy");
        styler.setDecimalPattern("#0.00");

        Font titleFont = new Font(Font.SANS_SERIF, Font.BOLD, 36);
        Font fontAxis = new Font(Font.SANS_SERIF, Font.PLAIN, 24);
        Font fontLegend = new Font(Font.SANS_SERIF, Font.PLAIN, 20);
        Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 24);
        styler.setChartTitleFont(titleFont);
        styler.setLegendFont(fontLegend);
        styler.setAxisTitleFont(fontAxis);
        styler.setAxisTickLabelsFont(font);
        //styler.setLegendSeriesLineLength(12);



        List<Date> xData = new ArrayList<>();
        for (TimeValue timeValue : meanLine.asTimeValueList(d -> d)) {
            DateTime t = timeValue.getTimestamp();
            xData.add(t.toDate());
        }

        List<Double> minData = new ArrayList<>();
        for (TimeValue timeValue : minLine.asTimeValueList(d -> d)) {
            minData.add(timeValue.getValue());
        }

        List<Double> meanData = new ArrayList<>();
        for (TimeValue timeValue : meanLine.asTimeValueList(d -> d)) {
            meanData.add(timeValue.getValue());
        }

        List<Double> maxData = new ArrayList<>();
        for (TimeValue timeValue : maxLine.asTimeValueList(d -> d)) {
            maxData.add(timeValue.getValue());
        }


        if(!minData.isEmpty()) {
            final XYSeries minSeries = chart.addSeries("min", xData, minData);
            minSeries.setMarker(SeriesMarkers.NONE);
            minSeries.setLineColor(new Color(160, 193, 255));
        }
        if(!maxData.isEmpty()) {
            final XYSeries maxSeries = chart.addSeries("max", xData, maxData);
            maxSeries.setMarker(SeriesMarkers.NONE);
            maxSeries.setLineColor(new Color(160, 193, 255));
        }
        if(!meanData.isEmpty()) {
            final XYSeries meanSeries = chart.addSeries("mean", xData, meanData);
            meanSeries.setMarker(SeriesMarkers.NONE);
            meanSeries.setLineColor(new Color(50, 50, 255));
        }
        // Write to file
        final String path = file.getPath();

        BitmapEncoder.saveBitmap(chart, path, BitmapEncoder.BitmapFormat.PNG);

        // Show it
        // new SwingWrapper(chart).displayChart();
    }


}
