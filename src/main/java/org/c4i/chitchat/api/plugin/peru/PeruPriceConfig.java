package org.c4i.chitchat.api.plugin.peru;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.util.Duration;
import io.dropwizard.validation.MinDuration;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Settings on where and how to retrieve the CSV price data for WFP Peru.
 * @author Arvid Halma
 * @version 21-5-19
 */
public class PeruPriceConfig {

    @JsonProperty
    protected String url;

    @JsonProperty(defaultValue = "10m")
    @NotNull
    @MinDuration(
            value = 1L,
            unit = TimeUnit.SECONDS,
            inclusive = true
    )
    protected Duration updateInterval = Duration.minutes(10);

    @JsonProperty
    protected List<String> stopwords;


    @JsonProperty(defaultValue = "7")
    protected int maxDaysBack;

    public PeruPriceConfig() {

    }


}
