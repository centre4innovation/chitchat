package org.c4i.chitchat.api.plugin.honduras;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Connection info to the database containing meteorological measurements.
 * @author Arvid Halma
 */
public class HNDatabaseConfig {
    @JsonProperty
    public String connectionUrl;
    @JsonProperty
    public String user;
    @JsonProperty
    public String password;
}
