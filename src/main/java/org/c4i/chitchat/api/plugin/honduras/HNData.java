package org.c4i.chitchat.api.plugin.honduras;

import org.apache.commons.io.FileUtils;
import org.c4i.chitchat.api.Config;
import org.c4i.chitchat.api.plugin.peru.PeruPriceHistoryUpdater;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.ner.DataSheet;
import org.c4i.util.Csv;
import org.c4i.util.time.DateTimeUtil;
import org.c4i.util.time.Timeline;
import org.jdbi.v3.core.Jdbi;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Live honduras weather station data sets.
 * @author Arvid Halma
 */
public class HNData {
    private final Logger logger = LoggerFactory.getLogger(HNData.class);
    private final File plotDir;

    private Config config;
    private Jdbi jdbi;

    private Timeline<HNWeatherRecord> timelineAllData = new Timeline<>();
    private Map<String, Timeline<HNWeatherRecord>> stationToTimeline = new LinkedHashMap<>();
    public static Map<String, String> DATATYPE_TO_NAME = new HashMap<>();

    private DateTime lastUpdate = DateTime.now();
    private ScheduledExecutorService scheduledExecutorService;
    private ScheduledFuture<?> scheduledFuture;
    private Map<String, HNStation> stations;



    static {
        DATATYPE_TO_NAME.put("Bateria", "Bateria");
        DATATYPE_TO_NAME.put("HumedadSuelo", "Humedad");
        DATATYPE_TO_NAME.put("HumedadAmbiente", "Humedad");
        DATATYPE_TO_NAME.put("TemperaturaAmbiente", "Temperatura");
        DATATYPE_TO_NAME.put("VelocidadViento", "Viento");
        DATATYPE_TO_NAME.put("DireccionViento", "Viento");
        DATATYPE_TO_NAME.put("Lluvia", "Lluvia");
        DATATYPE_TO_NAME.put("Luxometro", "Luz");
    }

    public static Map<String, String> DATATYPE_TO_UNIT = new HashMap<>();

    static {
        DATATYPE_TO_UNIT.put("Bateria", "%");
        DATATYPE_TO_UNIT.put("HumedadSuelo", "%");
        DATATYPE_TO_UNIT.put("HumedadAmbiente", "%");
        DATATYPE_TO_UNIT.put("TemperaturaAmbiente", "° C");
        DATATYPE_TO_UNIT.put("VelocidadViento", "m/s");
        DATATYPE_TO_UNIT.put("DireccionViento", " degrees");
        DATATYPE_TO_UNIT.put("Lluvia", "mm");
        DATATYPE_TO_UNIT.put("Luxometro", "lumen");
    }

    public HNData(Config config) {
        this.config = config;
        HNDatabaseConfig dbConfig = config.hnWheatherConfig.database;
        jdbi = Jdbi.create(dbConfig.connectionUrl, dbConfig.user, dbConfig.password);
        jdbi.registerRowMapper(new HNStationMapper());
        jdbi.registerRowMapper(new HNWeatherRecordMapper());

        this.plotDir = new File(config.appConfig.getDataDir(), "img/hnplot");

        updateStationsDataSheet();
        updateTimelines();
    }

    public void start(){
        logger.warn("Starting HNData service...");
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        UpdateTask task = new UpdateTask();

        // init Delay = 5 sec, repeat the task every config.updateInterval
        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(task, 5, config.hnWheatherConfig.updateInterval.toSeconds(), TimeUnit.SECONDS);
    }

    public void stop(){
        logger.warn("Stopping HNData service...");
        scheduledFuture.cancel(true);
        scheduledExecutorService.shutdown();
    }

    private class UpdateTask implements Runnable {

        @Override
        public void run() {
            try {
                if(DateTime.now().getMillis() - lastUpdate.getMillis() < 3000){
                    // called within 3 seconds. Prevent DoS attack with buffered/scheduled tasks.
                    return;
                }
                // Clear plot cache
                try {
                    FileUtils.cleanDirectory(plotDir);
                } catch (IOException e){
                    logger.error("Could not delete stale plots in {}, reason: {}", plotDir, e.getMessage());
                }

                updateStationsDataSheet();
                updateTimelines();

                lastUpdate = DateTime.now();
            } catch (Exception e) {
                logger.error("Can't update HNData", e);

            }
        }
    }

    public void loadTestData(){
        //        2019-10-14
        DateTime lastDate = new DateTime(2019, 10, 14, 0, 0);
        Days daysToShift = Days.daysBetween(lastDate, DateTime.now());

        Timeline<HNWeatherRecord> timeline = new Timeline<>();

        try {
            new Csv().setUseHeader(true).setColumnNameNormalizer(null).setInputFile("data/test/datosEstacionPMA.txt").process(row -> {
                DateTime fecha = DateTimeUtil.parseLiberalDateTime(row.getString("Fecha"));

                timeline.add(new HNWeatherRecord(
                        row.getLong("ID"),
                        fecha.plus(daysToShift),
                        row.getDouble("Bateria"),
                        row.getDouble("HumedadSuelo"),
                        row.getDouble("HumedadAmbiente"),
                        row.getDouble("TemperaturaAmbiente"),
                        row.getDouble("VelocidadViento"),
                        row.getDouble("DireccionViento"),
                        row.getDouble("Lluvia"),
                        row.getDouble("Luxometro")
                ));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        stationToTimeline.put("Morazan", timeline);

        System.out.println("HNWeatherEntityPlugin timeline.count() = " + timeline.count());
    }

    public void updateTimelines(){
        timelineAllData = new Timeline<>();
        DateTime minTime = DateTime.now().minus(Days.days(config.hnWheatherConfig.maxDaysBack));
        for (HNStation station : retrieveStations()) {
            try {
                logger.warn("Retrieving data for HN weather station: {}", station);
                String tabla = station.tabla;
                Timeline<HNWeatherRecord> timeline = new Timeline<>();
                timeline.addAll(retrieveRecords(tabla, minTime));
                stationToTimeline.put(tabla, timeline);
                timelineAllData.addAll(timeline);
            } catch (Exception e) {
                logger.error("Can't retrieve data for HN weather station: {}", station);
            }
        }
    }

    public void updateStationsDataSheet(){
        stations = new LinkedHashMap<>();

        StringBuilder csvBuilder = new StringBuilder();
        csvBuilder.append("key").append("\t");
        csvBuilder.append("id").append("\t");
        csvBuilder.append("nombre").append("\t");
        csvBuilder.append("latitud").append("\t");
        csvBuilder.append("longitud").append("\t");
        csvBuilder.append("tabla").append("\t");
        csvBuilder.append("departamentos").append("\t");
        csvBuilder.append("municipios").append("\t");
        csvBuilder.append("habilitada").append("\n");

        for (HNStation station : retrieveStations()) {
            csvBuilder.append(station.nombre).append(" - ").append(station.departamentos).append(" - ").append(station.municipios).append('\t');
            csvBuilder.append(station.id).append('\t');
            csvBuilder.append(station.nombre).append('\t');
            csvBuilder.append(station.latitud).append('\t');
            csvBuilder.append(station.longitud).append('\t');
            csvBuilder.append(station.tabla).append('\t');
            csvBuilder.append(station.departamentos).append('\t');
            csvBuilder.append(station.municipios).append('\t');
            csvBuilder.append(station.habilitada).append('\n');

            stations.put(station.tabla, station);
        }

        String csv = csvBuilder.toString();

        Nlp nlp = config.getNlp();

        String lang = "es";
        String sheetName = "HNSTATION";
        DataSheet sheet;

        try {
            sheet = new DataSheet(sheetName, csv, nlp.getNormalizer(lang), nlp.getWordTokenizer(lang), nlp.getStopWords(lang), true);
            logger.warn("HNStations data successfully parsed");
        } catch (Exception e) {
            logger.error("HNStations data could not be parsed: {}", e.getMessage());
            return;
        }


        String docName = lang + "/" + sheetName;
        try {
            config.databaseResource.saveTextDoc("datasheet", docName, csv, null, Collections.singleton("_public"));
            logger.warn("HNStations data successfully saved to DB as '{}'", docName);
        } catch (Exception e) {
            logger.error("HNStations data could not be save to DB as '{}', reason: {}", docName, e.getMessage());
            return;
        }

        try {
            config.getNlp().loadDataSheet(lang, sheetName, sheet);
            logger.warn("HNStations data available as '{}'", docName);
        } catch (Exception e) {
            logger.error("HNStations '{}' is not available, reason: {}", docName, e.getMessage());
        }
    }

    public HNStation getStation(String tabla){
        return stations.get(tabla);
    }

    public Map<String, HNStation> getStations() {
        return stations;
    }

    /**
     * Merge data from given stations into a single timeline
     * @param stations table names ("tabla")
     * @return timeline with selected {@link HNWeatherRecord}s.
     */
    public Timeline<HNWeatherRecord> timeline(Collection<String> stations) {
        if(stations == null || stations.isEmpty()){
            return timelineAllData;
        }

        Timeline<HNWeatherRecord> timeline = new Timeline<>();
        for (String station : stations) {
            timeline.addAll(stationToTimeline.get(station));
        }
        return timeline;
    }

    public List<HNWeatherRecord> retrieveRecords(HNStation station, DateTime minTime){
        return retrieveRecords(station.tabla, minTime);
    }

    public List<HNWeatherRecord> retrieveRecords(String stationTabla, DateTime minTime){
        if(!stationTabla.matches("\\w+")){
            // avoid SQL injection
            throw new IllegalArgumentException("This is not a valid table name for an HN Weather station: " + stationTabla);
        }
        return retrieve("SELECT * FROM PMA_ESTACIONES."+stationTabla+" WHERE fecha > '" + ISODateTimeFormat.date().print(minTime) + "';", HNWeatherRecord.class);
    }

    public List<String> retrieveTables(){
        return retrieve("SELECT table_name FROM information_schema.tables WHERE TABLE_NAME LIKE 'PMA_%';", String.class);
    }

    public List<HNStation> retrieveStations(){
        // Station info with location names (strings are converted from Latin1 to UTF-8 encoding)
        // Otherwise, close to:
        // return retrieve("SELECT * FROM PMA_ESTACIONES.estaciones;", HNStation.class);
        return retrieve(
                "SELECT PMA_ESTACIONES.estaciones.id AS id, convert(cast(convert(nombre using  latin1) as binary) using utf8) AS nombre," +
                        "    latitud, longitud, tabla, " +
                        "    convert(cast(convert(PMA_ESTACIONES.departamentos.name using  latin1) as binary) using utf8) AS departamentos, " +
                        "    convert(cast(convert(PMA_ESTACIONES.municipios.name using  latin1) as binary) using utf8) AS municipios, habilitada " +
                        "    FROM PMA_ESTACIONES.estaciones\n" +
                        "    JOIN PMA_ESTACIONES.municipios ON PMA_ESTACIONES.estaciones.municipios_id = PMA_ESTACIONES.municipios.id\n" +
                        "    JOIN PMA_ESTACIONES.departamentos ON PMA_ESTACIONES.estaciones.departamentos_id = PMA_ESTACIONES.departamentos.id;", HNStation.class);
    }

    public <T> List<T> retrieve(String sql, Class<T> target){
        return jdbi.withHandle(handle -> handle.createQuery(sql).mapTo(target).list());
    }

    public <T> List<T> retrieveBeans(String sql, Class<T> target){
        return jdbi.withHandle(handle -> handle.createQuery(sql).mapToBean(target).list());
    }


}
