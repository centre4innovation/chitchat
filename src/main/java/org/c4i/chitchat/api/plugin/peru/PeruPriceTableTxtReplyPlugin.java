package org.c4i.chitchat.api.plugin.peru;

import org.c4i.nlp.Nlp;
import org.c4i.nlp.match.Range;
import org.c4i.nlp.match.ReplyPlugin;
import org.c4i.nlp.match.Script;
import org.c4i.util.time.DateTimeUtil;
import org.c4i.util.StringUtil;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Render a tables of prices grouped by mercado.
 * @author Arvid Halma
 */
public class PeruPriceTableTxtReplyPlugin implements ReplyPlugin {

    private static final Pattern PERUTABLE_PROP = Pattern.compile("\\bPERUTABLETXT\\b");
    private static final Locale locale = Locale.forLanguageTag("es");



    @Override
    public String updateReply(String reply, String userText, Script script, List<Range> ranges, Nlp nlp) {
        Matcher matcher = PERUTABLE_PROP.matcher(reply);
        if (matcher.find()) {
            // determine the different mercados
            Set<String> mcdos = ranges.stream().filter(r -> r.props.get("mcdo") != null).map(r -> r.props.get("mcdo")).collect(Collectors.toSet());
            StringBuilder out = new StringBuilder();

            for (String mcdo : mcdos) {

                List<Range> rows = ranges.stream().filter(r -> mcdo.equals(r.props.get("mcdo"))).collect(Collectors.toList());
                StringBuilder mcdoSection = new StringBuilder(mcdo).append('\n');

                if(!rows.isEmpty()){
                    final Optional<String> fecha = rows.stream().map(range -> range.props.get("fecha")).max(String::compareTo);
                    fecha.ifPresent(s -> mcdoSection.append(DateTimeUtil.fullDateString(locale, s)).append("\n"));
                }

                StringBuilder table = new StringBuilder();
                if(rows.isEmpty()){
                    table.append("...\n");
                } else {
                    // render
                    // header
                    table.append("Variedad : S/ por kg\n");

                    // body
                    for (Range row : rows) {
                        final String v = row.props.getOrDefault("variedad", "");
                        table.append(StringUtil.properCase(v)).append(" : ");
                        table.append(String.format(locale,"%.2f", Double.parseDouble(row.props.getOrDefault("preciokg", "-"))));
                        table.append("\n");
                    }
                    table.append("\n");
                }

                mcdoSection.append(table).append("\n");
                out.append(mcdoSection);

            }

            reply = reply.replace("PERUTABLETXT", out);
        }
        return reply;
    }


}
