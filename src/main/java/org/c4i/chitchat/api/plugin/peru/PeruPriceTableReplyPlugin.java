package org.c4i.chitchat.api.plugin.peru;

import com.google.common.collect.ImmutableList;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.match.Range;
import org.c4i.nlp.match.ReplyPlugin;
import org.c4i.nlp.match.Script;
import org.c4i.util.time.DateTimeUtil;
import org.c4i.util.StringUtil;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Render a tables of prices grouped by mercado.
 * @author Arvid Halma
 */
public class PeruPriceTableReplyPlugin implements ReplyPlugin {

    private static final Pattern PERUTABLE_PROP = Pattern.compile("\\bPERUTABLE\\b");
    private static final Locale locale = Locale.forLanguageTag("es");



    @Override
    public String updateReply(String reply, String userText, Script script, List<Range> ranges, Nlp nlp) {
        Matcher matcher = PERUTABLE_PROP.matcher(reply);
        if (matcher.find()) {
            // determine the different mercados
            Set<String> mcdos = ranges.stream().filter(r -> r.props.get("mcdo") != null).map(r -> r.props.get("mcdo")).collect(Collectors.toSet());
            StringBuilder out = new StringBuilder();

            for (String mcdo : mcdos) {

                List<Range> rows = ranges.stream().filter(r -> mcdo.equals(r.props.get("mcdo"))).collect(Collectors.toList());
                StringBuilder mcdoSection = new StringBuilder("<p><b>"+mcdo+"</b><br>");
                if(!rows.isEmpty()){
                    final Optional<String> fecha = rows.stream().map(range -> range.props.get("fecha")).max(String::compareTo);
                    fecha.ifPresent(s -> mcdoSection.append(DateTimeUtil.fullDateString(locale, s)).append("<br>"));
                }

                StringBuilder table = new StringBuilder("<table style=\"width: 100%;\">");
                if(rows.isEmpty()){
                    table.append("<table style=\"border: 1px solid black;width: 100%;\"><tr><td>...</td></tr></table>");
                } else {
                    // determine columns to show
                    List<String> cols = ImmutableList.of("variedad", "preciokg");

                    // render
                    // header
                    table.append("<tr>")
                            .append("<th style=\"text-align: center;border: 1px solid black;\">").append("Variedad").append("</th>")
                            .append("<th style=\"text-align: center;border: 1px solid black;\">").append("S/ por kg").append("</th>")
                            .append("</tr>");

                    // body
                    for (Range row : rows) {
                        final String v = row.props.getOrDefault("variedad", "");
                        table.append("<tr>");
                        table.append("<td style=\"text-align: left;border: 1px solid gray;\">").append(StringUtil.properCase(v)).append("</td>");
                        table.append("<td style=\"text-align: right;;border: 1px solid gray;\">").append(String.format(locale,"%.2f", Double.parseDouble(row.props.getOrDefault("preciokg", "-")))).append("</td>");
                        table.append("</tr>");
                    }
                    table.append("</table>");
                }

                mcdoSection.append(table).append("</p>");
                out.append(mcdoSection);

            }

//            reply = PERUTABLE_PROP.matcher(reply).replaceAll(out.toString());
            reply = reply.replace("PERUTABLE", out);
        }
        return reply;
    }


}
