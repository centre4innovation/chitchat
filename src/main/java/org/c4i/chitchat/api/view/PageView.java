package org.c4i.chitchat.api.view;

import io.dropwizard.views.View;

/**
 * Base page
 * @author Arvid Halma
 * @version 18-9-2016 - 10:05
 */
public class PageView extends View {
    private final PageParams pageParams;

    public PageView(PageParams pageParams) {
        super("page.ftl");
        this.pageParams = pageParams;
    }

    public PageView(PageParams pageParams, String pageTemplate) {
        super(pageTemplate);
        this.pageParams = pageParams;
    }
    public PageParams getPageParams() {return pageParams;}

}
