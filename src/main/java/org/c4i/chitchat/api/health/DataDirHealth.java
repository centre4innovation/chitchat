package org.c4i.chitchat.api.health;

import com.codahale.metrics.health.HealthCheck;
import org.c4i.chitchat.api.Config;

/**
 * Check if the settings in settings.yml are correct.
 * @author Arvid Halma
 * @version 15-11-2015 - 12:13
 */
public class DataDirHealth extends HealthCheck {
    private Config configuration;

    public DataDirHealth(Config configuration) {
        this.configuration = configuration;
    }

    @Override
    protected Result check() throws Exception {
        return configuration.appConfig.getDataDir().exists() ? Result.healthy("Data directory accessible") : Result.healthy("Data directory inaccessible");
    }
}