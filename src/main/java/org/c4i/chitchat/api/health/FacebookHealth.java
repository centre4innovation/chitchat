package org.c4i.chitchat.api.health;

import com.codahale.metrics.health.HealthCheck;
import org.c4i.chitchat.api.Config;

/**
 * Facebook channel check.
 * @author Arvid Halma
 * @version 15-11-2015 - 12:13
 */
public class FacebookHealth extends HealthCheck {
    private Config configuration;

    public FacebookHealth(Config configuration) {
        this.configuration = configuration;
    }

    @Override
    protected Result check() {
        if(configuration.facebookSettings != null) {
            try {
                configuration.facebookResource.loadLiveScript(false);
                return Result.healthy("Facebook is live");
            } catch (Exception e) {
                return Result.unhealthy("Facebook is configured, but has no live script: " + e.getMessage());
            }
        } else {
            return Result.healthy("Facebook not enabled (not defined in settings)");
        }
    }
}