package org.c4i.chitchat.api.cmd;

import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;
import org.c4i.chitchat.api.Config;

import java.io.IOException;

/**
 * Kill process by port.
 * @author Arvid Halma
 */
public class TwilioTestCommand extends ConfiguredCommand<Config> {

    public TwilioTestCommand() {
        super("twiliotest", "Send a test message");
    }

    @Override
    public void configure(Subparser subparser) {
        super.configure(subparser);
    }

    @Override
    protected void run(Bootstrap<Config> bootstrap,
                       Namespace namespace,
                       Config config) throws Exception
    {
        Message message = Message.creator(new PhoneNumber("+1555867530931"),
                new PhoneNumber("+15017250604"),
                "This is the ship that made the Kessel Run in fourteen parsecs?").create();

        System.out.println(message.getSid());
    }




}
