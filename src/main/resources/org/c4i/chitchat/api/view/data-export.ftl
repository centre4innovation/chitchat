<#import "utils.ftl" as u>

<#macro pageTitle>
    ChitChat - Data Import/Export
</#macro>

<#macro pageContent>

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Data Import/Export
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="/api/v1/ui/dashboard" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Data
											</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Import/Export
											</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="row">
            <div class="col-lg-6">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-download"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Data Export
                                </h3>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">
                                <p>Export all conversation data as a zip file.</p>
                                <form class="m-form">
                                    <div class="m-form__group form-group row">
                                        <label class="col-4 col-form-label">Channel</label>
                                        <div class="col-8">
                                            <select id="channelExportSelect" class="form-control m-input custom-select"></select>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <label class="col-4 col-form-label">Anonymise IDs</label>
                                        <div class="col-4">
                                      <span class="m-switch m-switch--icon">
                                        <label>
						                        <input type="checkbox" checked="checked" name="" id="anonymiseDownload">
						                        <span></span>
						                        </label>
						                    </span>
                                        </div>
                                        <div class="col-4">
                                            <div class="btn btn-brand pull-right" id="downloadBtn" data-toggle="confirmation" data-singleton="true"><i class="la la-file-zip-o"></i> Download</div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-download"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Script Export
                                </h3>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">
                                <p>Export all scripts as a zip file.</p>
                                <form class="m-form">
                                    <div class="m-form__group form-group row">
                                        <div class="col-12">
                                            <div class="btn btn-brand pull-right" id="downloadScriptsBtn" data-toggle="confirmation" data-singleton="true"><i class="la la-file-zip-o"></i> Download</div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-upload"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Import Conversation Data
                                </h3>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">
                                <p>Upload previously exported conversation data (a zip file containing: conversation.txt, message.txt, label.txt)</p>
                                <form id="sampleUpload" class="m-dropzone dropzone dz-clickable"
                                      action="/api/v1/db/conversation/import" enctype="multipart/form-data" method="POST" type="files">
                                    <div class="m-dropzone__msg dz-message needsclick">
                                        <h3 class="m-dropzone__msg-title">Drop files here or click to upload.</h3>
                                        <span class="m-dropzone__msg-desc">Only .zip files are allowed. Click <a href="/assets/files/format.csv">here</a> for the expected format. </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-remove"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Delete data
                                </h3>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">
                                <p>Delete data per channel</p>
                                <form class="m-form">
                                    <div class="m-form__group form-group row">
                                        <label class="col-4 col-form-label">Channel</label>
                                        <div class="col-8">
                                            <select id="channelDeleteSelect" class="form-control m-input custom-select"></select>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <label class="col-4 col-form-label">Confirm</label>
                                        <div class="col-4">
                                      <span class="m-switch m-switch--icon m-switch--danger">
                                        <label>
						                        <input type="checkbox" name="" id="confirmDelete">
						                        <span></span>
						                        </label>
						                    </span>
                                        </div>
                                        <div class="col-4">
                                            <div class="btn btn-danger pull-right" id="deleteBtn" data-toggle="confirmation" data-singleton="true"><i class="la la-trash"></i> Delete</div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</#macro>

<#macro pageScript>
    <script src="/assets/app/js/app.util.js" type="application/javascript"></script>

    <script>
        // file upload. Note: events are not triggered within $(function{})
        // see: https://stackoverflow.com/questions/43434283/dropzone-js-events-not-firing
        // Dropzone.autoDiscover = false;

        $(document).ready(function () {
            $.hx.setCurrentPage('#menu-item-data-export')

            Dropzone.options.sampleUpload = {
                acceptedFiles: ".zip",
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 100,
                maxFilesize: 512, // MB
                addRemoveLinks: true,
                uploadMultiple: true,
                init: function () {
                    this.on("error", function (file, errorMessage) {
                        $.hx.notify(errorMessage.message || "Error uploading file", "danger");
                    });
                    this.on("success", function (file, response) {
                        $.hx.notify("Data uploaded successfully... <br>" + $.hx.toTable(response), "success");
                        createChannelSelect('#channelExportSelect', 'channelExport');
                        createChannelSelect('#channelDeleteSelect', 'channelDelete')
                    });
                }

            };


            // Channel select
            createChannelSelect('#channelExportSelect', 'channelExport');
            createChannelSelect('#channelDeleteSelect', 'channelDelete');

            function download(url) {
                document.getElementById('download_iframe').src = url;
            }

            $('#downloadBtn').click(() => {
                const ano = $('#anonymiseDownload').is(':checked') ? '/anonymous' : ''
                download('/api/v1/db/conversation/export' + ano + '?channel=' + encodeURIComponent($.hx.get('channelExport')))
            });

            $('#downloadScriptsBtn').click(() => {
                download('/api/v1/db/script/export')
            });


            $("#deleteBtn").click(function () {
                if($('#confirmDelete').is(':checked') ){
                    const channel = $.hx.get('channelDelete')
                    $.ajax({
                        method: 'DELETE',
                        url: "/api/v1/db/conversation?" + $.param({"channel": channel}),
                        success: function () {
                            $.hx.notify("Successfully deleted conversations for: " + channel, 'success');
                        },
                        error: function () {
                            $.hx.notify("An error occurred for channel: " + channel, 'danger');
                        }
                    });
                    createChannelSelect('#channelExportSelect', 'channelExport');
                    createChannelSelect('#channelDeleteSelect', 'channelDelete');
                } else {
                    $.hx.notify("Please toggle the confirmation switch if you really want to delete the selected data.", 'warning');
                }
            });
        });
    </script>
</#macro>