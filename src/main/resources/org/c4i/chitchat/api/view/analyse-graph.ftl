<#import "utils.ftl" as u>

<#macro pageTitle>
    ChitChat - Graph
</#macro>

<#macro pageContent>
    <style>

        #graphOutput i.la {
            font-size: 10px;
        }

        #map {
            height: 400px;
            z-index: 5;
        }

        .CodeMirror {
            border: 1px solid #eee;
        }

        .plotly .modebar {
            z-index: 1 !important;
        }

        #wordFreqPanel {
            font-weight: bold;
            color: #333;
            font-family: times, serif;
            font-style: italic;
            /*font-size: 16px;*/
        }


        #wordFreqPanel .text-muted{
            font-size: 13px;
            font-weight: lighter;
            color: #c0c0c0 !important;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-style: normal;
        }
    </style>
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Graph
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="/api/v1/ui/dashboard" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Analyse</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Graph</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div>
                <#--<div class="form-inline m-form__group">

                    <div class="input-group m-input-group  m-input-group--pill mr-2" style="box-shadow: 0 3px 20px 0 rgba(113,106,202,.17)!important">
                        <select id="channelSelect" class="form-control m-input custom-select">
                        </select>
                        <div class="input-group-append">
                            <label class="input-group-text m-input--pill" for="channelSelect" style="border-top-right-radius: 1.3rem;border-bottom-right-radius: 1.3rem;background: white;"><i class="la la-plug"></i></label>
                        </div>
                    </div>

                    <span id="m_dashboard_daterangepicker"></span>
                </div>-->

            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-filter"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Filter
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" id="runBtn" class="m-portlet__nav-link btn btn-primary m-btn m-btn--pill">
                                        <i class="la la-play"></i> Update
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-1 col-form-label" title="The period in which conversations should fall">
                                    Period
                                </label>
                                <div class="col-lg-2">
                                    <div class="input-group" id="m_daterangepicker">
                                        <input type="text" class="form-control m-input" readonly="" placeholder="Select date range">
                                        <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
                                        </div>
                                    </div>
                                </div>

                                <label class="col-lg-1 col-form-label" title="The message/document data source">
                                    Channel
                                </label>
                                <div class="col-lg-2">
                                    <select id="channelSelect" class="form-control m-input custom-select">
                                    </select>
                                </div>
                                <label class="col-lg-1 col-form-label" title="The way nodes and relations are formed">
                                    Method
                                </label>
                                <div class="col-lg-2">
                                    <select id="methodSelect" class="form-control m-input custom-select">
                                        <option value="" selected>Method 1</option>
                                        <option value="" selected>Method 2</option>
                                    </select>
                                </div>
                                <label class="col-lg-1 col-form-label" title="The way nodes and relations are formed">
                                    Limit
                                </label>
                                <div class="col-lg-2">
                                    <input type="number" class="form-control m-input" min="0" value="1" id="limitInput">
                                </div>
                            </div>

                            <div class="m-form__group m-form__group--last form-group row">
                                <label class="col-lg-1 col-form-label" title="Use custom labels, instead of previously matched labels">
                                    Script
                                </label>
                                <div class="col-lg-11 col-sm-12">

                                    <div class="row" id="scriptPanel" style="overflow:hidden; height: 40px;">
                                    <span class="col-12 m-form__help" style="height: 40px;">
                                        <label class="m-checkbox m-checkbox--solid m-checkbox--brand">
											<input type="checkbox" id="toggleScriptBtn"> Define custom labels
											<span></span>
                                        </label>
                                    </span>
                                        <div class="col-11" style="height: 400px; padding-bottom: 20px;">
                                            <textarea id="scriptEditor" style="display: none;"></textarea>

                                        </div>
                                        <div class="col-1">
                                            <ul class="m-portlet__nav" style="padding: 0;">
                                                <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                                    <a href="#" class="m-portlet__nav-link btn btn-secondary  m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill   m-dropdown__toggle">
                                                        <i class="la la-ellipsis-v"></i>
                                                    </a>
                                                    <div class="m-dropdown__wrapper" style="z-index: 101;">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 21.5px;"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">

                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">Actions</span>
                                                                        </li>

                                                                        <li class="m-nav__item">
                                                                            <a id="openScriptBtn" class="m-nav__link" data-toggle="modal" data-target="#loadExampleModal">
                                                                                <i class="m-nav__link-icon la la-star-o"></i>
                                                                                <span class="m-nav__link-text">Open example</span>
                                                                            </a>
                                                                        </li>

                                                                        <li class="m-nav__separator m-nav__separator--fit">
                                                                        <li class="m-nav__item">
                                                                            <a id="newScriptBtn" class="m-nav__link">
                                                                                <i class="m-nav__link-icon la la-file"></i>
                                                                                <span class="m-nav__link-text">New script</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            <a id="openScriptBtn" class="m-nav__link" data-toggle="modal" data-target="#openCssModal">
                                                                                <i class="m-nav__link-icon la la-folder-open-o"></i>
                                                                                <span class="m-nav__link-text">Open script</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            <a id="openVersionBtn" class="m-nav__link" data-toggle="modal" data-target="#openVersionModal">
                                                                                <i class="m-nav__link-icon la la-history"></i>
                                                                                <span class="m-nav__link-text">Open previous version</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            <a id="saveScriptBtn" class="m-nav__link" data-toggle="modal" data-target="#saveCssModal">
                                                                                <i class="m-nav__link-icon la la-floppy-o"></i>
                                                                                <span class="m-nav__link-text">Save script</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            <a id="downloadScriptBtn" class="m-nav__link">
                                                                                <i class="m-nav__link-icon la la-download"></i>
                                                                                <span class="m-nav__link-text">Download script</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="m-nav__separator m-nav__separator--fit">
                                                                        <li class="m-nav__item">
                                                                            <a id="normalizeScriptBtn" class="m-nav__link">
                                                                                <i class="m-nav__link-icon la la-magic"></i>
                                                                                <span class="m-nav__link-text">Normalize script</span>
                                                                            </a>
                                                                        </li>

                                                                        <li class="m-nav__separator m-nav__separator--fit">
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="m-portlet m-portlet--tabs" m-portlet="true" id="graphPortlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title truncate">
                                Graph
                            </div>

                        </div>

                        <div class="m-portlet__head-tools">

                            <ul class="m-portlet__nav" id="chatTools">
                                <li class="m-portlet__nav-item">
                                    <a id="layout1Btn" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-random"></i></a>
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a id="layout2Btn" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-random"></i></a>
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a id="layout3Btn" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-random"></i></a>
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a id="layout4Btn" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-random"></i></a>
                                </li>

                                <li class="m-portlet__nav-item">
                                    <div class="form-control mb-2 mr-sm-2 m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="networkSrchInput">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                                </li>
                                <#--<li class="m-portlet__nav-item">
                                    <a id="clearBtn"
                                       class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                       data-toggle="m-tooltip" data-placement="top" title=""
                                       data-original-title="Clear conversation">
                                        <i class="la la-close"></i>
                                    </a>
                                </li>-->
                                <li class="m-portlet__nav-item">
                                    <a m-portlet-tool="fullscreen"
                                       class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill">
                                        <i class="la la-expand"></i>
                                    </a>
                                </li>
                            </ul>

                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--right m-tabs-line-danger" style="float: left; display: none; min-width: 122px; margin-left: 5px;" id="matchTools">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" id="textInputBtn">
                                        Input
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" id="outputPanelBtn">
                                        Output
                                    </a>
                                </li>
                            </ul>



                        </div>
                    </div>
                    <div class="m-portlet__body" id="graphOutput" style="padding: 0; height: 600px;">

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-info-circle"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Details
                                </h3>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12" id="detailsPanel">

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-bar-chart"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Word frequencies
                                </h3>
                            </div>

                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">

                                    <li class="m-portlet__nav-item">
                                        <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                            <i class="la la-angle-down"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12" id="wordFreqPanel">

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-globe"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Location
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a m-portlet-tool="fullscreen"
                                       class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill">
                                        <i class="la la-expand"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="noLocationMsg">No location information available...</div>
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-cube"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Nodes
                                </h3>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">
                                <form class="form-inline">
                                    <div class="form-control mb-2 mr-sm-2 m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="nodeSearchInput">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                                    </div>


                                </form>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-12" id="nodeDataTableParent">
                                <div id="nodeDataTable"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-database"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Links
                                </h3>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12">
                                <form class="form-inline">
                                    <div class="form-control mb-2 mr-sm-2 m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="linkSearchInput">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                                    </div>


                                </form>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-12" id="linkDataTableParent">
                                <div id="linkDataTable"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-12" id="doc">
                <h2>Documentation</h2>
            </div>
        </div>

    </div>


<#-- Example modal -->
    <div class="modal fade" id="loadExampleModal" tabindex="-1" role="dialog" aria-labelledby="loadExampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="loadExampleModalLabel">Open example script</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="loadExampleSelect">Select an example script</label>
                            <select class="form-control" id="loadExampleSelect">

                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button id="loadExampleBtn" type="button" class="btn btn-brand">Open</button>
                </div>
            </div>
        </div>
    </div>
</#macro>

<#macro pageScript>
    <link rel="stylesheet" href="/assets/vendors/custom/cytoscape/cytoscape-panzoom.css" />
    <style>
        .cy-panzoom-zoom-button {
            padding: 0 6px 6px 0;
        }

        .cy-panzoom-slider-handle .icon {
            margin-left: 0;
            margin-top: -1px;
        }


        element.style {
            top: 2px;
        }

        .cy-panzoom {
            z-index: inherit;
        }

        .cy-panzoom-slider-handle {
            z-index: inherit;
        }
    </style>

    <script src="/assets/vendors/custom/cytoscape/cytoscape.js" type="text/javascript"></script>
    <script src="/assets/vendors/custom/cytoscape/cytoscape-euler.js" type="text/javascript"></script>
    <script src="/assets/vendors/custom/cytoscape/klay.js" type="text/javascript"></script>
    <script src="/assets/vendors/custom/cytoscape/cytoscape-klay.js" type="text/javascript"></script>
    <script src="/assets/vendors/custom/cytoscape/cola.min.js" type="text/javascript"></script>
    <script src="/assets/vendors/custom/cytoscape/cytoscape-cola.js" type="text/javascript"></script>
    <script src="/assets/vendors/custom/cytoscape/cytoscape-dagre.js" type="text/javascript"></script>
    <script src="/assets/vendors/custom/cytoscape/cytoscape-panzoom.js" type="text/javascript"></script>
    <script src="/assets/app/js/script-crud.js" type="application/javascript"></script>
    <script src="/assets/app/js/app.util.js" type="application/javascript"></script>

    <script>
        $(document).ready(function () {
            $.hx.setCurrentPage('#menu-item-analyse-graph')

            // Custom analysis script toggle
            let showScript = false;
            $('#toggleScriptBtn').change(() => {
                showScript = $('#toggleScriptBtn')[0].checked
                if(showScript) {
                    $('#scriptPanel').css({height: '420px', overflow: 'visible'});
                } else {
                    $('#scriptPanel').css({height: '40px', overflow: 'hidden'});
                }
            })

            // Date range selector
            let fromDate = $.hx.get('startDate', moment().subtract(6, 'days'));
            let toDate = $.hx.get('endDate', moment());
            let dateRangeUpdate = function(start, end, label) {
                let title = '';
                let range = '';

                if ((end - start) < 100 || label === 'Today') {
                    title = 'Today:';
                    range = start.format('MMM D');
                } else if (label === 'Yesterday') {
                    title = 'Yesterday:';
                    range = start.format('MMM D');
                } else {
                    let now = moment();
                    if (start.year() !== end.year()) {
                        range = start.format('MMM D YYYY') + ' - ' + end.format('MMM D YYYY');
                    } else {
                        if (now.year() === start.year()) {
                            range = start.format('MMM D') + ' - ' + end.format('MMM D');
                        } else {
                            range = start.format('MMM D') + ' - ' + end.format('MMM D YYYY');
                        }
                    }
                }

                $.hx.set('startDate', start);
                $.hx.set('endDate', end);
                $('#m_daterangepicker .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
                rerunHint()
            };

            $('#m_daterangepicker').daterangepicker({
                buttonClasses: 'm-btn btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                startDate: fromDate,
                endDate: toDate,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, dateRangeUpdate)

            dateRangeUpdate(fromDate, toDate, '');


            $('#linkSearchInput').val($.hx.getUrlParam('keyword'))
            let linkTable, nodeTable;
            let sleaflet = new Sleaflet('map', 'pk.eyJ1IjoiYWhhbG1hIiwiYSI6ImNpeWp2dGMzbjAwMHIyd3I0dHV4bXE4dHUifQ.6e6CLc5hZdRItmVWkA_N3g',
                0, 0, 3);

            // Channel select
            createChannelSelect('#channelSelect', 'channel', rerunHint);

            // hint to click run
            function rerunHint() {
                $('#runBtn').css('animation', 'flash 1.5s')
                setTimeout(() => $('#runBtn').css('animation', ''), 1500)
            }

            // Trigger window resize events after fullscreen portlet events, so widget may update layout
            $(".m-portlet").each(function () {
                let portlet = new mPortlet(this)
                portlet.on('afterFullscreenOn', function(portlet) {
                    window.dispatchEvent(new Event('resize'));
                });

                portlet.on('afterFullscreenOff', function(portlet) {
                    window.dispatchEvent(new Event('resize'));
                });
            })


            window.scriptEditor = new CcsEditor($('#scriptEditor')[0], 'demo.ccs')
            setScriptName($.hx.get('scriptName', 'New script'))

            $('#runBtn').click(function(){
                if($('#runBtn').hasClass('btn-warning')){
                    $('#runBtn').removeClass('btn-warning');
                    $('#runBtn i').removeClass('la-stop').addClass('la-play');
                    $.post('/api/v1/sna/cancel/global');
                } else {
                    update();
                }
            });

            function update(){

                const useScript = $('#toggleScriptBtn')[0].checked
                const channel = $.hx.get('channel', 'chitchat')
                const fromDate = $.hx.get('startDate').format()
                const toDate = $.hx.get('endDate').format()

                const formData = new FormData();
                if(useScript)
                    formData.append("script", scriptEditor.getText());
                formData.append("channel", channel);
                formData.append("from", fromDate);
                formData.append("to", toDate);
                formData.append("limit", $('#limitInput').val());
                sleaflet.clearMarkers()

                let algo = 'labelconv'
                if(useScript || channel.startsWith('doc/')){
                    algo = 'actor'
                }

                scriptEditor.removeMessages()
                $('#wordFreqPanel').html('')

                $('#runBtn').addClass('btn-warning');
                $('#runBtn i').removeClass('la-play').addClass('la-stop');
                $.ajax({
                    type: "POST",
                    url: "/api/v1/sna/graph/" + algo,
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: (tuple) => {
                        $('#runBtn').removeClass('btn-warning');
                        $('#runBtn i').removeClass('la-stop').addClass('la-play');

                        console.log(tuple);
                        const hist = tuple[0] && tuple[0].wordFrequencies;
                        const graph = tuple[1];



                        updateWordFreqs(hist)
                        updateGraph(graph)
                        updateEdgeTable(graph)
                        updateNodeTable(graph)
                        updateMap(graph)

                    },
                    error: (e) => {
                        $('#runBtn').removeClass('btn-warning');
                        $('#runBtn i').removeClass('la-stop').addClass('la-play');
                        const exceptions = JSON.parse(e.responseText);
                        let errorCount = 0, waringCount = 0;
                        let errorMsg, warningMsg;
                        for (const e of exceptions) {
                            let message = e.message;
                            let lineNr = e.line - 1;
                            if(lineNr !== undefined){
                                if(e.type === "WARNING") {
                                    scriptEditor.addWarningMessage(lineNr, message)
                                    waringCount++;
                                    warningMsg = message;
                                }else {
                                    scriptEditor.addErrorMessage(lineNr, message)
                                    errorCount++;
                                    errorMsg = message;
                                }
                            }
                        }

                        if(errorCount === 1)
                            $.hx.notify('There is an error in your script.<br>' + errorMsg, 'danger')
                        else if (errorCount > 1)
                            $.hx.notify('There are '+errorCount+' errors in your script.', 'danger')

                        if(waringCount === 1)
                            $.hx.notify('There is an warning for your script.<br>' + warningMsg, 'warning')
                        else if (waringCount > 1)
                            $.hx.notify('There are '+waringCount+' errors in your script.', 'warning')

                        scriptEditor.scrollToFirstMessage();
                    }
                });
            }



            const panZoomConfig = {
                zoomFactor: 0.05, // zoom factor per zoom tick
                zoomDelay: 45, // how many ms between zoom ticks
                minZoom: 0.1, // min zoom level
                maxZoom: 10, // max zoom level
                fitPadding: 50, // padding when fitting
                panSpeed: 10, // how many ms in between pan ticks
                panDistance: 10, // max pan distance per tick
                panDragAreaSize: 75, // the length of the pan drag box in which the vector for panning is calculated (bigger = finer control of pan speed and direction)
                panMinPercentSpeed: 0.25, // the slowest speed we can pan by (as a percent of panSpeed)
                panInactiveArea: 8, // radius of inactive area in pan drag box
                panIndicatorMinOpacity: 0.5, // min opacity of pan indicator (the draggable nib); scales from this to 1.0
                zoomOnly: false, // a minimal version of the ui only with zooming (useful on systems with bad mousewheel resolution)
                fitSelector: undefined, // selector of elements to fit
                animateOnFit: function(){ // whether to animate on fit
                    return false;
                },
                fitAnimationDuration: 1000, // duration of animation on fit

                // icon class names
                sliderHandleIcon: 'la la-minus',
                zoomInIcon: 'la la-plus',
                zoomOutIcon: 'la la-minus',
                resetIcon: 'la la-expand'
            }

            function tapGraphForDetails(cyto){
                cyto.on('tap', function(evt){
                    if(!evt.target.data){
                        return; // nothing clicked
                    }
                    const data = evt.target.data()
                    console.log(data);
                    $('#detailsPanel').html($.c4i.toTable(data));
                    if(data.source !== undefined){
                        // clicked an edge


                        /*$.get('/api/v1/cdr/call/'+data.source+'/to/'+data.target, {
                            fromDate: fromDateStr,
                            toDate: toDateStr,
                        }, calls => {
                            updateCallTable(calls)
                        })*/

                    } else {
                        // clicked a vertex

                        /*$.get('/api/v1/cdr/call/'+data.id+'/summary', {
                            fromDate: fromDateStr,
                            toDate: toDateStr,
                        }, summary => {
                            console.log( summary )
                            showCallDetails(summary)
                        })*/


                    }
                })
            }

            function updateWordFreqs(freqsPerLabel) {
                const $wordFreqPanel = $('#wordFreqPanel');
                $wordFreqPanel.html('')
                if(!freqsPerLabel) {
                    $wordFreqPanel.html('No word frequency data available for this type of analysis...')
                    return
                }

                for (const label in freqsPerLabel) {
                    const h = freqsPerLabel[label]
                    $wordFreqPanel.append('<p style="font-style: normal; font-family: monospace; font-size: 20px; color: #8d46c1"><b>@'+label+'</b></p>')

                    const sortedKeys = Object.keys(h).sort(function(a, b){return h[b]-h[a]});
                    for(const w in sortedKeys){
                        const k = sortedKeys[w];
                        $wordFreqPanel.append('<span class="">' + k + '</span> <span class="text-muted"> (' + h[k] + ')</span> ')
                    }
                    $wordFreqPanel.append('<hr />');

                }
            }


            function updateGraph(graph){
                let dataElts = graph.nodes.map( v => {

                    if(v.type === 'conv') {
                        return {
                            data: {
                                id: v.id,
                                convId: v.convId,
                                type: v.type,
                                betweenness: v.betweenness,
                            }
                            , classes: 'outline'

                        }
                    } else if(v.type === 'match') {
                        return {
                            data: {
                                id: v.id,
                                type: v.type,
                                betweenness: v.betweenness,
                            }
                            ,classes: 'outline'

                        }
                    } else if(v.type === 'actor') {
                        return {
                            data: {
                                id: v.id,
                                type: v.type,
                                betweenness: v.betweenness,
                                category: v.cat,
                                snippet: v.snippet,
                            }
                            , classes: 'outline'

                        }
                    } else if(v.type === 'loc') {
                        return {
                            data: {
                                id: v.id,
                                type: v.type,
                                betweenness: v.betweenness,
                                country: v.country,
                                latitude: v.lat,
                                longitude: v.lon,
                                snippet: v.snippet,
                            }
                            ,classes: 'outline'
                        }
                    } else {
                        return {
                            data: {
                                id: v.id,
                                type: v.type,
                                betweenness: v.betweenness,
                                snippet: v.snippet,
                            }
                            ,classes: 'outline'
                        }

                    }});

                const maxEdgeW = Math.max(...graph.edges.map(edge => edge.w));

                dataElts = dataElts.concat(graph.edges.map( edge => {
                    return {
                        data: {
                            source: edge.a,
                            target: edge.b,
                            snippet: edge.snippet,
                        },
                        style: {
                            'width': 3 * (edge.w * 1) / maxEdgeW
                        }
                    }
                }));


                const cyto = cytoscape({
                    container: document.getElementById('graphOutput'), // container to render in
                    elements: dataElts,
                    style: [ // the stylesheet for the graph
                        {
                            selector: "node",
                            style: {
                                'width': 30,
                                'height': 30,
                                'label': 'data(id)',
                                'text-valign': 'center',
                                'color': 'white',
                                'text-outline-width': 2,
                                'text-outline-color': '#dc852a',
//                    'fill': 'red',
//                    'display':'inline',
                                'background-color': '#dc852a',
                                'font-size': '10px'
                            }
                        },{
                            selector: "node[type = 'conv']",
                            style: {
                                'width': 30,
                                'height': 30,
                                'label': 'data(id)',
                                'text-valign': 'center',
                                'color': 'white',
                                'text-outline-width': 2,
                                'text-outline-color': '#928ee9',
//                    'fill': 'red',
//                    'display':'inline',
                                'background-color': '#928ee9',
                                'font-size': '10px'
                            }
                        }, {
                            selector: "node [type = 'match']",
                            style: {
                                'width': 30,
                                'height': 30,
                                'label': 'data(id)',
                                'text-valign': 'center',
                                'color': 'white',
                                'text-outline-width': 2,
                                'text-outline-color': '#7bc6dc',
//                    'fill': 'red',
//                    'display':'inline',
                                'background-color': '#7bc6dc',
                                'font-size': '10px'
                            }
                        }, {
                            selector: 'node[type = "actor"]',
                            style: {
                                'width': 30,
                                'height': 30,
                                'label': 'data(id)',
                                'text-valign': 'center',
                                'color': 'white',
                                'text-outline-width': 2,
                                'text-outline-color': 'mapData(betweenness, 0, 1, #3598dc, #e53e49)',
//                    'fill': 'red',
//                    'display':'inline',
                                'background-color': 'mapData(betweenness, 0, 1, #3598dc, #e53e49)',
                                'font-size': '10px'
                            }
                        },
                        {
                            selector: 'node[type = "verb"]',
                            style: {
                                'width': 30,
                                'height': 30,
                                'label': 'data(id)',
                                'text-valign': 'center',
                                'color': 'white',
                                'text-outline-width': 2,
                                'text-outline-color': 'mapData(betweenness, 0, 1, #FFC733, #e53e49)',
//                    'fill': 'red',
//                    'display':'inline',
                                'background-color': 'mapData(betweenness, 0, 1, #FFC733, #e53e49)',
                                'font-size': '10px'
                            }
                        },
                        {
                            selector: 'node[type = "loc"]',
                            style: {
                                'width': 30,
                                'height': 30,
                                'label': 'data(id)',
                                'text-valign': 'center',
                                'color': 'white',
                                'text-outline-width': 2,
                                'text-outline-color': 'mapData(betweenness, 0, 1, #30D14C, #e53e49)',
//                    'fill': 'red',
//                    'display':'inline',
                                'background-color': 'mapData(betweenness, 0, 1, #30D14C, #e53e49)',
                                'font-size': '10px'
                            }
                        },
                        {
                            selector: 'edge',
                            style: {
                                'width': 6,
                                'line-color': '#333',
//                    'target-arrow-color': '#333',
//                    'target-arrow-shape': 'triangle',
                                'curve-style': 'haystack'
                            }
                        },
                    ],

                    layout: {
                        name: 'euler',
//                animate: false, // whether to animate changes to the layout
                        animate: 'end', // whether to animate changes to the layout
//                maxSimulationTime: 4000,
                        maxIterations: 250,
                    }
                });

                // cy here is an instance of Cytoscape
                $('#layout1Btn').click(function() {
                    const layout = cyto.layout({
                        name: 'random',
                        animate: 'end',
                    });
                    layout.run();
                })

                // cy here is an instance of Cytoscape
                $('#layout2Btn').click(function() {
                    const layout = cyto.layout({
                        name: 'klay',
                        animate: 'end',
                    });
                    layout.run();
                })

                // cy here is an instance of Cytoscape
                $('#layout3Btn').click(function() {
                    const layout = cyto.layout({
                        name: 'cola',
                        animate: 'end',
                    });
                    layout.run();
                })

                // cy here is an instance of Cytoscape
                $('#layout4Btn').click(function() {
                    const layout = cyto.layout({
                        name: 'euler',
                        animate: 'end', // whether to animate changes to the layout
                        maxIterations: 250,
                    });
                    layout.run();
                })

                // window.setInterval(cyto.resize, 1000);

                $('#networkSrchInput').on('input', () => {
                    let id = $('#networkSrchInput').val()
                    cyto.fit( cyto.$("[id *= '"+id+"']"), 50 )
                })

//            $('#fitGraphBtn').click(cyto.fit)
                $('#saveGraphBtn').click(() =>
                {
                    let img = cyto.jpg({
                        maxWidth:10000,
                        maxHeight:10000,
                    })
                    $.hx.saveAs(img, 'graph'+Math.floor(Date.now() / 1000)+'.jpg');
                })
//            $('#fullScreenBtn').click(() => {
//              window.setTimeout(cyto.resize, 1000);
//            })

                $('#networkSrchInput').on('input', () => {
                    let id = $('#networkSrchInput').val()
                    cyto.fit( cyto.$("[id *= '"+id+"']"), 50 )
                })

                tapGraphForDetails(cyto)
                cyto.panzoom( panZoomConfig );

            }

            function updateEdgeTable(graph){
                let edges = graph.edges;

                if (linkTable ) {
                    linkTable.destroy()
                    linkTable = undefined
                    // $('#msgDataTableParent').html('<div id="msgDataTable"></div>');
                }

                linkTable = $('#linkDataTable').mDatatable({
                    // datasource definition
                    data: {
                        type: 'local',
                        source: edges,
                        pageSize: 10
                    },

                    // layout definition
                    layout: {
                        theme: 'default', // datatable theme
                        scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                        // height: 430, // datatable's body's fixed height
                        footer: false // display/hide footer
                    },

                    toolbar: {
                        layout: ['pagination', 'info'],
                        placement: ['bottom'],  //'top', 'bottom'
                        items: {
                            pagination: {
                                type: 'default',

                                pages: {
                                    desktop: {
                                        layout: 'default',
                                        pagesNumber: 6
                                    },
                                    tablet: {
                                        layout: 'default',
                                        pagesNumber: 3
                                    },
                                    mobile: {
                                        layout: 'compact'
                                    }
                                },

                                navigation: {
                                    prev: true,
                                    next: true,
                                    first: true,
                                    last: true
                                },

                                pageSizeSelect: [5, 10, 50, -1]
                            },

                            info: true
                        }
                    },

                    // column sorting
                    sortable: true,
                    pagination: true,

                    search: {
                        input: $('#linkSearchInput'),
                        delay: 200,
                    },
                    // inline and bactch editing(cooming soon)
                    // editable: false,

                    // columns definition
                    columns: [

                        {field: "a", width: 80, title: "from", template: row => row.a},
                        {field: "b", width: 80, title: "to", template: row => row.b},
                        // {field: "w", title: "centrality", template: row => row.w},
                        {field: "src", width: 120, title: "source", template: row => row.src},
                        {field: "snippet", title: "snippet", template: row => row.snippet},


                    ]
                });
                /*$('#linkDataTable')
                        .on('click', `.conversationDetailBtn`, function () {
                            const convId = $(this).data('conv-id');
                            updateMessageTable(convId)
                            $.hx.scrollIntoView('#msgPortlet', -85);
                        })
                        .on('click', `.deleteConvBtn`, function () {
                            const convId = $(this).data('conv-id');
                            $.ajax({
                                url: '/api/v1/db/conversation/' + encodeURIComponent(convId),
                                type: 'DELETE',
                                success: function(result) {
                                    $.hx.notify('Conversation deleted.', 'success')
                                    update()
                                }
                            });

                        })*/
            }

            function updateNodeTable(graph){
                let nodes = graph.nodes;

                if (nodeTable ) {
                    nodeTable.destroy()
                    nodeTable = undefined
                    // $('#msgDataTableParent').html('<div id="msgDataTable"></div>');
                }

                nodeTable = $('#nodeDataTable').mDatatable({
                    // datasource definition
                    data: {
                        type: 'local',
                        source: nodes,
                        pageSize: 10
                    },

                    // layout definition
                    layout: {
                        theme: 'default', // datatable theme
                        scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                        // height: 430, // datatable's body's fixed height
                        footer: false // display/hide footer
                    },

                    toolbar: {
                        layout: ['pagination', 'info'],
                        placement: ['bottom'],  //'top', 'bottom'
                        items: {
                            pagination: {
                                type: 'default',

                                pages: {
                                    desktop: {
                                        layout: 'default',
                                        pagesNumber: 6
                                    },
                                    tablet: {
                                        layout: 'default',
                                        pagesNumber: 3
                                    },
                                    mobile: {
                                        layout: 'compact'
                                    }
                                },

                                navigation: {
                                    prev: true,
                                    next: true,
                                    first: true,
                                    last: true
                                },

                                pageSizeSelect: [5, 10, 50, -1]
                            },

                            info: true
                        }
                    },

                    // column sorting
                    sortable: true,
                    pagination: true,

                    search: {
                        input: $('#nodeSearchInput'),
                        delay: 200,
                    },
                    // inline and bactch editing(cooming soon)
                    // editable: false,

                    // columns definition
                    columns: [

                        {field: "id", width: 100, title: "term", template: row => row.id},
                        {field: "type", width: 40, title: "type", template: row => row.type},
                        {field: "betweenness", width: 40, title: "centrality", template: row => Number.parseFloat(row.betweenness).toFixed(3)},
                        {field: "src", width: 140, title: "source", template: row => row.src},
                        {field: "snippet", title: "snippet", template: row => row.snippet},


                    ]
                });
                /*$('#linkDataTable')
                        .on('click', `.conversationDetailBtn`, function () {
                            const convId = $(this).data('conv-id');
                            updateMessageTable(convId)
                            $.hx.scrollIntoView('#msgPortlet', -85);
                        })
                        .on('click', `.deleteConvBtn`, function () {
                            const convId = $(this).data('conv-id');
                            $.ajax({
                                url: '/api/v1/db/conversation/' + encodeURIComponent(convId),
                                type: 'DELETE',
                                success: function(result) {
                                    $.hx.notify('Conversation deleted.', 'success')
                                    update()
                                }
                            });

                        })*/
            }




            function updateMap(graph){
                $('#map').show()
                let locationCount = 0;

                for (let node of graph.nodes) {
                    if(node.lat * 1){
                        // found a location
                        locationCount++;
                        // sleaflet.addCircle(node.lat * 1, node.lon * 1, 2000, 'rgb(11, 98, 164)', node.id, 0.25, true)
                        sleaflet.addMarker(node.lat * 1, node.lon * 1, node.id, true)
                    }
                }
                if(locationCount) {
                    $('#map').show()
                    $('#noLocationMsg').hide()
                    sleaflet.addCluster()
                    sleaflet.zoomToMarkers()
                } else {
                    $('#map').hide()
                    $('#noLocationMsg').show()

                }

            }



        })
    </script>
</#macro>