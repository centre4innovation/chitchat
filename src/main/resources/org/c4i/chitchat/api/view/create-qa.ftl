<#import "utils.ftl" as u>

<#macro pageTitle>
ChitChat - Script Chatbots
</#macro>

<#macro pageContent>
<style>
    #outputPanel, #textInput {
        font-family: Roboto, sans-serif;
        font-weight: normal;
        padding: 1em;
    }

    #textInput:focus {
        outline: none;
        color: black;
    }

    #scriptEditor {
        width: 100%;
        min-width: 200px;
        overflow: hidden;
        height: 500px;
    }
</style>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Create Q&A Bot
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="/api/v1/ui/dashboard" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Create</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Q&A</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-warning alert-dismissible fade show">
                This Question and Answer module is an experimental feature and not fully supported.
            </div>
        </div>
        <div class="col-sm-12">

            <div class="splitPanel"
                 style="width:100%; box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);margin-bottom: 2.2rem;background: white;">

                <div id="rulePanel" class="split-flex split-horizontal-flex">


                    <div class="m-portlet" m-portlet="true" id="scriptPortlet" style="margin: 0;height:100%;">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption truncate">
                                <div class="m-portlet__head-title truncate">
                                <#--<h3 class="m-portlet__head-text truncate" id="scriptName">
                                    My Script
                                </h3>-->
                                    <input type="text" value="My Script" id="scriptName"
                                           style="width: 100%;height: 100%;border: none;overflow: hidden;text-overflow: ellipsis;font-family: Poppins;"
                                           readonly>
                                </div>
                            </div>

                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                <#--<li class="m-portlet__nav-item">
                                    <a id="runBtn"
                                       class="m-portlet__nav-link btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--pill"
                                       data-toggle="m-tooltip" data-placement="top" title=""
                                       data-original-title="Run script" style="color:white">
                                        <i class="la la-play"></i>
                                    </a>
                                </li>-->

                                    <li class="m-portlet__nav-item">
                                        <a id="runBtn" class="m-portlet__nav-link btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Run script" style="color:white; display: none">
                                            <i class="la la-play"></i>
                                        </a>
                                    </li>
                                    <li class="m-portlet__nav-item">
                                        <a m-portlet-tool="fullscreen"
                                           class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill">
                                            <i class="la la-expand"></i>
                                        </a>
                                    </li>

                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                        m-dropdown-toggle="hover" aria-expanded="true">
                                        <a href="#"
                                           class="m-portlet__nav-link btn btn-secondary  m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill   m-dropdown__toggle">
                                            <i class="la la-ellipsis-v"></i>
                                        </a>
                                        <div class="m-dropdown__wrapper" style="z-index: 101;">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"
                                                  style="left: auto; right: 21.5px;"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">

                                                        <ul class="m-nav">
                                                            <li class="m-nav__section m-nav__section--first">
                                                                <span class="m-nav__section-text">Actions</span>
                                                            </li>

                                                            <li class="m-nav__item">
                                                                <a id="openScriptBtn" class="m-nav__link"
                                                                   data-toggle="modal" data-target="#loadExampleModal">
                                                                    <i class="m-nav__link-icon la la-star-o"></i>
                                                                    <span class="m-nav__link-text">Open example</span>
                                                                </a>
                                                            </li>

                                                            <li class="m-nav__separator m-nav__separator--fit">
                                                            <li class="m-nav__item">
                                                                <a id="newScriptBtn" class="m-nav__link">
                                                                    <i class="m-nav__link-icon la la-file"></i>
                                                                    <span class="m-nav__link-text">New script</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a id="openScriptBtn" class="m-nav__link"
                                                                   data-toggle="modal" data-target="#openQAModal">
                                                                    <i class="m-nav__link-icon la la-folder-open-o"></i>
                                                                    <span class="m-nav__link-text">Open script</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a id="openVersionBtn" class="m-nav__link"
                                                                   data-toggle="modal" data-target="#openVersionModal">
                                                                    <i class="m-nav__link-icon la la-history"></i>
                                                                    <span class="m-nav__link-text">Open previous version</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a id="saveScriptBtn" class="m-nav__link"
                                                                   data-toggle="modal" data-target="#saveQAModal">
                                                                    <i class="m-nav__link-icon la la-floppy-o"></i>
                                                                    <span class="m-nav__link-text">Save script</span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a id="downloadScriptBtn" class="m-nav__link">
                                                                    <i class="m-nav__link-icon la la-download"></i>
                                                                    <span class="m-nav__link-text">Download script</span>
                                                                </a>
                                                            </li>

                                                            <li class="m-nav__separator m-nav__separator--fit">
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="#"
                                                                   class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="m-portlet__body" style="padding: 0;">
                            <div id="scriptEditor"></div>
                        </div>
                    </div>
                </div>


                <div id="textPanel" class="split-flex split-horizontal-flex;" style="overflow: hidden">

                    <div class="m-portlet m-portlet--tabs" m-portlet="true" id="chatPortlet"
                         style="margin: 0;height: 100%;width: 100%;">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <div>
                                    <#--<span class="m-bootstrap-switch m-bootstrap-switch--pill">
                                        <input  id="testMode" data-switch="true" type="checkbox" data-on-text="Match" data-handle-width="70" data-off-text="Chat" data-on-color="brand" data-off-color="accent">
                                    </span>-->
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn m-btn m-btn--pill btn-secondary chatModeBtn active">
                                                <input type="radio" name="runMode" value="chatMode" autocomplete="off" checked
                                                       data-toggle="m-tooltip" data-placement="top" title=""
                                                       data-original-title="Try out a conversation">
                                                <i class="la la-comments"></i> Chat
                                            </label>
                                            <label class="btn m-btn m-btn--pill btn-secondary matchModeBtn">
                                                <input type="radio" name="runMode" value="matchMode" autocomplete="off"
                                                       data-toggle="m-tooltip" data-placement="top" title=""
                                                       data-original-title="See how labels match a text">
                                                <i class="la la-binoculars"></i> Match
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav" id="chatTools">
                                    <#--<li class="m-portlet__nav-item">
                                        <a id="infoBtn"
                                           class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                           data-toggle="m-tooltip" data-placement="top" title=""
                                           data-original-title="See matched text">
                                            <i class="la la-info"></i>
                                        </a>
                                    </li>-->
                                    <li class="m-portlet__nav-item">
                                        <a id="clearBtn"
                                           class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                           data-toggle="m-tooltip" data-placement="top" title=""
                                           data-original-title="Clear conversation">
                                            <i class="la la-close"></i>
                                        </a>
                                    </li>
                                <#--<li class="m-portlet__nav-item">
                                    <a m-portlet-tool="fullscreen"
                                       class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill">
                                        <i class="la la-expand"></i>
                                    </a>
                                </li>-->
                                </ul>

                                <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--right m-tabs-line-danger" style="float: left; display: none; min-width: 122px; margin-left: 5px;" id="matchTools">
                                    <li class="nav-item m-tabs__item">
                                        <a class="nav-link m-tabs__link active" id="textInputBtn">
                                            Input
                                        </a>
                                    </li>
                                    <li class="nav-item m-tabs__item">
                                        <a class="nav-link m-tabs__link" id="outputPanelBtn">
                                            Output
                                        </a>
                                    </li>
                                </ul>



                            </div>
                        </div>
                        <div class="m-portlet__body" id="chatbody" style="padding: 0; height: calc(100% - 71px);"></div>
                        <div class="m-portlet__body" id="matchbody" style="padding: 0; height: calc(100% - 71px);">
                            <div style="height: 100%">
                                <div class="" style="height: 100%" id="textInputTab">
                                    <textarea id="textInput" style="width: 100%; height: 100%;border: none;" placeholder="Your text goes here..."></textarea>
                                </div>
                                <div class="" style="height: 100%; display: none" id="outputPanelTab">
                                    <p id="outputPanel" style="overflow-y: scroll;height: 100%; margin: 0"><i>Click <i class="la la-play-circle-o"></i> to see the result...</i></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<#-- Example modal -->
<div class="modal fade" id="loadExampleModal" tabindex="-1" role="dialog" aria-labelledby="loadExampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loadExampleModalLabel">Open example script</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="loadExampleSelect">Select an example script</label>
                        <select class="form-control" id="loadExampleSelect">

                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button id="loadExampleBtn" type="button" class="btn btn-brand">Open</button>
            </div>
        </div>
    </div>
</div>

</#macro>

<#macro pageScript>
<link rel="stylesheet" href="/assets/app/js/chitchatclient.css">
<script src="/assets/app/js/chitchatclient.js" type="application/javascript"></script>
<link rel="stylesheet" type="text/css" href="/assets/vendors/custom/handsontable/dist/handsontable.full.min.css">
<script src="/assets/vendors/custom/handsontable/dist/handsontable.full.min.js"></script>
<script>
    $(document).ready(function () {
        $.hx.setCurrentPage('#menu-item-create-script')




        let botResult;

        // Setup speech
        const speechSynth = window.speechSynthesis;

        let chatbox = new ChitChatClient(document.getElementById('chatbody'), {
            showLeftUser: true,
            showRightUser: true,
            scrollToLastMessage: true,
        });

        Split(['#rulePanel', '#textPanel'], {
            sizes: [50, 50],
            gutterSize: 8,
            cursor: 'col-resize',
            elementStyle: function (dimension, size, gutterSize) {
                return {'flex-basis': 'calc(' + size + '% - ' + gutterSize + 'px)'}
            },
            gutterStyle: function (dimension, gutterSize) {
                return {'flex-basis': gutterSize + 'px'}
            }
        });

        let data = [
            ['', ''],
            ['', ''],
        ];

        let settings = {
            data: data,
            colWidths: [200,200],
            stretchH: 'all',
            autoWrapRow: true,
            rowHeaders: true,
            colHeaders: ['Question', 'Answer'],
            contextMenu: true,
            manualRowMove: true,
            manualColumnResize: true,
        };
        let hot = new Handsontable(document.getElementById('scriptEditor'), settings);

        /*$.get('/api/v1/db/qa', function(map){
            // because Object.keys(new Date()).length === 0;
            // we have to do some additional check
            if(!(Object.keys(map).length === 0 && map.constructor === Object)){
                hot.loadData(Object.entries(map))
            }
        })*/


        $('#clearBtn').click(clear);

        $('#infoBtn').click(function () {
            $.hx.notify(botResult.highlight, 'info')
        });

        function setScriptName(name) {
            $('#scriptName').val(name)
            $.hx.set('scriptName', name);
        }


        function clear() {
            $.ajax({
                type: "POST",
                url: "/api/v1/channel/devbot/reset",
                contentType: 'text/plain',
                data: "you",
                success: function () {
                    chatbox.clear()
                    // chatbox.focusOnInput()
                    // scriptEditor.removeMessages()
                    botResult = {highlight: '<i>none</i>'}
                },
                error: function (e) {
                    $.hx.notify(e, 'danger');
                }
            });
        }
        clear(); //reset cache


        const examples = [
            {
                title: '1. Grandpa',
                mode: 'chat',
                description: 'Interview a grumpy grandpa.',
                rules:
                        `what do you like to eat?\tthe same as yesterday
what is you favorite color\tnot too bright ones
what's your name\tI forgot
do you have a hobby\tNobody ever asked me that
do you like music\tOnly the things before 1900
`,
                text:
                        `What music do you like?`,

            },
        ];

        // fill examples
        const exampleSelect = $("#loadExampleSelect");
        exampleSelect.append($('<option value="" disabled selected>Examples...</option>'));
        examples.forEach(function (ex, ix) {
            exampleSelect.append($("<option />").val(ix).text(ex.title));
        })
        ;

        function getScript(){
            return $.hx.csvString(hot.getData())
        }

        function setScript(csv){
            let data = $.hx.csvParse(csv);
            for (const error of data.errors) {
                $.hx.notify(error, 'danger');
            }
            hot.loadData(data.data);
        }



        // on option select
        function updateExample() {
            let example = examples[exampleSelect.val()];
            if(example.mode === 'chat'){
                $('.chatModeBtn').click()
            } else {
                $('.matchModeBtn').click()
            }

            $('#scriptName').val(example.title)
            setScript(example.rules);
            $.hx.notify(example.description, 'primary', null, 15000);
            $('#textInput').val(example.text);
            $('#outputPanel').html('<i>Click [Run] to see the result...</i>');
            $('.nav-tabs a[href="#1"]').tab('show');
        }

        $('#loadExampleBtn').click(() => {
            updateExample()
            $('#loadExampleModal').modal('hide')
        });

        $('#newScriptBtn').click(function () {
                    setScript("\t\n\t")
                    setScriptName("New script")
                }
        )

        $('#downloadScriptBtn').click(() => {
            $.hx.csvDownload( hot.getData(), '\t', 'chitchat-QA.csv')
        })

        let openDocDialog = new OpenDocDialog('body', 'openQAModal', 'qa', 'Open a Q&A sheet', function (name, src) {
            setScriptName(name)
            setScript(src);
        } );

        let openVerionDocDialog = new OpenHistoryDocDialog('body', 'openVersionModal', 'qa',
            function(){return "Q&A sheet"}, 'Open previous version', function (name, src) {
                setScriptName(name)
                setScript(src)
            } );

        let saveDocDialog = new SaveDocDialog('body', 'saveQAModal', 'qa', 'Save Q&A sheet',
            function(){return $.hx.csvString(hot.getData())},
            function(){return "Q&A sheet"},
            function (name) {
                $.hx.notify('Q&A sheet updated', 'success')
                setScriptName(name);
            },
            function (name, e) {
                $.hx.notify('The Q&A sheet could not be saved.<br>' + e, 'danger')
            }
        );

        // load last state
        setScriptName($.hx.get('scriptName', 'New Q&A'));
        setScript($.hx.get('qacsv', '\t\n\t'));

        // auto-save csv content every second
        window.setInterval(() => {
            $.hx.set('qacsv', getScript())
        }, 1000)



        let speechLang = 'en-US';

        let onSend = function (params) {
            const formData = new FormData();
            let script = getScript();
            formData.append("tsv", script);
            formData.append("msg",
                    JSON.stringify({
                        id: '' + Date.now(),
                        text: params.text,
                        senderId: "you",
                        recipientId: "devbot",
                        timestamp: params.timestamp
                    }));

            // Set speech lang
            speechLang = 'en';


            $.ajax({
                type: "POST",
                url: "/api/v1/channel/devqa/reply",
                data: formData,
                processData: false,
                contentType: false,
                success: function (a) {
                    let warningCount = 0, warningMsg;
                    for (const warning of a.warnings) {
                        let message = warning.message;
                        let lineNr = warning.line - 1;
                        if(lineNr !== undefined){
                            $.hx.notify(message + ', line ' + line, 'warning')
                            warningCount++;
                            warningMsg = message;
                        }
                    }

                    if(warningCount === 1)
                        $.hx.notify('There is an warning for your script.<br>' + warningMsg, 'warning')
                    else if (warningCount > 1)
                        $.hx.notify('There are '+warningCount+' errors in your script.', 'warning')

                    botResult = a
                    a.replies.forEach(msg => {
                        let text = msg.text
                        // render image
                        text = text.replace(/\bIMAGE *\((.*?)\)/gm, '<img src="$1" style="display: block;max-width:100%; width: auto; height: auto;"/>')

                        // render buttons
                        text = text.replace(/\bBUTTON *\( *(.*?) *, *(.*?) *\)/gm, '<button type="button" class="btn btn-success chatBtn" data-value="$2">$1</button>')

                        // speech synthesis
                        let speakPattern = /\bSPEAK *\((.*?)\)/gm;
                        let utterances;
                        while ((utterances = speakPattern.exec(text)) !== null) {
                            const utterThis = new SpeechSynthesisUtterance(utterances[1]);
                            if (speechLang){
                                utterThis.lang = speechLang;
                            }
                            speechSynth.speak(utterThis);
                        }
                        text = text.replace(speakPattern, '');

                        chatbox.addMessageLeft(text, "BOT");
                    })
                },
                error: function (e) {
                    const exceptions = JSON.parse(e.responseText);
                    let errorCount = 0, warningCount = 0;
                    let errorMsg, warningMsg;
                    for (const e of exceptions) {
                        let message = e.message;
                        let lineNr = e.line - 1;
                        if(lineNr !== undefined){
                            if(e.type === "WARNING") {
                                $.hx.notify(message + ', line ' + line, 'warning')
                                warningCount++;
                                warningMsg = message;
                            }else {
                                $.hx.notify(message + ', line ' + line, 'warning')
                                errorCount++;
                                errorMsg = message;
                            }
                        }
                    }

                    if(errorCount === 1)
                        $.hx.notify('There is an error in your script.<br>' + errorMsg, 'danger')
                    else if (errorCount > 1)
                        $.hx.notify('There are '+errorCount+' errors in your script.', 'danger')

                    if(warningCount === 1)
                        $.hx.notify('There is an warning for your script.<br>' + warningMsg, 'warning')
                    else if (warningCount > 1)
                        $.hx.notify('There are '+warningCount+' errors in your script.', 'warning')


                }
            })
        };
        chatbox.onSend(onSend, false);
        // link button actions
        $('#chatbody').on('click', '.chatBtn', function(){
            let value = $(this).data('value');
            const btnParams = {
                id: '' + Date.now(),
                text: value,
                senderId: "you",
                recipientId: "devbot",
                timestamp: new Date()
            }
            $(this).addClass('chosen').parent().children().prop('disabled', true);
            onSend(btnParams)
        })

        new ShowdownAccordion('/ccsdoc.md', '#doc', 1, "cssmode")

        $('input[type=radio][name=runMode]').change(function() {
            if (this.value === 'matchMode') {
                matchMode()
            } else if (this.value === 'chatMode') {
                chatMode()
            }
        })

        function matchMode(){
            $('#chatTools').hide()
            $('#chatbody').hide()
            $('#matchTools').show()
            $('#matchbody').show()
            $('#runBtn').show()
        }

        function chatMode(){
            $('#chatTools').show()
            $('#chatbody').show()
            $('#matchTools').hide()
            $('#matchbody').hide()
            $('#runBtn').hide()
        }


        $('#textInputBtn').click(() => {
            $('#textInputBtn').addClass('active');
            $('#outputPanelBtn').removeClass('active');
            $('#textInputTab').show()
            $('#outputPanelTab').hide()
        })

        $('#outputPanelBtn').click(() => {
            $('#textInputBtn').removeClass('active');
            $('#outputPanelBtn').addClass('active');
            $('#textInputTab').hide()
            $('#outputPanelTab').show()
        })


        $('#runBtn').click(() => {

            let formData = new FormData();
            formData.append("script", getScript());
            formData.append("text", $('#textInput').val());

            $.ajax({
                type: "POST",
                url: "/api/v1/channel/devbot/match",
                data: formData,
                processData: false,
                contentType: false,
                success: (a) => {
                    console.log(a);
                    let warningCount = 0, warningMsg;
                    for (const warning of a.warnings) {
                        let message = warning.message;
                        let lineNr = warning.line - 1;
                        if(lineNr !== undefined){
                            $.hx.notify(message + ', line ' + line, 'warning')
                            warningCount++;
                            warningMsg = message;
                        }
                    }

                    if(warningCount === 1)
                        $.hx.notify('There is an warning for your script.<br>' + warningMsg, 'warning')
                    else if (warningCount > 1)
                        $.hx.notify('There are '+warningCount+' errors in your script.', 'warning')

                    $('#outputPanel').html(a.highlight);
                    $('#outputPanelBtn').click()
                    if(a.replies && a.replies.length > 0){
                        a.replies.forEach(msg => {
                            $.hx.notify(msg.text, msg.text  ? 'brand' : 'danger')
                        })
                    }

                },
                error: (e) => {
                        $.hx.notify('There is an error in your Q&A sheet. ' + e, 'warning')
                }
            });
        })
    })
</script>
</#macro>