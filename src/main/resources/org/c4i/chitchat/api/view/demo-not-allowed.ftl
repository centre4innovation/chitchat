<#import "utils.ftl" as u>

<#macro pageTitle>
ChitChat Demo - Not Allowed
</#macro>

<#macro pageContent>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Demo: Not Allowed
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="/api/v1/ui/dashboard" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="row">
        <div class="col-lg-12" >
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-chain-broken"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                This functionality is not available...
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="m-portlet__body">
                    <p>
                        This ChitChat server is a limited one...
                    </p>
                    <p>You can:</p>
                    <ul>
                        <li>Do the tutorial, to learn some scripting basics</li>
                        <li>Create scripts, and see how they behave</li>
                        <li>Access the documentation</li>
                        <li>Learn how Data Sheets work</li>
                    </ul>
                    <p>But you can't:</p>
                    <ul>
                        <li>Save/open scripts from others</li>
                        <li>Use your chatbot externally</li>
                        <li>Add data sheets</li>
                        <li>Analyse how visitors used the chatbot</li>
                        <li>Access the Rest API for programmers</li>
                    </ul>


                </div>
            </div>
        </div>
    </div>
</div>

</#macro>

<#macro pageScript>

<script>


</script>

</#macro>