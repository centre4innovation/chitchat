<#-- @ftlvariable name="" type="org.c4i.chitchat.api.view.PageView" -->
<#import "${contentFtl}" as content>

<!DOCTYPE html>
<!--
   ________    _ __  ________          __
  / ____/ /_  (_) /_/ ____/ /_  ____ _/ /_
 / /   / __ \/ / __/ /   / __ \/ __ `/ __/
/ /___/ / / / / /_/ /___/ / / / /_/ / /_
\____/_/ /_/_/\__/\____/_/ /_/\__,_/\__/ DEMO

Leiden university - Centre for Innovation - HumanityX

-->
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>
        Demo <@content.pageTitle />
    </title>
    <meta name="description" content="Script information retrieval and conversations">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="/assets/vendors/base/fonts/poppins.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/vendors/base/fonts/roboto.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="/assets/app/js/chitchatclient.core.css">
    <link rel="stylesheet" href="/assets/app/js/chitchatclient.components.css">

    <!--begin::Base Styles -->
    <link href="/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css"/>
    <!--end::Base Styles -->

    <link rel="stylesheet" href="/assets/vendors/custom/codemirror/lib/codemirror.css">
    <link rel="stylesheet" href="/assets/vendors/custom/codemirror/addon/dialog/dialog.css">
    <link rel="stylesheet" href="/assets/vendors/custom/codemirror/addon/fold/foldgutter.css">
    <link rel="stylesheet" href="/assets/vendors/custom/codemirror/addon/scroll/simplescrollbars.css">
    <link rel="stylesheet" href="/assets/vendors/custom/codemirror/addon/search/matchesonscrollbar.css">
    <link rel="stylesheet" href="/assets/vendors/custom/codemirror/addon/hint/show-hint.css">
    <link rel="stylesheet" href="/assets/app/js/chitchatscript.css">
    <link rel="stylesheet" href="/assets/app/js/mddoc.css">
    <link rel="stylesheet" href="/assets/vendors/custom/leaflet/leaflet.css" />
    <link rel="stylesheet" href="/assets/vendors/custom/leaflet/markercluster/MarkerCluster.css" />
    <link rel="stylesheet" href="/assets/vendors/custom/leaflet/markercluster/MarkerCluster.Default.css" />

    <style>
        .hxlogo {
            width: 150px;
            font-size: 20px;
            display: inline-block;
        }

        .hxlogo a {
            text-decoration: none;
            color: #ffb822;
        }

        .hxlogo a:hover {
            text-decoration: none;
            color: #bea8f2;
        }

        .truncate {
            /*width: 250px;*/
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .m-nav__link, dropdown-item {
            cursor: pointer;
        }

        .leaflet-map-pane canvas {
            z-index: 4;
        }

        .m-portlet.m-portlet--fullscreen {
            z-index: 6;
        }

        .Codemirror pre {
            font-family: SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;;
        }

        .btn-default {
            color: #212529;
        }
    </style>

    <link rel="shortcut icon" href="/assets/app/media/img/favicon.ico"/>
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default m-aside-left--fixed">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand  m-brand--skin-dark ">
                    <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo hxlogo">
                            <a href="/api/v1/ui/dashboard">
                                <img alt="" width="40px" src="/assets/app/media/img/chitchat-logo-demo.svg"
                                     style="margin-right: 4px"/> ChitChat
                            </a>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">
                            <!-- BEGIN: Left Aside Minimize Toggle -->
                            <a href="javascript:" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
                                <span></span>
                            </a>
                            <!-- END -->
                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <a href="javascript:" id="m_aside_left_offcanvas_toggle"
                               class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>
                            <!-- END -->
                            <!-- BEGIN: Topbar Toggler -->
                            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:"
                               class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                <i class="flaticon-more"></i>
                            </a>
                            <!-- BEGIN: Topbar Toggler -->
                        </div>
                    </div>
                </div>
                <!-- END: Brand -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                    <!-- BEGIN: Horizontal Menu -->
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark "
                            id="m_aside_header_menu_mobile_close_btn">
                        <i class="la la-close"></i>
                    </button>
                    <!-- END: Horizontal Menu -->                                <!-- BEGIN: Topbar -->
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item" style="padding: 12px;">
                                    <span class="alert alert-warning" style="top: 12px;" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Feel free to try out some chatbot scripting. Analytics and saving scripts are not allowed, though.">
                                        This is a public <b>demo server</b> with limited functionality
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- END: Topbar -->
                </div>
            </div>
        </div>
    </header>
    <!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
            <i class="la la-close"></i>
        </button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
            <!-- BEGIN: Aside Menu -->
            <div
                    id="m_ver_menu"
                    class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark m-scroller ps ps--active-y"
                    m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                    <li id="menu-item-dashboard" class="m-menu__item " aria-haspopup="true">
                        <a href="/api/v1/ui/dashboard" class="m-menu__link ">
                            <i class="m-menu__link-icon la la-home"></i>
                            <span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												Dashboard
											</span>
										</span>
									</span>
                        </a>
                    </li>

                    <li id="menu-item-tutorial" class="m-menu__item " aria-haspopup="true">
                        <a href="/api/v1/ui/tutorial" class="m-menu__link ">
                            <i class="m-menu__link-icon la la-graduation-cap"></i>
                            <span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												Tutorial
											</span>
										</span>
									</span>
                        </a>
                    </li>

                    <li id="menu-item-tutorial" class="m-menu__item " aria-haspopup="true">
                        <a href="/api/v1/ui/system/docs" class="m-menu__link">
                            <i class="m-menu__link-icon la la-life-saver"></i>
                            <span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												Documentation
											</span>
										</span>
									</span>
                        </a>
                    </li>

                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">
                            Create
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>

                    <li id="menu-item-create-script" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/create/script" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-edit"></i>
                            <span class="m-menu__link-text">Script</span>
                        </a>
                    </li>
                    <li id="menu-item-create-survey" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/create/survey" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-clipboard"></i>
                            <span class="m-menu__link-text">Survey</span>
                        </a>
                    </li>

                    <li id="menu-item-create-levi" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/create/qa" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-question"></i>
                            <span class="m-menu__link-text">Q&A</span>
                        </a>
                    </li>

                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">
                            Data
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>
                    <li id="menu-item-data-sheets" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/data/sheets" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-th-list"></i>
                            <span class="m-menu__link-text">Data sheets</span>
                        </a>
                    </li>
                    <li id="menu-item-data-variables" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/data/variables" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-dollar"></i>
                            <span class="m-menu__link-text">Reply variables</span>
                        </a>
                    </li>
                    <li id="menu-item-data-export" class="m-menu__item  m-menu__item--submenu disabled" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/data/export" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-exchange"></i>
                            <span class="m-menu__link-text">Import/Export</span>
                        </a>
                    </li>

                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">
                            Analyse
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>
                    <li id="menu-item-analyse-overview" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/analyse/overview" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-eye"></i>
                            <span class="m-menu__link-text">Overview</span>
                        </a>
                    </li>
                    <#--<li id="menu-item-analyse-conversation" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/analyse/conversations" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-comments"></i>
                            <span class="m-menu__link-text">Conversations</span>
                        </a>
                    </li>
                    <li id="menu-item-analyse-analyse" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/analyse/statistics" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-area-chart"></i>
                            <span class="m-menu__link-text">Analyse</span>
                        </a>
                    </li>-->
                    <li id="menu-item-analyse-analyse" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/analyse/trend" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-area-chart"></i>
                            <span class="m-menu__link-text">Trends</span>
                        </a>
                    </li>

                    <li id="menu-item-analyse-analyse" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/analyse/graph" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-share-alt"></i>
                            <span class="m-menu__link-text">Graph</span>
                        </a>
                    </li>



                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">
                            Live channels
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>
                    <li id="menu-item-live-chitchat" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/live/chitchat" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-comment-o"></i>
                            <span class="m-menu__link-text">Chitchat</span>
                        </a>
                    </li>
                    <li id="menu-item-live-fb" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/live/fb" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-facebook"></i>
                            <span class="m-menu__link-text">Facebook</span>
                        </a>
                    </li>
                    <li id="menu-item-live-telegram" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/live/telegram" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-paper-plane-o"></i>
                            <span class="m-menu__link-text">Telegram</span>
                        </a>
                    </li>

                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">
                            System
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>

                    <li id="menu-item-system-info" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/system/info" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-server"></i>
                            <span class="m-menu__link-text">Server info</span>
                        </a>
                    </li>
                    <li id="menu-item-system-performance" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/system/performance" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-line-chart"></i>
                            <span class="m-menu__link-text">Performance</span>
                        </a>
                    </li>
                    <li id="menu-item-system-sppech" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/system/speech" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-volume-up"></i>
                            <span class="m-menu__link-text">Speech test</span>
                        </a>
                    </li>
                    <li id="menu-item-system-apidoc" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/system/apidoc" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-code"></i>
                            <span class="m-menu__link-text">REST API</span>
                        </a>
                    </li>
                    <li id="menu-item-system-about" class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="/api/v1/ui/about" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon la la-info-circle"></i>
                            <span class="m-menu__link-text">About</span>
                        </a>
                    </li>

                </ul>
            </div>
            <!-- END: Aside Menu -->
        </div>
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <@content.pageContent />
        </div>
    </div>
    <!-- end:: Body -->
    <!-- begin::Footer -->
    <footer class="m-grid__item		m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								2019 &copy; Centre for Innovation | Leiden University
							</span>
                </div>
                <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                    <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                        <li class="m-nav__item">
                            <a href="/api/v1/ui/about" class="m-nav__link">
                                <span class="m-nav__link-text">About</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="/api/v1/ui/privacy" class="m-nav__link">
                                <span class="m-nav__link-text">Privacy</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- end::Footer -->
</div>
<!-- end:: Page -->

<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->


<!--begin::Base Scripts -->
<script src="/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<!--end::Base Scripts -->
<#--<script src="/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>-->

<script src="/assets/vendors/custom/papaparse/papaparse.min.js" type="text/javascript"></script>

<script src="/assets/app/js/hx.util.js" type="text/javascript"></script>
<script src="/assets/app/js/hx.clientstate.js" type="text/javascript"></script>
<script src="/assets/app/js/hx.ui.js" type="text/javascript"></script>
<script src="/assets/app/js/hx.dbdoc.js" type="text/javascript"></script>

<script src="/assets/vendors/custom/filesaver/FileSaver.min.js" type="text/javascript"></script>
<script src="/assets/vendors/custom/plotly/plotly.min.js" type="text/javascript"></script>

<script src="/assets/app/js/hx.plotly.js" type="text/javascript"></script>

<script src="/assets/vendors/custom/split/split.min.js"></script>

<script src="/assets/vendors/custom/leaflet/leaflet.js"></script>
<script src="/assets/vendors/custom/leaflet/markercluster/leaflet.markercluster.js"></script>
<script src="/assets/vendors/custom/leaflet/leaflet.hotline.js"></script>
<script src="/assets/vendors/custom/c4i/Sleaflet.js"></script>

<#--<script src="/assets/vendors/custom/js-yaml.js"></script>-->
<script src="/assets/vendors/custom/codemirror/lib/codemirror.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/mode/simple.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/runmode/runmode.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/dialog/dialog.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/scroll/annotatescrollbar.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/edit/matchbrackets.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/edit/closebrackets.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/search/search.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/search/searchcursor.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/search/jump-to-line.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/search/matchesonscrollbar.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/comment/comment.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/selection/active-line.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/mode/overlay.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/mode/multiplex.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/fold/foldcode.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/fold/foldgutter.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/fold/indent-fold.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/fold/brace-fold.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/scroll/simplescrollbars.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/hint/show-hint.js"></script>
<script src="/assets/vendors/custom/codemirror/addon/hint/anyword-hint.js"></script>
<#--<script src="/assets/vendors/custom/codemirror/mode/yaml-frontmatter/yaml-frontmatter.js"></script>-->
<script src="/assets/vendors/custom/codemirror/mode/yaml/yaml.js"></script>
<script src="/assets/vendors/custom/c4i/ccs-editor.js"></script>

<script src="/assets/vendors/custom/showdown/showdown.min.js"></script>
<script src="/assets/vendors/custom/showdown/showdown.accordion.js"></script>
<script src="/assets/vendors/custom/c4i/c4i.js" type="text/javascript"></script>

<script>
    $(document).ready(() => {
        $('.logoutBtn').click($.hx.logout);
        $('body').append($('<iframe id="download_iframe" style="display:none;"></iframe>'));
    })
</script>

<@content.pageScript />
</body>
<!-- end::Body -->
</html>
