<#import "utils.ftl" as u>

<#macro pageTitle>
ChitChat - Dashboard
</#macro>

<#macro pageContent>
<style>
    .m-widget7.m-widget7--skin-dark .m-widget7__desc {
        margin-top: 1rem;
        margin-bottom: 1rem;
    }

    .scriptBg {
        background: linear-gradient(142deg, #716aca, #9816f4, #00c5dc);
        background-size: 200% 200%;

        -webkit-animation: BgAnimation 19s ease infinite;
        -moz-animation: BgAnimation 19s ease infinite;
        animation: BgAnimation 19s ease infinite;
    }

    .matchBg {
        background: linear-gradient(142deg, #f4e75c, #00c5dc, #716aca);
        background-size: 200% 200%;

        -webkit-animation: BgAnimation 19s ease infinite;
        -moz-animation: BgAnimation 19s ease infinite;
        animation: BgAnimation 19s ease infinite;
    }

    .manageBg {
        background: linear-gradient(142deg, #928ee9, #b6b6b9,  #f4cb2c);
        background-size: 200% 200%;

        -webkit-animation: BgAnimation 19s ease infinite;
        -moz-animation: BgAnimation 19s ease infinite;
        animation: BgAnimation 19s ease infinite;
    }

    @-webkit-keyframes BgAnimation {
        0%{background-position:14% 0%}
        50%{background-position:87% 100%}
        100%{background-position:14% 0%}
    }
    @-moz-keyframes BgAnimation {
        0%{background-position:14% 0%}
        50%{background-position:87% 100%}
        100%{background-position:14% 0%}
    }
    @keyframes BgAnimation {
        0%{background-position:14% 0%}
        50%{background-position:87% 100%}
        100%{background-position:14% 0%}
    }
</style>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                ChitChat
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Script information retrieval and conversations
											</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="row">
        <div class="col-lg-4">
            <!--begin:: portlet-->
            <div class="m-portlet m--bg-brand m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height scriptBg">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Script
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
                                <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
                                    <i class="la la-ellipsis-h m--font-light"></i>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <li class="m-nav__section m-nav__section--first">
																			<span class="m-nav__section-text">
																				Quick Actions
																			</span>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="/api/v1/ui/system/docs" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-life-saver"></i>
                                                            <span class="m-nav__link-text">Documentation</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="/api/v1/ui/create/script" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-edit"></i>
                                                            <span class="m-nav__link-text">Create script</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin::Widget 7-->
                    <div class="m-widget7 m-widget7--skin-dark">
                        <div class="m-widget7__desc">
                            Categorize texts by looking for word patterns. The expressive scripting language allows you to automate dialogues.
                        </div>
                        <div class="m-widget7__button">
                            <a class="m-btn m-btn--pill btn btn-warning" href="/api/v1/ui/system/docs" role="button">
                                Learn
                            </a>
                        </div>
                    </div>
                    <!--end::Widget 7-->
                </div>
            </div>
            <!--end:: Widgets/Announcements 1-->
        </div>
        <div class="col-lg-4">
            <!--begin:: portlet-->
            <div class="m-portlet m--bg-accent m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height matchBg">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Data
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
                                <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
                                    <i class="la la-ellipsis-h m--font-light"></i>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <li class="m-nav__section m-nav__section--first">
																			<span class="m-nav__section-text">
																				Quick Actions
																			</span>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="/api/v1/ui/analyse/overview" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-eye"></i>
                                                            <span class="m-nav__link-text">Overview</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="/api/v1/ui/analyse/conversations?keyword=alert&channel=fb" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-bell-o"></i>
                                                            <span class="m-nav__link-text">Alerts</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin::Widget 7-->
                    <div class="m-widget7 m-widget7--skin-dark">
                        <div class="m-widget7__desc">
                            Gain insight in topics of conversations instantly, by leveraging your scripts to do information retrieval.
                        </div>
                        <div class="m-widget7__button">
                            <a class="m-btn m-btn--pill btn btn-danger" href="/api/v1/ui/analyse/overview" role="button">
                                Overview
                            </a>
                        </div>
                    </div>
                    <!--end::Widget 7-->
                </div>
            </div>
            <!--end:: Widgets/Announcements 1-->
        </div>
        <div class="col-lg-4">
            <!--begin:: portlet-->
            <div class="m-portlet m--bg-metal m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height manageBg">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Manage
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
                                <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
                                    <i class="la la-ellipsis-h m--font-light"></i>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <li class="m-nav__section m-nav__section--first">
																			<span class="m-nav__section-text">
																				Quick Actions
																			</span>
                                                    </li>

                                                    <li class="m-nav__item">
                                                        <a href="/api/v1/ui/live/chitchatb" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-comment-o"></i>
                                                            <span class="m-nav__link-text">Chitchat settings</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="/api/v1/ui/live/fb" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-facebook"></i>
                                                            <span class="m-nav__link-text">Facebook settings</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="/api/v1/ui/live/fb" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-paper-plane-o"></i>
                                                            <span class="m-nav__link-text">Telegram settings</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="/api/v1/ui/system/info" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-server"></i>
                                                            <span class="m-nav__link-text">System properties</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin::Widget 7-->
                    <div class="m-widget7 m-widget7--skin-dark">
                        <div class="m-widget7__desc">
                            Select which bots should go live and monitor system resources. <br><br>
                        </div>
                        <div class="m-widget7__button">
                            <a class="m-btn m-btn--pill btn btn-focus" href="/api/v1/ui/system/info" role="button">
                                Server info
                            </a>
                        </div>
                    </div>
                    <!--end::Widget 7-->
                </div>
            </div>
            <!--end:: Widgets/Announcements 1-->
        </div>
    </div>

    <p>To learn more about writing scripts, do the <a class="btn  btn-default" href="/api/v1/ui/totorial"><i class="la la-graduation-cap"></i> Tutorial</a> or take a look at the <a class="btn  btn-default" href="/api/v1/ui/system/docs"><i class="la la-life-saver"></i> Documentation</a>, this information is also available at the bottom of each scripting page.

    <hr>
    <p>The ChitChat project is a collaboration between <b>Centre for Innovation<b> (Leiden University), <b>Free Press Unlimited</b>, <b>Radio Dabanga</b>, <b>World Food Programme</b>.</p>
    <p></p>
</div>
</#macro>

<#macro pageScript>
<script>
    $(document).ready(function () {
        $.hx.setCurrentPage('#menu-item-dashboard')
    })

</script>

</#macro>