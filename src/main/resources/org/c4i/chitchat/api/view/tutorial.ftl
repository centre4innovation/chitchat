<#import "utils.ftl" as u>

<#macro pageTitle>
    ChitChat - Tutorial
</#macro>

<#macro pageContent>
    <style>
        #outputPanel, #textInput {
            font-family: Roboto, sans-serif;
            font-weight: normal;
            padding: 1em;
        }

        #textInput:focus {
            outline: none;
            color: black;
        }

        #tutorialContent li {
            list-style-type: circle;
            padding-left: 5px;
            cursor: pointer;
            color: #336e9b;
        }

        #tutorialContent li.here {
            font-weight: bold;
            list-style-type: square;
        }

        @keyframes shadow-pulse
        {
            0% {
                box-shadow: 0 0 0 0px rgba(244, 203, 44, 0.2);
            }
            100% {
                box-shadow: 0 0 0 35px rgba(244, 203, 44, 0);
            }
        }

        .intercom-composer-popover-input{
            font-size-adjust: none;
            font-size: 100%;
            font-style: normal;
            letter-spacing: normal;
            font-stretch: normal;
            font-variant: normal;
            font-weight: 400;
            font: normal normal 100% "intercom-font", "Helvetica Neue", Helvetica, Arial, sans-serif;
            text-align: left;
            text-align-last: auto;
            text-decoration: none;
            -webkit-text-emphasis: none;
            text-emphasis: none;
            text-height: auto;
            text-indent: 0;
            text-justify: auto;
            text-outline: none;
            text-shadow: none;
            text-transform: none;
            text-wrap: normal;
            alignment-adjust: auto;
            alignment-baseline: baseline;
            -webkit-animation: none 0 ease 0 1 normal;
            animation: none 0 ease 0 1 normal;
            -webkit-animation-play-state: running;
            animation-play-state: running;
            -webkit-appearance: normal;
            -moz-appearance: normal;
            appearance: normal;
            azimuth: center;
            -webkit-backface-visibility: visible;
            backface-visibility: visible;
            background: none 0 0 auto repeat scroll padding-box transparent;
            background-color: transparent;
            background-image: none;
            baseline-shift: baseline;
            binding: none;
            bleed: 6pt;
            bookmark-label: content();
            bookmark-level: none;
            bookmark-state: open;
            bookmark-target: none;
            border: 0 none transparent;
            border-radius: 0;
            bottom: auto;
            box-align: stretch;
            -webkit-box-decoration-break: slice;
            box-decoration-break: slice;
            box-direction: normal;
            box-flex: 0.0;
            box-flex-group: 1;
            box-lines: single;
            box-ordinal-group: 1;
            box-orient: inline-axis;
            box-pack: start;
            box-shadow: none;
            box-sizing: content-box;
            -webkit-column-break-after: auto;
            break-after: auto;
            -webkit-column-break-before: auto;
            break-before: auto;
            -webkit-column-break-inside: auto;
            break-inside: auto;
            caption-side: top;
            clear: none;
            clip: auto;
            color: inherit;
            color-profile: auto;
            -webkit-column-count: auto;
            -moz-column-count: auto;
            column-count: auto;
            -webkit-column-fill: balance;
            -moz-column-fill: balance;
            column-fill: balance;
            -webkit-column-gap: normal;
            -moz-column-gap: normal;
            column-gap: normal;
            -webkit-column-rule: medium medium #1f1f1f;
            -moz-column-rule: medium medium #1f1f1f;
            column-rule: medium medium #1f1f1f;
            -webkit-column-span: 1;
            -moz-column-span: 1;
            column-span: 1;
            -webkit-column-width: auto;
            -moz-column-width: auto;
            column-width: auto;
            -webkit-columns: auto auto;
            -moz-columns: auto auto;
            columns: auto auto;
            content: normal;
            counter-increment: none;
            counter-reset: none;
            crop: auto;
            cursor: auto;
            direction: ltr;
            display: inline;
            dominant-baseline: auto;
            drop-initial-after-adjust: text-after-edge;
            drop-initial-after-align: baseline;
            drop-initial-before-adjust: text-before-edge;
            drop-initial-before-align: caps-height;
            drop-initial-size: auto;
            drop-initial-value: initial;
            elevation: level;
            empty-cells: show;
            fit: fill;
            fit-position: 0 0;
            float: none;
            float-offset: 0 0;
            grid-columns: none;
            grid-rows: none;
            hanging-punctuation: none;
            height: auto;
            hyphenate-after: auto;
            hyphenate-before: auto;
            hyphenate-character: auto;
            hyphenate-lines: no-limit;
            hyphenate-resource: none;
            -webkit-hyphens: manual;
            -ms-hyphens: manual;
            hyphens: manual;
            icon: auto;
            image-orientation: auto;
            image-rendering: auto;
            image-resolution: normal;
            inline-box-align: last;
            left: auto;
            line-height: inherit;
            line-stacking: inline-line-height exclude-ruby consider-shifts;
            list-style: disc outside none;
            margin: 0;
            marks: none;
            marquee-direction: forward;
            marquee-loop: 1;
            marquee-play-count: 1;
            marquee-speed: normal;
            marquee-style: scroll;
            max-height: none;
            max-width: none;
            min-height: 0;
            min-width: 0;
            move-to: normal;
            nav-down: auto;
            nav-index: auto;
            nav-left: auto;
            nav-right: auto;
            nav-up: auto;
            opacity: 1;
            orphans: 2;
            outline: medium none invert;
            outline-offset: 0;
            overflow: visible;
            overflow-style: auto;
            padding: 0;
            page: auto;
            page-break-after: auto;
            page-break-before: auto;
            page-break-inside: auto;
            page-policy: start;
            -webkit-perspective: none;
            perspective: none;
            -webkit-perspective-origin: 50% 50%;
            perspective-origin: 50% 50%;
            pointer-events: auto;
            position: static;
            presentation-level: 0;
            punctuation-trim: none;
            quotes: none;
            rendering-intent: auto;
            resize: none;
            right: auto;
            rotation: 0;
            rotation-point: 50% 50%;
            ruby-align: auto;
            ruby-overhang: none;
            ruby-position: before;
            ruby-span: none;
            size: auto;
            string-set: none;
            table-layout: auto;
            top: auto;
            -webkit-transform: none;
            -ms-transform: none;
            transform: none;
            -webkit-transform-origin: 50% 50% 0;
            -ms-transform-origin: 50% 50% 0;
            transform-origin: 50% 50% 0;
            -webkit-transform-style: flat;
            transform-style: flat;
            transition: all 0 ease 0;
            unicode-bidi: normal;
            vertical-align: baseline;
            white-space: normal;
            white-space-collapse: collapse;
            widows: 2;
            width: auto;
            word-break: normal;
            word-spacing: normal;
            word-wrap: normal;
            z-index: auto;
            text-align: start;
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(enabled=false)";
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;

        }
        .intercom-composer-popover {
            z-index: 2147483003;
            position: absolute;
            bottom: 50px;
            right: calc(50% - 165px);
            box-shadow: 0 1px 15px 1px rgba(0,0,0,.08);
            background-color: #fff;
            border-radius: 6px;
            transition-duration: 200ms;
            transition-delay: 0ms;
            transform-style: flat;
            transform-origin: 50% 50% 0;
            opacity: 0;
            transition: all 0.2s linear;
            visibility: hidden;
        }
        .intercom-composer-popover.active{
            visibility: visible;
            opacity:1;
            bottom: 70px;
        }
        .intercom-emoji-picker {
            width: 330px;
            height: 260px;
        }
        .intercom-composer-popover-header {
            position: absolute;
            top: 0;
            left: 20px;
            right: 20px;
            height: 40px;
            border-bottom: 1px solid #edeff1;
        }
        .intercom-composer-popover-input {
            background-image: url(https://js.intercomcdn.com/images/search.7ae40c25.png);
            background-size: 16px 16px;
            background-repeat: no-repeat;
            background-position: 0 12px;
            font-weight: 400;
            font-size: 14px;
            color: #6e7a89;
            padding-left: 25px;
            height: 40px;
            width: 100%;
            box-sizing: border-box;
            background-image: url(https://js.intercomcdn.com/images/search@2x.9f02b9f3.png);
            border:none;
            outline: none;
        }
        .intercom-composer-popover-body {
            position: absolute;
            top: 40px;
            left: 0;
            right: 0;
            bottom: 5px;
            padding: 0 20px;
            overflow-y: scroll;
        }
        .intercom-emoji-picker-group {
            margin: 10px -5px;
        }
        .intercom-emoji-picker-group {
            margin: 10px -5px;
        }
        .intercom-emoji-picker-group-title {
            color: #b8c3ca;
            font-weight: 400;
            font-size: 13px;
            margin: 5px;
        }
        .intercom-emoji-picker-emoji {
            padding: 5px;
            width: 30px;
            line-height: 30px;
            display: inline-table;
            text-align: center;
            cursor: pointer;
            vertical-align: middle;
            font-size: 28px;
            transition: -webkit-transform 60ms ease-out;
            transition: transform 60ms ease-out;
            transition: transform 60ms ease-out,-webkit-transform 60ms ease-out;
            transition-delay: 60ms;
            font-family: Apple Color Emoji,Segoe UI Emoji,NotoColorEmoji,Segoe UI Symbol,Android Emoji,EmojiSymbols;
        }
        .intercom-emoji-picker-emoji:hover {
            transition-delay: 0ms;
            -webkit-transform: scale(1.4);
            -ms-transform: scale(1.4);
            transform: scale(1.4);
        }
        .intercom-composer-popover-caret {
            position: absolute;
            bottom: -8px;
            right: 0;
            width: 0;
            height: 0;
            border-left: 8px solid transparent;
            border-right: 8px solid transparent;
            border-top: 8px solid #fff;
            left:20px;
        }

        .chat-input-tool{
            background-color: #32A8E6;
            padding: 10px;
            border:none;
            border-radius: 5px;
            position: absolute;
            bottom:20px;
            left: 50%;
            outline:none;
        }

    </style>

    <link rel="stylesheet" href="/assets/vendors/custom/intro/introjs.min.css">

<#--<div class="m-subheader " style="margin-bottom: -50px;">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Tutorial
            </h3>
            &lt;#&ndash;<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="/api/v1/ui/dashboard" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
            </ul>&ndash;&gt;
        </div>
    </div>
</div>-->

    <div class="m-content" style="padding-top: 0">
        <div class="row"  data-step="1" data-intro="I'll explain you what to do and why here..." style="min-height: 174px;">
            <div class="col-sm-2" id="tutorialContent">
                <img src="/assets/app/media/img/robot-tutor.png" alt="Tutor" style="width:120px ">
            </div>
            <div class="col-sm-10">
                <svg height="10" width="20" style="margin-bottom: -28px;margin-left: -10px;"><polygon points="0,0 20,0 10,10" style="stroke:none;stroke-width:0;fill:#336e9b;"></polygon></svg>
                <div class="alert alert-info" style="min-height: 68px;border: none;padding-right: 250px;background-color: #336e9b;">
                <span style="font-size: larger" id="tutorialDescription">
                    -
                </span>
                    <div style="position: absolute;right: 12px;top: 12px;" data-step="4" data-intro="When you think you got it, you can go to the next exercise. Or have a look at the proposed script to solve the challenge. <p> You're ready for the next step!">
                        <div class="btn btn-info" id="tutorialSolutionBtn" style="background-color: #45aaf8"><i class="la la-lightbulb-o"></i> Solution</div>
                        <div class="btn btn-info" id="tutorialNextBtn" style="background-color: #2c6c9e" >Next <i class="la la-angle-right"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" >
            <div class="col-sm-2 well well-sm" id="tutorialContent">
                <ul></ul>
            </div>
            <div class="col-sm-10">

                <div class="splitPanel"
                     style="width:100%; box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);margin-bottom: 2.2rem;background: white; height:500px;">

                    <div id="rulePanel" class="split-flex split-horizontal-flex" data-step="2" data-intro="The chatbot behaves according to a script. You can edit that here...">


                        <div class="m-portlet" m-portlet="true" id="scriptPortlet" style="margin: 0;height:100%;">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption truncate">
                                    <div class="m-portlet__head-title truncate">
                                        <#--<h3 class="m-portlet__head-text truncate" id="scriptName">
                                            My Script
                                        </h3>-->
                                        <input type="text" value="My Script" id="scriptName"
                                               style="width: 100%;height: 100%;border: none;overflow: hidden;text-overflow: ellipsis;font-family: Poppins;"
                                               readonly>
                                    </div>
                                </div>

                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <#--<li class="m-portlet__nav-item">
                                            <a id="runBtn"
                                               class="m-portlet__nav-link btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--pill"
                                               data-toggle="m-tooltip" data-placement="top" title=""
                                               data-original-title="Run script" style="color:white">
                                                <i class="la la-play"></i>
                                            </a>
                                        </li>-->

                                        <li class="m-portlet__nav-item">
                                            <a id="runBtn" class="m-portlet__nav-link btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Run script" style="color:white; display: none">
                                                <i class="la la-play"></i>
                                            </a>
                                        </li>
                                        <li class="m-portlet__nav-item">
                                            <a m-portlet-tool="fullscreen"
                                               class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill">
                                                <i class="la la-expand"></i>
                                            </a>
                                        </li>

                                        <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                            m-dropdown-toggle="hover" aria-expanded="true">
                                            <a href="#"
                                               class="m-portlet__nav-link btn btn-secondary  m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill   m-dropdown__toggle">
                                                <i class="la la-ellipsis-v"></i>
                                            </a>
                                            <div class="m-dropdown__wrapper" style="z-index: 101;">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"
                                                  style="left: auto; right: 21.5px;"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">

                                                            <ul class="m-nav">
                                                                <li class="m-nav__section m-nav__section--first">
                                                                    <span class="m-nav__section-text">Actions</span>
                                                                </li>

                                                                <li class="m-nav__item">
                                                                    <a class="m-nav__link"
                                                                       data-toggle="modal" data-target="#loadExampleModal">
                                                                        <i class="m-nav__link-icon la la-star-o"></i>
                                                                        <span class="m-nav__link-text">Open example</span>
                                                                    </a>
                                                                </li>

                                                                <li class="m-nav__separator m-nav__separator--fit">
                                                                <li class="m-nav__item">
                                                                    <a id="newScriptBtn" class="m-nav__link">
                                                                        <i class="m-nav__link-icon la la-file"></i>
                                                                        <span class="m-nav__link-text">New script</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a id="openScriptBtn" class="m-nav__link"
                                                                       data-toggle="modal" data-target="#openCssModal">
                                                                        <i class="m-nav__link-icon la la-folder-open-o"></i>
                                                                        <span class="m-nav__link-text">Open script</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a id="openVersionBtn" class="m-nav__link"
                                                                       data-toggle="modal" data-target="#openVersionModal">
                                                                        <i class="m-nav__link-icon la la-history"></i>
                                                                        <span class="m-nav__link-text">Open previous version</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a id="saveScriptBtn" class="m-nav__link"
                                                                       data-toggle="modal" data-target="#saveCssModal">
                                                                        <i class="m-nav__link-icon la la-floppy-o"></i>
                                                                        <span class="m-nav__link-text">Save script</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a id="downloadScriptBtn" class="m-nav__link">
                                                                        <i class="m-nav__link-icon la la-download"></i>
                                                                        <span class="m-nav__link-text">Download script</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__separator m-nav__separator--fit">
                                                            <li class="m-nav__item">
                                                                <a id="emoji-picker" class="m-nav__link">
                                                                    <i class="m-nav__link-icon la la-smile-o"></i>
                                                                    <span class="m-nav__link-text">Emoticon</span>
                                                                </a>
                                                            </li>
                                                                <li class="m-nav__item">
                                                                    <a id="normalizeScriptBtn" class="m-nav__link">
                                                                        <i class="m-nav__link-icon la la-magic"></i>
                                                                        <span class="m-nav__link-text">Normalize script</span>
                                                                    </a>
                                                                </li>

                                                                <li class="m-nav__separator m-nav__separator--fit">
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#"
                                                                       class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="m-portlet__body" style="padding: 0; height: calc(100% - 66px);">
                                <textarea id="scriptEditor" style="display: none;"></textarea>
                            </div>
                        <div class="intercom-composer-popover intercom-composer-emoji-popover"><div class="intercom-emoji-picker"><div class="intercom-composer-popover-header"><input class="intercom-composer-popover-input" placeholder="Search" value=""></div><div class="intercom-composer-popover-body-container"><div class="intercom-composer-popover-body"><div class="intercom-emoji-picker-groups"><div class="intercom-emoji-picker-group"><div class="intercom-emoji-picker-group-title">Frequently used</div><span class="intercom-emoji-picker-emoji" title="thumbs_up">👍</span><span class="intercom-emoji-picker-emoji" title="-1">👎</span><span class="intercom-emoji-picker-emoji" title="sob">😭</span><span class="intercom-emoji-picker-emoji" title="confused">😕</span><span class="intercom-emoji-picker-emoji" title="neutral_face">😐</span><span class="intercom-emoji-picker-emoji" title="blush">😊</span><span class="intercom-emoji-picker-emoji" title="heart_eyes">😍</span></div><div class="intercom-emoji-picker-group"><div class="intercom-emoji-picker-group-title">People</div><span class="intercom-emoji-picker-emoji" title="smile">😄</span><span class="intercom-emoji-picker-emoji" title="smiley">😃</span><span class="intercom-emoji-picker-emoji" title="grinning">😀</span><span class="intercom-emoji-picker-emoji" title="blush">😊</span><span class="intercom-emoji-picker-emoji" title="wink">😉</span><span class="intercom-emoji-picker-emoji" title="heart_eyes">😍</span><span class="intercom-emoji-picker-emoji" title="kissing_heart">😘</span><span class="intercom-emoji-picker-emoji" title="kissing_closed_eyes">😚</span><span class="intercom-emoji-picker-emoji" title="kissing">😗</span><span class="intercom-emoji-picker-emoji" title="kissing_smiling_eyes">😙</span><span class="intercom-emoji-picker-emoji" title="stuck_out_tongue_winking_eye">😜</span><span class="intercom-emoji-picker-emoji" title="stuck_out_tongue_closed_eyes">😝</span><span class="intercom-emoji-picker-emoji" title="stuck_out_tongue">😛</span><span class="intercom-emoji-picker-emoji" title="flushed">😳</span><span class="intercom-emoji-picker-emoji" title="grin">😁</span><span class="intercom-emoji-picker-emoji" title="pensive">😔</span><span class="intercom-emoji-picker-emoji" title="relieved">😌</span><span class="intercom-emoji-picker-emoji" title="unamused">😒</span><span class="intercom-emoji-picker-emoji" title="disappointed">😞</span><span class="intercom-emoji-picker-emoji" title="persevere">😣</span><span class="intercom-emoji-picker-emoji" title="cry">😢</span><span class="intercom-emoji-picker-emoji" title="joy">😂</span><span class="intercom-emoji-picker-emoji" title="sob">😭</span><span class="intercom-emoji-picker-emoji" title="sleepy">😪</span><span class="intercom-emoji-picker-emoji" title="disappointed_relieved">😥</span><span class="intercom-emoji-picker-emoji" title="cold_sweat">😰</span><span class="intercom-emoji-picker-emoji" title="sweat_smile">😅</span><span class="intercom-emoji-picker-emoji" title="sweat">😓</span><span class="intercom-emoji-picker-emoji" title="weary">😩</span><span class="intercom-emoji-picker-emoji" title="tired_face">😫</span><span class="intercom-emoji-picker-emoji" title="fearful">😨</span><span class="intercom-emoji-picker-emoji" title="scream">😱</span><span class="intercom-emoji-picker-emoji" title="angry">😠</span><span class="intercom-emoji-picker-emoji" title="rage">😡</span><span class="intercom-emoji-picker-emoji" title="triumph">😤</span><span class="intercom-emoji-picker-emoji" title="confounded">😖</span><span class="intercom-emoji-picker-emoji" title="laughing">😆</span><span class="intercom-emoji-picker-emoji" title="yum">😋</span><span class="intercom-emoji-picker-emoji" title="mask">😷</span><span class="intercom-emoji-picker-emoji" title="sunglasses">😎</span><span class="intercom-emoji-picker-emoji" title="sleeping">😴</span><span class="intercom-emoji-picker-emoji" title="dizzy_face">😵</span><span class="intercom-emoji-picker-emoji" title="astonished">😲</span><span class="intercom-emoji-picker-emoji" title="worried">😟</span><span class="intercom-emoji-picker-emoji" title="frowning">😦</span><span class="intercom-emoji-picker-emoji" title="anguished">😧</span><span class="intercom-emoji-picker-emoji" title="imp">👿</span><span class="intercom-emoji-picker-emoji" title="open_mouth">😮</span><span class="intercom-emoji-picker-emoji" title="grimacing">😬</span><span class="intercom-emoji-picker-emoji" title="neutral_face">😐</span><span class="intercom-emoji-picker-emoji" title="confused">😕</span><span class="intercom-emoji-picker-emoji" title="hushed">😯</span><span class="intercom-emoji-picker-emoji" title="smirk">😏</span><span class="intercom-emoji-picker-emoji" title="expressionless">😑</span><span class="intercom-emoji-picker-emoji" title="man_with_gua_pi_mao">👲</span><span class="intercom-emoji-picker-emoji" title="man_with_turban">👳</span><span class="intercom-emoji-picker-emoji" title="cop">👮</span><span class="intercom-emoji-picker-emoji" title="construction_worker">👷</span><span class="intercom-emoji-picker-emoji" title="guardsman">💂</span><span class="intercom-emoji-picker-emoji" title="baby">👶</span><span class="intercom-emoji-picker-emoji" title="boy">👦</span><span class="intercom-emoji-picker-emoji" title="girl">👧</span><span class="intercom-emoji-picker-emoji" title="man">👨</span><span class="intercom-emoji-picker-emoji" title="woman">👩</span><span class="intercom-emoji-picker-emoji" title="older_man">👴</span><span class="intercom-emoji-picker-emoji" title="older_woman">👵</span><span class="intercom-emoji-picker-emoji" title="person_with_blond_hair">👱</span><span class="intercom-emoji-picker-emoji" title="angel">👼</span><span class="intercom-emoji-picker-emoji" title="princess">👸</span><span class="intercom-emoji-picker-emoji" title="smiley_cat">😺</span><span class="intercom-emoji-picker-emoji" title="smile_cat">😸</span><span class="intercom-emoji-picker-emoji" title="heart_eyes_cat">😻</span><span class="intercom-emoji-picker-emoji" title="kissing_cat">😽</span><span class="intercom-emoji-picker-emoji" title="smirk_cat">😼</span><span class="intercom-emoji-picker-emoji" title="scream_cat">🙀</span><span class="intercom-emoji-picker-emoji" title="crying_cat_face">😿</span><span class="intercom-emoji-picker-emoji" title="joy_cat">😹</span><span class="intercom-emoji-picker-emoji" title="pouting_cat">😾</span><span class="intercom-emoji-picker-emoji" title="japanese_ogre">👹</span><span class="intercom-emoji-picker-emoji" title="japanese_goblin">👺</span><span class="intercom-emoji-picker-emoji" title="see_no_evil">🙈</span><span class="intercom-emoji-picker-emoji" title="hear_no_evil">🙉</span><span class="intercom-emoji-picker-emoji" title="speak_no_evil">🙊</span><span class="intercom-emoji-picker-emoji" title="skull">💀</span><span class="intercom-emoji-picker-emoji" title="alien">👽</span><span class="intercom-emoji-picker-emoji" title="hankey">💩</span><span class="intercom-emoji-picker-emoji" title="fire">🔥</span><span class="intercom-emoji-picker-emoji" title="sparkles">✨</span><span class="intercom-emoji-picker-emoji" title="star2">🌟</span><span class="intercom-emoji-picker-emoji" title="dizzy">💫</span><span class="intercom-emoji-picker-emoji" title="boom">💥</span><span class="intercom-emoji-picker-emoji" title="anger">💢</span><span class="intercom-emoji-picker-emoji" title="sweat_drops">💦</span><span class="intercom-emoji-picker-emoji" title="droplet">💧</span><span class="intercom-emoji-picker-emoji" title="zzz">💤</span><span class="intercom-emoji-picker-emoji" title="dash">💨</span><span class="intercom-emoji-picker-emoji" title="ear">👂</span><span class="intercom-emoji-picker-emoji" title="eyes">👀</span><span class="intercom-emoji-picker-emoji" title="nose">👃</span><span class="intercom-emoji-picker-emoji" title="tongue">👅</span><span class="intercom-emoji-picker-emoji" title="lips">👄</span><span class="intercom-emoji-picker-emoji" title="thumbs_up">👍</span><span class="intercom-emoji-picker-emoji" title="-1">👎</span><span class="intercom-emoji-picker-emoji" title="ok_hand">👌</span><span class="intercom-emoji-picker-emoji" title="facepunch">👊</span><span class="intercom-emoji-picker-emoji" title="fist">✊</span><span class="intercom-emoji-picker-emoji" title="wave">👋</span><span class="intercom-emoji-picker-emoji" title="hand">✋</span><span class="intercom-emoji-picker-emoji" title="open_hands">👐</span><span class="intercom-emoji-picker-emoji" title="point_up_2">👆</span><span class="intercom-emoji-picker-emoji" title="point_down">👇</span><span class="intercom-emoji-picker-emoji" title="point_right">👉</span><span class="intercom-emoji-picker-emoji" title="point_left">👈</span><span class="intercom-emoji-picker-emoji" title="raised_hands">🙌</span><span class="intercom-emoji-picker-emoji" title="pray">🙏</span><span class="intercom-emoji-picker-emoji" title="clap">👏</span><span class="intercom-emoji-picker-emoji" title="muscle">💪</span><span class="intercom-emoji-picker-emoji" title="walking">🚶</span><span class="intercom-emoji-picker-emoji" title="runner">🏃</span><span class="intercom-emoji-picker-emoji" title="dancer">💃</span><span class="intercom-emoji-picker-emoji" title="couple">👫</span><span class="intercom-emoji-picker-emoji" title="family">👪</span><span class="intercom-emoji-picker-emoji" title="couplekiss">💏</span><span class="intercom-emoji-picker-emoji" title="couple_with_heart">💑</span><span class="intercom-emoji-picker-emoji" title="dancers">👯</span><span class="intercom-emoji-picker-emoji" title="ok_woman">🙆</span><span class="intercom-emoji-picker-emoji" title="no_good">🙅</span><span class="intercom-emoji-picker-emoji" title="information_desk_person">💁</span><span class="intercom-emoji-picker-emoji" title="raising_hand">🙋</span><span class="intercom-emoji-picker-emoji" title="massage">💆</span><span class="intercom-emoji-picker-emoji" title="haircut">💇</span><span class="intercom-emoji-picker-emoji" title="nail_care">💅</span><span class="intercom-emoji-picker-emoji" title="bride_with_veil">👰</span><span class="intercom-emoji-picker-emoji" title="person_with_pouting_face">🙎</span><span class="intercom-emoji-picker-emoji" title="person_frowning">🙍</span><span class="intercom-emoji-picker-emoji" title="bow">🙇</span><span class="intercom-emoji-picker-emoji" title="tophat">🎩</span><span class="intercom-emoji-picker-emoji" title="crown">👑</span><span class="intercom-emoji-picker-emoji" title="womans_hat">👒</span><span class="intercom-emoji-picker-emoji" title="athletic_shoe">👟</span><span class="intercom-emoji-picker-emoji" title="mans_shoe">👞</span><span class="intercom-emoji-picker-emoji" title="sandal">👡</span><span class="intercom-emoji-picker-emoji" title="high_heel">👠</span><span class="intercom-emoji-picker-emoji" title="boot">👢</span><span class="intercom-emoji-picker-emoji" title="shirt">👕</span><span class="intercom-emoji-picker-emoji" title="necktie">👔</span><span class="intercom-emoji-picker-emoji" title="womans_clothes">👚</span><span class="intercom-emoji-picker-emoji" title="dress">👗</span><span class="intercom-emoji-picker-emoji" title="running_shirt_with_sash">🎽</span><span class="intercom-emoji-picker-emoji" title="jeans">👖</span><span class="intercom-emoji-picker-emoji" title="kimono">👘</span><span class="intercom-emoji-picker-emoji" title="bikini">👙</span><span class="intercom-emoji-picker-emoji" title="briefcase">💼</span><span class="intercom-emoji-picker-emoji" title="handbag">👜</span><span class="intercom-emoji-picker-emoji" title="pouch">👝</span><span class="intercom-emoji-picker-emoji" title="purse">👛</span><span class="intercom-emoji-picker-emoji" title="eyeglasses">👓</span><span class="intercom-emoji-picker-emoji" title="ribbon">🎀</span><span class="intercom-emoji-picker-emoji" title="closed_umbrella">🌂</span><span class="intercom-emoji-picker-emoji" title="lipstick">💄</span><span class="intercom-emoji-picker-emoji" title="yellow_heart">💛</span><span class="intercom-emoji-picker-emoji" title="blue_heart">💙</span><span class="intercom-emoji-picker-emoji" title="purple_heart">💜</span><span class="intercom-emoji-picker-emoji" title="green_heart">💚</span><span class="intercom-emoji-picker-emoji" title="broken_heart">💔</span><span class="intercom-emoji-picker-emoji" title="heartpulse">💗</span><span class="intercom-emoji-picker-emoji" title="heartbeat">💓</span><span class="intercom-emoji-picker-emoji" title="two_hearts">💕</span><span class="intercom-emoji-picker-emoji" title="sparkling_heart">💖</span><span class="intercom-emoji-picker-emoji" title="revolving_hearts">💞</span><span class="intercom-emoji-picker-emoji" title="cupid">💘</span><span class="intercom-emoji-picker-emoji" title="love_letter">💌</span><span class="intercom-emoji-picker-emoji" title="kiss">💋</span><span class="intercom-emoji-picker-emoji" title="ring">💍</span><span class="intercom-emoji-picker-emoji" title="gem">💎</span><span class="intercom-emoji-picker-emoji" title="bust_in_silhouette">👤</span><span class="intercom-emoji-picker-emoji" title="speech_balloon">💬</span><span class="intercom-emoji-picker-emoji" title="footprints">👣</span></div><div class="intercom-emoji-picker-group"><div class="intercom-emoji-picker-group-title">Nature</div><span class="intercom-emoji-picker-emoji" title="dog">🐶</span><span class="intercom-emoji-picker-emoji" title="wolf">🐺</span><span class="intercom-emoji-picker-emoji" title="cat">🐱</span><span class="intercom-emoji-picker-emoji" title="mouse">🐭</span><span class="intercom-emoji-picker-emoji" title="hamster">🐹</span><span class="intercom-emoji-picker-emoji" title="rabbit">🐰</span><span class="intercom-emoji-picker-emoji" title="frog">🐸</span><span class="intercom-emoji-picker-emoji" title="tiger">🐯</span><span class="intercom-emoji-picker-emoji" title="koala">🐨</span><span class="intercom-emoji-picker-emoji" title="bear">🐻</span><span class="intercom-emoji-picker-emoji" title="pig">🐷</span><span class="intercom-emoji-picker-emoji" title="pig_nose">🐽</span><span class="intercom-emoji-picker-emoji" title="cow">🐮</span><span class="intercom-emoji-picker-emoji" title="boar">🐗</span><span class="intercom-emoji-picker-emoji" title="monkey_face">🐵</span><span class="intercom-emoji-picker-emoji" title="monkey">🐒</span><span class="intercom-emoji-picker-emoji" title="horse">🐴</span><span class="intercom-emoji-picker-emoji" title="sheep">🐑</span><span class="intercom-emoji-picker-emoji" title="elephant">🐘</span><span class="intercom-emoji-picker-emoji" title="panda_face">🐼</span><span class="intercom-emoji-picker-emoji" title="penguin">🐧</span><span class="intercom-emoji-picker-emoji" title="bird">🐦</span><span class="intercom-emoji-picker-emoji" title="baby_chick">🐤</span><span class="intercom-emoji-picker-emoji" title="hatched_chick">🐥</span><span class="intercom-emoji-picker-emoji" title="hatching_chick">🐣</span><span class="intercom-emoji-picker-emoji" title="chicken">🐔</span><span class="intercom-emoji-picker-emoji" title="snake">🐍</span><span class="intercom-emoji-picker-emoji" title="turtle">🐢</span><span class="intercom-emoji-picker-emoji" title="bug">🐛</span><span class="intercom-emoji-picker-emoji" title="bee">🐝</span><span class="intercom-emoji-picker-emoji" title="ant">🐜</span><span class="intercom-emoji-picker-emoji" title="beetle">🐞</span><span class="intercom-emoji-picker-emoji" title="snail">🐌</span><span class="intercom-emoji-picker-emoji" title="octopus">🐙</span><span class="intercom-emoji-picker-emoji" title="shell">🐚</span><span class="intercom-emoji-picker-emoji" title="tropical_fish">🐠</span><span class="intercom-emoji-picker-emoji" title="fish">🐟</span><span class="intercom-emoji-picker-emoji" title="dolphin">🐬</span><span class="intercom-emoji-picker-emoji" title="whale">🐳</span><span class="intercom-emoji-picker-emoji" title="racehorse">🐎</span><span class="intercom-emoji-picker-emoji" title="dragon_face">🐲</span><span class="intercom-emoji-picker-emoji" title="blowfish">🐡</span><span class="intercom-emoji-picker-emoji" title="camel">🐫</span><span class="intercom-emoji-picker-emoji" title="poodle">🐩</span><span class="intercom-emoji-picker-emoji" title="feet">🐾</span><span class="intercom-emoji-picker-emoji" title="bouquet">💐</span><span class="intercom-emoji-picker-emoji" title="cherry_blossom">🌸</span><span class="intercom-emoji-picker-emoji" title="tulip">🌷</span><span class="intercom-emoji-picker-emoji" title="four_leaf_clover">🍀</span><span class="intercom-emoji-picker-emoji" title="rose">🌹</span><span class="intercom-emoji-picker-emoji" title="sunflower">🌻</span><span class="intercom-emoji-picker-emoji" title="hibiscus">🌺</span><span class="intercom-emoji-picker-emoji" title="maple_leaf">🍁</span><span class="intercom-emoji-picker-emoji" title="leaves">🍃</span><span class="intercom-emoji-picker-emoji" title="fallen_leaf">🍂</span><span class="intercom-emoji-picker-emoji" title="herb">🌿</span><span class="intercom-emoji-picker-emoji" title="ear_of_rice">🌾</span><span class="intercom-emoji-picker-emoji" title="mushroom">🍄</span><span class="intercom-emoji-picker-emoji" title="cactus">🌵</span><span class="intercom-emoji-picker-emoji" title="palm_tree">🌴</span><span class="intercom-emoji-picker-emoji" title="chestnut">🌰</span><span class="intercom-emoji-picker-emoji" title="seedling">🌱</span><span class="intercom-emoji-picker-emoji" title="blossom">🌼</span><span class="intercom-emoji-picker-emoji" title="new_moon">🌑</span><span class="intercom-emoji-picker-emoji" title="first_quarter_moon">🌓</span><span class="intercom-emoji-picker-emoji" title="moon">🌔</span><span class="intercom-emoji-picker-emoji" title="full_moon">🌕</span><span class="intercom-emoji-picker-emoji" title="first_quarter_moon_with_face">🌛</span><span class="intercom-emoji-picker-emoji" title="crescent_moon">🌙</span><span class="intercom-emoji-picker-emoji" title="earth_asia">🌏</span><span class="intercom-emoji-picker-emoji" title="volcano">🌋</span><span class="intercom-emoji-picker-emoji" title="milky_way">🌌</span><span class="intercom-emoji-picker-emoji" title="stars">🌠</span><span class="intercom-emoji-picker-emoji" title="partly_sunny">⛅</span><span class="intercom-emoji-picker-emoji" title="snowman">⛄</span><span class="intercom-emoji-picker-emoji" title="cyclone">🌀</span><span class="intercom-emoji-picker-emoji" title="foggy">🌁</span><span class="intercom-emoji-picker-emoji" title="rainbow">🌈</span><span class="intercom-emoji-picker-emoji" title="ocean">🌊</span></div><div class="intercom-emoji-picker-group"><div class="intercom-emoji-picker-group-title">Objects</div><span class="intercom-emoji-picker-emoji" title="bamboo">🎍</span><span class="intercom-emoji-picker-emoji" title="gift_heart">💝</span><span class="intercom-emoji-picker-emoji" title="dolls">🎎</span><span class="intercom-emoji-picker-emoji" title="school_satchel">🎒</span><span class="intercom-emoji-picker-emoji" title="mortar_board">🎓</span><span class="intercom-emoji-picker-emoji" title="flags">🎏</span><span class="intercom-emoji-picker-emoji" title="fireworks">🎆</span><span class="intercom-emoji-picker-emoji" title="sparkler">🎇</span><span class="intercom-emoji-picker-emoji" title="wind_chime">🎐</span><span class="intercom-emoji-picker-emoji" title="rice_scene">🎑</span><span class="intercom-emoji-picker-emoji" title="jack_o_lantern">🎃</span><span class="intercom-emoji-picker-emoji" title="ghost">👻</span><span class="intercom-emoji-picker-emoji" title="santa">🎅</span><span class="intercom-emoji-picker-emoji" title="christmas_tree">🎄</span><span class="intercom-emoji-picker-emoji" title="gift">🎁</span><span class="intercom-emoji-picker-emoji" title="tanabata_tree">🎋</span><span class="intercom-emoji-picker-emoji" title="tada">🎉</span><span class="intercom-emoji-picker-emoji" title="confetti_ball">🎊</span><span class="intercom-emoji-picker-emoji" title="balloon">🎈</span><span class="intercom-emoji-picker-emoji" title="crossed_flags">🎌</span><span class="intercom-emoji-picker-emoji" title="crystal_ball">🔮</span><span class="intercom-emoji-picker-emoji" title="movie_camera">🎥</span><span class="intercom-emoji-picker-emoji" title="camera">📷</span><span class="intercom-emoji-picker-emoji" title="video_camera">📹</span><span class="intercom-emoji-picker-emoji" title="vhs">📼</span><span class="intercom-emoji-picker-emoji" title="cd">💿</span><span class="intercom-emoji-picker-emoji" title="dvd">📀</span><span class="intercom-emoji-picker-emoji" title="minidisc">💽</span><span class="intercom-emoji-picker-emoji" title="floppy_disk">💾</span><span class="intercom-emoji-picker-emoji" title="computer">💻</span><span class="intercom-emoji-picker-emoji" title="iphone">📱</span><span class="intercom-emoji-picker-emoji" title="telephone_receiver">📞</span><span class="intercom-emoji-picker-emoji" title="pager">📟</span><span class="intercom-emoji-picker-emoji" title="fax">📠</span><span class="intercom-emoji-picker-emoji" title="satellite">📡</span><span class="intercom-emoji-picker-emoji" title="tv">📺</span><span class="intercom-emoji-picker-emoji" title="radio">📻</span><span class="intercom-emoji-picker-emoji" title="loud_sound">🔊</span><span class="intercom-emoji-picker-emoji" title="bell">🔔</span><span class="intercom-emoji-picker-emoji" title="loudspeaker">📢</span><span class="intercom-emoji-picker-emoji" title="mega">📣</span><span class="intercom-emoji-picker-emoji" title="hourglass_flowing_sand">⏳</span><span class="intercom-emoji-picker-emoji" title="hourglass">⌛</span><span class="intercom-emoji-picker-emoji" title="alarm_clock">⏰</span><span class="intercom-emoji-picker-emoji" title="watch">⌚</span><span class="intercom-emoji-picker-emoji" title="unlock">🔓</span><span class="intercom-emoji-picker-emoji" title="lock">🔒</span><span class="intercom-emoji-picker-emoji" title="lock_with_ink_pen">🔏</span><span class="intercom-emoji-picker-emoji" title="closed_lock_with_key">🔐</span><span class="intercom-emoji-picker-emoji" title="key">🔑</span><span class="intercom-emoji-picker-emoji" title="mag_right">🔎</span><span class="intercom-emoji-picker-emoji" title="bulb">💡</span><span class="intercom-emoji-picker-emoji" title="flashlight">🔦</span><span class="intercom-emoji-picker-emoji" title="electric_plug">🔌</span><span class="intercom-emoji-picker-emoji" title="battery">🔋</span><span class="intercom-emoji-picker-emoji" title="mag">🔍</span><span class="intercom-emoji-picker-emoji" title="bath">🛀</span><span class="intercom-emoji-picker-emoji" title="toilet">🚽</span><span class="intercom-emoji-picker-emoji" title="wrench">🔧</span><span class="intercom-emoji-picker-emoji" title="nut_and_bolt">🔩</span><span class="intercom-emoji-picker-emoji" title="hammer">🔨</span><span class="intercom-emoji-picker-emoji" title="door">🚪</span><span class="intercom-emoji-picker-emoji" title="smoking">🚬</span><span class="intercom-emoji-picker-emoji" title="bomb">💣</span><span class="intercom-emoji-picker-emoji" title="gun">🔫</span><span class="intercom-emoji-picker-emoji" title="hocho">🔪</span><span class="intercom-emoji-picker-emoji" title="pill">💊</span><span class="intercom-emoji-picker-emoji" title="syringe">💉</span><span class="intercom-emoji-picker-emoji" title="moneybag">💰</span><span class="intercom-emoji-picker-emoji" title="yen">💴</span><span class="intercom-emoji-picker-emoji" title="dollar">💵</span><span class="intercom-emoji-picker-emoji" title="credit_card">💳</span><span class="intercom-emoji-picker-emoji" title="money_with_wings">💸</span><span class="intercom-emoji-picker-emoji" title="calling">📲</span><span class="intercom-emoji-picker-emoji" title="e-mail">📧</span><span class="intercom-emoji-picker-emoji" title="inbox_tray">📥</span><span class="intercom-emoji-picker-emoji" title="outbox_tray">📤</span><span class="intercom-emoji-picker-emoji" title="envelope_with_arrow">📩</span><span class="intercom-emoji-picker-emoji" title="incoming_envelope">📨</span><span class="intercom-emoji-picker-emoji" title="mailbox">📫</span><span class="intercom-emoji-picker-emoji" title="mailbox_closed">📪</span><span class="intercom-emoji-picker-emoji" title="postbox">📮</span><span class="intercom-emoji-picker-emoji" title="package">📦</span><span class="intercom-emoji-picker-emoji" title="memo">📝</span><span class="intercom-emoji-picker-emoji" title="page_facing_up">📄</span><span class="intercom-emoji-picker-emoji" title="page_with_curl">📃</span><span class="intercom-emoji-picker-emoji" title="bookmark_tabs">📑</span><span class="intercom-emoji-picker-emoji" title="bar_chart">📊</span><span class="intercom-emoji-picker-emoji" title="chart_with_upwards_trend">📈</span><span class="intercom-emoji-picker-emoji" title="chart_with_downwards_trend">📉</span><span class="intercom-emoji-picker-emoji" title="scroll">📜</span><span class="intercom-emoji-picker-emoji" title="clipboard">📋</span><span class="intercom-emoji-picker-emoji" title="date">📅</span><span class="intercom-emoji-picker-emoji" title="calendar">📆</span><span class="intercom-emoji-picker-emoji" title="card_index">📇</span><span class="intercom-emoji-picker-emoji" title="file_folder">📁</span><span class="intercom-emoji-picker-emoji" title="open_file_folder">📂</span><span class="intercom-emoji-picker-emoji" title="pushpin">📌</span><span class="intercom-emoji-picker-emoji" title="paperclip">📎</span><span class="intercom-emoji-picker-emoji" title="straight_ruler">📏</span><span class="intercom-emoji-picker-emoji" title="triangular_ruler">📐</span><span class="intercom-emoji-picker-emoji" title="closed_book">📕</span><span class="intercom-emoji-picker-emoji" title="green_book">📗</span><span class="intercom-emoji-picker-emoji" title="blue_book">📘</span><span class="intercom-emoji-picker-emoji" title="orange_book">📙</span><span class="intercom-emoji-picker-emoji" title="notebook">📓</span><span class="intercom-emoji-picker-emoji" title="notebook_with_decorative_cover">📔</span><span class="intercom-emoji-picker-emoji" title="ledger">📒</span><span class="intercom-emoji-picker-emoji" title="books">📚</span><span class="intercom-emoji-picker-emoji" title="book">📖</span><span class="intercom-emoji-picker-emoji" title="bookmark">🔖</span><span class="intercom-emoji-picker-emoji" title="name_badge">📛</span><span class="intercom-emoji-picker-emoji" title="newspaper">📰</span><span class="intercom-emoji-picker-emoji" title="art">🎨</span><span class="intercom-emoji-picker-emoji" title="clapper">🎬</span><span class="intercom-emoji-picker-emoji" title="microphone">🎤</span><span class="intercom-emoji-picker-emoji" title="headphones">🎧</span><span class="intercom-emoji-picker-emoji" title="musical_score">🎼</span><span class="intercom-emoji-picker-emoji" title="musical_note">🎵</span><span class="intercom-emoji-picker-emoji" title="notes">🎶</span><span class="intercom-emoji-picker-emoji" title="musical_keyboard">🎹</span><span class="intercom-emoji-picker-emoji" title="violin">🎻</span><span class="intercom-emoji-picker-emoji" title="trumpet">🎺</span><span class="intercom-emoji-picker-emoji" title="saxophone">🎷</span><span class="intercom-emoji-picker-emoji" title="guitar">🎸</span><span class="intercom-emoji-picker-emoji" title="space_invader">👾</span><span class="intercom-emoji-picker-emoji" title="video_game">🎮</span><span class="intercom-emoji-picker-emoji" title="black_joker">🃏</span><span class="intercom-emoji-picker-emoji" title="flower_playing_cards">🎴</span><span class="intercom-emoji-picker-emoji" title="mahjong">🀄</span><span class="intercom-emoji-picker-emoji" title="game_die">🎲</span><span class="intercom-emoji-picker-emoji" title="dart">🎯</span><span class="intercom-emoji-picker-emoji" title="football">🏈</span><span class="intercom-emoji-picker-emoji" title="basketball">🏀</span><span class="intercom-emoji-picker-emoji" title="soccer">⚽</span><span class="intercom-emoji-picker-emoji" title="baseball">⚾</span><span class="intercom-emoji-picker-emoji" title="tennis">🎾</span><span class="intercom-emoji-picker-emoji" title="8ball">🎱</span><span class="intercom-emoji-picker-emoji" title="bowling">🎳</span><span class="intercom-emoji-picker-emoji" title="golf">⛳</span><span class="intercom-emoji-picker-emoji" title="checkered_flag">🏁</span><span class="intercom-emoji-picker-emoji" title="trophy">🏆</span><span class="intercom-emoji-picker-emoji" title="ski">🎿</span><span class="intercom-emoji-picker-emoji" title="snowboarder">🏂</span><span class="intercom-emoji-picker-emoji" title="swimmer">🏊</span><span class="intercom-emoji-picker-emoji" title="surfer">🏄</span><span class="intercom-emoji-picker-emoji" title="fishing_pole_and_fish">🎣</span><span class="intercom-emoji-picker-emoji" title="tea">🍵</span><span class="intercom-emoji-picker-emoji" title="sake">🍶</span><span class="intercom-emoji-picker-emoji" title="beer">🍺</span><span class="intercom-emoji-picker-emoji" title="beers">🍻</span><span class="intercom-emoji-picker-emoji" title="cocktail">🍸</span><span class="intercom-emoji-picker-emoji" title="tropical_drink">🍹</span><span class="intercom-emoji-picker-emoji" title="wine_glass">🍷</span><span class="intercom-emoji-picker-emoji" title="fork_and_knife">🍴</span><span class="intercom-emoji-picker-emoji" title="pizza">🍕</span><span class="intercom-emoji-picker-emoji" title="hamburger">🍔</span><span class="intercom-emoji-picker-emoji" title="fries">🍟</span><span class="intercom-emoji-picker-emoji" title="poultry_leg">🍗</span><span class="intercom-emoji-picker-emoji" title="meat_on_bone">🍖</span><span class="intercom-emoji-picker-emoji" title="spaghetti">🍝</span><span class="intercom-emoji-picker-emoji" title="curry">🍛</span><span class="intercom-emoji-picker-emoji" title="fried_shrimp">🍤</span><span class="intercom-emoji-picker-emoji" title="bento">🍱</span><span class="intercom-emoji-picker-emoji" title="sushi">🍣</span><span class="intercom-emoji-picker-emoji" title="fish_cake">🍥</span><span class="intercom-emoji-picker-emoji" title="rice_ball">🍙</span><span class="intercom-emoji-picker-emoji" title="rice_cracker">🍘</span><span class="intercom-emoji-picker-emoji" title="rice">🍚</span><span class="intercom-emoji-picker-emoji" title="ramen">🍜</span><span class="intercom-emoji-picker-emoji" title="stew">🍲</span><span class="intercom-emoji-picker-emoji" title="oden">🍢</span><span class="intercom-emoji-picker-emoji" title="dango">🍡</span><span class="intercom-emoji-picker-emoji" title="egg">🍳</span><span class="intercom-emoji-picker-emoji" title="bread">🍞</span><span class="intercom-emoji-picker-emoji" title="doughnut">🍩</span><span class="intercom-emoji-picker-emoji" title="custard">🍮</span><span class="intercom-emoji-picker-emoji" title="icecream">🍦</span><span class="intercom-emoji-picker-emoji" title="ice_cream">🍨</span><span class="intercom-emoji-picker-emoji" title="shaved_ice">🍧</span><span class="intercom-emoji-picker-emoji" title="birthday">🎂</span><span class="intercom-emoji-picker-emoji" title="cake">🍰</span><span class="intercom-emoji-picker-emoji" title="cookie">🍪</span><span class="intercom-emoji-picker-emoji" title="chocolate_bar">🍫</span><span class="intercom-emoji-picker-emoji" title="candy">🍬</span><span class="intercom-emoji-picker-emoji" title="lollipop">🍭</span><span class="intercom-emoji-picker-emoji" title="honey_pot">🍯</span><span class="intercom-emoji-picker-emoji" title="apple">🍎</span><span class="intercom-emoji-picker-emoji" title="green_apple">🍏</span><span class="intercom-emoji-picker-emoji" title="tangerine">🍊</span><span class="intercom-emoji-picker-emoji" title="cherries">🍒</span><span class="intercom-emoji-picker-emoji" title="grapes">🍇</span><span class="intercom-emoji-picker-emoji" title="watermelon">🍉</span><span class="intercom-emoji-picker-emoji" title="strawberry">🍓</span><span class="intercom-emoji-picker-emoji" title="peach">🍑</span><span class="intercom-emoji-picker-emoji" title="melon">🍈</span><span class="intercom-emoji-picker-emoji" title="banana">🍌</span><span class="intercom-emoji-picker-emoji" title="pineapple">🍍</span><span class="intercom-emoji-picker-emoji" title="sweet_potato">🍠</span><span class="intercom-emoji-picker-emoji" title="eggplant">🍆</span><span class="intercom-emoji-picker-emoji" title="tomato">🍅</span><span class="intercom-emoji-picker-emoji" title="corn">🌽</span></div><div class="intercom-emoji-picker-group"><div class="intercom-emoji-picker-group-title">Places</div><span class="intercom-emoji-picker-emoji" title="house">🏠</span><span class="intercom-emoji-picker-emoji" title="house_with_garden">🏡</span><span class="intercom-emoji-picker-emoji" title="school">🏫</span><span class="intercom-emoji-picker-emoji" title="office">🏢</span><span class="intercom-emoji-picker-emoji" title="post_office">🏣</span><span class="intercom-emoji-picker-emoji" title="hospital">🏥</span><span class="intercom-emoji-picker-emoji" title="bank">🏦</span><span class="intercom-emoji-picker-emoji" title="convenience_store">🏪</span><span class="intercom-emoji-picker-emoji" title="love_hotel">🏩</span><span class="intercom-emoji-picker-emoji" title="hotel">🏨</span><span class="intercom-emoji-picker-emoji" title="wedding">💒</span><span class="intercom-emoji-picker-emoji" title="church">⛪</span><span class="intercom-emoji-picker-emoji" title="department_store">🏬</span><span class="intercom-emoji-picker-emoji" title="city_sunrise">🌇</span><span class="intercom-emoji-picker-emoji" title="city_sunset">🌆</span><span class="intercom-emoji-picker-emoji" title="japanese_castle">🏯</span><span class="intercom-emoji-picker-emoji" title="european_castle">🏰</span><span class="intercom-emoji-picker-emoji" title="tent">⛺</span><span class="intercom-emoji-picker-emoji" title="factory">🏭</span><span class="intercom-emoji-picker-emoji" title="tokyo_tower">🗼</span><span class="intercom-emoji-picker-emoji" title="japan">🗾</span><span class="intercom-emoji-picker-emoji" title="mount_fuji">🗻</span><span class="intercom-emoji-picker-emoji" title="sunrise_over_mountains">🌄</span><span class="intercom-emoji-picker-emoji" title="sunrise">🌅</span><span class="intercom-emoji-picker-emoji" title="night_with_stars">🌃</span><span class="intercom-emoji-picker-emoji" title="statue_of_liberty">🗽</span><span class="intercom-emoji-picker-emoji" title="bridge_at_night">🌉</span><span class="intercom-emoji-picker-emoji" title="carousel_horse">🎠</span><span class="intercom-emoji-picker-emoji" title="ferris_wheel">🎡</span><span class="intercom-emoji-picker-emoji" title="fountain">⛲</span><span class="intercom-emoji-picker-emoji" title="roller_coaster">🎢</span><span class="intercom-emoji-picker-emoji" title="ship">🚢</span><span class="intercom-emoji-picker-emoji" title="boat">⛵</span><span class="intercom-emoji-picker-emoji" title="speedboat">🚤</span><span class="intercom-emoji-picker-emoji" title="rocket">🚀</span><span class="intercom-emoji-picker-emoji" title="seat">💺</span><span class="intercom-emoji-picker-emoji" title="station">🚉</span><span class="intercom-emoji-picker-emoji" title="bullettrain_side">🚄</span><span class="intercom-emoji-picker-emoji" title="bullettrain_front">🚅</span><span class="intercom-emoji-picker-emoji" title="metro">🚇</span><span class="intercom-emoji-picker-emoji" title="railway_car">🚃</span><span class="intercom-emoji-picker-emoji" title="bus">🚌</span><span class="intercom-emoji-picker-emoji" title="blue_car">🚙</span><span class="intercom-emoji-picker-emoji" title="car">🚗</span><span class="intercom-emoji-picker-emoji" title="taxi">🚕</span><span class="intercom-emoji-picker-emoji" title="truck">🚚</span><span class="intercom-emoji-picker-emoji" title="rotating_light">🚨</span><span class="intercom-emoji-picker-emoji" title="police_car">🚓</span><span class="intercom-emoji-picker-emoji" title="fire_engine">🚒</span><span class="intercom-emoji-picker-emoji" title="ambulance">🚑</span><span class="intercom-emoji-picker-emoji" title="bike">🚲</span><span class="intercom-emoji-picker-emoji" title="barber">💈</span><span class="intercom-emoji-picker-emoji" title="busstop">🚏</span><span class="intercom-emoji-picker-emoji" title="ticket">🎫</span><span class="intercom-emoji-picker-emoji" title="traffic_light">🚥</span><span class="intercom-emoji-picker-emoji" title="construction">🚧</span><span class="intercom-emoji-picker-emoji" title="beginner">🔰</span><span class="intercom-emoji-picker-emoji" title="fuelpump">⛽</span><span class="intercom-emoji-picker-emoji" title="izakaya_lantern">🏮</span><span class="intercom-emoji-picker-emoji" title="slot_machine">🎰</span><span class="intercom-emoji-picker-emoji" title="moyai">🗿</span><span class="intercom-emoji-picker-emoji" title="circus_tent">🎪</span><span class="intercom-emoji-picker-emoji" title="performing_arts">🎭</span><span class="intercom-emoji-picker-emoji" title="round_pushpin">📍</span><span class="intercom-emoji-picker-emoji" title="triangular_flag_on_post">🚩</span></div><div class="intercom-emoji-picker-group"><div class="intercom-emoji-picker-group-title">Symbols</div><span class="intercom-emoji-picker-emoji" title="keycap_ten">🔟</span><span class="intercom-emoji-picker-emoji" title="1234">🔢</span><span class="intercom-emoji-picker-emoji" title="symbols">🔣</span><span class="intercom-emoji-picker-emoji" title="capital_abcd">🔠</span><span class="intercom-emoji-picker-emoji" title="abcd">🔡</span><span class="intercom-emoji-picker-emoji" title="abc">🔤</span><span class="intercom-emoji-picker-emoji" title="arrow_up_small">🔼</span><span class="intercom-emoji-picker-emoji" title="arrow_down_small">🔽</span><span class="intercom-emoji-picker-emoji" title="rewind">⏪</span><span class="intercom-emoji-picker-emoji" title="fast_forward">⏩</span><span class="intercom-emoji-picker-emoji" title="arrow_double_up">⏫</span><span class="intercom-emoji-picker-emoji" title="arrow_double_down">⏬</span><span class="intercom-emoji-picker-emoji" title="ok">🆗</span><span class="intercom-emoji-picker-emoji" title="new">🆕</span><span class="intercom-emoji-picker-emoji" title="up">🆙</span><span class="intercom-emoji-picker-emoji" title="cool">🆒</span><span class="intercom-emoji-picker-emoji" title="free">🆓</span><span class="intercom-emoji-picker-emoji" title="ng">🆖</span><span class="intercom-emoji-picker-emoji" title="signal_strength">📶</span><span class="intercom-emoji-picker-emoji" title="cinema">🎦</span><span class="intercom-emoji-picker-emoji" title="koko">🈁</span><span class="intercom-emoji-picker-emoji" title="u6307">🈯</span><span class="intercom-emoji-picker-emoji" title="u7a7a">🈳</span><span class="intercom-emoji-picker-emoji" title="u6e80">🈵</span><span class="intercom-emoji-picker-emoji" title="u5408">🈴</span><span class="intercom-emoji-picker-emoji" title="u7981">🈲</span><span class="intercom-emoji-picker-emoji" title="ideograph_advantage">🉐</span><span class="intercom-emoji-picker-emoji" title="u5272">🈹</span><span class="intercom-emoji-picker-emoji" title="u55b6">🈺</span><span class="intercom-emoji-picker-emoji" title="u6709">🈶</span><span class="intercom-emoji-picker-emoji" title="u7121">🈚</span><span class="intercom-emoji-picker-emoji" title="restroom">🚻</span><span class="intercom-emoji-picker-emoji" title="mens">🚹</span><span class="intercom-emoji-picker-emoji" title="womens">🚺</span><span class="intercom-emoji-picker-emoji" title="baby_symbol">🚼</span><span class="intercom-emoji-picker-emoji" title="wc">🚾</span><span class="intercom-emoji-picker-emoji" title="no_smoking">🚭</span><span class="intercom-emoji-picker-emoji" title="u7533">🈸</span><span class="intercom-emoji-picker-emoji" title="accept">🉑</span><span class="intercom-emoji-picker-emoji" title="cl">🆑</span><span class="intercom-emoji-picker-emoji" title="sos">🆘</span><span class="intercom-emoji-picker-emoji" title="id">🆔</span><span class="intercom-emoji-picker-emoji" title="no_entry_sign">🚫</span><span class="intercom-emoji-picker-emoji" title="underage">🔞</span><span class="intercom-emoji-picker-emoji" title="no_entry">⛔</span><span class="intercom-emoji-picker-emoji" title="negative_squared_cross_mark">❎</span><span class="intercom-emoji-picker-emoji" title="white_check_mark">✅</span><span class="intercom-emoji-picker-emoji" title="heart_decoration">💟</span><span class="intercom-emoji-picker-emoji" title="vs">🆚</span><span class="intercom-emoji-picker-emoji" title="vibration_mode">📳</span><span class="intercom-emoji-picker-emoji" title="mobile_phone_off">📴</span><span class="intercom-emoji-picker-emoji" title="ab">🆎</span><span class="intercom-emoji-picker-emoji" title="diamond_shape_with_a_dot_inside">💠</span><span class="intercom-emoji-picker-emoji" title="ophiuchus">⛎</span><span class="intercom-emoji-picker-emoji" title="six_pointed_star">🔯</span><span class="intercom-emoji-picker-emoji" title="atm">🏧</span><span class="intercom-emoji-picker-emoji" title="chart">💹</span><span class="intercom-emoji-picker-emoji" title="heavy_dollar_sign">💲</span><span class="intercom-emoji-picker-emoji" title="currency_exchange">💱</span><span class="intercom-emoji-picker-emoji" title="x">❌</span><span class="intercom-emoji-picker-emoji" title="exclamation">❗</span><span class="intercom-emoji-picker-emoji" title="question">❓</span><span class="intercom-emoji-picker-emoji" title="grey_exclamation">❕</span><span class="intercom-emoji-picker-emoji" title="grey_question">❔</span><span class="intercom-emoji-picker-emoji" title="o">⭕</span><span class="intercom-emoji-picker-emoji" title="top">🔝</span><span class="intercom-emoji-picker-emoji" title="end">🔚</span><span class="intercom-emoji-picker-emoji" title="back">🔙</span><span class="intercom-emoji-picker-emoji" title="on">🔛</span><span class="intercom-emoji-picker-emoji" title="soon">🔜</span><span class="intercom-emoji-picker-emoji" title="arrows_clockwise">🔃</span><span class="intercom-emoji-picker-emoji" title="clock12">🕛</span><span class="intercom-emoji-picker-emoji" title="clock1">🕐</span><span class="intercom-emoji-picker-emoji" title="clock2">🕑</span><span class="intercom-emoji-picker-emoji" title="clock3">🕒</span><span class="intercom-emoji-picker-emoji" title="clock4">🕓</span><span class="intercom-emoji-picker-emoji" title="clock5">🕔</span><span class="intercom-emoji-picker-emoji" title="clock6">🕕</span><span class="intercom-emoji-picker-emoji" title="clock7">🕖</span><span class="intercom-emoji-picker-emoji" title="clock8">🕗</span><span class="intercom-emoji-picker-emoji" title="clock9">🕘</span><span class="intercom-emoji-picker-emoji" title="clock10">🕙</span><span class="intercom-emoji-picker-emoji" title="clock11">🕚</span><span class="intercom-emoji-picker-emoji" title="heavy_plus_sign">➕</span><span class="intercom-emoji-picker-emoji" title="heavy_minus_sign">➖</span><span class="intercom-emoji-picker-emoji" title="heavy_division_sign">➗</span><span class="intercom-emoji-picker-emoji" title="white_flower">💮</span><span class="intercom-emoji-picker-emoji" title="100">💯</span><span class="intercom-emoji-picker-emoji" title="radio_button">🔘</span><span class="intercom-emoji-picker-emoji" title="link">🔗</span><span class="intercom-emoji-picker-emoji" title="curly_loop">➰</span><span class="intercom-emoji-picker-emoji" title="trident">🔱</span><span class="intercom-emoji-picker-emoji" title="small_red_triangle">🔺</span><span class="intercom-emoji-picker-emoji" title="black_square_button">🔲</span><span class="intercom-emoji-picker-emoji" title="white_square_button">🔳</span><span class="intercom-emoji-picker-emoji" title="red_circle">🔴</span><span class="intercom-emoji-picker-emoji" title="large_blue_circle">🔵</span><span class="intercom-emoji-picker-emoji" title="small_red_triangle_down">🔻</span><span class="intercom-emoji-picker-emoji" title="white_large_square">⬜</span><span class="intercom-emoji-picker-emoji" title="black_large_square">⬛</span><span class="intercom-emoji-picker-emoji" title="large_orange_diamond">🔶</span><span class="intercom-emoji-picker-emoji" title="large_blue_diamond">🔷</span><span class="intercom-emoji-picker-emoji" title="small_orange_diamond">🔸</span><span class="intercom-emoji-picker-emoji" title="small_blue_diamond">🔹</span></div></div></div></div></div><div class="intercom-composer-popover-caret"></div></div>
                        </div>
                    </div>


                    <div id="textPanel" class="split-flex split-horizontal-flex;" style="overflow: hidden" data-step="3" data-intro="This is the <b>message window</b>. You can test it in this chat screen. Just type a message below and hit the Send button...">

                        <div class="m-portlet m-portlet--tabs" m-portlet="true" id="chatPortlet"
                             style="margin: 0;height: 100%;width: 100%;">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <div>
                                            <#--<span class="m-bootstrap-switch m-bootstrap-switch--pill">
                                                <input  id="testMode" data-switch="true" type="checkbox" data-on-text="Match" data-handle-width="70" data-off-text="Chat" data-on-color="brand" data-off-color="accent">
                                            </span>-->
                                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="btn m-btn m-btn--pill btn-secondary chatModeBtn active">
                                                    <input type="radio" name="runMode" value="chatMode" autocomplete="off" checked
                                                           data-toggle="m-tooltip" data-placement="top" title=""
                                                           data-original-title="Try out a conversation">
                                                    <i class="la la-comments"></i> Chat
                                                </label>
                                                <label class="btn m-btn m-btn--pill btn-secondary matchModeBtn">
                                                    <input type="radio" name="runMode" value="matchMode" autocomplete="off"
                                                           data-toggle="m-tooltip" data-placement="top" title=""
                                                           data-original-title="See how labels match a text">
                                                    <i class="la la-binoculars"></i> Match
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav" id="chatTools">
                                        <li class="m-portlet__nav-item">
                                            <a id="externalBtn"
                                               class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                               data-toggle="m-tooltip" data-placement="top" title=""
                                               data-original-title="Open externally">
                                                <i class="la la-external-link"></i>
                                            </a>
                                        </li>
                                        <li class="m-portlet__nav-item">
                                            <a id="infoBtn"
                                               class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                               data-toggle="m-tooltip" data-placement="top" title=""
                                               data-original-title="See matched text">
                                                <i class="la la-info"></i>
                                            </a>
                                        </li>
                                        <li class="m-portlet__nav-item">
                                            <a id="clearBtn"
                                               class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                               data-toggle="m-tooltip" data-placement="top" title=""
                                               data-original-title="Clear conversation">
                                                <i class="la la-close"></i>
                                            </a>
                                        </li>
                                        <#--<li class="m-portlet__nav-item">
                                            <a m-portlet-tool="fullscreen"
                                               class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill">
                                                <i class="la la-expand"></i>
                                            </a>
                                        </li>-->
                                    </ul>

                                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--right m-tabs-line-danger" style="float: left; display: none; min-width: 122px; margin-left: 5px;" id="matchTools">
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link active" id="textInputBtn">
                                                Input
                                            </a>
                                        </li>
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link" id="outputPanelBtn">
                                                Output
                                            </a>
                                        </li>
                                    </ul>



                                </div>
                            </div>
                            <div class="m-portlet__body" id="chatbody" style="padding: 0; height: calc(100% - 71px);"></div>
                            <div class="m-portlet__body" id="matchbody" style="padding: 0; height: calc(100% - 71px);">
                                <div style="height: 100%">
                                    <div class="" style="height: 100%" id="textInputTab">
                                        <textarea id="textInput" style="width: 100%; height: 100%;border: none;" placeholder="Your text goes here..."></textarea>
                                    </div>
                                    <div class="" style="height: 100%; display: none" id="outputPanelTab">
                                        <p id="outputPanel" style="overflow-y: scroll;height: 100%; margin: 0"><i>Click <i class="la la-play-circle-o"></i> to see the result...</i></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12" id="doc">
                <h2>Documentation</h2>
            </div>
        </div>
    </div>

<#-- Example modal -->
    <div class="modal fade" id="loadExampleModal" tabindex="-1" role="dialog" aria-labelledby="loadExampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="loadExampleModalLabel">Open example script</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="loadExampleSelect">Select an example script</label>
                            <select class="form-control" id="loadExampleSelect">

                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button id="loadExampleBtn" type="button" class="btn btn-brand">Open</button>
                </div>
            </div>
        </div>
    </div>

</#macro>

<#macro pageScript>
    <script src="/assets/app/js/chitchatclient.util.js" type="application/javascript"></script>
    <script src="/assets/app/js/chitchatclient.core.js" type="application/javascript"></script>
    <script src="/assets/app/js/script-crud.js" type="application/javascript"></script>
    <script src="/assets/vendors/custom/intro/intro.min.js" type="application/javascript"></script>

    <script>
        $(document).ready(function () {
            $.hx.setCurrentPage('#menu-item-tutorial')




            let tutorialStep = 0;
            let didChat = new Set(); // tutorial user
            let botResult;

            // Setup speech
            const speechSynth = window.speechSynthesis;

            let chatbox = new ChitChatClient(document.getElementById('chatbody'), {
                showLeftUser: true,
                showRightUser: true,
                scrollToLastMessage: true,
            });

            // Setup speech recognition
            const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
            const micBtn = document.querySelector('.micBtn');
            if (SpeechRecognition) {
                micBtn.style.display = 'inline-flex';
                const micBtnColor = micBtn.style.backgroundColor;

                const recognition = new SpeechRecognition();

                micBtn.addEventListener('click', () => {
                    recognition.start();
                    micBtn.style.backgroundColor = '#f66e84';
                    recognition.onresult = (event) => {
                        const speechToText = event.results[0][0].transcript;
                        const $input = document.querySelector('.send input');
                        $input.value = speechToText;
                        micBtn.style.backgroundColor = micBtnColor;
                    }
                });
            } else {
                micBtn.style.display = 'none';
            }

            Split(['#rulePanel', '#textPanel'], {
                sizes: [50, 50],
                gutterSize: 8,
                cursor: 'col-resize',
                elementStyle: function (dimension, size, gutterSize) {
                    return {'flex-basis': 'calc(' + size + '% - ' + gutterSize + 'px)'}
                },
                gutterStyle: function (dimension, gutterSize) {
                    return {'flex-basis': gutterSize + 'px'}
                }
            });

            $('#clearBtn').click(clear);

            $('#infoBtn').click(function () {
                $.hx.notify(botResult.highlight, 'info')
            });

            $('#externalBtn').click(function () {
                window.open('/api/v1/ui/chatclient?script='+encodeURIComponent($('#scriptName').val())+'&lang='+scriptEditor.guessScriptLang(), '_blank');
            });

            window.scriptEditor = new CcsEditor($('#scriptEditor')[0], 'chat.ccs')

            function clear() {
                $.ajax({
                    type: "POST",
                    url: "/api/v1/channel/devbot/reset",
                    contentType: 'text/plain',
                    data: "you",
                    success: function () {
                        chatbox.clear()
                        // chatbox.focusOnInput()
                        scriptEditor.removeMessages()
                        botResult = {highlight: '<i>none</i>'}
                    },
                    error: function (e) {
                        $.hx.notify(e, 'danger');
                    }
                });
            }
            clear(); //reset cache




            let speechLang = 'en-US';

            let onSend = function (params) {
                const formData = new FormData();
                let script = scriptEditor.getText();
                formData.append("script", script);

                didChat.add(tutorialStep) ; // tutorial

                formData.append("msg",
                    JSON.stringify({
                        id: '' + Date.now(),
                        text: params.text,
                        senderId: "you",
                        recipientId: "devbot",
                        timestamp: params.timestamp
                    }));

                // Set speech lang
                speechLang = scriptEditor.guessScriptLang();

                scriptEditor.removeMessages()

                $.ajax({
                    type: "POST",
                    url: "/api/v1/channel/devbot/reply",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (a) {
                        console.log(a);
                        let warningCount = 0, warningMsg;
                        for (const warning of a.warnings) {
                            let message = warning.message;
                            let lineNr = warning.line - 1;
                            if(lineNr !== undefined){
                                scriptEditor.addWarningMessage(lineNr, message)
                                warningCount++;
                                warningMsg = message;
                            }
                        }

                        if(warningCount === 1)
                            $.hx.notify('There is an warning for your script.<br>' + warningMsg, 'warning')
                        else if (warningCount > 1)
                            $.hx.notify('There are '+warningCount+' errors in your script.', 'warning')

                        botResult = a
                        a.replies.forEach(msg => {
                            let text = msg.text
                            // render image
                            text = text.replace(/\bIMAGE *\((.*?)\)/gm, '<img src="$1" style="display: block;max-width:100%; width: auto; height: auto;"/>')

                            // render buttons
                            text = text.replace(/\bBUTTON *\( *(.*?) *, *(.*?) *\)/gm, '<button type="button" class="btn btn-success chatBtn" data-value="$2">$1</button>')
                            text = text.replace(/\bGEOBUTTON *\( *(.*?) *\)/gm, '<button type="button" class="btn btn-success chatBtn geoBtn" data-value="geoBtn">$1</button>')
                            const textBefore = text;
                            text = text.replace(/\bCHECKBOX *\( *(.*?) *, *(.*?) *\)/gm, '<label class="checkBoxRow">$1 <input type="checkbox" value="$2"><span class="checkmark"></span></label>')
                            if(textBefore !== text){
                                // checkboxes detected
                                text += '<button type="button" class="btn btn-success checkboxSendBtn">OK</button>'
                            }

                            // speech synthesis
                            let speakPattern = /\bSPEAK *\((.*?)\)/gm;
                            if (speechSynth) {
                                // if speech supported
                                let utterances;
                                while ((utterances = speakPattern.exec(text)) !== null) {
                                    const utterThis = new SpeechSynthesisUtterance(utterances[1]);
                                    if (speechLang) {
                                        utterThis.lang = speechLang;
                                    }
                                    speechSynth.speak(utterThis);
                                }
                            }
                            text = text.replace(/\b(SPEAK|WAIT) *\([^)]*\)/gm, '');

                            chatbox.addMessageLeft(text, "BOT");
                        })

                        const successLabel = tutorial[tutorialStep].successLabel;
                        if(successLabel in botResult.matches || successLabel.some && successLabel.some( sl => sl in botResult.matches)){
                            window.setTimeout(() => {
                                $.hx.notify("Well done!", "success");
                            }, 4000);
                        }
                    },
                    error: function (e) {
                        const exceptions = JSON.parse(e.responseText);
                        let errorCount = 0, warningCount = 0;
                        let errorMsg, warningMsg;
                        for (const e of exceptions) {
                            let message = e.message;
                            let lineNr = e.line - 1;
                            if(lineNr !== undefined){
                                if(e.type === "WARNING") {
                                    scriptEditor.addWarningMessage(lineNr, message)
                                    warningCount++;
                                    warningMsg = message;
                                }else {
                                    scriptEditor.addErrorMessage(lineNr, message)
                                    errorCount++;
                                    errorMsg = message;
                                }
                            }
                        }

                        if(errorCount === 1)
                            $.hx.notify('There is an error in your script.<br>' + errorMsg, 'danger')
                        else if (errorCount > 1)
                            $.hx.notify('There are '+errorCount+' errors in your script.', 'danger')

                        if(warningCount === 1)
                            $.hx.notify('There is an warning for your script.<br>' + warningMsg, 'warning')
                        else if (warningCount > 1)
                            $.hx.notify('There are '+warningCount+' errors in your script.', 'warning')

                        scriptEditor.scrollToFirstMessage();
                    }
                })
            };
            chatbox.onSend(onSend, false);
            // link button actions
            $('#chatbody').on('click', '.chatBtn', function(){
                let value = $(this).data('value');

                if(value === 'geoBtn'){
                    // share geolocation button
                    if ("geolocation" in navigator) {
                        /* geolocation is available */
                        navigator.geolocation.getCurrentPosition(function(position) {
                            const btnParams = {
                                id: '' + Date.now(),
                                text: 'latitude '+position.coords.latitude+' longitude '+position.coords.longitude,
                                senderId: "you",
                                recipientId: "devbot",
                                timestamp: new Date()
                            }
                            $(this).addClass('chosen').parent().children().prop('disabled', true);
                            onSend(btnParams)
                        });
                    } else {
                        /* geolocation IS NOT available */
                        chitChatClient.addInfo("<div style='color:#f06;font-style: italic'>The geoloation is not available...</div>")
                        // normal button
                        const btnParams = {
                            id: '' + Date.now(),
                            text: 'unknownlocation',
                            senderId: "you",
                            recipientId: "devbot",
                            timestamp: new Date()
                        }
                        $(this).addClass('chosen').parent().children().prop('disabled', true);
                        onSend(btnParams)
                    }

                } else {
                    // normal button
                    const btnParams = {
                        id: '' + Date.now(),
                        text: value,
                        senderId: "you",
                        recipientId: "devbot",
                        timestamp: new Date()
                    }
                    $(this).addClass('chosen').parent().children().prop('disabled', true);
                    onSend(btnParams)
                }
            })

            // link button actions
            document.addEventListener('click',function(e) {
                if (e.target && e.target.classList.contains('checkboxSendBtn')) {
                    let checkedList = [...e.target.parentNode.querySelectorAll('input:checked')];
                    let txt = checkedList.map(cb => cb.value).join(", ");
                    const msgParams = {
                        id: '' + Date.now(),
                        text: txt,
                        senderId: "you",
                        recipientId: "devbot",
                        timestamp: new Date()
                    }

                    // submit button
                    e.target.setAttribute('disabled', true)

                    // options
                    checkedList.forEach(cb => cb.classList.add('chosen'))
                    e.target.parentNode.querySelectorAll('input').forEach(cb => cb.setAttribute('disabled', true));
                    onSend(msgParams)
                }
            })

            new ShowdownAccordion('/ccsdoc.md', '#doc', 1, "cssmode")

            $('input[type=radio][name=runMode]').change(function() {
                if (this.value === 'matchMode') {
                    matchMode()
                } else if (this.value === 'chatMode') {
                    chatMode()
                }
            })

            function matchMode(){
                $('#chatTools').hide()
                $('#chatbody').hide()
                $('#matchTools').show()
                $('#matchbody').show()
                $('#runBtn').show()
            }

            function chatMode(){
                $('#chatTools').show()
                $('#chatbody').show()
                $('#matchTools').hide()
                $('#matchbody').hide()
                $('#runBtn').hide()
            }


            $('#textInputBtn').click(() => {
                $('#textInputBtn').addClass('active');
                $('#outputPanelBtn').removeClass('active');
                $('#textInputTab').show()
                $('#outputPanelTab').hide()
            })

            $('#outputPanelBtn').click(() => {
                $('#textInputBtn').removeClass('active');
                $('#outputPanelBtn').addClass('active');
                $('#textInputTab').hide()
                $('#outputPanelTab').show()
            })


            $('#runBtn').click(() => {
                scriptEditor.removeMessages()

                let formData = new FormData();
                formData.append("script", scriptEditor.getText());
                formData.append("text", $('#textInput').val());

                $.ajax({
                    type: "POST",
                    url: "/api/v1/channel/devbot/match",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: (a) => {
                        console.log(a);
                        let warningCount = 0, warningMsg;
                        for (const warning of a.warnings) {
                            let message = warning.message;
                            let lineNr = warning.line - 1;
                            if(lineNr !== undefined){
                                scriptEditor.addWarningMessage(lineNr, message)
                                warningCount++;
                                warningMsg = message;
                            }
                        }

                        if(warningCount === 1)
                            $.hx.notify('There is an warning for your script.<br>' + warningMsg, 'warning')
                        else if (warningCount > 1)
                            $.hx.notify('There are '+warningCount+' errors in your script.', 'warning')

                        $('#outputPanel').html(a.highlight);
                        $('#outputPanelBtn').click()
                        if(a.replies && a.replies.length > 0){
                            a.replies.forEach(msg => {
                                $.hx.notify(msg.text, msg.text  ? 'brand' : 'danger')
                            })
                        }

                    },
                    error: (e) => {
                        const exceptions = JSON.parse(e.responseText);
                        let errorCount = 0, warningCount = 0;
                        let errorMsg, warningMsg;
                        for (const e of exceptions) {
                            let message = e.message;
                            let lineNr = e.line - 1;
                            if(lineNr !== undefined){
                                if(e.type === "WARNING") {
                                    scriptEditor.addWarningMessage(lineNr, message)
                                    warningCount++;
                                    warningMsg = message;
                                }else {
                                    scriptEditor.addErrorMessage(lineNr, message)
                                    errorCount++;
                                    errorMsg = message;
                                }
                            }
                        }

                        if(errorCount === 1)
                            $.hx.notify('There is an error in your script.<br>' + errorMsg, 'danger')
                        else if (errorCount > 1)
                            $.hx.notify('There are '+errorCount+' errors in your script.', 'danger')

                        if(warningCount === 1)
                            $.hx.notify('There is an warning for your script.<br>' + warningMsg, 'warning')
                        else if (warningCount > 1)
                            $.hx.notify('There are '+warningCount+' errors in your script.', 'warning')

                        scriptEditor.scrollToFirstMessage();
                    }
                });
            })

            const tutorials = {
                basic: [
                    {
                        "title": "1: Showing you around",
                        "successLabel": "foo",
                        "errorLabel": "",
                        "description": "First, let me explain you what you see here... <p><div class='btn btn-warning' style='animation: shadow-pulse 1s infinite;' onclick='javascript:introJs().start();'>Start!</div>",
                        "script": `
# your script will go here
                `,
                    },
                    {
                        "title": "2: Hi",
                        "successLabel": "welcome",
                        "errorLabel": "",
                        "description":
                            `Below is the first script. <ul>
<li>It contains a rule for detecting when the user says 'hi' (line 2),</li>
<li>and if that is the case, it replies the chatbot replies "Hello there!" (line 5).</li>
</ul>
<p><b>Try this out, by typing "hi" in the message window.</b>`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!`
                    },
                    {
                        "title": "3: HI!",
                        "successLabel": "welcome",
                        "errorLabel": "",
                        "description":
                            `The same script is already able to detect "hi", when it is somewhere in the message a user sent us. Or when it is uppercase.
<p><b> Try typing "Well HI chatbot!"</b>`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!`
                    },
                    {
                        "title": "4: Hi OR hello",
                        "successLabel": "welcome",
                        "errorLabel": "",
                        "description":
                            `Let's make it a bit more flexible. What if the user says "hello" instead of "hi"?
<p>Line 2 is changed so it will behave the same when "hi" OR "hello" is sent.
<p><b>Try sending "Hello!" this time.</b>`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi OR hello

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!`
                    },
                    {
                        "title": "5: Ask color",
                        "successLabel": "welcome",
                        "errorLabel": "",
                        "description":
                            `Now the chat bot will also ask for your favorite color.
<p>Line 6 add an additional message in the reply with "&amp;". <br>It has spaces at the start of this line to indocate it is part of the same reply statement.
<p><b>Try sending "hi" again... </b>
<p>When is ask you the question, you can type something again. It won't do much yet, though...`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi OR hello

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!
    & What is your favorite color?`
                    },
                    {
                        "title": "6: Comment on color",
                        "successLabel": ["warmColor", "coldColor"],
                        "errorLabel": "",
                        "description":
                            `Let's do something with the color mentioned now...
<ul>
<li>Lines 4 and 5 trigger on colors the user may mention.
<li>Lines 11 and 13 let the chatbot comment on your choice
</ul>
<p><b>Try sending "hi" again...</b>`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi OR hello

@warmColor <- red OR orange
@coldColor <- blue OR green

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!
    & What is your favorite color?

@warmColor -> So you like @warmColor. That is a nice warm color.

@coldColor -> Are cold colors your thing? @coldColor is very nice.
`
                    },
                    {
                        "title": "7: Add case for yellow",
                        "successLabel": ["warmColor", "coldColor"],
                        "errorLabel": "",
                        "description":
                            `What if the user mentions "yellow"?
<p><b>Change the script. Add yellow as an option to the warm colors.</b>
<p>Remember the Solution button on the right, if it doesn't work ;)`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi OR hello

@warmColor <- red OR orange
@coldColor <- blue OR green

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!
    & What is your favorite color?

@warmColor -> So you like @warmColor. That is a nice warm color.

@coldColor -> Are cold colors your thing? @coldColor is very nice.
`,      "solution":
                            `# check if someone types "hi"
@welcome <- hi OR hello

@warmColor <- red OR orange OR yellow
@coldColor <- blue OR green

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!
    & What is your favorite color?

@warmColor -> So you like @warmColor. That is a nice warm color.

@coldColor -> Are cold colors your thing? @coldColor is very nice.
`
                    },
                    {
                        "title": "8: Unknown input",
                        "successLabel": ["warmColor", "coldColor"],
                        "errorLabel": "",
                        "description":
                            `Now what if the user mentions yet another color? Or types something completely different?
<p><ul>
<li>You see that the rules in the script from line 8 are all replies.</li>
<li>You recognize them easily with the blue arrow <code>-&gt;</code>.</li>
<li>The chatbot only uses them if the label in front (e.g. <code>@welcome</code> or <code>@warmColor</code>) is recognized.</li>
<li>Line 16 is used when nothing is recognized! It is called the FALLBACK reply.</li>
</ul>
<p><b>Type something strange. Then type "I like red". This chatbot is a bit more prepared for different messages.</b>`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi OR hello

@warmColor <- red OR orange OR yellow
@coldColor <- blue OR green

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!
    & What is your favorite color?

@warmColor -> So you like @warmColor. That is a nice warm color.

@coldColor -> Are cold colors your thing? @coldColor is very nice.

() -> I don't understand that. Write me something else please...
`
                    },

                    {
                        "title": "9: Repeat replies",
                        "successLabel": ["warmColor", "coldColor"],
                        "errorLabel": "",
                        "description":
                            `The chatbot will not repeat itself by default.
To make it re-use a reply, for example the last line, <code>{repeat}</code> is added  right before the arrow.
<p><b>Try sending "hi" three times. The first time it uses line 8, later it will only use line 15.</b>`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi OR hello

@warmColor <- red OR orange OR yellow
@coldColor <- blue OR green

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!
    & What is your favorite color?

@warmColor -> So you like @warmColor. That is a nice warm color.

@coldColor -> Are cold colors your thing? @coldColor is very nice.

() {repeat} -> I don't understand that. Write me something else please...
`
                    },

                    {
                        "title": "10: Skip the welcome",
                        "successLabel": ["coldColor"],
                        "errorLabel": "",
                        "description":
                            `This script is becoming a bit more sophisticated. But now you already know:
<ul>
<li> How to recognize words in messages from the user (lines 2-5)
<li> How to send replies, depending on a user's messages (lines 8 - 15)
<li> What to do when you don't know (lines 15)
</ul>
You don't always have to start with "hi".
<p><b>Just write "I like blue"</b>. It will skip welcome message...`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi OR hello

@warmColor <- red OR orange OR yellow
@coldColor <- blue OR green

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!
    & What is your favorite color?

@warmColor -> So you like @warmColor. That is a nice warm color.

@coldColor -> Are cold colors your thing? @coldColor is very nice.

() {repeat} -> I don't understand that. Write me something else please...
`
                    },
                    {
                        "title": "11: Use buttons",
                        "successLabel": ["coldColor", "warmColor"],
                        "errorLabel": "",
                        "description":
                            `Let's introduce some buttons, so the user doesn't have to type. For closed questions this may be clearer.
<ul>
<li> A button has a text displayed on the button itself
<li> A button has a text that is send, as if the user typed it when it is pressed
<li> To show a button in a reply, use: <code>BUTTON(Text on button, Text send)</code>
<li> What to do when you don't know (lines 15)
</ul>
<p>Lines 10 and 11 add the buttons...
<p><b>See the result by typing "hi"</b>.`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi OR hello

@warmColor <- red OR orange OR yellow
@coldColor <- blue OR green

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!
    & What is your favorite color?
    & BUTTON(Red, red) BUTTON(Orange, orange)
      BUTTON(Green, green) BUTTON(Blue, blue)

@warmColor -> So you like @warmColor. That is a nice warm color.

@coldColor -> Are cold colors your thing? @coldColor is very nice.

() {repeat} -> I don't understand that. Write me something else please...
`
                    },
                    {
                        "title": "12: Add Yellow again",
                        "successLabel": ["warmColor"],
                        "errorLabel": "",
                        "description":
                            `<b>Like before, add a button for yellow and test it...</b>`,
                        "script":
                            `# check if someone types "hi"
@welcome <- hi OR hello

@warmColor <- red OR orange OR yellow
@coldColor <- blue OR green

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!
    & What is your favorite color?
    & BUTTON(Red, red) BUTTON(Orange, orange)
      BUTTON(Green, green) BUTTON(Blue, blue)

@warmColor -> So you like @warmColor. That is a nice warm color.

@coldColor -> Are cold colors your thing? @coldColor is very nice.

() {repeat} -> I don't understand that. Write me something else please...
`,
                        "solution":
                            `# check if someone types "hi"
@welcome <- hi OR hello

@warmColor <- red OR orange OR yellow
@coldColor <- blue OR green

# the chatbot replies "Hello there!" if the user says "hi"
@welcome -> Hello there!
    & What is your favorite color?
    & BUTTON(Red, red) BUTTON(Orange, orange)
      BUTTON(Yellow, yellow) BUTTON(Green, green) BUTTON(Blue, blue)

@warmColor -> So you like @warmColor. That is a nice warm color.

@coldColor -> Are cold colors your thing? @coldColor is very nice.

() {repeat} -> I don't understand that. Write me something else please...
`
                    },
                ]
            }

            let tutorial = tutorials[$.hx.get("tutorial", "basic")];

            const $tutorialContentList = $('#tutorialContent ul');
            tutorial.forEach((step, i) => {
                $tutorialContentList.append('<li data-step="'+i+'">'+step.title+'</li>')
            })


            introJs().setOption("scrollToElement", false);
            introJs().setOption("scrollPadding", 10);


            function gotoStep(i){
                introJs().exit();
                clear();

                const step = tutorial[i];
                $('#tutorialDescription').html(step.description);
                scriptEditor.setText(step.script);

                $('#tutorialContent li').removeClass('here');
                $('#tutorialContent :nth-child('+(i+1)+')').addClass('here');

            }

            $('#tutorialSolutionBtn').click(() => {
                const code = tutorial[tutorialStep].solution;
                if(code) {
                    scriptEditor.setText(code);
                } else {
                    $.hx.notify("You don't have to change the script here. <br>Just try out sending a message in the message panel.")
                    scriptEditor.setText(tutorial[tutorialStep].script);
                }
            })


            $('#tutorialNextBtn').click(() => {
                tutorialStep = Math.min(tutorialStep + 1, tutorial.length-1);
                gotoStep(tutorialStep);
            })

            $('#tutorialContent li').click(function() {
                tutorialStep = $(this).data('step')
                gotoStep(tutorialStep);
            })

            gotoStep($.hx.get("step", -1) + 1);

            // emoji
            $(document).on("click","#emoji-picker",function(e){
                e.stopPropagation();
                $('.intercom-composer-emoji-popover').toggleClass("active");
            });

            $(document).click(function (e) {
                if ($(e.target).attr('class') != '.intercom-composer-emoji-popover' && $(e.target).parents(".intercom-composer-emoji-popover").length == 0) {
                    $(".intercom-composer-emoji-popover").removeClass("active");
                }
            });

            $(document).on("click",".intercom-emoji-picker-emoji",function(e){
                scriptEditor.insertTextAtCursor($(this).html());
            });

            $('.intercom-composer-popover-input').on('input', function() {
                var query = this.value;
                if(query != ""){
                    $(".intercom-emoji-picker-emoji:not([title*='"+query+"'])").hide();
                }
                else{
                    $(".intercom-emoji-picker-emoji").show();
                }
            });

        })
    </script>
</#macro>