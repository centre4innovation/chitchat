<#import "utils.ftl" as u>

<#macro pageTitle>
ChitChat System Performance
</#macro>

<#macro pageContent>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                System Performance
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="/api/v1/ui/dashboard" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												System
											</span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Performance
											</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                                            <span class="m-portlet__head-icon">
                                                <i class="la la-line-chart"></i>
                                            </span>
                            <h3 class="m-portlet__head-text">Metrics</h3>
                        </div>
                    </div>

                </div>
                <div class="m-portlet__body">
                    <h5>Resources</h5>
                    <div id="appPerformancePlot"></div>
                    <h5>Database server</h5>
                    <div id="dbPerformancePlot"></div>
                    <h5>Jetty server</h5>
                    <div id="jettyPerformancePlot"></div>

                    <div id="performanceTable" class="table-sm"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<#macro pageScript>

<script>
    $.hx.setCurrentPage('#menu-item-system-performance')

    $(document).ready(function () {
        function updateDetails() {
            $.get('/admin/metrics', function (data) {

                // $('#performanceTable').html($.hx.toTable(data));

                plot('appPerformancePlot', data.timers, 'org.c4i.chitchat.api.resource.')
                plot('dbPerformancePlot', data.timers, 'org.c4i.chitchat.api.db.')
                plot('jettyPerformancePlot', data.timers, 'io.dropwizard.jetty.')


                function plot(element, data, pattern) {
                    const filtered = {}
                    Object.keys(data).forEach(key => {
                            if (key.includes(pattern)) {
                                filtered[key] = data[key];
                            }
                        }
                    )

                    const plotdata = [];
                    Object.keys(filtered).forEach(k => {
                        const v = filtered[k];
                        plotdata.push({
                            x: [v.min, v.p50, v.p50, v.p75, v.p95, v.p95, v.max], // [min, q1, q1, median, q3, q3, max]
                            // hoverinfo: 'text',
                            type: 'box',
                            name: k.replace(pattern, '') +  ' ('+v.count+')',
                            // text: hoverLabels //['min: '+v.min, 'p50: '+v.p50, 'p75: '+v.p75, 'p75: '+v.p75, 'p95: '+v.p95, 'max: '+v.max],
                        })
                    })


                    const layout = {
                        title: 'Timing for ' + pattern + '*',
                        height: 200 + 40*plotdata.length,
                        margin: {
                            l: 350
                        },
                        xaxis: {
                            title: {
                                text: 'seconds'
                            }
                        },
                        showlegend: false

                    };

                    Plotly.newPlot(element, plotdata, layout);
                }
            });


        }

        updateDetails();
    })

</script>

</#macro>