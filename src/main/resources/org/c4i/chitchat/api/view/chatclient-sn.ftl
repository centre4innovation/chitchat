<#macro pageContent>
<!DOCTYPE html>
<!--
   ________    _ __  ________          __
  / ____/ /_  (_) /_/ ____/ /_  ____ _/ /_
 / /   / __ \/ / __/ /   / __ \/ __ `/ __/
/ /___/ / / / / /_/ /___/ / / / /_/ / /_
\____/_/ /_/_/\__/\____/_/ /_/\__,_/\__/

Leiden university - Centre for Innovation - HumanityX

-->
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        ChitChat Client
    </title>
    <meta name="description" content="Chat with us!">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/assets/app/js/chitchatclient.core.css">
    <link rel="stylesheet" href="/assets/app/js/chitchatclient.components.css">

    <style>
        html, body {
            box-sizing: border-box;
            height: 100%;
            width: 100%;
            padding: 0;
            margin: 0;
        }

        body {
            background-color: #ebedf4;
            font-family: "Open Sans", sans-serif;
            display: flex;
            flex-direction: column;
        }

        #welcome {
            min-width: 400px;
            max-width: 800px;
            width: 50vw;
            margin: 20px auto 0px auto;
        }

        #chitchatclient-title {
            color: var(--chat-right-bg-color);
            margin-bottom: 0;
            font-weight: 200;
        }

        #chitchatclient-description {
            color: var(--chat-right-bg-color);
            margin-bottom: 0;
        }

        #chitchatclient {
            flex: 1;
            max-width: 800px;
            min-width: 400px;
            width: 50vw;
            min-height: 400px;
            margin: 20px auto 50px auto;
            background-color: white;
            border-radius: 20px;
            box-shadow: 0 5px 15px 1px rgba(69,65,78,.15)
        }

        .quickbar {
            min-width: 400px;
            max-width: 800px;
            width: 50vw;
            margin: 20px auto 0px auto;
            display: flex;
        }

        .quickbar .btn {
            border-radius: 10px;
            flex: 1;
            background-color: #fff;
            color: #999
        }
    </style>

    <link rel="shortcut icon" href="/assets/app/media/img/favicon.ico"/>
</head>

<body>
<div id="welcome">
    <img alt="" width="60px" src="/assets/app/media/img/chitchat-logo2.svg"
         style="margin-right: 4px;vertical-align: bottom;"/>

    <p style="font-size: 40px; font-weight: lighter; color: #4f30a2" id="chitchatclient-title">ChitChat
    </p>
    <a id="reload" class="btn" style="float: right;background-color: #fff; color: #999">Recommencer</a>
    <p id="chitchatclient-description" style="display: none"></p>
</div>

<div id="chitchatclient"></div>
<#--<div id="quickbar">
    <a  class="btn" style="float: right;background-color: #fff; color: #999">Recommancer</a>

</div>-->


<script src="/assets/app/js/chitchatclient.core.js" type="application/javascript"></script>
<script src="/assets/app/js/chitchatclient.rich.sn.js" type="application/javascript"></script>

<script>
    if (typeof Object.assign != 'function') {
        // Must be writable: true, enumerable: false, configurable: true
        Object.defineProperty(Object, "assign", {
            value: function assign(target, varArgs) { // .length of function is 2
                'use strict';
                if (target == null) { // TypeError if undefined or null
                    throw new TypeError('Cannot convert undefined or null to object');
                }

                var to = Object(target);

                for (var index = 1; index < arguments.length; index++) {
                    var nextSource = arguments[index];

                    if (nextSource != null) { // Skip over if undefined or null
                        for (var nextKey in nextSource) {
                            // Avoid bugs when hasOwnProperty is shadowed
                            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                                to[nextKey] = nextSource[nextKey];
                            }
                        }
                    }
                }
                return to;
            },
            writable: true,
            configurable: true
        });
    }
    document.addEventListener('DOMContentLoaded', function () {
        var parent = document.getElementById('chitchatclient');

        var chitChatClient = new RichChitChatClient(parent, {
            title: 'ChitChat',
            chitChatHost: ''
        });
        document.getElementById('reload').onclick = function(){window.location.reload()};
    });



</script>
</body>
</html>
</#macro>