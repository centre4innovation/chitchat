<#import "utils.ftl" as u>

<#macro pageTitle>
ChitChat - Live Settings
</#macro>

<#macro pageContent>
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Live Channel settings
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="/api/v1/ui/dashboard" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Live channels</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet" m-portlet="true" id="liveChannels">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption truncate">
                        <div class="m-portlet__head-title truncate">
                            <h3 class="m-portlet__head-text truncate" id="liveChannels">
                                Live Channels
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body" id="LiveChannelsBody">
                    <div class="row">
                        <div class="col-12">
                            <form class="m-form">
                                <div class="m-form__group form-group row">
                                    <label class="col-6 col-form-label">Select a Channel</label>
                                    <div class="col-6">
                                        <select id="channelSelect" class="form-control m-input custom-select"></select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet" m-portlet="true" id="scriptPortlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption truncate">
                        <div class="m-portlet__head-title truncate">
                            <#--<h3 class="m-portlet__head-text truncate" id="scriptName">
                                My Script
                            </h3>-->
                            <input type="text" value="My Script" id="scriptName" style="width: 100%;height: 100%;border: none;overflow: hidden;text-overflow: ellipsis;font-family: Poppins;" readonly>
                        </div>
                    </div>

                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a id="openScriptBtn" data-toggle="modal" data-target="#openCssModal" class="m-portlet__nav-link btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Run script" style="color:white">
                                    <i class="la la-folder-open-o"></i>
                                </a>
                            </li>

                            <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                <a href="#" class="m-portlet__nav-link btn btn-secondary  m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill   m-dropdown__toggle">
                                    <i class="la la-ellipsis-v"></i>
                                </a>
                                <div class="m-dropdown__wrapper" style="z-index: 101;">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 21.5px;"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">

                                                <ul class="m-nav">
                                                    <li class="m-nav__section m-nav__section--first">
                                                        <span class="m-nav__section-text">Actions</span>
                                                    </li>


                                                    <li class="m-nav__item">
                                                        <a id="openScriptBtn" class="m-nav__link" data-toggle="modal" data-target="#openCssModal">
                                                            <i class="m-nav__link-icon la la-folder-open-o"></i>
                                                            <span class="m-nav__link-text">Open script</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a id="openVersionBtn" class="m-nav__link" data-toggle="modal" data-target="#openVersionModal">
                                                            <i class="m-nav__link-icon la la-history"></i>
                                                            <span class="m-nav__link-text">Open previous version</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a id="saveScriptBtn" class="m-nav__link" data-toggle="modal" data-target="#saveCssModal">
                                                            <i class="m-nav__link-icon la la-floppy-o"></i>
                                                            <span class="m-nav__link-text">Save script</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a id="downloadScriptBtn" class="m-nav__link">
                                                            <i class="m-nav__link-icon la la-download"></i>
                                                            <span class="m-nav__link-text">Download script</span>
                                                        </a>
                                                    </li>

                                                    <li class="m-nav__separator m-nav__separator--fit">
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body" style="padding: 0; height: 300px;">
                    <textarea id="scriptEditor" style="display: none;"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

</#macro>

<#macro pageScript>
    <script src="https://kit.fontawesome.com/28b620e29c.js" crossorigin="anonymous"></script>
    <script>
        let createChannelSelect = function (element = '#channelSelect', hxVar = 'channel', onChange = () => {
        }) {
            const channelSelect = $(element);
            channelSelect.html(''); // clear
            $.when($.get('/api/v1/channel/live/channelInfo')).done(function (channelInfo) {
                let types = new Set(channelInfo.map(ci => ci.channelType));

                for (let type of types) {
                    let optgroup = document.createElement("optgroup");
                    optgroup.setAttribute("label", type);
                    channelInfo.filter(ci => {return ci.channelType === type})
                        .forEach(ci => {
                            let tmp = document.createElement("option");
                            tmp.setAttribute("value", ci.channelName);
                            tmp.innerText = ci.channelName;
                            optgroup.append(tmp);
                        });
                    channelSelect.append(optgroup);
                }
                channelSelect
                    .val($.hx.get(hxVar, ''))
                    .change(function () {
                        let selectedVal = $(this).find(":selected").val();
                        $.hx.set(hxVar, selectedVal);
                        onChange()
                    })
            })
        };

    $(document).ready(function () {
        $.hx.setCurrentPage('#menu-item-live')

        const scriptEditor = new CcsEditor($('#scriptEditor')[0], 'demo.ccs')

        createChannelSelect('#channelSelect', 'channel');

        const facebookLiveScript = 'ChitChat live script';
        const noLiveScript = 'No live script';

                function setScriptName(name) {
            $('#scriptName').val(name)
            $.hx.set('scriptName', name);
        }

        $('#downloadScriptBtn').click( e =>
                saveAs(new Blob([scriptEditor.getText()], {type: "text/plain;charset=utf-8"}), $('#scriptName').val()+".ccs")
        )

        $('#setLiveBtn').click( e => {
          // save under new name
            const formData = new FormData();
            const name = facebookLiveScript;
            formData.append("name", name);
            formData.append("body", scriptEditor.getText());

            $.ajax({
                type: "POST",
                url: "/api/v1/db/textdoc/liveccs/" + encodeURIComponent(name) + "/" + encodeURIComponent(name),
                data: formData,
                processData: false,
                contentType: false,
                success: function (a) {
                    $.post('/api/v1/channel/chitchat/script/reload', function(){
                        $.hx.notify('The script is now live!', 'success')
                    })
                },
                error: function (e) {
                }
            })




        })

        let openDocDialog = new OpenDocDialog('body', 'openCssModal', 'ccs', 'Open script', function (name, src) {
            scriptEditor.setText(src)
            setScriptName(name)
        } );

        let openVerionDocDialog = new OpenHistoryDocDialog('body', 'openVersionModal', 'ccs', function(){return openDocDialog.getOpenDocName()}, 'Open previous version', function (name, src) {
            scriptEditor.setText(src)
        } );



        let saveDocDialog = new SaveDocDialog('body', 'saveCssModal', 'ccs', 'Save script',
                function(){return scriptEditor.getText()},
                function(){
                    let name = $('#scriptName').val()
                    if(name === noLiveScript)
                        name = 'New script'
                    return name
                },
                function (name) {
                    setScriptName(name)
                    $.hx.notify('Script saved: ' + name, 'success')
                },
                function (name, e) {
                    const resp = JSON.parse(e.responseText);
                    if(resp.message) {
                        const message = resp.message;
                        let lineNr = resp.line - 1;

                        $.hx.notify('There is an error in your script.<br>' + resp.message, 'danger')
                        scriptEditor.setErrorMessage(lineNr, message)
                    } else {
                        $.hx.notify('The script could not be saved.<br>' + e, 'danger')
                    }
                }
        );

        $.get('/api/v1/db/textdoc/ccs/' + encodeURIComponent(facebookLiveScript), function (src) {
            setScriptName(name);
            scriptEditor.setText(src)
        }).fail(function() {
            setScriptName(noLiveScript);
            scriptEditor.setText("# Open the script you want to use...")
        });



    })
</script>

</#macro>