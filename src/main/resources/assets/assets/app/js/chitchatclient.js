"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Pure JS chat client widget.
 * @author Arvid Halma, Center for Innovation, Leiden University
 */
var ChitChatClient = function () {

    /**
     * Create and show a new chat client.
     * @param parent {HTMLElement} container
     * @param options {object} with fields to customize the client.
     * {
        showLeftUser: true,
        showRightUser: false,
        scrollToLastMessage: true,
        onSend: () => {},
        rightUserInitials: "ME",
        leftUserInitials: "YOU",
        }
     */
    function ChitChatClient(parent, options) {
        _classCallCheck(this, ChitChatClient);

        var self = this;
        this.parent = parent;
        this.parent.classList.add('chitchatclient');
        this.options = Object.assign({
            showLeftUser: true,
            showRightUser: false,
            scrollToLastMessage: true,
            onSend: function onSend() {},
            rightUserInitials: "ME",
            leftUserInitials: "YOU",
            leftMessageClass: "animate fadeInLeft",
            rightMessageClass: "animate fadeInRight"
        }, options);

        if (!this.options.showRightUser) {
            parent.classList.add('norightuser');
        }

        if (!this.options.showLeftUser) {
            parent.classList.add('noleftuser');
        }

        // internal state
        this.leftRightState = 0; // -1 = left, 1 = right
        this.lastMessageContainer = undefined;

        // build dom
        this.messagesDiv = document.createElement('div');
        this.messagesDiv.classList.add('messages');
        this.parent.appendChild(this.messagesDiv);

        this.sendDiv = document.createElement('div');
        this.sendDiv.classList.add('send');
        this.sendDiv.innerHTML += "<input type=\"text\" placeholder=\"Type here...\"> <a class=\"sendBtn\">\n            <svg xmlns=\"http://www.w3.org/2000/svg\"  version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 486.736 486.736\" style=\"enable-background:new 0 0 486.736 486.736;\" xml:space=\"preserve\" width=\"20px\" height=\"20px\">\n            <g><path d=\"M481.883,61.238l-474.3,171.4c-8.8,3.2-10.3,15-2.6,20.2l70.9,48.4l321.8-169.7l-272.4,203.4v82.4c0,5.6,6.3,9,11,5.9   l60-39.8l59.1,40.3c5.4,3.7,12.8,2.1,16.3-3.5l214.5-353.7C487.983,63.638,485.083,60.038,481.883,61.238z\" fill=\"#FFFFFF\"/></g>\n            </svg>\n            </a>";
        this.parent.appendChild(this.sendDiv);

        var $input = parent.querySelector('.send input');

        function onSend() {
            var msg = $input.value;
            if (msg.length === 0) {
                return;
            }
            var timestamp = new Date();
            var timestampStr = timestamp.toTimeString().substring(0, 5);
            self.addMessageRight(msg, self.options.rightUserInitials, timestampStr);
            $input.value = '';
            self.options.onSend({ text: msg, timestamp: timestamp, from: self.options.rightUserInitials });
        }

        parent.querySelector('.sendBtn').addEventListener('click', onSend);

        $input.addEventListener("keydown", function (event) {
            if (event.which === 13 || event.keyCode === 13) {
                onSend();
                return false;
            }
            return true;
        });
    }

    /**
     * Render an incoming message (message received).
     * @param msg {string} html content
     * @param user {string} user initials (space for 2-3 characters).
     *   If undefined, the default this.options.leftUserInitials is used.
     * @param timestr {string} an indication of when this message arrived.
     *   If it is undefined the current time will be used.
     */


    _createClass(ChitChatClient, [{
        key: "addMessageLeft",
        value: function addMessageLeft(msg, user, timestr) {
            var _this = this;

            window.setTimeout(function () {
                if (!timestr) {
                    timestr = new Date().toTimeString().substring(0, 5);
                }
                var userHtml = _this.options.showLeftUser ? "<div class=\"user\">" + (user || _this.options.leftUserInitials) + "</div>" : '';
                var arrowHtml = _this.leftRightState !== -1 ? "<div class=\"arrow\"><svg height=\"10\" width=\"20\"><polygon points=\"0,0 20,0 10,10\" style=\"stroke:none;stroke-width:0\" /></svg></div>" : '';
                if (_this.leftRightState !== -1) {
                    // last message not from left user
                    _this.lastMessageContainer = document.createElement('div');
                    _this.lastMessageContainer.classList.add('left');
                    _this.lastMessageContainer.innerHTML += userHtml;
                    _this.lastMessageContainer.innerHTML += "<div class=\"msg " + _this.options.leftMessageClass + "\">" + arrowHtml + " " + msg + "<div class=\"timestamp\">" + timestr + "</div></div>";
                    _this.messagesDiv.appendChild(_this.lastMessageContainer);
                } else {
                    var _msgDiv$classList;

                    // append message from left user
                    var msgDiv = document.createElement('div');
                    (_msgDiv$classList = msgDiv.classList).add.apply(_msgDiv$classList, ['msg'].concat(_toConsumableArray(_this.options.leftMessageClass.split(' '))));
                    msgDiv.innerHTML += msg + "<div class=\"timestamp\">" + timestr + "</div>";
                    _this.lastMessageContainer.appendChild(msgDiv);
                }
                if (_this.options.scrollToLastMessage) {
                    _this.messagesDiv.scrollTo({ "behavior": "smooth", "top": 1000000 });
                }
                _this.leftRightState = -1;
            }, 500);
        }

        /**
         * Render an outgoing message (message sent).
         * @param msg {string} html content
         * @param user {string} user initials (space for 2-3 characters).
         *   If undefined, the default this.options.rightUserInitials is used.
         * @param timestr {string} an indication of when this message was sent.
         *   If it is undefined the current time will be used.
         */

    }, {
        key: "addMessageRight",
        value: function addMessageRight(msg, user, timestr) {
            var _this2 = this;

            window.setTimeout(function () {
                var userHtml = _this2.options.showRightUser ? "<div class=\"user\">" + (user || _this2.options.rightUserInitials) + "</div>" : '';
                var arrowHtml = _this2.leftRightState !== 1 ? "<div class=\"arrow\"><svg height=\"10\" width=\"20\"><polygon points=\"0,0 20,0 10,10\" style=\"stroke:none;stroke-width:0\" /></svg></div>" : '';

                if (_this2.leftRightState !== 1) {
                    // last message not from right user
                    _this2.lastMessageContainer = document.createElement('div');
                    _this2.lastMessageContainer.classList.add('right');
                    _this2.lastMessageContainer.innerHTML += userHtml;
                    _this2.lastMessageContainer.innerHTML += "<div class=\"msg " + _this2.options.rightMessageClass + "\">" + arrowHtml + " " + msg + "<div class=\"timestamp\">" + timestr + "</div></div>";
                    _this2.messagesDiv.appendChild(_this2.lastMessageContainer);
                } else {
                    var _msgDiv$classList2;

                    // append message from right user
                    // append message from left user
                    var msgDiv = document.createElement('div');
                    (_msgDiv$classList2 = msgDiv.classList).add.apply(_msgDiv$classList2, ['msg'].concat(_toConsumableArray(_this2.options.rightMessageClass.split(' '))));
                    msgDiv.innerHTML += msg + "<div class=\"timestamp\">" + timestr + "</div>";
                    _this2.lastMessageContainer.appendChild(msgDiv);
                }

                if (_this2.options.scrollToLastMessage) {
                    _this2.messagesDiv.scrollTo({ "behavior": "smooth", "top": 1000000 });
                }

                _this2.leftRightState = 1;
            }, 100);
        }

        /**
         * A centered notification
         * @param msg {string} html message.
         */

    }, {
        key: "addInfo",
        value: function addInfo(msg) {
            var infoElm = document.createElement('div');
            infoElm.classList.add('info');
            infoElm.innerHTML = msg;
            this.messagesDiv.appendChild(infoElm);
            this.leftRightState = 0;
        }

        /**
         * Remove all messages
         */

    }, {
        key: "clear",
        value: function clear() {
            this.messagesDiv.innerHTML = "";
        }

        /**
         * Set hook for when a message was send by the user.
         *
         * @param f {Function} to be called. It receives an object as argument with the following fields.
         * {text:msg, timestamp:timestamp, from:self.options.rightUserInitials}
         */

    }, {
        key: "onSend",
        value: function onSend(f) {
            this.options.onSend = f;
        }
    }]);

    return ChitChatClient;
}();