'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Pure JS chat client widget, with predefined behavior to connect to a ChitChat server.
 * @author Arvid Halma, Center for Innovation, Leiden University
 */
var RichChitChatClient = function () {

    /**
     * Create and show a new rich chat client.
     * @param parent {HTMLElement} container
     * @param options {object} with fields to customize the client. In addition to the core client properties,
     * you can specify:
     * {
            chitChatHost: '', // ChitChat server base url
            title: 'ChitChat',
            description: undefined,
            userId: 'user-' + randomString(8),
            autoSendAfterSpeech: true
        }
     */
    function RichChitChatClient(parent, options) {
        _classCallCheck(this, RichChitChatClient);

        var self = this;
        this.options = Object.assign({
            chitChatHost: '', // ChitChat server base url
            chitChatEndpoint: undefined, // url path to /reply
            title: 'ChitChat',
            description: undefined,
            userId: 'user-' + randomString(8),
            autoSendAfterSpeech: true
        }, options);

        var info = {};
        info.userAgent = navigator.userAgent;

        // Chat bot script
        var script = getQueryString('script');

        // Get language setting
        var lang = getQueryString('lang') || 'en';
        info.lang = lang;
        function speechLangString(lang) {
            speechLang = 'en-US';
            if (lang) {
                if (lang.length === 2 && lang !== 'en') {
                    speechLang = lang.toLowerCase() + '-' + lang.toUpperCase();
                } else {
                    speechLang = lang;
                }
            }
            return speechLang;
        }

        // Update page title
        var title = getQueryString('title') || this.options.title;
        var titleElt = document.getElementById('chitchatclient-title');
        if (title) {
            document.title = title;
            if (titleElt) {
                titleElt.innerText = title;
                titleElt.style.display = 'inline';
            }
        } else {
            // hide title
            if (titleElt) {
                titleElt.style.display = 'none';
            }
        }

        // Update description
        var description = getQueryString('description') || this.options.description;
        var descriptionElt = document.getElementById('chitchatclient-description');
        if (descriptionElt) {
            if (description) {
                descriptionElt.innerText = description;
                descriptionElt.style.display = 'inline';
            } else {
                // hide title
                descriptionElt.style.display = 'none';
            }
        }

        // Create new unique userId
        var userId = 'user-' + randomString(8);
        info.userId = userId;

        // Create core client
        var chitChatClient = new ChitChatClient(parent, options);

        // Setup speech synthesis
        var speechSynth = void 0;
        var speechLang = void 0;
        if ("speechSynthesis" in window) {
            info.speechSynthesis = 'enabled';
            speechSynth = window.speechSynthesis;
            speechLang = speechLangString(lang);
        } else {
            info.speechSynthesis = 'disabled';
        }

        // Setup speech recognition
        var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
        var micBtn = parent.querySelector('.micBtn');
        if (SpeechRecognition) {
            info.speechRecognition = 'enabled';
            micBtn.style.display = 'inline-flex';
            var micBtnColor = micBtn.style.backgroundColor;

            var recognition = new SpeechRecognition();
            recognition.lang = speechLangString(lang);

            micBtn.addEventListener('click', function () {
                recognition.start();
                micBtn.style.backgroundColor = '#f66e84';
                recognition.onresult = function (event) {
                    var speechToText = event.results[0][0].transcript;
                    var $input = parent.querySelector('.send input');
                    $input.value = speechToText;
                    micBtn.style.backgroundColor = micBtnColor;

                    if (self.options.autoSendAfterSpeech) {
                        parent.querySelector('.sendBtn').click();
                    }
                };
            });
        } else {
            info.speechRecognition = 'disabled';
            micBtn.style.display = 'none';
        }

        function renderBotMessage(reply) {
            var text = reply.text;

            // render image
            text = text.replace(/\bIMAGE *\((.*?)\)/gm, '<img src="$1" style="display: inline;max-width:100%; width: auto; height: auto;"/>');

            // render buttons
            text = text.replace(/\bBUTTON *\( *(.*?) *, *(.*?) *\)/gm, '<button type="button" class="btn btn-success chatBtn" data-value="$2">$1</button>');
            text = text.replace(/\bGEOBUTTON *\( *(.*?) *\)/gm, '<button type="button" class="btn btn-success chatBtn geoBtn" data-value="geoBtn">$1</button>');
            var textBefore = text;
            text = text.replace(/\bCHECKBOX *\( *(.*?) *, *(.*?) *\)/gm, '<label class="checkBoxRow">$1 <input type="checkbox" value="$2"><span class="checkmark"></span></label>');
            if (textBefore !== text) {
                // checkboxes detected
                text += '<button type="button" class="btn btn-success checkboxSendBtn">OK</button>';
            }

            // speech synthesis
            var speakPattern = /\bSPEAK *\((.*?)\)/gm;
            if (speechSynth) {
                // if speech supported
                var utterances = void 0;
                while ((utterances = speakPattern.exec(text)) !== null) {
                    var utterThis = new SpeechSynthesisUtterance(utterances[1]);
                    if (speechLang) {
                        utterThis.lang = speechLang;
                    }
                    speechSynth.speak(utterThis);
                }
            }

            // wait
            var waitPattern = /\bWAIT *\((\d+)\)/gm;
            var waits = void 0;
            while ((waits = waitPattern.exec(text)) !== null) {
                chitChatClient.addWait(waits[1] * 1);
            }

            // remove special codes
            text = text.replace(/\b(SPEAK|WAIT|CHECKBOX) *\([^)]*\)/gm, '');

            // add text to panel
            chitChatClient.addMessageLeft(text, "BOT");
        }

        var onSend = function onSend(params) {
            if (params.text === 'chitchatclientinfo') {

                if (speechSynth) {
                    info.speechSynthesisLangs = speechSynth.getVoices().map(function (v) {
                        return v.lang;
                    }).join(', ');
                }
                chitChatClient.addMessageLeft('ChitChat client details...', '?');
                chitChatClient.addMessageLeft('User ID: ' + info.userId, '?');
                chitChatClient.addMessageLeft('Language code: ' + info.lang, '?');
                chitChatClient.addMessageLeft('Browser/user agent: ' + info.userAgent, '?');
                chitChatClient.addMessageLeft('Speech recognition: ' + info.speechRecognition, '?');
                chitChatClient.addMessageLeft('Speech synthesis: ' + info.speechSynthesis + ': ' + info.speechSynthesisLangs, '?');
                return;
            }
            var formData = new FormData();
            formData.append("msg", JSON.stringify({
                id: '' + Date.now(),
                text: params.text,
                senderId: userId,
                recipientId: "chitchat",
                timestamp: params.timestamp
            }));

            var url = void 0;
            if (self.options.chitChatEndpoint) {
                url = self.options.chitChatEndpoint;
            } else if (script) {
                url = self.options.chitChatHost + "/api/v1/channel/chitchat/mt/" + encodeURIComponent(script) + "/reply";
            } else {
                url = self.options.chitChatHost + "/api/v1/channel/chitchat/reply";
            }

            var xhr = new XMLHttpRequest();
            xhr.open('POST', url);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    var a = JSON.parse(xhr.responseText);
                    a.replies.forEach(renderBotMessage);
                } else if (xhr.status !== 200) {
                    chitChatClient.addInfo("<div style='color:#f06;font-style: italic'>The chatbot is not available...</div>");
                }
            };
            xhr.send(formData);
        };

        chitChatClient.onSend(onSend);

        // link button actions
        document.addEventListener('click', function (e) {
            if (e.target && e.target.classList.contains('chatBtn')) {
                var value = e.target.getAttribute('data-value');
                if (value === 'geoBtn') {
                    // share geolocation button
                    if ("geolocation" in navigator) {
                        /* geolocation is available */
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var btnParams = {
                                id: '' + Date.now(),
                                text: 'latitude:' + position.coords.latitude + ' longitude:' + position.coords.longitude,
                                senderId: userId,
                                recipientId: "chitchat",
                                timestamp: new Date()
                            };

                            e.target.classList.add('chosen');
                            /*e.target.setAttribute('disabled', true);
                            getSiblings(e.target).forEach(function (btn) {
                                return btn.setAttribute('disabled', true);
                            });*/
                            onSend(btnParams);
                        });
                    } else {
                        /* geolocation IS NOT available */
                        chitChatClient.addInfo("<div style='color:#f06;font-style: italic'>The geolocation is not available...</div>");
                        // normal button
                        var btnParams = {
                            id: '' + Date.now(),
                            text: 'unknownlocation',
                            senderId: userId,
                            recipientId: "chitchat",
                            timestamp: new Date()
                        };
                        e.target.classList.add('chosen');
                        /*e.target.setAttribute('disabled', true);
                        getSiblings(e.target).forEach(function (btn) {
                            return btn.setAttribute('disabled', true);
                        });*/
                        onSend(btnParams);
                    }
                } else {
                    // normal button
                    var _btnParams = {
                        id: '' + Date.now(),
                        text: value,
                        senderId: userId,
                        recipientId: "chitchat",
                        timestamp: new Date()
                    };
                    e.target.classList.add('chosen');
                    /*e.target.setAttribute('disabled', true);
                    getSiblings(e.target).forEach(function (btn) {
                        return btn.setAttribute('disabled', true);
                    });*/
                    onSend(_btnParams);
                }
            }
        });

        // link button actions
        document.addEventListener('click', function (e) {
            if (e.target && e.target.classList.contains('checkboxSendBtn')) {
                var checkedList = [].concat(_toConsumableArray(e.target.parentNode.querySelectorAll('input:checked')));
                var txt = checkedList.map(function (cb) {
                    return cb.value;
                }).join(", ");
                var msgParams = {
                    id: '' + Date.now(),
                    text: txt,
                    senderId: userId,
                    recipientId: "chitchat",
                    timestamp: new Date()

                    // submit button
                };e.target.setAttribute('disabled', true);

                // options
                checkedList.forEach(function (cb) {
                    return cb.classList.add('chosen');
                });
                e.target.parentNode.querySelectorAll('input').forEach(function (cb) {
                    return cb.setAttribute('disabled', true);
                });
                onSend(msgParams);
            }
        });

        // Show initial message
        var welcomeMsg = getQueryString('welcome');
        if (welcomeMsg) {
            welcomeMsg.split("&").forEach(function (msg) {
                renderBotMessage({ text: msg });
            });
        }

        // Send initial/implicit message by user
        var givenMsg = getQueryString('given');
        if (givenMsg) {
            onSend({ text: givenMsg });
        }

        this.info = info;
        this.chitChatClient = chitChatClient;

        // helper functions
        function getSiblings(elem) {
            // Setup siblings array and get the first sibling
            var siblings = [];
            var sibling = elem.parentNode.firstChild;

            // Loop through each sibling and push to the array
            while (sibling) {
                if (sibling.nodeType === 1 && sibling !== elem) {
                    siblings.push(sibling);
                }
                sibling = sibling.nextSibling;
            }

            return siblings;
        }

        function randomString(len, charSet) {
            charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var randomString = '';
            for (var i = 0; i < len; i++) {
                var randomPoz = Math.floor(Math.random() * charSet.length);
                randomString += charSet.substring(randomPoz, randomPoz + 1);
            }
            return randomString;
        }

        function getQueryString() {
            var key = false,
                res = {},
                itm = null;
            // get the query string without the ?
            var qs = location.search.substring(1);
            // check for the key as an argument
            if (arguments.length > 0 && arguments[0].length > 1) key = arguments[0];
            // make a regex pattern to grab key/value
            var pattern = /([^&=]+)=([^&]*)/g;
            // loop the items in the query string, either
            // find a match to the argument, or build an object
            // with key/value pairs
            while (itm = pattern.exec(qs)) {
                if (key !== false && decodeURIComponent(itm[1]) === key) return decodeURIComponent(itm[2]);else if (key === false) res[decodeURIComponent(itm[1])] = decodeURIComponent(itm[2]);
            }

            return key === false ? res : null;
        }
    }

    _createClass(RichChitChatClient, [{
        key: 'coreClient',
        value: function coreClient() {
            return this.chitChatClient;
        }
    }, {
        key: 'info',
        value: function info() {
            return this.info;
        }
    }, {
        key: 'addMessageLeft',
        value: function addMessageLeft(msg, user, delay) {
            this.chitChatClient.addMessageLeft(msg, user, delay);
        }
    }, {
        key: 'addWait',
        value: function addWait(delay) {
            this.chitChatClient.addWait(delay);
        }
    }, {
        key: 'addMessageRight',
        value: function addMessageRight(msg, user, delay) {
            this.chitChatClient.addMessageRight(msg, user, delay);
        }
    }, {
        key: 'addInfo',
        value: function addInfo(msg) {
            this.chitChatClient.addInfo(msg);
        }
    }, {
        key: 'clear',
        value: function clear() {
            this.chitChatClient.clear();
        }
    }, {
        key: 'onSend',
        value: function onSend(f) {
            this.chitChatClient.options.onSend = f;
        }
    }]);

    return RichChitChatClient;
}();