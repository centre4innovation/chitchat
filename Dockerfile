FROM openjdk:8-jre-alpine3.8

COPY mysettings.yml /data/ChitChat/mysettings.yml
COPY /target/chitchat-0.12.0.jar /data/ChitChat/chitchat-0.12.0.jar
COPY /data /data/Chitchat/data

WORKDIR /data/ChitChat

ENTRYPOINT ["java", "-jar", "chitchat-0.12.0.jar"]
CMD ["server", "mysettings.yml"]

EXPOSE 5678 5432
